﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Konstanten
{
    public partial class FrmConst : Form
    {
        private const int MaxWert = 75;
        private const int MinWert = 10;
        const string Eintrag = "Glüwein";

        private enum Ampel : int
        {
            Rot=100, Gelb=10, Gruen=1, Aus = -1, Gelbblinken = 0
        }

        private enum Geschlecht : int
        {
            Maennlich = 1, Weiblich = 0, Gender = -1
        }

        public FrmConst()
        {
            InitializeComponent();
        }

        private void BtnSchlieszen_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnEnum1_Click(object sender, EventArgs e)
        {
            LblTxt.Text = "Ampel:\n" + Ampel.Rot + " " + (int)Ampel.Rot +
                "\n" + Ampel.Gelb + " " + (int)Ampel.Gelb +
                "\n" + Ampel.Gruen + " " + (int)Ampel.Gruen +
                "\n" + Ampel.Gelbblinken + " " + (int)Ampel.Gelbblinken +
                "\n" + Ampel.Aus + " " + (int)Ampel.Aus +
                "\n\nGeschlecht:\n" + Geschlecht.Maennlich + " " + (int)Geschlecht.Maennlich +
                "\n" + Geschlecht.Gender + " " + (int)Geschlecht.Gender +
                "\n" + Geschlecht.Weiblich + " " + (int)Geschlecht.Weiblich;
        }

        private void BtnKon_Click(object sender, EventArgs e)
        {
            const int MaxWert = 55;
            const int MinWert = 5;
            LblTxt.Text = "Lokal: " + (MaxWert - MinWert) / 2 + 
                "\nGlobal: " + (FrmConst.MaxWert - FrmConst.MinWert) / 2 + "\n" + Eintrag;
        }

        private void BtnEnum2_Click(object sender, EventArgs e)
        {
            LblTxt.Text = "Sonntag: " + DayOfWeek.Sunday + " " +
            (int)DayOfWeek.Sunday + "\n" + "Samstag: " +
            DayOfWeek.Saturday + " " + (int)DayOfWeek.Saturday;
        }

        private void BtnPi_Click(object sender, EventArgs e)
        {
            const decimal pi = 3.14159265358979323846264338327m;
            LblTxt.Text = "Pi: " + Math.PI +
                "\nEigene Konstante: " + pi;
        }

        private void BtnMeld_Click(object sender, EventArgs e)
        {
            DateTime weinachten = new DateTime(DateTime.Now.Year, 12, 24);
            TimeSpan dauer = new TimeSpan(weinachten.Ticks - DateTime.Now.Ticks);

            DialogResult mb1, mb2, mb3, mb4 = DialogResult.None;
            MessageBoxButtons mbb = MessageBoxButtons.YesNoCancel;
            MessageBoxIcon ico = MessageBoxIcon.Asterisk;
            string capt = "Weinachten", cont = "Zeit bis Weinachten:\n" +
                dauer.Days + " Tage\nund"
                + dauer.Hours + " Stunden";
            LblTxt.Text = "Nix Antwort: " + mb4 + "; " + (int)mb4;

            mb1 = MessageBox.Show(cont, capt, mbb, ico);

            LblTxt.Text = "Meldungsfensterbedienungen " + mb1 + "; " + (int)mb1 +
                "\nNix Antwort: " + mb4 + "; " + (int)mb4;

            mb2 = MessageBox.Show("Teste mich!", "Test", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Question);

            LblTxt.Text = "Meldungsfensterbedienungen " + mb1 + "; " + (int)mb1 +
                "\nTest: " + mb2 + "; " + (int)mb2 +
                "\nNix Antwort: " + mb4 + "; " + (int)mb4;

            mb3 = MessageBox.Show("Drück mich!", ":)", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            LblTxt.Text = "Meldungsfensterbedienungen " + mb1 + "; " + (int)mb1 +
                "\nTest: " + mb2 + "; " + (int)mb2 +
                "\nTest2: " + mb3 + "; " + (int)mb3 +
                "\nNix Antwort: " + mb4 + "; " + (int)mb4;
        }
    }
}
