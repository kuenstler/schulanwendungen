﻿namespace Konstanten
{
    partial class FrmConst
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnKon = new System.Windows.Forms.Button();
            this.BtnEnum1 = new System.Windows.Forms.Button();
            this.BtnEnum2 = new System.Windows.Forms.Button();
            this.BtnMeld = new System.Windows.Forms.Button();
            this.BtnPi = new System.Windows.Forms.Button();
            this.BtnSchlieszen = new System.Windows.Forms.Button();
            this.LblTxt = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnKon
            // 
            this.BtnKon.Location = new System.Drawing.Point(325, 13);
            this.BtnKon.Name = "BtnKon";
            this.BtnKon.Size = new System.Drawing.Size(158, 23);
            this.BtnKon.TabIndex = 0;
            this.BtnKon.Text = "Konstanten";
            this.BtnKon.UseVisualStyleBackColor = true;
            this.BtnKon.Click += new System.EventHandler(this.BtnKon_Click);
            // 
            // BtnEnum1
            // 
            this.BtnEnum1.Location = new System.Drawing.Point(325, 42);
            this.BtnEnum1.Name = "BtnEnum1";
            this.BtnEnum1.Size = new System.Drawing.Size(158, 23);
            this.BtnEnum1.TabIndex = 1;
            this.BtnEnum1.Text = "Enumeration1";
            this.BtnEnum1.UseVisualStyleBackColor = true;
            this.BtnEnum1.Click += new System.EventHandler(this.BtnEnum1_Click);
            // 
            // BtnEnum2
            // 
            this.BtnEnum2.Location = new System.Drawing.Point(325, 71);
            this.BtnEnum2.Name = "BtnEnum2";
            this.BtnEnum2.Size = new System.Drawing.Size(158, 23);
            this.BtnEnum2.TabIndex = 2;
            this.BtnEnum2.Text = "Enumeration2";
            this.BtnEnum2.UseVisualStyleBackColor = true;
            this.BtnEnum2.Click += new System.EventHandler(this.BtnEnum2_Click);
            // 
            // BtnMeld
            // 
            this.BtnMeld.Location = new System.Drawing.Point(325, 100);
            this.BtnMeld.Name = "BtnMeld";
            this.BtnMeld.Size = new System.Drawing.Size(158, 23);
            this.BtnMeld.TabIndex = 3;
            this.BtnMeld.Text = "Meldungen";
            this.BtnMeld.UseVisualStyleBackColor = true;
            this.BtnMeld.Click += new System.EventHandler(this.BtnMeld_Click);
            // 
            // BtnPi
            // 
            this.BtnPi.Location = new System.Drawing.Point(325, 129);
            this.BtnPi.Name = "BtnPi";
            this.BtnPi.Size = new System.Drawing.Size(158, 23);
            this.BtnPi.TabIndex = 4;
            this.BtnPi.Text = "Mathematische Konstante Pi";
            this.BtnPi.UseVisualStyleBackColor = true;
            this.BtnPi.Click += new System.EventHandler(this.BtnPi_Click);
            // 
            // BtnSchlieszen
            // 
            this.BtnSchlieszen.Location = new System.Drawing.Point(325, 308);
            this.BtnSchlieszen.Name = "BtnSchlieszen";
            this.BtnSchlieszen.Size = new System.Drawing.Size(158, 23);
            this.BtnSchlieszen.TabIndex = 5;
            this.BtnSchlieszen.Text = "Beenden";
            this.BtnSchlieszen.UseVisualStyleBackColor = true;
            this.BtnSchlieszen.Click += new System.EventHandler(this.BtnSchlieszen_Click);
            // 
            // LblTxt
            // 
            this.LblTxt.AutoSize = true;
            this.LblTxt.Location = new System.Drawing.Point(13, 13);
            this.LblTxt.Name = "LblTxt";
            this.LblTxt.Size = new System.Drawing.Size(16, 13);
            this.LblTxt.TabIndex = 6;
            this.LblTxt.Text = "...";
            // 
            // FrmConst
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(495, 343);
            this.Controls.Add(this.LblTxt);
            this.Controls.Add(this.BtnSchlieszen);
            this.Controls.Add(this.BtnPi);
            this.Controls.Add(this.BtnMeld);
            this.Controls.Add(this.BtnEnum2);
            this.Controls.Add(this.BtnEnum1);
            this.Controls.Add(this.BtnKon);
            this.Name = "FrmConst";
            this.Text = "Konstanten";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnKon;
        private System.Windows.Forms.Button BtnEnum1;
        private System.Windows.Forms.Button BtnEnum2;
        private System.Windows.Forms.Button BtnMeld;
        private System.Windows.Forms.Button BtnPi;
        private System.Windows.Forms.Button BtnSchlieszen;
        private System.Windows.Forms.Label LblTxt;
    }
}

