﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Operatoren
{
    public partial class FrmOperatoren : Form
    {
        Random r = new Random();
        public FrmOperatoren()
        {
            InitializeComponent();
        }

        private void BtnInc_Click(object sender, EventArgs e)
        {
            int i1 = r.Next(0, 11), i2 = i1, i3 = r.Next(0,11), i4 = i3;
            LblTxt.Text = "Increment:" +
                "\ni1 = " + i1 + "; i1++ = " + (i1++) +
                "\ni1 nach Zugriff: " + i1 +
                "\ni2 = " + i2 + "; ++i2 = " + (++i2) +
                "\ni2 nach Zugriff: " + i2 +
                "\n\nDecrement:" +
                "\ni3 = " + i3 + "; i3-- = " + (i3--) +
                "\ni3 nach Zugriff: " + i3 +
                "\ni4 = " + i4 + "; --i4 = " + (--i4) +
                "\ni4 nach Zugriff: " + i4;
        }

        private void BtnArth_Click(object sender, EventArgs e)
        {
            int z1 = r.Next(1, 11), z2 = r.Next(1, 11);
            LblTxt.Text = "Addition: " + z1 + " + " + z2 + " = " + (z1 + z2) +
                "\nSubtraktion: " + z1 + " - " + z2 + " = " + (z1 - z2) +
                "\nMultiplikation: " + z1 + " * " + z2 + " = " + (z1 * z2) +
                "\nDivision: " + z1 + " / " + z2 + " = " + ((float)z1 / z2) +
                "\nModulo: " + z1 + " % " + z2 + " = " + (z1 % z2) +
                "\nModulo 2: " + z2 + " % " + z1 + " = " + (z2 % z1);
        }

        private void BtnVerknuepfung_Click(object sender, EventArgs e)
        {
            bool a1, a2, a3, a4, a5, a6;
            int A = r.Next(0, 10), B = r.Next(0, 10), C = r.Next(0, 10);
            a1 = !(A < B);
            a2 = (B > A) && (C > B);
            a3 = (B < A) || (C < B);
            a4 = (B < A) ^ (C > B);
            a5 = 4 > 3 && -4 > -3;
            a6 = 4 > 3 || -4 > -3;
            LblTxt.Text = "Logische Operatoren:" +
                "\na1 = !(" + A + " < " + B + ") = " + a1 +
                "\na2 = (" + B + " > " + A + ") &&&& (" + C + " > " + B + ") = " + a2 +
                "\na3 = (" + B + " < " + A + ") || (" + C + " < " + B + ") = " + a3 +
                "\na4 = (" + B + " < " + A + ") ^ (" + C + " > " + B + ") = " + a4 +
                "\na5 = 4 > 3 &&&& -4 > -3 = " + a5 +
                "\na6 = 4 > 3 || -4 > -3 = " + a6;
        }

        private void BtnVerkett_Click(object sender, EventArgs e)
        {
            string a, b;
            double d;
            int x;
            b = "Hallo";
            d = 4.6;
            x = -5;
            a = b + " Welt " + d + " " + x + " " + 12;
            LblTxt.Text = a;
            LblTxt.Text += "\n" + x.ToString();
        }

        private void BtnUebn_Click(object sender, EventArgs e)
        {
            LblTxt.Text = "Aufgabe Lehrbuch Seite 53:" +
                "\n1.Ausdruck: 3*-2.5+4*2 = " + (3 * -2.5 + 4 * 2) +
                "\n2.Ausdruck: 3*(-2.5+4)*2 = " + (3 * (-2.5 + 4) * 2);
        }

        private void BtnZwsng_Click(object sender, EventArgs e)
        {
            int x;
            string z = "Hallo";
            LblTxt.Text = "x=7 : " + (x = 7) +
                "\nx += 15 : " + (x += 15) +
                "\nx -= 11 : " + (x -= 11) +
                "\nx *= 3 : " + (x *= 3) +
                "\nx /= 5 : " + (x /= 5) +
                "\nx %= 3 : " + (x %= 3) +
                "\n\"Hallo\" += \" Welt\": " + (z += " Welt");
        }

        private void BtnVergleich_Click(object sender, EventArgs e)
        {
            bool a1, a2, a3, a4, a5, a6;
            a1 = 5 > 3;
            a2 = 3 == 3.2;
            a3 = 5 + 3 * 2 >= 12;
            a4 = "Maier" == "Mayer";
            a5 = 15 - 3 >= 4 * 2.5;
            a6 = "Maier" != "Mayer";
            LblTxt.Text = "Vergleichsoperatoren" +
                "\na1 = 5 > 3 = " + a1 +
                "\na2 = 3 == 3.2 = " + a2 +
                "\na3 = 5 + 3 * 2 >= 12 = " + a3 +
                "\na4 = \"Maier\" == \"Mayer\" = " + a4 +
                "\na5 = a5 = 15 - 3 >= 4 * 2.5 = " + a5 +
                "\na6 = a6 = \"Maier\" != \"Mayer\" = " + a6;

        }

        private void BtnUebungLogik_Click(object sender, EventArgs e)
        {
            bool a1, a2, a3, a4, a5, a6, a7, a8;
            int a = 5, b = 10, z = b, w = 100, n1 = 1, n2 = 17;
            float x = (float)1.0, y = (float)5.7;
            a1 = a > 0 && b != 10;
            a2 = a > 0 || b != 10;
            a3 = z != 0 || z > w || w - z == 90;
            a4 = z == 11 && z > w || w - z == 90;
            a5 = x >= .9 && y <= 5.8;
            a6 = x >= .9 && !(y <= 5.8);
            a7 = n1 > 0 && n2 > 0 || n1 > n2 && n2 != 17;
            a8 = n1 > 0 && (n2 > 0 || n1 > n2) && n2 != 17;
            LblTxt.Text = "Übung 1:" +
                "\n" + a + " > 0 &&&& " + b + " != 10: " + a1 +
                "\nÜbung 2:" +
                "\n" + a + " > 0 || " + b + " != 10: " + a2 +
                "\nÜbung 3:" +
                "\n" + z + " != 0 || " + z + " > " + w + " || " + w + " - " + z + " == 90: " + a3 +
                "\nÜbung 4:" +
                "\n" + z + " == 11 &&&& " + z + " > " + w + " || " + w + " - " + z + " == 90: " + a4 +
                "\nÜbung 5:" +
                "\n" + x + " >= .9 &&&& " + y + " <= 5.8: " + a5 +
                "\nÜbung 6:" +
                "\n" + x + " >= .9 &&&& !(" + y + " <= 5.8): " + a6 +
                "\nÜbung 7:" +
                "\n" + n1 + " > 0 &&&& " + n2 + " > 0 || " + n1 + " > " + n2 + " &&&& " + n2 + " != 17: " + a7 +
                "\nÜbung 8:" +
                "\n" + z + " != 0 || " + z + " > " + w + " || " + w + " - " + z + " == 90: " + a8;
        }

        private void BtnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
