﻿namespace Operatoren
{
    partial class FrmOperatoren
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnVerknuepfung = new System.Windows.Forms.Button();
            this.BtnArth = new System.Windows.Forms.Button();
            this.BtnVerkett = new System.Windows.Forms.Button();
            this.BtnInc = new System.Windows.Forms.Button();
            this.BtnUebn = new System.Windows.Forms.Button();
            this.BtnZwsng = new System.Windows.Forms.Button();
            this.BtnVergleich = new System.Windows.Forms.Button();
            this.BtnUebungLogik = new System.Windows.Forms.Button();
            this.LblTxt = new System.Windows.Forms.Label();
            this.BtnClose = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnVerknuepfung
            // 
            this.BtnVerknuepfung.Location = new System.Drawing.Point(16, 331);
            this.BtnVerknuepfung.Name = "BtnVerknuepfung";
            this.BtnVerknuepfung.Size = new System.Drawing.Size(91, 23);
            this.BtnVerknuepfung.TabIndex = 2;
            this.BtnVerknuepfung.Text = "&&&& || ^ !";
            this.BtnVerknuepfung.UseVisualStyleBackColor = true;
            this.BtnVerknuepfung.Click += new System.EventHandler(this.BtnVerknuepfung_Click);
            // 
            // BtnArth
            // 
            this.BtnArth.Location = new System.Drawing.Point(16, 302);
            this.BtnArth.Name = "BtnArth";
            this.BtnArth.Size = new System.Drawing.Size(91, 23);
            this.BtnArth.TabIndex = 1;
            this.BtnArth.Text = "+ - * / %";
            this.BtnArth.UseVisualStyleBackColor = true;
            this.BtnArth.Click += new System.EventHandler(this.BtnArth_Click);
            // 
            // BtnVerkett
            // 
            this.BtnVerkett.Location = new System.Drawing.Point(113, 331);
            this.BtnVerkett.Name = "BtnVerkett";
            this.BtnVerkett.Size = new System.Drawing.Size(91, 23);
            this.BtnVerkett.TabIndex = 4;
            this.BtnVerkett.Text = "Verkettung +";
            this.BtnVerkett.UseVisualStyleBackColor = true;
            this.BtnVerkett.Click += new System.EventHandler(this.BtnVerkett_Click);
            // 
            // BtnInc
            // 
            this.BtnInc.Location = new System.Drawing.Point(113, 302);
            this.BtnInc.Name = "BtnInc";
            this.BtnInc.Size = new System.Drawing.Size(91, 23);
            this.BtnInc.TabIndex = 3;
            this.BtnInc.Text = "++ --";
            this.BtnInc.UseVisualStyleBackColor = true;
            this.BtnInc.Click += new System.EventHandler(this.BtnInc_Click);
            // 
            // BtnUebn
            // 
            this.BtnUebn.Location = new System.Drawing.Point(210, 302);
            this.BtnUebn.Name = "BtnUebn";
            this.BtnUebn.Size = new System.Drawing.Size(91, 23);
            this.BtnUebn.TabIndex = 5;
            this.BtnUebn.Text = "Übung LB S. 53";
            this.BtnUebn.UseVisualStyleBackColor = true;
            this.BtnUebn.Click += new System.EventHandler(this.BtnUebn_Click);
            // 
            // BtnZwsng
            // 
            this.BtnZwsng.Location = new System.Drawing.Point(210, 331);
            this.BtnZwsng.Name = "BtnZwsng";
            this.BtnZwsng.Size = new System.Drawing.Size(91, 23);
            this.BtnZwsng.TabIndex = 6;
            this.BtnZwsng.Text = "Zuweisung =";
            this.BtnZwsng.UseVisualStyleBackColor = true;
            this.BtnZwsng.Click += new System.EventHandler(this.BtnZwsng_Click);
            // 
            // BtnVergleich
            // 
            this.BtnVergleich.Location = new System.Drawing.Point(307, 302);
            this.BtnVergleich.Name = "BtnVergleich";
            this.BtnVergleich.Size = new System.Drawing.Size(91, 23);
            this.BtnVergleich.TabIndex = 7;
            this.BtnVergleich.Text = "< = > !";
            this.BtnVergleich.UseVisualStyleBackColor = true;
            this.BtnVergleich.Click += new System.EventHandler(this.BtnVergleich_Click);
            // 
            // BtnUebungLogik
            // 
            this.BtnUebungLogik.Location = new System.Drawing.Point(307, 331);
            this.BtnUebungLogik.Name = "BtnUebungLogik";
            this.BtnUebungLogik.Size = new System.Drawing.Size(91, 23);
            this.BtnUebungLogik.TabIndex = 8;
            this.BtnUebungLogik.Text = "Übung LB S. 57";
            this.BtnUebungLogik.UseVisualStyleBackColor = true;
            this.BtnUebungLogik.Click += new System.EventHandler(this.BtnUebungLogik_Click);
            // 
            // LblTxt
            // 
            this.LblTxt.AutoSize = true;
            this.LblTxt.Location = new System.Drawing.Point(13, 13);
            this.LblTxt.Name = "LblTxt";
            this.LblTxt.Size = new System.Drawing.Size(16, 13);
            this.LblTxt.TabIndex = 8;
            this.LblTxt.Text = "...";
            // 
            // BtnClose
            // 
            this.BtnClose.Location = new System.Drawing.Point(404, 331);
            this.BtnClose.Name = "BtnClose";
            this.BtnClose.Size = new System.Drawing.Size(91, 23);
            this.BtnClose.TabIndex = 9;
            this.BtnClose.Text = "Beenden";
            this.BtnClose.UseVisualStyleBackColor = true;
            this.BtnClose.Click += new System.EventHandler(this.BtnClose_Click);
            // 
            // FrmOperatoren
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(506, 366);
            this.Controls.Add(this.BtnClose);
            this.Controls.Add(this.LblTxt);
            this.Controls.Add(this.BtnUebungLogik);
            this.Controls.Add(this.BtnVergleich);
            this.Controls.Add(this.BtnZwsng);
            this.Controls.Add(this.BtnUebn);
            this.Controls.Add(this.BtnInc);
            this.Controls.Add(this.BtnVerkett);
            this.Controls.Add(this.BtnArth);
            this.Controls.Add(this.BtnVerknuepfung);
            this.Name = "FrmOperatoren";
            this.Text = "Operatoren";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnVerknuepfung;
        private System.Windows.Forms.Button BtnArth;
        private System.Windows.Forms.Button BtnVerkett;
        private System.Windows.Forms.Button BtnInc;
        private System.Windows.Forms.Button BtnUebn;
        private System.Windows.Forms.Button BtnZwsng;
        private System.Windows.Forms.Button BtnVergleich;
        private System.Windows.Forms.Button BtnUebungLogik;
        private System.Windows.Forms.Label LblTxt;
        private System.Windows.Forms.Button BtnClose;
    }
}

