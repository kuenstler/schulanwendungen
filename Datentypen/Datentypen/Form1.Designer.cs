﻿namespace Datentypen
{
    partial class FrmDatentypen
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblTypen = new System.Windows.Forms.Label();
            this.BtnSchlieszen = new System.Windows.Forms.Button();
            this.BtnFest = new System.Windows.Forms.Button();
            this.BtnFliesz = new System.Windows.Forms.Button();
            this.BtnBool = new System.Windows.Forms.Button();
            this.BtnZeichen = new System.Windows.Forms.Button();
            this.BtnUniv = new System.Windows.Forms.Button();
            this.LblTypen2 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LblTypen
            // 
            this.LblTypen.AllowDrop = true;
            this.LblTypen.AutoSize = true;
            this.LblTypen.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.LblTypen.Location = new System.Drawing.Point(13, 13);
            this.LblTypen.Name = "LblTypen";
            this.LblTypen.Size = new System.Drawing.Size(62, 13);
            this.LblTypen.TabIndex = 0;
            this.LblTypen.Text = "Datentypen";
            // 
            // BtnSchlieszen
            // 
            this.BtnSchlieszen.Location = new System.Drawing.Point(538, 236);
            this.BtnSchlieszen.Name = "BtnSchlieszen";
            this.BtnSchlieszen.Size = new System.Drawing.Size(141, 23);
            this.BtnSchlieszen.TabIndex = 2;
            this.BtnSchlieszen.Text = "Programm beenden";
            this.BtnSchlieszen.UseVisualStyleBackColor = true;
            this.BtnSchlieszen.Click += new System.EventHandler(this.BtnSchlieszen_Click);
            // 
            // BtnFest
            // 
            this.BtnFest.Location = new System.Drawing.Point(538, 12);
            this.BtnFest.Name = "BtnFest";
            this.BtnFest.Size = new System.Drawing.Size(141, 23);
            this.BtnFest.TabIndex = 3;
            this.BtnFest.Text = "Festkomma-DT anzeigen";
            this.BtnFest.UseVisualStyleBackColor = true;
            this.BtnFest.Click += new System.EventHandler(this.BtnFest_Click);
            // 
            // BtnFliesz
            // 
            this.BtnFliesz.Location = new System.Drawing.Point(538, 41);
            this.BtnFliesz.Name = "BtnFliesz";
            this.BtnFliesz.Size = new System.Drawing.Size(141, 23);
            this.BtnFliesz.TabIndex = 4;
            this.BtnFliesz.Text = "Fließkomma-DT anzeigen";
            this.BtnFliesz.UseVisualStyleBackColor = true;
            this.BtnFliesz.Click += new System.EventHandler(this.BtnFliesz_Click);
            // 
            // BtnBool
            // 
            this.BtnBool.Location = new System.Drawing.Point(538, 70);
            this.BtnBool.Name = "BtnBool";
            this.BtnBool.Size = new System.Drawing.Size(141, 23);
            this.BtnBool.TabIndex = 5;
            this.BtnBool.Text = "Boolsche-DT anzeigen";
            this.BtnBool.UseVisualStyleBackColor = true;
            this.BtnBool.Click += new System.EventHandler(this.BtnBool_Click);
            // 
            // BtnZeichen
            // 
            this.BtnZeichen.Location = new System.Drawing.Point(538, 99);
            this.BtnZeichen.Name = "BtnZeichen";
            this.BtnZeichen.Size = new System.Drawing.Size(141, 23);
            this.BtnZeichen.TabIndex = 6;
            this.BtnZeichen.Text = "Zeichen-DT anzeigen";
            this.BtnZeichen.UseVisualStyleBackColor = true;
            this.BtnZeichen.Click += new System.EventHandler(this.BtnZeichen_Click);
            // 
            // BtnUniv
            // 
            this.BtnUniv.Location = new System.Drawing.Point(538, 128);
            this.BtnUniv.Name = "BtnUniv";
            this.BtnUniv.Size = new System.Drawing.Size(141, 23);
            this.BtnUniv.TabIndex = 7;
            this.BtnUniv.Text = "Universelle-DT anzeigen";
            this.BtnUniv.UseVisualStyleBackColor = true;
            this.BtnUniv.Click += new System.EventHandler(this.BtnUniv_Click);
            // 
            // LblTypen2
            // 
            this.LblTypen2.AllowDrop = true;
            this.LblTypen2.AutoSize = true;
            this.LblTypen2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.LblTypen2.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblTypen2.Location = new System.Drawing.Point(13, 13);
            this.LblTypen2.Name = "LblTypen2";
            this.LblTypen2.Size = new System.Drawing.Size(105, 24);
            this.LblTypen2.TabIndex = 8;
            this.LblTypen2.Text = "Datentypen";
            this.LblTypen2.Visible = false;
            // 
            // FrmDatentypen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 444);
            this.Controls.Add(this.LblTypen2);
            this.Controls.Add(this.BtnUniv);
            this.Controls.Add(this.BtnZeichen);
            this.Controls.Add(this.BtnBool);
            this.Controls.Add(this.BtnFliesz);
            this.Controls.Add(this.BtnFest);
            this.Controls.Add(this.BtnSchlieszen);
            this.Controls.Add(this.LblTypen);
            this.Name = "FrmDatentypen";
            this.Text = "Datentypen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnSchlieszen;
        private System.Windows.Forms.Button BtnFest;
        private System.Windows.Forms.Button BtnFliesz;
        private System.Windows.Forms.Button BtnBool;
        private System.Windows.Forms.Button BtnZeichen;
        private System.Windows.Forms.Button BtnUniv;
        public System.Windows.Forms.Label LblTypen;
        public System.Windows.Forms.Label LblTypen2;
    }
}

