﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Datentypen
{
    public partial class FrmDatentypen : Form
    {
        public FrmDatentypen()
        {
            InitializeComponent();
        }

        private void BtnSchlieszen_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnFest_Click(object sender, EventArgs e)
        {
            // Deklaration
            byte by; Byte by2;
            sbyte sby; SByte sby2;
            short sh; Int16 sh2;
            ushort ush; UInt16 ush2;
            int it, hex; Int32 it2;
            uint uit; UInt32 uit2;
            long lng; Int64 lng2;
            ulong ulng; UInt64 ulng2;
            // Definition
            by = 200; by2 = 150;
            sby = -50; sby2 = -100;
            sh = 30000; sh2 = -20001;
            ush = 40000; ush2 = 50000;
            it = -123303344; it2 = -111222121; hex = 0x3a;
            uit = 4000000000; uit2 = 1211211;
            lng = -13456364; lng2 = 121312312312;
            ulng = 4000000000; ulng2 = 90310238213;
            // Ausgabe
            LblTypen.Text = "Byte by:" + by + "; by2:" + by2 +
                "\nMinimum: " + byte.MinValue +
                "\nMaximum: " + byte.MaxValue +
                "\nSByte sby:" + sby + "; sby2:" + sby2 +
                "\nMinimum: " + sbyte.MinValue +
                "\nMaximum: " + sbyte.MaxValue +
                "\n\nShort sh:" + string.Format("{0:n}", sh) + "; sh2:" + string.Format("{0:n}", sh2) +
                "\nMinimum: " + string.Format("{0:n}", short.MinValue) +
                "\nMaximum: " + string.Format("{0:n}", short.MaxValue) +
                "\nUShort ush:" + string.Format("{0:n}", ush) + "; ush2:" + string.Format("{0:n}", ush2) +
                "\nMinimum: " + ushort.MinValue +
                "\nMaximum: " + string.Format("{0:n}", ushort.MaxValue) +
                "\n\nInteger it:" + string.Format("{0:n}", it) + "; it2:" + string.Format("{0:n}", it2) +
                "\nHexadezimale Zahl hex:" + hex +
                "\nMinimum: " + string.Format("{0:n}", int.MinValue) +
                "\nMaximum: " + string.Format("{0:n}", int.MaxValue) +
                "\nUInteger uit:" + string.Format("{0:n}", uit) + "; uit2:" + string.Format("{0:n}", uit2) +
                "\nMinimum: " + uint.MinValue +
                "\nMaximum: " + string.Format("{0:n}", uint.MaxValue) +
                "\n\nLong lng:" + string.Format("{0:n}", lng) + "; lng2:" + string.Format("{0:n}", lng2) +
                "\nMinimum: " + string.Format("{0:n}", long.MinValue) +
                "\nMaximum: " + string.Format("{0:n}", long.MaxValue) +
                "\nULong ulng:" + string.Format("{0:n}", ulng) + "; ulng2:" + string.Format("{0:n}", ulng2) +
                "\nMinimum: " + ulong.MinValue +
                "\nMaximum: " + string.Format("{0:n}", ulong.MaxValue);
            LblTypen.Visible = true;
            LblTypen2.Visible = false;
        }

        private void BtnFliesz_Click(object sender, EventArgs e)
        {
            // Fließkommavariablen mit Deklaration und Definition in einem Schritt
            float fl = 1.0f / 7; Single fl2 = 345.116423f;
            double db = 1 / 7, db2 = 1.0 / 7, exp = 1.5e3, exp2 = 1.5e-3; Double db3 = 5648.54646121;
            decimal dc = 1.0m / 7; Decimal dc2 = 1235.8498456486479846m;
            // Ausgabe
            LblTypen.Text = "Fließkommazahlen" +
                "\nFloat fl: " + fl + "; fl2: " + fl2 +
                "\nMinimum: " + float.MinValue +
                "\nMaximum: " + float.MaxValue +
                "\n\nDouble db: " + db + "; db3: " + db3 +
                "\nDouble db2: " + db2 +
                "\nDouble exp: " + exp +
                "\nDouble exp2: " + exp2 +
                "\nMinimum: " + double.MinValue +
                "\nMaximum: " + double.MaxValue +
                "\n\nDecimal de: " + dc + "; dc2: " + dc2 +
                "\nMinimum: " + string.Format("{0:n}", decimal.MinValue) +
                "\nMaximum: " + string.Format("{0:n}", decimal.MaxValue);
            LblTypen.Visible = true;
            LblTypen2.Visible = false;
        }

        private void BtnBool_Click(object sender, EventArgs e)
        {
            // Booleschevariablen mit Deklaration und Definition in einem Schritt
            bool bo = true, bo2 = !bo; Boolean bo3 = false;
            // Ausgabe
            LblTypen2.Text = "Boolean bo:" + bo + "; bo3: " + bo3 +
                "\nNegation bo: " + bo2;
            LblTypen.Visible = false;
            LblTypen2.Visible = true;
        }

        private void BtnZeichen_Click(object sender, EventArgs e)
        {
            // Deklaration von Zeichenvariablen
            Random r = new Random();
            char ch = '\u1184', ch3;
            int ch2 = r.Next(0, 4096);
            string st = "Zeichenkette", st2 = "K";
            ch3 = Convert.ToChar(st2);
            // Definition
            // Ausgabe
            LblTypen2.Text = "Char ch: " + ch +
                "\nString st: " + st +
                "\nZufallszahl ch2: " + (char)ch2 +
                "\nString an index 5: " + st[5] +
                "\nZeichenkettenlänge: " + st.Length +
                "\nChar ch3 aus st2: " + ch3;
            LblTypen.Visible = false;
            LblTypen2.Visible = true;
        }

        private void BtnUniv_Click(object sender, EventArgs e)
        {
            // Universellevariablen mit Deklaration und Definition in einem Schritt
            var i1 = -312;
            var f1 = 123.312f;
            var b1 = true;
            var v1 = LblTypen;
            object ob = BtnSchlieszen;
            object ob2 = 156.58648646;
            object ob3 = "Zeichenkette";
            // Ausgabe
            LblTypen.Text = "Universal i1:" + i1 + "; mit DT: " + i1.GetTypeCode() +
                "\nUniversal f1: " + f1 + "; mit DT: " + f1.GetTypeCode() +
                "\nUniversal b1: " + b1 + "; mit DT: " + b1.GetTypeCode() +
                "\nUniversal v1: " + v1 + "; mit DT: " + v1.GetType() +
                "\nObject ob: " + ob + "; mit DT: " + ob.GetType() +
                "\nObject ob2: " + ob2 + "; mit DT: " + ob2.GetType() +
                "\nObject ob3: " + ob3 + "; mit DT: " + ob3.GetType();
            LblTypen.Visible = true;
            LblTypen2.Visible = false;
        }

    }
}
