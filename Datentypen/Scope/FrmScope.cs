﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Scope
{
    public partial class FrmScope : Form
    {
        // Globale Variablen
        private int Mx = 0;
        public int x = 0;

        public FrmScope()
        {
            InitializeComponent();
        }

        private void BtnEnd_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnAnz1_Click(object sender, EventArgs e)
        {
            // Lokale Variable
            int x = 0;
            x = x + 1;
            // Globale Variable
            Mx = Mx + 1;
            LblTxt.Text = "Lokale Variable x: " + x +
                "\nGlobale Variable x: " + this.x +
                "\nGlobale Variable Mx: " + Mx;
        }

        private void BtnAnz2_Click(object sender, EventArgs e)
        {
            // Lokale Variable
            int Mx = 0;
            Mx = Mx + 1;
            // Globale Variable
            x = x + 1;
            LblTxt.Text = "Lokale Variable Mx: " + Mx +
                "\nGlobale Variable Mx: " + this.Mx +
                "\nGlobale Variable x: " + x;
        }
    }
}
