﻿namespace Scope
{
    partial class FrmScope
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnAnz1 = new System.Windows.Forms.Button();
            this.BtnAnz2 = new System.Windows.Forms.Button();
            this.LblTxt = new System.Windows.Forms.Label();
            this.BtnEnd = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnAnz1
            // 
            this.BtnAnz1.Location = new System.Drawing.Point(687, 12);
            this.BtnAnz1.Name = "BtnAnz1";
            this.BtnAnz1.Size = new System.Drawing.Size(75, 23);
            this.BtnAnz1.TabIndex = 0;
            this.BtnAnz1.Text = "Anzeigen1";
            this.BtnAnz1.UseVisualStyleBackColor = true;
            this.BtnAnz1.Click += new System.EventHandler(this.BtnAnz1_Click);
            // 
            // BtnAnz2
            // 
            this.BtnAnz2.Location = new System.Drawing.Point(687, 41);
            this.BtnAnz2.Name = "BtnAnz2";
            this.BtnAnz2.Size = new System.Drawing.Size(75, 23);
            this.BtnAnz2.TabIndex = 1;
            this.BtnAnz2.Text = "Anzeigen2";
            this.BtnAnz2.UseVisualStyleBackColor = true;
            this.BtnAnz2.Click += new System.EventHandler(this.BtnAnz2_Click);
            // 
            // LblTxt
            // 
            this.LblTxt.AutoSize = true;
            this.LblTxt.Location = new System.Drawing.Point(12, 9);
            this.LblTxt.Name = "LblTxt";
            this.LblTxt.Size = new System.Drawing.Size(16, 13);
            this.LblTxt.TabIndex = 2;
            this.LblTxt.Text = "...";
            // 
            // BtnEnd
            // 
            this.BtnEnd.Location = new System.Drawing.Point(687, 464);
            this.BtnEnd.Name = "BtnEnd";
            this.BtnEnd.Size = new System.Drawing.Size(75, 23);
            this.BtnEnd.TabIndex = 3;
            this.BtnEnd.Text = "Benden";
            this.BtnEnd.UseVisualStyleBackColor = true;
            this.BtnEnd.Click += new System.EventHandler(this.BtnEnd_Click);
            // 
            // FrmScope
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(774, 499);
            this.Controls.Add(this.BtnEnd);
            this.Controls.Add(this.LblTxt);
            this.Controls.Add(this.BtnAnz2);
            this.Controls.Add(this.BtnAnz1);
            this.Name = "FrmScope";
            this.Text = "Gültigkeitsbereiche/Scope";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnAnz1;
        private System.Windows.Forms.Button BtnAnz2;
        private System.Windows.Forms.Label LblTxt;
        private System.Windows.Forms.Button BtnEnd;
    }
}

