﻿using System;
using Gtk;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnIdentCreateButtonClicked(object sender, EventArgs e)
    {
    }

    protected void OnAusfuehrenSqlBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnLoadBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnSaveBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnExistanceBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnDeleteBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnEmptyBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnCreateDirBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnDeleteDirBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnCreateCustomBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnOpenDialougeBtnClicked(object sender, EventArgs e)
    {
    }

    protected void OnXMLBtnClicked(object sender, EventArgs e)
    {
    }
}
