using System;
namespace Digitalgatter
{
    public class TXor:TGatter2Eingaenge
    {
        public TXor()
        {
            bezeichnung = "XOR";
        }

        public override void berechnen()
        {
            a = (e0 && !e1) || (!e0 && e1);
        }
    }
}
