using System;
namespace Digitalgatter
{
    public class TNand:TGatter2Eingaenge
    {
        public TNand()
        {
            bezeichnung = "NAND";
        }

        public override void berechnen()
        {
            a = !(e0 && e1);
        }
    }
}
