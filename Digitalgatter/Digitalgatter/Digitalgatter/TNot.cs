using System;
namespace Digitalgatter
{
    public class TNot:TGatter1Eingang
    {
        public TNot()
        {
            bezeichnung = "NOT";
        }

        public override void berechnen()
        {
            a = !e0;
        }
    }
}
