using System;
namespace Digitalgatter
{
    public class TXnor:TGatter2Eingaenge
    {
        public TXnor()
        {
            bezeichnung = "XNOR";
        }

        public override void berechnen()
        {
            a = (e0 && e1) || (!e0 && !e1);
        }
    }
}
