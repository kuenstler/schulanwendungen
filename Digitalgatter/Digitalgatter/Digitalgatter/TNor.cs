using System;
namespace Digitalgatter
{
    public class TNor:TGatter2Eingaenge
    {
        public TNor()
        {
            bezeichnung = "NOR";
        }

        public override void berechnen()
        {
            a = !(e0 || e1);
        }
    }
}
