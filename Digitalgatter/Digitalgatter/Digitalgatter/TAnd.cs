using System;
namespace Digitalgatter
{
    public class TAnd:TGatter2Eingaenge
    {
        public TAnd()
        {
            bezeichnung = "AND";
        }

        public override void berechnen()
        {
            a = e0 && e1;
        }
    }
}
