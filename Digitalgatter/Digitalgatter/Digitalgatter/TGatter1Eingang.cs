using System;
namespace Digitalgatter
{
    public abstract class TGatter1Eingang
    {
        protected bool a;
        protected string bezeichnung;
        protected bool e0;

        public TGatter1Eingang()
        {

        }

        public virtual string A
        {
            get
            {
                berechnen();
                return e0.ToString() + " " + bezeichnung + " = " + a;
            }
        }

        public void eingeben(bool e0)
        {
            this.e0 = e0;
        }

        public virtual void berechnen()
        {
            throw new NotImplementedException();
        }

        public virtual string ausgeben()
        {
            return A;
        }
    }
}
