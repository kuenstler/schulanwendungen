using System;
namespace Digitalgatter
{
    public class TGatter2Eingaenge:TGatter1Eingang
    {
        protected bool e1;

        public TGatter2Eingaenge()
        {
        }

        public override string A
        {
            get
            {
                berechnen();
                return e0.ToString() + " " + bezeichnung + " " + e1 + " = " + a;
            }
        }

        public void eingeben(bool e0, bool e1)
        {
            eingeben(e0);
            this.e1 = e1;
        }
    }
}
