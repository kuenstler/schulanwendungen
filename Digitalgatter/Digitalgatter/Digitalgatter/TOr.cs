using System;
namespace Digitalgatter
{
    public class TOr:TGatter2Eingaenge
    {
        public TOr()
        {
            bezeichnung = "OR";
        }

        public override void berechnen()
        {
            a = e0 || e1;
        }
    }
}
