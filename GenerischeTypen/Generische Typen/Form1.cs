﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Generische_Typen
{
    public partial class Form1 : Form
    {
        Random r = new Random();
        List<int> list = new List<int>();
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnZahl_Click(object sender, EventArgs e)
        {
            list.Clear();
            for (int i= 0; i < 10; i++)
            {
                int tmp = r.Next(0, 10);
                if (!list.Contains(tmp)) {
                    list.Add(tmp);
                }
                else
                {
                    --i;
                }
            }
            ListeAusgeben(list);
        }

        private void ListeAusgeben(List<int> list)
        {
            LstBoxZahl.Items.Clear();
            foreach(int i in list)
            {
                LstBoxZahl.Items.Add(i);
            }
        }
    }
}
