﻿namespace Generische_Typen
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrpBoxLand = new System.Windows.Forms.GroupBox();
            this.BtnLand = new System.Windows.Forms.Button();
            this.LstBoxLand = new System.Windows.Forms.ListBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.BtnZahl = new System.Windows.Forms.Button();
            this.LstBoxZahl = new System.Windows.Forms.ListBox();
            this.GrpBoxLand.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpBoxLand
            // 
            this.GrpBoxLand.Controls.Add(this.LstBoxLand);
            this.GrpBoxLand.Controls.Add(this.BtnLand);
            this.GrpBoxLand.Location = new System.Drawing.Point(13, 13);
            this.GrpBoxLand.Name = "GrpBoxLand";
            this.GrpBoxLand.Size = new System.Drawing.Size(200, 155);
            this.GrpBoxLand.TabIndex = 0;
            this.GrpBoxLand.TabStop = false;
            this.GrpBoxLand.Text = "Länderliste";
            // 
            // BtnLand
            // 
            this.BtnLand.Location = new System.Drawing.Point(7, 20);
            this.BtnLand.Name = "BtnLand";
            this.BtnLand.Size = new System.Drawing.Size(187, 23);
            this.BtnLand.TabIndex = 0;
            this.BtnLand.Text = "Länderliste anzeigen";
            this.BtnLand.UseVisualStyleBackColor = true;
            // 
            // LstBoxLand
            // 
            this.LstBoxLand.FormattingEnabled = true;
            this.LstBoxLand.Location = new System.Drawing.Point(7, 50);
            this.LstBoxLand.Name = "LstBoxLand";
            this.LstBoxLand.Size = new System.Drawing.Size(187, 95);
            this.LstBoxLand.TabIndex = 1;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.LstBoxZahl);
            this.groupBox1.Controls.Add(this.BtnZahl);
            this.groupBox1.Location = new System.Drawing.Point(220, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(200, 155);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "groupBox1";
            // 
            // BtnZahl
            // 
            this.BtnZahl.Location = new System.Drawing.Point(6, 19);
            this.BtnZahl.Name = "BtnZahl";
            this.BtnZahl.Size = new System.Drawing.Size(188, 23);
            this.BtnZahl.TabIndex = 0;
            this.BtnZahl.Text = "Neue Zahlenliste";
            this.BtnZahl.UseVisualStyleBackColor = true;
            this.BtnZahl.Click += new System.EventHandler(this.BtnZahl_Click);
            // 
            // LstBoxZahl
            // 
            this.LstBoxZahl.FormattingEnabled = true;
            this.LstBoxZahl.Location = new System.Drawing.Point(7, 50);
            this.LstBoxZahl.Name = "LstBoxZahl";
            this.LstBoxZahl.Size = new System.Drawing.Size(187, 95);
            this.LstBoxZahl.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.GrpBoxLand);
            this.Name = "Form1";
            this.Text = "Form1";
            this.GrpBoxLand.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpBoxLand;
        private System.Windows.Forms.ListBox LstBoxLand;
        private System.Windows.Forms.Button BtnLand;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.ListBox LstBoxZahl;
        private System.Windows.Forms.Button BtnZahl;
    }
}

