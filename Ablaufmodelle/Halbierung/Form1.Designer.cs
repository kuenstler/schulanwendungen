﻿namespace Halbierung
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblAus = new System.Windows.Forms.Label();
            this.TxbZahl = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // LblAus
            // 
            this.LblAus.AutoSize = true;
            this.LblAus.Location = new System.Drawing.Point(13, 13);
            this.LblAus.Name = "LblAus";
            this.LblAus.Size = new System.Drawing.Size(22, 13);
            this.LblAus.TabIndex = 0;
            this.LblAus.Text = "[...]";
            // 
            // TxbZahl
            // 
            this.TxbZahl.Location = new System.Drawing.Point(197, 6);
            this.TxbZahl.Name = "TxbZahl";
            this.TxbZahl.Size = new System.Drawing.Size(75, 20);
            this.TxbZahl.TabIndex = 2;
            this.TxbZahl.TextChanged += new System.EventHandler(this.TxbZahl_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.TxbZahl);
            this.Controls.Add(this.LblAus);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblAus;
        private System.Windows.Forms.TextBox TxbZahl;
    }
}

