﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Halbierung
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void TxbZahl_TextChanged(object sender, EventArgs e)
        {
            if (TxbZahl.Text.All(char.IsDigit)&&TxbZahl.Text!="0"&&TxbZahl.Text!="")
            {
                LblAus.Text = "";
                double i = Convert.ToDouble(TxbZahl.Text), c = 0;
                c++;
                while (i > 0.001)
                {
                    if (i != Convert.ToDouble(TxbZahl.Text)) LblAus.Text += i + "\n";
                    i /= 2;
                }
            }
        }
    }
}
