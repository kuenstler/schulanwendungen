﻿namespace Schleifen
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnIpp = new System.Windows.Forms.Button();
            this.BtnIp2 = new System.Windows.Forms.Button();
            this.BtnImm = new System.Windows.Forms.Button();
            this.Btndp15 = new System.Windows.Forms.Button();
            this.Btn5 = new System.Windows.Forms.Button();
            this.BtnK = new System.Windows.Forms.Button();
            this.BtnF = new System.Windows.Forms.Button();
            this.LblIpp = new System.Windows.Forms.Label();
            this.LblIp2 = new System.Windows.Forms.Label();
            this.LblImm = new System.Windows.Forms.Label();
            this.Lbl15 = new System.Windows.Forms.Label();
            this.Lbl5 = new System.Windows.Forms.Label();
            this.LblK = new System.Windows.Forms.Label();
            this.LblF = new System.Windows.Forms.Label();
            this.BtnHang = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnIpp
            // 
            this.BtnIpp.Location = new System.Drawing.Point(13, 13);
            this.BtnIpp.Name = "BtnIpp";
            this.BtnIpp.Size = new System.Drawing.Size(166, 23);
            this.BtnIpp.TabIndex = 0;
            this.BtnIpp.Text = "i++";
            this.BtnIpp.UseVisualStyleBackColor = true;
            this.BtnIpp.Click += new System.EventHandler(this.BtnIpp_Click);
            // 
            // BtnIp2
            // 
            this.BtnIp2.Location = new System.Drawing.Point(185, 13);
            this.BtnIp2.Name = "BtnIp2";
            this.BtnIp2.Size = new System.Drawing.Size(166, 23);
            this.BtnIp2.TabIndex = 1;
            this.BtnIp2.Text = "i += 2";
            this.BtnIp2.UseVisualStyleBackColor = true;
            this.BtnIp2.Click += new System.EventHandler(this.BtnIp2_Click);
            // 
            // BtnImm
            // 
            this.BtnImm.Location = new System.Drawing.Point(357, 13);
            this.BtnImm.Name = "BtnImm";
            this.BtnImm.Size = new System.Drawing.Size(166, 23);
            this.BtnImm.TabIndex = 2;
            this.BtnImm.Text = "i--";
            this.BtnImm.UseVisualStyleBackColor = true;
            this.BtnImm.Click += new System.EventHandler(this.BtnImm_Click);
            // 
            // Btndp15
            // 
            this.Btndp15.Location = new System.Drawing.Point(529, 13);
            this.Btndp15.Name = "Btndp15";
            this.Btndp15.Size = new System.Drawing.Size(166, 23);
            this.Btndp15.TabIndex = 3;
            this.Btndp15.Text = "d += 1,5";
            this.Btndp15.UseVisualStyleBackColor = true;
            this.Btndp15.Click += new System.EventHandler(this.Btndp15_Click);
            // 
            // Btn5
            // 
            this.Btn5.Location = new System.Drawing.Point(701, 13);
            this.Btn5.Name = "Btn5";
            this.Btn5.Size = new System.Drawing.Size(166, 23);
            this.Btn5.TabIndex = 4;
            this.Btn5.Text = "5";
            this.Btn5.UseVisualStyleBackColor = true;
            this.Btn5.Click += new System.EventHandler(this.Btn5_Click);
            // 
            // BtnK
            // 
            this.BtnK.Location = new System.Drawing.Point(13, 222);
            this.BtnK.Name = "BtnK";
            this.BtnK.Size = new System.Drawing.Size(166, 23);
            this.BtnK.TabIndex = 5;
            this.BtnK.Text = "KSchleife";
            this.BtnK.UseVisualStyleBackColor = true;
            this.BtnK.Click += new System.EventHandler(this.BtnK_Click);
            // 
            // BtnF
            // 
            this.BtnF.Location = new System.Drawing.Point(185, 222);
            this.BtnF.Name = "BtnF";
            this.BtnF.Size = new System.Drawing.Size(166, 23);
            this.BtnF.TabIndex = 6;
            this.BtnF.Text = "FSchleife";
            this.BtnF.UseVisualStyleBackColor = true;
            this.BtnF.Click += new System.EventHandler(this.BtnF_Click);
            // 
            // LblIpp
            // 
            this.LblIpp.AutoSize = true;
            this.LblIpp.Location = new System.Drawing.Point(13, 43);
            this.LblIpp.Name = "LblIpp";
            this.LblIpp.Size = new System.Drawing.Size(22, 13);
            this.LblIpp.TabIndex = 7;
            this.LblIpp.Text = "[...]";
            // 
            // LblIp2
            // 
            this.LblIp2.AutoSize = true;
            this.LblIp2.Location = new System.Drawing.Point(185, 43);
            this.LblIp2.Name = "LblIp2";
            this.LblIp2.Size = new System.Drawing.Size(22, 13);
            this.LblIp2.TabIndex = 8;
            this.LblIp2.Text = "[...]";
            // 
            // LblImm
            // 
            this.LblImm.AutoSize = true;
            this.LblImm.Location = new System.Drawing.Point(357, 43);
            this.LblImm.Name = "LblImm";
            this.LblImm.Size = new System.Drawing.Size(22, 13);
            this.LblImm.TabIndex = 9;
            this.LblImm.Text = "[...]";
            // 
            // Lbl15
            // 
            this.Lbl15.AutoSize = true;
            this.Lbl15.Location = new System.Drawing.Point(529, 43);
            this.Lbl15.Name = "Lbl15";
            this.Lbl15.Size = new System.Drawing.Size(22, 13);
            this.Lbl15.TabIndex = 10;
            this.Lbl15.Text = "[...]";
            // 
            // Lbl5
            // 
            this.Lbl5.AutoSize = true;
            this.Lbl5.Location = new System.Drawing.Point(701, 43);
            this.Lbl5.Name = "Lbl5";
            this.Lbl5.Size = new System.Drawing.Size(22, 13);
            this.Lbl5.TabIndex = 11;
            this.Lbl5.Text = "[...]";
            // 
            // LblK
            // 
            this.LblK.AutoSize = true;
            this.LblK.Location = new System.Drawing.Point(13, 252);
            this.LblK.Name = "LblK";
            this.LblK.Size = new System.Drawing.Size(22, 13);
            this.LblK.TabIndex = 12;
            this.LblK.Text = "[...]";
            // 
            // LblF
            // 
            this.LblF.AutoSize = true;
            this.LblF.Location = new System.Drawing.Point(185, 252);
            this.LblF.Name = "LblF";
            this.LblF.Size = new System.Drawing.Size(22, 13);
            this.LblF.TabIndex = 13;
            this.LblF.Text = "[...]";
            // 
            // BtnHang
            // 
            this.BtnHang.Location = new System.Drawing.Point(357, 221);
            this.BtnHang.Name = "BtnHang";
            this.BtnHang.Size = new System.Drawing.Size(75, 23);
            this.BtnHang.TabIndex = 14;
            this.BtnHang.Text = "Aufhängen";
            this.BtnHang.UseVisualStyleBackColor = true;
            this.BtnHang.Click += new System.EventHandler(this.BtnHang_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1009, 487);
            this.Controls.Add(this.BtnHang);
            this.Controls.Add(this.LblF);
            this.Controls.Add(this.LblK);
            this.Controls.Add(this.Lbl5);
            this.Controls.Add(this.Lbl15);
            this.Controls.Add(this.LblImm);
            this.Controls.Add(this.LblIp2);
            this.Controls.Add(this.LblIpp);
            this.Controls.Add(this.BtnF);
            this.Controls.Add(this.BtnK);
            this.Controls.Add(this.Btn5);
            this.Controls.Add(this.Btndp15);
            this.Controls.Add(this.BtnImm);
            this.Controls.Add(this.BtnIp2);
            this.Controls.Add(this.BtnIpp);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnIpp;
        private System.Windows.Forms.Button BtnIp2;
        private System.Windows.Forms.Button BtnImm;
        private System.Windows.Forms.Button Btndp15;
        private System.Windows.Forms.Button Btn5;
        private System.Windows.Forms.Button BtnK;
        private System.Windows.Forms.Button BtnF;
        private System.Windows.Forms.Label LblIpp;
        private System.Windows.Forms.Label LblIp2;
        private System.Windows.Forms.Label LblImm;
        private System.Windows.Forms.Label Lbl15;
        private System.Windows.Forms.Label Lbl5;
        private System.Windows.Forms.Label LblK;
        private System.Windows.Forms.Label LblF;
        private System.Windows.Forms.Button BtnHang;
    }
}

