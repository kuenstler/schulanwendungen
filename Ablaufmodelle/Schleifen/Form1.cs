﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Schleifen
{
    public partial class Form1 : Form
    {
        Random r = new Random();
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnIpp_Click(object sender, EventArgs e)
        {
            int i;
            LblIpp.Text = "";
            for (i = 3; i <= 9; i++) LblIpp.Text += i + "\n";
        }

        private void BtnIp2_Click(object sender, EventArgs e)
        {
            int i;
            LblIp2.Text = "";
            for (i = 2; i <= 10; i += 2) LblIp2.Text += i + "\n";
        }

        private void BtnImm_Click(object sender, EventArgs e)
        {
            int i;
            LblImm.Text = "";
            for (i = 8; i >= 4; i--) LblImm.Text += i + "\n";
        }

        private void Btndp15_Click(object sender, EventArgs e)
        {
            double d;
            Lbl15.Text = "";
            for (d = 3.2; d <= 8.0; d+=0.8) Lbl15.Text += d + "\n";
        }

        private void Btn5_Click(object sender, EventArgs e)
        {
            int i;
            Lbl5.Text = "";
            for (i = 0; i <= 25; i++) {
                if (i > 5 && i < 9 || i > 13 && i < 17) continue;
                else if (i > 20) break;
                Lbl5.Text += i + "\n";
            }
        }

        private void BtnK_Click(object sender, EventArgs e)
        {
            int i = 0;
            int ir = 0;
            LblK.Text = "";
            while (i<=40){
                if (i != 0) LblK.Text += ir + "; " + i + "\n";
                ir = r.Next(1, 10);
                i += ir;
            }
        }

        private void BtnF_Click(object sender, EventArgs e)
        {
            int i = 0;
            int ir = 0;
            LblF.Text = "";
            do
            {
                if (i != 0) LblF.Text += ir + "; " + i + "\n";
                ir = r.Next(1, 10);
                i += ir;
            } while (i <= 40);
        }

        private void BtnHang_Click(object sender, EventArgs e)
        {
            while (true) { }
        }
    }
}
