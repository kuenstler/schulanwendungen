﻿namespace Zahlenraten
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnNeu = new System.Windows.Forms.Button();
            this.TxbIn = new System.Windows.Forms.TextBox();
            this.LblOut = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnNeu
            // 
            this.BtnNeu.Location = new System.Drawing.Point(197, 13);
            this.BtnNeu.Name = "BtnNeu";
            this.BtnNeu.Size = new System.Drawing.Size(75, 23);
            this.BtnNeu.TabIndex = 0;
            this.BtnNeu.Text = "Neue Zahl";
            this.BtnNeu.UseVisualStyleBackColor = true;
            this.BtnNeu.Click += new System.EventHandler(this.BtnNeu_Click);
            // 
            // TxbIn
            // 
            this.TxbIn.Location = new System.Drawing.Point(13, 13);
            this.TxbIn.Name = "TxbIn";
            this.TxbIn.Size = new System.Drawing.Size(100, 20);
            this.TxbIn.TabIndex = 1;
            this.TxbIn.TextChanged += new System.EventHandler(this.TxbIn_TextChanged);
            // 
            // LblOut
            // 
            this.LblOut.AutoSize = true;
            this.LblOut.Location = new System.Drawing.Point(13, 40);
            this.LblOut.Name = "LblOut";
            this.LblOut.Size = new System.Drawing.Size(22, 13);
            this.LblOut.TabIndex = 2;
            this.LblOut.Text = "[...]";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.LblOut);
            this.Controls.Add(this.TxbIn);
            this.Controls.Add(this.BtnNeu);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnNeu;
        private System.Windows.Forms.TextBox TxbIn;
        private System.Windows.Forms.Label LblOut;
    }
}

