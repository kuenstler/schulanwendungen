﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zahlenraten
{
    public partial class Form1 : Form
    {
        Random r = new Random();
        int z = 0;
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnNeu_Click(object sender, EventArgs e)
        {
            z = r.Next(1,101);
            LblOut.Text = "[...]";
        }

        private void TxbIn_TextChanged(object sender, EventArgs e)
        {
            if (TxbIn.Text.All(char.IsDigit)&&TxbIn.Text!="")
            {
                int i = Convert.ToInt32(TxbIn.Text);
                if (i < z) LblOut.Text = "" + i + " ist zu klein";
                else if (i > z) LblOut.Text = "" + i + " ist zu groß";
                else LblOut.Text = "" + i + " ist richtig";
            }
        }
    }
}
