﻿namespace Kalkulationsanwendung
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.radBtnForward = new System.Windows.Forms.RadioButton();
            this.radBtnDiff = new System.Windows.Forms.RadioButton();
            this.radBtnBack = new System.Windows.Forms.RadioButton();
            this.grpType = new System.Windows.Forms.GroupBox();
            this.grpLiefer = new System.Windows.Forms.GroupBox();
            this.txbBezug = new System.Windows.Forms.TextBox();
            this.lblBezug = new System.Windows.Forms.Label();
            this.txbSkontoLiefer = new System.Windows.Forms.TextBox();
            this.lblSkontoLiefer = new System.Windows.Forms.Label();
            this.txbRabattLiefer = new System.Windows.Forms.TextBox();
            this.lblRabattLiefer = new System.Windows.Forms.Label();
            this.grpUnternehmen = new System.Windows.Forms.GroupBox();
            this.txbRabattUnternehmen = new System.Windows.Forms.TextBox();
            this.lblRabattUnternehmen = new System.Windows.Forms.Label();
            this.txbSkontoUnternehmen = new System.Windows.Forms.TextBox();
            this.lblSkontoUnternehmen = new System.Windows.Forms.Label();
            this.txbGewinnZuschlag = new System.Windows.Forms.TextBox();
            this.lblGewinnZuschlag = new System.Windows.Forms.Label();
            this.txbHandlung = new System.Windows.Forms.TextBox();
            this.lblHandlung = new System.Windows.Forms.Label();
            this.grpStaat = new System.Windows.Forms.GroupBox();
            this.txbSteuer = new System.Windows.Forms.TextBox();
            this.lblSteuer = new System.Windows.Forms.Label();
            this.lblEinkauf = new System.Windows.Forms.Label();
            this.txbEinkauf = new System.Windows.Forms.TextBox();
            this.lblGewinn = new System.Windows.Forms.Label();
            this.txbGewinn = new System.Windows.Forms.TextBox();
            this.lblVerkauf = new System.Windows.Forms.Label();
            this.txbVerkauf = new System.Windows.Forms.TextBox();
            this.btnShowCalc = new System.Windows.Forms.Button();
            this.grpType.SuspendLayout();
            this.grpLiefer.SuspendLayout();
            this.grpUnternehmen.SuspendLayout();
            this.grpStaat.SuspendLayout();
            this.SuspendLayout();
            // 
            // radBtnForward
            // 
            this.radBtnForward.AutoSize = true;
            this.radBtnForward.Checked = true;
            this.radBtnForward.Location = new System.Drawing.Point(6, 19);
            this.radBtnForward.Name = "radBtnForward";
            this.radBtnForward.Size = new System.Drawing.Size(117, 17);
            this.radBtnForward.TabIndex = 0;
            this.radBtnForward.TabStop = true;
            this.radBtnForward.Text = "Vorwärtskalkulation";
            this.radBtnForward.UseVisualStyleBackColor = true;
            this.radBtnForward.CheckedChanged += new System.EventHandler(this.radBtnForward_CheckedChanged);
            // 
            // radBtnDiff
            // 
            this.radBtnDiff.AutoSize = true;
            this.radBtnDiff.Location = new System.Drawing.Point(6, 42);
            this.radBtnDiff.Name = "radBtnDiff";
            this.radBtnDiff.Size = new System.Drawing.Size(118, 17);
            this.radBtnDiff.TabIndex = 1;
            this.radBtnDiff.Text = "Differenzkalkulation";
            this.radBtnDiff.UseVisualStyleBackColor = true;
            this.radBtnDiff.CheckedChanged += new System.EventHandler(this.radBtnDiff_CheckedChanged);
            // 
            // radBtnBack
            // 
            this.radBtnBack.AutoSize = true;
            this.radBtnBack.Location = new System.Drawing.Point(6, 65);
            this.radBtnBack.Name = "radBtnBack";
            this.radBtnBack.Size = new System.Drawing.Size(127, 17);
            this.radBtnBack.TabIndex = 2;
            this.radBtnBack.Text = "Rückwärtskalkulation";
            this.radBtnBack.UseVisualStyleBackColor = true;
            this.radBtnBack.CheckedChanged += new System.EventHandler(this.radBtnBack_CheckedChanged);
            // 
            // grpType
            // 
            this.grpType.Controls.Add(this.radBtnForward);
            this.grpType.Controls.Add(this.radBtnBack);
            this.grpType.Controls.Add(this.radBtnDiff);
            this.grpType.Location = new System.Drawing.Point(12, 12);
            this.grpType.Name = "grpType";
            this.grpType.Size = new System.Drawing.Size(271, 100);
            this.grpType.TabIndex = 3;
            this.grpType.TabStop = false;
            this.grpType.Text = "Bitte die Berechnungsart wählen";
            // 
            // grpLiefer
            // 
            this.grpLiefer.Controls.Add(this.txbBezug);
            this.grpLiefer.Controls.Add(this.lblBezug);
            this.grpLiefer.Controls.Add(this.txbSkontoLiefer);
            this.grpLiefer.Controls.Add(this.lblSkontoLiefer);
            this.grpLiefer.Controls.Add(this.txbRabattLiefer);
            this.grpLiefer.Controls.Add(this.lblRabattLiefer);
            this.grpLiefer.Location = new System.Drawing.Point(13, 119);
            this.grpLiefer.Name = "grpLiefer";
            this.grpLiefer.Size = new System.Drawing.Size(157, 138);
            this.grpLiefer.TabIndex = 4;
            this.grpLiefer.TabStop = false;
            this.grpLiefer.Text = "Lieferant";
            // 
            // txbBezug
            // 
            this.txbBezug.Location = new System.Drawing.Point(107, 69);
            this.txbBezug.Name = "txbBezug";
            this.txbBezug.Size = new System.Drawing.Size(34, 20);
            this.txbBezug.TabIndex = 5;
            this.txbBezug.Text = "6,50";
            this.txbBezug.TextChanged += new System.EventHandler(this.txbBezug_TextChanged);
            // 
            // lblBezug
            // 
            this.lblBezug.AutoSize = true;
            this.lblBezug.Location = new System.Drawing.Point(7, 72);
            this.lblBezug.Name = "lblBezug";
            this.lblBezug.Size = new System.Drawing.Size(94, 13);
            this.lblBezug.TabIndex = 4;
            this.lblBezug.Text = "Bezugskosten in €";
            // 
            // txbSkontoLiefer
            // 
            this.txbSkontoLiefer.Location = new System.Drawing.Point(107, 43);
            this.txbSkontoLiefer.Name = "txbSkontoLiefer";
            this.txbSkontoLiefer.Size = new System.Drawing.Size(34, 20);
            this.txbSkontoLiefer.TabIndex = 3;
            this.txbSkontoLiefer.Text = "1,0";
            this.txbSkontoLiefer.TextChanged += new System.EventHandler(this.txbSkontoLiefer_TextChanged);
            // 
            // lblSkontoLiefer
            // 
            this.lblSkontoLiefer.AutoSize = true;
            this.lblSkontoLiefer.Location = new System.Drawing.Point(7, 46);
            this.lblSkontoLiefer.Name = "lblSkontoLiefer";
            this.lblSkontoLiefer.Size = new System.Drawing.Size(63, 13);
            this.lblSkontoLiefer.TabIndex = 2;
            this.lblSkontoLiefer.Text = "Skonto in %";
            // 
            // txbRabattLiefer
            // 
            this.txbRabattLiefer.Location = new System.Drawing.Point(107, 17);
            this.txbRabattLiefer.Name = "txbRabattLiefer";
            this.txbRabattLiefer.Size = new System.Drawing.Size(34, 20);
            this.txbRabattLiefer.TabIndex = 1;
            this.txbRabattLiefer.Text = "3,0";
            this.txbRabattLiefer.TextChanged += new System.EventHandler(this.txbRabattLiefer_TextChanged);
            // 
            // lblRabattLiefer
            // 
            this.lblRabattLiefer.AutoSize = true;
            this.lblRabattLiefer.Location = new System.Drawing.Point(7, 20);
            this.lblRabattLiefer.Name = "lblRabattLiefer";
            this.lblRabattLiefer.Size = new System.Drawing.Size(61, 13);
            this.lblRabattLiefer.TabIndex = 0;
            this.lblRabattLiefer.Text = "Rabatt in %";
            // 
            // grpUnternehmen
            // 
            this.grpUnternehmen.Controls.Add(this.txbRabattUnternehmen);
            this.grpUnternehmen.Controls.Add(this.lblRabattUnternehmen);
            this.grpUnternehmen.Controls.Add(this.txbSkontoUnternehmen);
            this.grpUnternehmen.Controls.Add(this.lblSkontoUnternehmen);
            this.grpUnternehmen.Controls.Add(this.txbGewinnZuschlag);
            this.grpUnternehmen.Controls.Add(this.lblGewinnZuschlag);
            this.grpUnternehmen.Controls.Add(this.txbHandlung);
            this.grpUnternehmen.Controls.Add(this.lblHandlung);
            this.grpUnternehmen.Location = new System.Drawing.Point(176, 119);
            this.grpUnternehmen.Name = "grpUnternehmen";
            this.grpUnternehmen.Size = new System.Drawing.Size(212, 138);
            this.grpUnternehmen.TabIndex = 6;
            this.grpUnternehmen.TabStop = false;
            this.grpUnternehmen.Text = "Unternehmen";
            // 
            // txbRabattUnternehmen
            // 
            this.txbRabattUnternehmen.Location = new System.Drawing.Point(167, 95);
            this.txbRabattUnternehmen.Name = "txbRabattUnternehmen";
            this.txbRabattUnternehmen.Size = new System.Drawing.Size(34, 20);
            this.txbRabattUnternehmen.TabIndex = 7;
            this.txbRabattUnternehmen.Text = "2,0";
            this.txbRabattUnternehmen.TextChanged += new System.EventHandler(this.txbRabattUnternehmen_TextChanged);
            // 
            // lblRabattUnternehmen
            // 
            this.lblRabattUnternehmen.AutoSize = true;
            this.lblRabattUnternehmen.Location = new System.Drawing.Point(7, 98);
            this.lblRabattUnternehmen.Name = "lblRabattUnternehmen";
            this.lblRabattUnternehmen.Size = new System.Drawing.Size(61, 13);
            this.lblRabattUnternehmen.TabIndex = 6;
            this.lblRabattUnternehmen.Text = "Rabatt in %";
            // 
            // txbSkontoUnternehmen
            // 
            this.txbSkontoUnternehmen.Location = new System.Drawing.Point(167, 69);
            this.txbSkontoUnternehmen.Name = "txbSkontoUnternehmen";
            this.txbSkontoUnternehmen.Size = new System.Drawing.Size(34, 20);
            this.txbSkontoUnternehmen.TabIndex = 5;
            this.txbSkontoUnternehmen.Text = "0,5";
            this.txbSkontoUnternehmen.TextChanged += new System.EventHandler(this.txbSkontoUnternehmen_TextChanged);
            // 
            // lblSkontoUnternehmen
            // 
            this.lblSkontoUnternehmen.AutoSize = true;
            this.lblSkontoUnternehmen.Location = new System.Drawing.Point(7, 72);
            this.lblSkontoUnternehmen.Name = "lblSkontoUnternehmen";
            this.lblSkontoUnternehmen.Size = new System.Drawing.Size(63, 13);
            this.lblSkontoUnternehmen.TabIndex = 4;
            this.lblSkontoUnternehmen.Text = "Skonto in %";
            // 
            // txbGewinnZuschlag
            // 
            this.txbGewinnZuschlag.Location = new System.Drawing.Point(167, 43);
            this.txbGewinnZuschlag.Name = "txbGewinnZuschlag";
            this.txbGewinnZuschlag.Size = new System.Drawing.Size(34, 20);
            this.txbGewinnZuschlag.TabIndex = 3;
            this.txbGewinnZuschlag.Text = "15,0";
            this.txbGewinnZuschlag.TextChanged += new System.EventHandler(this.txbGewinnZuschlag_TextChanged);
            // 
            // lblGewinnZuschlag
            // 
            this.lblGewinnZuschlag.AutoSize = true;
            this.lblGewinnZuschlag.Location = new System.Drawing.Point(7, 46);
            this.lblGewinnZuschlag.Name = "lblGewinnZuschlag";
            this.lblGewinnZuschlag.Size = new System.Drawing.Size(107, 13);
            this.lblGewinnZuschlag.TabIndex = 2;
            this.lblGewinnZuschlag.Text = "Gewinnzuschlag in %";
            // 
            // txbHandlung
            // 
            this.txbHandlung.Location = new System.Drawing.Point(167, 17);
            this.txbHandlung.Name = "txbHandlung";
            this.txbHandlung.Size = new System.Drawing.Size(34, 20);
            this.txbHandlung.TabIndex = 1;
            this.txbHandlung.Text = "30,0";
            this.txbHandlung.TextChanged += new System.EventHandler(this.txbHandlung_TextChanged);
            // 
            // lblHandlung
            // 
            this.lblHandlung.AutoSize = true;
            this.lblHandlung.Location = new System.Drawing.Point(7, 20);
            this.lblHandlung.Name = "lblHandlung";
            this.lblHandlung.Size = new System.Drawing.Size(154, 13);
            this.lblHandlung.TabIndex = 0;
            this.lblHandlung.Text = "Handlungskostenzuschlag in %";
            // 
            // grpStaat
            // 
            this.grpStaat.Controls.Add(this.txbSteuer);
            this.grpStaat.Controls.Add(this.lblSteuer);
            this.grpStaat.Location = new System.Drawing.Point(394, 119);
            this.grpStaat.Name = "grpStaat";
            this.grpStaat.Size = new System.Drawing.Size(159, 138);
            this.grpStaat.TabIndex = 6;
            this.grpStaat.TabStop = false;
            this.grpStaat.Text = "Staat";
            // 
            // txbSteuer
            // 
            this.txbSteuer.Location = new System.Drawing.Point(113, 17);
            this.txbSteuer.Name = "txbSteuer";
            this.txbSteuer.Size = new System.Drawing.Size(34, 20);
            this.txbSteuer.TabIndex = 1;
            this.txbSteuer.Text = "16,0";
            this.txbSteuer.TextChanged += new System.EventHandler(this.txbSteuer_TextChanged);
            // 
            // lblSteuer
            // 
            this.lblSteuer.AutoSize = true;
            this.lblSteuer.Location = new System.Drawing.Point(7, 20);
            this.lblSteuer.Name = "lblSteuer";
            this.lblSteuer.Size = new System.Drawing.Size(93, 13);
            this.lblSteuer.TabIndex = 0;
            this.lblSteuer.Text = "Umsatzsteuer in %";
            // 
            // lblEinkauf
            // 
            this.lblEinkauf.AutoSize = true;
            this.lblEinkauf.Location = new System.Drawing.Point(10, 270);
            this.lblEinkauf.Name = "lblEinkauf";
            this.lblEinkauf.Size = new System.Drawing.Size(70, 13);
            this.lblEinkauf.TabIndex = 7;
            this.lblEinkauf.Text = "Einkaufspreis";
            // 
            // txbEinkauf
            // 
            this.txbEinkauf.Location = new System.Drawing.Point(86, 267);
            this.txbEinkauf.Name = "txbEinkauf";
            this.txbEinkauf.Size = new System.Drawing.Size(100, 20);
            this.txbEinkauf.TabIndex = 8;
            this.txbEinkauf.Text = "3000";
            this.txbEinkauf.TextChanged += new System.EventHandler(this.txbEinkauf_TextChanged);
            // 
            // lblGewinn
            // 
            this.lblGewinn.AutoSize = true;
            this.lblGewinn.Location = new System.Drawing.Point(192, 270);
            this.lblGewinn.Name = "lblGewinn";
            this.lblGewinn.Size = new System.Drawing.Size(43, 13);
            this.lblGewinn.TabIndex = 9;
            this.lblGewinn.Text = "Gewinn";
            // 
            // txbGewinn
            // 
            this.txbGewinn.Location = new System.Drawing.Point(241, 267);
            this.txbGewinn.Name = "txbGewinn";
            this.txbGewinn.ReadOnly = true;
            this.txbGewinn.Size = new System.Drawing.Size(100, 20);
            this.txbGewinn.TabIndex = 10;
            // 
            // lblVerkauf
            // 
            this.lblVerkauf.AutoSize = true;
            this.lblVerkauf.Location = new System.Drawing.Point(347, 270);
            this.lblVerkauf.Name = "lblVerkauf";
            this.lblVerkauf.Size = new System.Drawing.Size(71, 13);
            this.lblVerkauf.TabIndex = 11;
            this.lblVerkauf.Text = "Verkaufspreis";
            // 
            // txbVerkauf
            // 
            this.txbVerkauf.Location = new System.Drawing.Point(424, 267);
            this.txbVerkauf.Name = "txbVerkauf";
            this.txbVerkauf.ReadOnly = true;
            this.txbVerkauf.Size = new System.Drawing.Size(100, 20);
            this.txbVerkauf.TabIndex = 12;
            this.txbVerkauf.TextChanged += new System.EventHandler(this.txbVerkauf_TextChanged);
            // 
            // btnShowCalc
            // 
            this.btnShowCalc.Location = new System.Drawing.Point(13, 293);
            this.btnShowCalc.Name = "btnShowCalc";
            this.btnShowCalc.Size = new System.Drawing.Size(540, 23);
            this.btnShowCalc.TabIndex = 13;
            this.btnShowCalc.Text = "Kalkulation anzeigen";
            this.btnShowCalc.UseVisualStyleBackColor = true;
            this.btnShowCalc.Click += new System.EventHandler(this.btnShowCalc_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(567, 326);
            this.Controls.Add(this.btnShowCalc);
            this.Controls.Add(this.txbVerkauf);
            this.Controls.Add(this.lblVerkauf);
            this.Controls.Add(this.txbGewinn);
            this.Controls.Add(this.lblGewinn);
            this.Controls.Add(this.txbEinkauf);
            this.Controls.Add(this.lblEinkauf);
            this.Controls.Add(this.grpStaat);
            this.Controls.Add(this.grpUnternehmen);
            this.Controls.Add(this.grpLiefer);
            this.Controls.Add(this.grpType);
            this.Name = "Form1";
            this.Text = "Kalkulationsanwendung";
            this.grpType.ResumeLayout(false);
            this.grpType.PerformLayout();
            this.grpLiefer.ResumeLayout(false);
            this.grpLiefer.PerformLayout();
            this.grpUnternehmen.ResumeLayout(false);
            this.grpUnternehmen.PerformLayout();
            this.grpStaat.ResumeLayout(false);
            this.grpStaat.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton radBtnForward;
        private System.Windows.Forms.RadioButton radBtnDiff;
        private System.Windows.Forms.RadioButton radBtnBack;
        private System.Windows.Forms.GroupBox grpType;
        private System.Windows.Forms.GroupBox grpLiefer;
        private System.Windows.Forms.TextBox txbBezug;
        private System.Windows.Forms.Label lblBezug;
        private System.Windows.Forms.TextBox txbSkontoLiefer;
        private System.Windows.Forms.Label lblSkontoLiefer;
        private System.Windows.Forms.TextBox txbRabattLiefer;
        private System.Windows.Forms.Label lblRabattLiefer;
        private System.Windows.Forms.GroupBox grpUnternehmen;
        private System.Windows.Forms.TextBox txbRabattUnternehmen;
        private System.Windows.Forms.Label lblRabattUnternehmen;
        private System.Windows.Forms.TextBox txbSkontoUnternehmen;
        private System.Windows.Forms.Label lblSkontoUnternehmen;
        private System.Windows.Forms.TextBox txbGewinnZuschlag;
        private System.Windows.Forms.Label lblGewinnZuschlag;
        private System.Windows.Forms.TextBox txbHandlung;
        private System.Windows.Forms.Label lblHandlung;
        private System.Windows.Forms.GroupBox grpStaat;
        private System.Windows.Forms.TextBox txbSteuer;
        private System.Windows.Forms.Label lblSteuer;
        private System.Windows.Forms.Label lblEinkauf;
        private System.Windows.Forms.TextBox txbEinkauf;
        private System.Windows.Forms.Label lblGewinn;
        private System.Windows.Forms.TextBox txbGewinn;
        private System.Windows.Forms.Label lblVerkauf;
        private System.Windows.Forms.TextBox txbVerkauf;
        private System.Windows.Forms.Button btnShowCalc;
    }
}

