﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Kalkulationsanwendung
{
    public partial class Form1 : Form
    {
        private TKalk current;

        public Form1()
        {
            InitializeComponent();
            calculate();
        }

        private void calculate()
        {
            var LRabattS = Convert.ToDouble(txbRabattLiefer.Text);
            var LSkontoS = Convert.ToDouble(txbRabattLiefer.Text);
            var BZ = Convert.ToDouble(txbBezug.Text);
            var HKz = Convert.ToDouble(txbHandlung.Text);
            var KSkontoS = Convert.ToDouble(txbSkontoUnternehmen.Text);
            var KRabattS = Convert.ToDouble(txbRabattUnternehmen.Text);
            var UstdS = Convert.ToDouble(txbSteuer.Text);
            var Brutto = radBtnForward.Checked ? 0.0 : Convert.ToDouble(txbVerkauf.Text);
            var Gz = radBtnDiff.Checked ? 0.0 : Convert.ToDouble(txbGewinnZuschlag.Text);
            var LEKP = radBtnBack.Checked ? 0.0 : Convert.ToDouble(txbEinkauf.Text);
            var common_arguments = (LRabattS, LSkontoS, BZ, HKz, KSkontoS, KRabattS, UstdS);

            if (radBtnForward.Checked)
            {
                TVorwaerts vor = new TVorwaerts(
                    common_arguments,
                    (LEKP, Gz)
                );
                vor.berechnen();
                txbGewinn.Text = vor.Gewinn();
                txbVerkauf.Text = vor.Verkauf();
                current = vor;
            }
            else if (radBtnBack.Checked)
            {
                TRueckwaerts rueck = new TRueckwaerts(
                    common_arguments,
                    (Brutto, Gz)
                );
                rueck.berechnen();
                txbGewinn.Text = rueck.Gewinn();
                txbEinkauf.Text = rueck.Einkauf();
                current = rueck;
            }
            else
            {
                TDifferenz diff = new TDifferenz(
                    common_arguments,
                    (Brutto, LEKP)
                );
                diff.berechnen();
                txbGewinn.Text = diff.Gewinn();
                txbGewinnZuschlag.Text = diff.Gewinnzuschlag();
                current = diff;
            }
        }

        private void radBtnForward_CheckedChanged(object sender, EventArgs e)
        {
            txbGewinnZuschlag.ReadOnly = false;
            txbGewinnZuschlag.Text = "15";
            txbEinkauf.ReadOnly = false;
            txbEinkauf.Text = "3000";
            txbVerkauf.ReadOnly = true;
            calculate();
        }

        private void radBtnDiff_CheckedChanged(object sender, EventArgs e)
        {
            txbGewinnZuschlag.ReadOnly = true;
            txbEinkauf.ReadOnly = false;
            txbEinkauf.Text = "3000";
            txbVerkauf.ReadOnly = false;
            txbVerkauf.Text = "5195";
            calculate();
        }

        private void radBtnBack_CheckedChanged(object sender, EventArgs e)
        {
            txbGewinnZuschlag.ReadOnly = false;
            txbGewinnZuschlag.Text = "15";
            txbEinkauf.ReadOnly = true;
            txbVerkauf.ReadOnly = false;
            txbVerkauf.Text = "5195";
            calculate();
        }

        private void txbRabattLiefer_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void txbSkontoLiefer_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void txbBezug_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void txbHandlung_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void txbGewinnZuschlag_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void txbSkontoUnternehmen_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void txbRabattUnternehmen_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void txbSteuer_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void txbEinkauf_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void txbVerkauf_TextChanged(object sender, EventArgs e)
        {
            calculate();
        }

        private void btnShowCalc_Click(object sender, EventArgs e)
        {
            MessageBox.Show(current.dialouge());
        }
    }
}
