﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulationsanwendung
{
    class TVorwaerts : TKalk
    {
        public TVorwaerts(double LRabattS, double LSkontoS, double BZ, double HKz, double KSkontoS, double KRabattS, double UstdS, double LEKP, double Gz) : base((LRabattS, LSkontoS, BZ, HKz, KSkontoS, KRabattS, UstdS))
        {
            this.LEKP = LEKP;
            this.Gz = Gz / 100;
        }

        public TVorwaerts((double LRabattS, double LSkontoS, double BZ, double HKz, double KSkontoS, double KRabattS, double UstdS) b, (double LEKP, double Gz) c) : base(b)
        {
            LEKP = c.LEKP;
            Gz = c.Gz / 100;
        }

        override public void berechnen()
        {
            vorwaerts_bis_selbskosten();
            G = SK * Gz;
            BarVKP = SK + G;
            KSkonto = BarVKP * KSkontoS / (1 - KSkontoS);
            ZielVKP = BarVKP + KSkonto;
            KRabatt = ZielVKP * KRabattS / (1 - KRabattS);
            Netto = ZielVKP + KRabatt;
            Ust = UstdS * Netto;
            Brutto = Netto + Ust;
        }

        public string Verkauf()
        {
            return String.Format("{0:0.00}", Brutto);
        }
    }
}
