﻿using System;
namespace Kalkulationsanwendung
{
    class TDifferenz : TKalk
    {
        public TDifferenz(double LRabattS, double LSkontoS, double BZ, double HKz, double KSkontoS, double KRabattS, double UstdS, double Brutto, double LEKP) : base((LRabattS, LSkontoS, BZ, HKz, KSkontoS, KRabattS, UstdS))
        {
            this.LEKP = LEKP;
            this.Brutto = Brutto;
        }

        public TDifferenz((double LRabattS, double LSkontoS, double BZ, double HKz, double KSkontoS, double KRabattS, double UstdS) b, (double Brutto, double LEKP)c) : base(b)
        {
            LEKP = c.LEKP;
            Brutto = c.Brutto;
        }

        public override void berechnen()
        {
            vorwaerts_bis_selbskosten();
            rueckwaerts_bis_barverkauf();
            G = BarVKP - SK;
            Gz = G / SK;
        }

        public string Gewinnzuschlag()
        {
            return String.Format("{0:0.0}", Gz * 100);
        }
    }
}
