﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kalkulationsanwendung
{
    abstract class TKalk
    {
        protected double LEKP; //Listeneinkaufspreis
        protected double LRabatt; //Rabattwert €
        protected double LRabattS; //Rabattsatz %
        protected double ZielEKP; //Zieleinkaufspreis
        protected double LSkonto; //Skontowert €
        protected double LSkontoS; //Skontosatz 
        protected double BarEKPV; //Bareinkaufspreis
        protected double BZ;// Bezugskosten
        //----Handelsunternehmen
        protected double EinstandsP; //Einstandspreis
        protected double HK; //Handlungskosten € 
        protected double HKz; //Handlungskostenzuschlag
        protected double SK; //Selbstkosten
        protected double G; //Gewinn €
        protected double Gz; //Gewinnzuschlag %
        protected double BarVKP; //Barverkaufspreis
        protected double KSkonto; //Kundenskonto €
        //für den Kunden
        protected double KSkontoS; //Kundenskontosatz 
        protected double ZielVKP; //Zielverkaufspreis
        protected double KRabatt; //Rabatt €
        protected double KRabattS; //Rabattsatz %
        protected double Netto; //Verkaufspreis (Netto)
        //----Finanzamt
        protected double Ust; //Umsatzsteuer €
        protected double UstdS; //Umsatzsteuersatz %
        //----Endkundenmarkt
        protected double Brutto; //Verkaufspreis (Brutto)

        public TKalk((double LRabattS, double LSkontoS, double BZ, double HKz, double KSkontoS, double KRabattS, double UstdS) c)
        {
            LRabattS = c.LRabattS / 100;
            LSkontoS = c.LSkontoS / 100;
            BZ = c.BZ;
            HKz = c.HKz / 100;
            KSkontoS = c.KSkontoS / 100;
            KRabattS = c.KRabattS / 100;
            UstdS = c.UstdS / 100;
        }

        protected void vorwaerts_bis_selbskosten()
        {
            LRabatt = LEKP * LRabattS;
            ZielEKP = LEKP - LRabatt;
            LSkonto = ZielEKP * LSkontoS;
            BarEKPV = ZielEKP - LSkonto;
            EinstandsP = BarEKPV + BZ;
            HK = EinstandsP * HKz;
            SK = EinstandsP + HK;
        }

        protected void rueckwaerts_bis_barverkauf()
        {
            Ust = Brutto * UstdS / (1 + UstdS);
            Netto = Brutto - Ust;
            KRabatt = Netto * KRabattS;
            ZielVKP = Netto - KRabatt;
            KSkonto = ZielVKP * KSkontoS;
            BarVKP = ZielVKP - KSkonto;
        }

        public string Gewinn()
        {
            return String.Format("{0:0.00}", G);
        }

        public string dialouge()
        {
            bool vor = GetType().Name == nameof(TVorwaerts);
            bool rueck = GetType().Name == nameof(TRueckwaerts);
            return ((vor ? "Vorwärtskalkulation" : rueck ? "Rückwärtskalkulation" :
                "Differenzkalkulation") +
                "\nListeneinkaufspreis: " + String.Format("{0:c}", LEKP) +
                "\n-Lieferrabatt: " + String.Format("{0:c}", LRabatt) +
                "\n=Ziel Einkaufspreis: " + String.Format("{0:c}", ZielEKP) +
                "\n-Lieferskonto: " + String.Format("{0:c}", LSkonto) +
                "\n=Bar Einkaufspreis: " + String.Format("{0:c}", BarEKPV) +
                "\n+Bezugskosten: " + String.Format("{0:c}", BZ) +
                "\n=Einstandspreis: " + String.Format("{0:c}", EinstandsP) +
                "\n+Handelskosten: " + String.Format("{0:c}", HK) +
                "\n=Selbstkosten: " + String.Format("{0:c}", SK) +
                "\n+Gewinn: " + String.Format("{0:c}", G) +
                "\n=Bar Verkaufspreis: " + String.Format("{0:c}", BarVKP) +
                "\n+Kundenskonto: " + String.Format("{0:c}", KSkonto) +
                "\n=Ziel Verkaufspreis: " + String.Format("{0:c}", ZielVKP) +
                "\n+Kundenrabatt: " + String.Format("{0:c}", KRabatt) +
                "\n=Nettopreis: " + String.Format("{0:c}", Netto) +
                "\n+Märchensteuer: " + String.Format("{0:c}", Ust) +
                "\n=Bruttopreis: " + String.Format("{0:c}", Brutto)
            );
        }

        abstract public void berechnen();
    }
}
