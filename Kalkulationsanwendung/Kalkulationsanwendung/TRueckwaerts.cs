﻿using System;
namespace Kalkulationsanwendung
{
    class TRueckwaerts : TKalk
    {
        public TRueckwaerts(double LRabattS, double LSkontoS, double BZ, double HKz, double KSkontoS, double KRabattS, double UstdS, double Brutto, double Gz) : base((LRabattS, LSkontoS, BZ, HKz, KSkontoS, KRabattS, UstdS))
        {
            this.Brutto = Brutto;
            this.Gz = Gz / 100;
        }
        public TRueckwaerts((double LRabattS, double LSkontoS, double BZ, double HKz, double KSkontoS, double KRabattS, double UstdS) b, (double Brutto, double Gz) c) : base(b)
        {
            Brutto = c.Brutto;
            Gz = c.Gz / 100;
        }

        override public void berechnen()
        {
            rueckwaerts_bis_barverkauf();
            G = BarVKP * Gz / (1 + Gz);
            SK = BarVKP - G;
            HK = SK * HKz / (1 + HKz);
            EinstandsP = SK - HK;
            BarEKPV = EinstandsP - BZ;
            LSkonto = BarEKPV * LSkontoS / (1 - LSkontoS);
            ZielEKP = BarEKPV + LSkonto;
            LRabatt = ZielEKP * LRabattS / (1 - LRabattS);
            LEKP = ZielEKP + LRabatt;
        }

        public string Einkauf()
        {
            return String.Format("{0:0.00}", LEKP);
        }
    }
}
