﻿namespace Hallo
{
    partial class FrmHallo
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FrmHallo));
            this.btnSchlieszen = new System.Windows.Forms.Button();
            this.btnHallo = new System.Windows.Forms.Button();
            this.lblAnzeige = new System.Windows.Forms.Label();
            this.btnLoeschen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnSchlieszen
            // 
            this.btnSchlieszen.BackColor = System.Drawing.SystemColors.Control;
            this.btnSchlieszen.Location = new System.Drawing.Point(30, 178);
            this.btnSchlieszen.Name = "btnSchlieszen";
            this.btnSchlieszen.Size = new System.Drawing.Size(203, 66);
            this.btnSchlieszen.TabIndex = 2;
            this.btnSchlieszen.Text = "Fenster schließen";
            this.btnSchlieszen.UseVisualStyleBackColor = false;
            this.btnSchlieszen.Click += new System.EventHandler(this.btnSchlieszen_Click);
            // 
            // btnHallo
            // 
            this.btnHallo.Location = new System.Drawing.Point(30, 30);
            this.btnHallo.Name = "btnHallo";
            this.btnHallo.Size = new System.Drawing.Size(203, 66);
            this.btnHallo.TabIndex = 0;
            this.btnHallo.Text = "Inhalt Anzeigen";
            this.btnHallo.UseVisualStyleBackColor = true;
            this.btnHallo.Click += new System.EventHandler(this.btnHallo_Click);
            // 
            // lblAnzeige
            // 
            this.lblAnzeige.AutoSize = true;
            this.lblAnzeige.Location = new System.Drawing.Point(113, 9);
            this.lblAnzeige.Name = "lblAnzeige";
            this.lblAnzeige.Size = new System.Drawing.Size(36, 13);
            this.lblAnzeige.TabIndex = 3;
            this.lblAnzeige.Text = "[Infos]";
            this.lblAnzeige.Click += new System.EventHandler(this.lblAnzeige_Click);
            // 
            // btnLoeschen
            // 
            this.btnLoeschen.Location = new System.Drawing.Point(30, 104);
            this.btnLoeschen.Name = "btnLoeschen";
            this.btnLoeschen.Size = new System.Drawing.Size(203, 66);
            this.btnLoeschen.TabIndex = 1;
            this.btnLoeschen.Text = "Inhalt Löschen";
            this.btnLoeschen.UseVisualStyleBackColor = true;
            this.btnLoeschen.Click += new System.EventHandler(this.btnLoeschen_Click);
            // 
            // FrmHallo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(292, 261);
            this.Controls.Add(this.btnLoeschen);
            this.Controls.Add(this.lblAnzeige);
            this.Controls.Add(this.btnHallo);
            this.Controls.Add(this.btnSchlieszen);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "FrmHallo";
            this.Text = "meine erste Anwendung";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnSchlieszen;
        private System.Windows.Forms.Button btnHallo;
        private System.Windows.Forms.Label lblAnzeige;
        private System.Windows.Forms.Button btnLoeschen;
    }
}

