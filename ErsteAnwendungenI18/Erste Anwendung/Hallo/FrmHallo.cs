﻿/*****************************
 * Verfasser Fabian Wunsch
 * erstellt: 2018-10-02
 * letzte Änderung: 2018-10-23
******************************/

using System;
using System.Windows.Forms;

namespace Hallo
{
    public partial class FrmHallo : Form
    {
        public FrmHallo()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void btnSchlieszen_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnLoeschen_Click(object sender, EventArgs e)
        {

        }

        private void btnHallo_Click(object sender, EventArgs e)
        {

        }

        private void lblAnzeige_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
