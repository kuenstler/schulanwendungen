﻿namespace Avatar
{
    partial class FrmAvatar
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.BtnRelPos = new System.Windows.Forms.Button();
            this.BtnAbsPos = new System.Windows.Forms.Button();
            this.BtnAttribute = new System.Windows.Forms.Button();
            this.BtnRelSkal = new System.Windows.Forms.Button();
            this.BtnAbsSkal = new System.Windows.Forms.Button();
            this.BtnFarbe = new System.Windows.Forms.Button();
            this.LblAttribute = new System.Windows.Forms.Label();
            this.GrpHintergrund = new System.Windows.Forms.GroupBox();
            this.LblBlau = new System.Windows.Forms.Label();
            this.LblGruen = new System.Windows.Forms.Label();
            this.LblRot = new System.Windows.Forms.Label();
            this.TrkBlau = new System.Windows.Forms.TrackBar();
            this.TrkGruen = new System.Windows.Forms.TrackBar();
            this.TrkRot = new System.Windows.Forms.TrackBar();
            this.BtnAvatar = new System.Windows.Forms.Button();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.GrpHintergrund.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrkBlau)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrkGruen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrkRot)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnRelPos
            // 
            this.BtnRelPos.Location = new System.Drawing.Point(15, 15);
            this.BtnRelPos.Margin = new System.Windows.Forms.Padding(6);
            this.BtnRelPos.Name = "BtnRelPos";
            this.BtnRelPos.Size = new System.Drawing.Size(200, 40);
            this.BtnRelPos.TabIndex = 3;
            this.BtnRelPos.Text = "relativ positionieren";
            this.BtnRelPos.UseVisualStyleBackColor = true;
            this.BtnRelPos.Click += new System.EventHandler(this.BtnRelPos_Click);
            // 
            // BtnAbsPos
            // 
            this.BtnAbsPos.Location = new System.Drawing.Point(227, 15);
            this.BtnAbsPos.Margin = new System.Windows.Forms.Padding(6);
            this.BtnAbsPos.Name = "BtnAbsPos";
            this.BtnAbsPos.Size = new System.Drawing.Size(200, 40);
            this.BtnAbsPos.TabIndex = 4;
            this.BtnAbsPos.Text = "absolut positionieren";
            this.BtnAbsPos.UseVisualStyleBackColor = true;
            this.BtnAbsPos.Click += new System.EventHandler(this.BtnAbsPos_Click);
            // 
            // BtnAttribute
            // 
            this.BtnAttribute.Location = new System.Drawing.Point(439, 15);
            this.BtnAttribute.Margin = new System.Windows.Forms.Padding(6);
            this.BtnAttribute.Name = "BtnAttribute";
            this.BtnAttribute.Size = new System.Drawing.Size(200, 40);
            this.BtnAttribute.TabIndex = 5;
            this.BtnAttribute.Text = "Attribute zeigen";
            this.BtnAttribute.UseVisualStyleBackColor = true;
            this.BtnAttribute.Click += new System.EventHandler(this.BtnAttribute_Click);
            // 
            // BtnRelSkal
            // 
            this.BtnRelSkal.Location = new System.Drawing.Point(15, 67);
            this.BtnRelSkal.Margin = new System.Windows.Forms.Padding(6);
            this.BtnRelSkal.Name = "BtnRelSkal";
            this.BtnRelSkal.Size = new System.Drawing.Size(200, 40);
            this.BtnRelSkal.TabIndex = 6;
            this.BtnRelSkal.Text = "relativ skalieren";
            this.BtnRelSkal.UseVisualStyleBackColor = true;
            this.BtnRelSkal.Click += new System.EventHandler(this.BtnRelSkal_Click);
            // 
            // BtnAbsSkal
            // 
            this.BtnAbsSkal.Location = new System.Drawing.Point(227, 67);
            this.BtnAbsSkal.Margin = new System.Windows.Forms.Padding(6);
            this.BtnAbsSkal.Name = "BtnAbsSkal";
            this.BtnAbsSkal.Size = new System.Drawing.Size(200, 40);
            this.BtnAbsSkal.TabIndex = 7;
            this.BtnAbsSkal.Text = "absolut skalieren";
            this.BtnAbsSkal.UseVisualStyleBackColor = true;
            this.BtnAbsSkal.Click += new System.EventHandler(this.BtnAbsSkal_Click);
            // 
            // BtnFarbe
            // 
            this.BtnFarbe.Location = new System.Drawing.Point(651, 15);
            this.BtnFarbe.Margin = new System.Windows.Forms.Padding(6);
            this.BtnFarbe.Name = "BtnFarbe";
            this.BtnFarbe.Size = new System.Drawing.Size(200, 40);
            this.BtnFarbe.TabIndex = 8;
            this.BtnFarbe.Text = "Farbe ändern";
            this.BtnFarbe.UseVisualStyleBackColor = true;
            this.BtnFarbe.Click += new System.EventHandler(this.BtnFarbe_Click);
            // 
            // LblAttribute
            // 
            this.LblAttribute.AutoSize = true;
            this.LblAttribute.Location = new System.Drawing.Point(436, 75);
            this.LblAttribute.Name = "LblAttribute";
            this.LblAttribute.Size = new System.Drawing.Size(137, 24);
            this.LblAttribute.TabIndex = 9;
            this.LblAttribute.Text = "[Avatarattribute]";
            // 
            // GrpHintergrund
            // 
            this.GrpHintergrund.Controls.Add(this.LblBlau);
            this.GrpHintergrund.Controls.Add(this.LblGruen);
            this.GrpHintergrund.Controls.Add(this.LblRot);
            this.GrpHintergrund.Controls.Add(this.TrkBlau);
            this.GrpHintergrund.Controls.Add(this.TrkGruen);
            this.GrpHintergrund.Controls.Add(this.TrkRot);
            this.GrpHintergrund.Location = new System.Drawing.Point(439, 203);
            this.GrpHintergrund.Name = "GrpHintergrund";
            this.GrpHintergrund.Size = new System.Drawing.Size(412, 186);
            this.GrpHintergrund.TabIndex = 10;
            this.GrpHintergrund.TabStop = false;
            this.GrpHintergrund.Text = "Hintergrundfarbe einstellen";
            // 
            // LblBlau
            // 
            this.LblBlau.AutoSize = true;
            this.LblBlau.Location = new System.Drawing.Point(6, 133);
            this.LblBlau.Name = "LblBlau";
            this.LblBlau.Size = new System.Drawing.Size(91, 24);
            this.LblBlau.TabIndex = 5;
            this.LblBlau.Text = "Blauanteil";
            // 
            // LblGruen
            // 
            this.LblGruen.AutoSize = true;
            this.LblGruen.Location = new System.Drawing.Point(6, 81);
            this.LblGruen.Name = "LblGruen";
            this.LblGruen.Size = new System.Drawing.Size(96, 24);
            this.LblGruen.TabIndex = 4;
            this.LblGruen.Text = "Grünanteil";
            // 
            // LblRot
            // 
            this.LblRot.AutoSize = true;
            this.LblRot.Location = new System.Drawing.Point(7, 29);
            this.LblRot.Name = "LblRot";
            this.LblRot.Size = new System.Drawing.Size(82, 24);
            this.LblRot.TabIndex = 3;
            this.LblRot.Text = "Rotanteil";
            // 
            // TrkBlau
            // 
            this.TrkBlau.LargeChange = 4;
            this.TrkBlau.Location = new System.Drawing.Point(108, 133);
            this.TrkBlau.Maximum = 255;
            this.TrkBlau.Name = "TrkBlau";
            this.TrkBlau.Size = new System.Drawing.Size(298, 45);
            this.TrkBlau.TabIndex = 2;
            this.TrkBlau.TickFrequency = 16;
            this.TrkBlau.Value = 255;
            this.TrkBlau.Scroll += new System.EventHandler(this.TrkBlau_Scroll);
            // 
            // TrkGruen
            // 
            this.TrkGruen.LargeChange = 4;
            this.TrkGruen.Location = new System.Drawing.Point(108, 81);
            this.TrkGruen.Maximum = 255;
            this.TrkGruen.Name = "TrkGruen";
            this.TrkGruen.Size = new System.Drawing.Size(298, 45);
            this.TrkGruen.TabIndex = 1;
            this.TrkGruen.TickFrequency = 16;
            this.TrkGruen.Value = 255;
            this.TrkGruen.Scroll += new System.EventHandler(this.TrkGruen_Scroll);
            // 
            // TrkRot
            // 
            this.TrkRot.LargeChange = 4;
            this.TrkRot.Location = new System.Drawing.Point(108, 29);
            this.TrkRot.Maximum = 255;
            this.TrkRot.Name = "TrkRot";
            this.TrkRot.Size = new System.Drawing.Size(298, 45);
            this.TrkRot.TabIndex = 0;
            this.TrkRot.TickFrequency = 16;
            this.TrkRot.Value = 255;
            this.TrkRot.Scroll += new System.EventHandler(this.TrkRot_Scroll);
            // 
            // BtnAvatar
            // 
            this.BtnAvatar.BackColor = System.Drawing.Color.Red;
            this.BtnAvatar.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnAvatar.ForeColor = System.Drawing.Color.White;
            this.BtnAvatar.Location = new System.Drawing.Point(15, 170);
            this.BtnAvatar.Name = "BtnAvatar";
            this.BtnAvatar.Size = new System.Drawing.Size(80, 80);
            this.BtnAvatar.TabIndex = 11;
            this.BtnAvatar.Text = "Avatar";
            this.BtnAvatar.UseVisualStyleBackColor = false;
            this.BtnAvatar.Click += new System.EventHandler(this.BtnAvatar_Click);
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // FrmAvatar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(11F, 24F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(860, 427);
            this.Controls.Add(this.BtnAvatar);
            this.Controls.Add(this.GrpHintergrund);
            this.Controls.Add(this.LblAttribute);
            this.Controls.Add(this.BtnFarbe);
            this.Controls.Add(this.BtnAbsSkal);
            this.Controls.Add(this.BtnRelSkal);
            this.Controls.Add(this.BtnAttribute);
            this.Controls.Add(this.BtnAbsPos);
            this.Controls.Add(this.BtnRelPos);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Margin = new System.Windows.Forms.Padding(6);
            this.Name = "FrmAvatar";
            this.Text = "Avatar Schaltfläche";
            this.GrpHintergrund.ResumeLayout(false);
            this.GrpHintergrund.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TrkBlau)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrkGruen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TrkRot)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnRelPos;
        private System.Windows.Forms.Button BtnAbsPos;
        private System.Windows.Forms.Button BtnAttribute;
        private System.Windows.Forms.Button BtnRelSkal;
        private System.Windows.Forms.Button BtnAbsSkal;
        private System.Windows.Forms.Button BtnFarbe;
        private System.Windows.Forms.Label LblAttribute;
        private System.Windows.Forms.GroupBox GrpHintergrund;
        private System.Windows.Forms.Label LblRot;
        private System.Windows.Forms.TrackBar TrkBlau;
        private System.Windows.Forms.TrackBar TrkGruen;
        private System.Windows.Forms.TrackBar TrkRot;
        private System.Windows.Forms.Label LblBlau;
        private System.Windows.Forms.Label LblGruen;
        private System.Windows.Forms.Button BtnAvatar;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;

    }
}

