﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Avatar
{
    // ----------Formularklasse---------------------
    public partial class FrmAvatar : Form
    {
        // ----------Formularattribute--------------
        Point startpos = new Point(0, 0);
        Size startgroe = new Size(0, 0);
        int rot, gruen, blau;//Hintergrundformularfarben
        // ----------Formularmethoden---------------
        
        public void TrkUpdate()// Nach änderung der Slider die Hintergrundfarbe ändern
        {
            rot = TrkRot.Value;
            gruen = TrkGruen.Value;
            blau = TrkBlau.Value;
            BackColor = Color.FromArgb(rot, gruen, blau);
            LblBlau.Text = "Blauanteil:\n" + TrkBlau.Value;// Labels an den Slidern ändern
            LblGruen.Text = "Grünanteil:\n" + TrkGruen.Value;
            LblRot.Text = "Rotanteil:\n" + TrkRot.Value;
        }

        public void AttributUpdate()// Werte der Attribute ändern
        {
            LblAttribute.Text = "Position: X: " + BtnAvatar.Location.X +
                ", Y: " + BtnAvatar.Location.Y + "\nGröße: Breite: " +
                BtnAvatar.Size.Width + ", Höhe: " + BtnAvatar.Size.Height +
                "\nAvatarhintergrund: " + BtnAvatar.BackColor.ToString() +
                "\nAvatarfordergrund: " + BtnAvatar.ForeColor.ToString();
        }

        public FrmAvatar()// Konstruktor
        {
            InitializeComponent();
            // Avatarposition und Größe merken
            startpos = BtnAvatar.Location;
            startgroe = BtnAvatar.Size;
            //Hintergrundfarbe nach den Random stellen
            Random r = new Random();
            TrkBlau.Value = r.Next(0, 255);
            TrkGruen.Value = r.Next(0, 255);
            TrkRot.Value = r.Next(0, 255);
            TrkUpdate();
            AttributUpdate();
        }

        //Schaltflächenmethoden
        private void BtnRelPos_Click(object sender, EventArgs e)
        {
            BtnAvatar.Location = new Point(
                BtnAvatar.Location.X + 10, BtnAvatar.Location.Y);// Avatar nach rechts verschieben
            AttributUpdate();
        }

        private void BtnAbsPos_Click(object sender, EventArgs e)
        {
            BtnAvatar.Location = startpos;// Ausgangsposition
            AttributUpdate();
        }

        private void BtnRelSkal_Click(object sender, EventArgs e)
        {
            BtnAvatar.Size = new Size(
                BtnAvatar.Size.Width + 5, BtnAvatar.Size.Height + 5);// Avatar vergrößern
            AttributUpdate();
        }

        private void BtnAbsSkal_Click(object sender, EventArgs e)
        {
            BtnAvatar.Size = startgroe;// Ausgangsgröße
            AttributUpdate();
        }

        private void BtnAttribute_Click(object sender, EventArgs e)
        {
            AttributUpdate();
        }

        private void BtnFarbe_Click(object sender, EventArgs e)
        {
            Random r = new Random();
            BtnAvatar.BackColor = Color.FromArgb(
                r.Next(0,256), r.Next(0,256), r.Next(0,256));// Hintergrundfarbe des Avatars ändern
            AttributUpdate();
        }

        private void TrkRot_Scroll(object sender, EventArgs e)
        {
            TrkUpdate();
        }

        private void TrkGruen_Scroll(object sender, EventArgs e)
        {
            TrkUpdate();
        }

        private void TrkBlau_Scroll(object sender, EventArgs e)
        {
            TrkUpdate();
        }


        private void BtnAvatar_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Autor: Fabian Wunsch\nKlasse: IG18\nSchule: BSZET Dresden", "OK", MessageBoxButtons.OK);
        }

    }
}