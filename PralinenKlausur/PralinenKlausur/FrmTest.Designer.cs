﻿namespace PralinenKlausur
{
    partial class frmTest
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.grbAuswahl = new System.Windows.Forms.GroupBox();
            this.optSpitze = new System.Windows.Forms.RadioButton();
            this.optKugel = new System.Windows.Forms.RadioButton();
            this.lblDurchmesser = new System.Windows.Forms.Label();
            this.lblHoehe = new System.Windows.Forms.Label();
            this.txtHoehe = new System.Windows.Forms.TextBox();
            this.txtDurchmesser = new System.Windows.Forms.TextBox();
            this.grbSchoko = new System.Windows.Forms.GroupBox();
            this.label100 = new System.Windows.Forms.Label();
            this.lbl0 = new System.Windows.Forms.Label();
            this.trkSchoko = new System.Windows.Forms.TrackBar();
            this.grbDim = new System.Windows.Forms.GroupBox();
            this.btnErstellen = new System.Windows.Forms.Button();
            this.btnZeigen = new System.Windows.Forms.Button();
            this.btnAuswerten = new System.Windows.Forms.Button();
            this.btnLoeschen = new System.Windows.Forms.Button();
            this.lstAnzeigen = new System.Windows.Forms.ListBox();
            this.grbAuswahl.SuspendLayout();
            this.grbSchoko.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkSchoko)).BeginInit();
            this.grbDim.SuspendLayout();
            this.SuspendLayout();
            // 
            // grbAuswahl
            // 
            this.grbAuswahl.Controls.Add(this.optSpitze);
            this.grbAuswahl.Controls.Add(this.optKugel);
            this.grbAuswahl.Location = new System.Drawing.Point(12, 12);
            this.grbAuswahl.Name = "grbAuswahl";
            this.grbAuswahl.Size = new System.Drawing.Size(200, 87);
            this.grbAuswahl.TabIndex = 0;
            this.grbAuswahl.TabStop = false;
            this.grbAuswahl.Text = "Pralinenform";
            // 
            // optSpitze
            // 
            this.optSpitze.AutoSize = true;
            this.optSpitze.Location = new System.Drawing.Point(15, 52);
            this.optSpitze.Name = "optSpitze";
            this.optSpitze.Size = new System.Drawing.Size(72, 24);
            this.optSpitze.TabIndex = 1;
            this.optSpitze.Text = "Spitze";
            this.optSpitze.UseVisualStyleBackColor = true;
            this.optSpitze.CheckedChanged += new System.EventHandler(this.optKegel_CheckedChanged);
            // 
            // optKugel
            // 
            this.optKugel.AutoSize = true;
            this.optKugel.Checked = true;
            this.optKugel.Location = new System.Drawing.Point(15, 22);
            this.optKugel.Name = "optKugel";
            this.optKugel.Size = new System.Drawing.Size(67, 24);
            this.optKugel.TabIndex = 0;
            this.optKugel.TabStop = true;
            this.optKugel.Text = "Kugel";
            this.optKugel.UseVisualStyleBackColor = true;
            this.optKugel.CheckedChanged += new System.EventHandler(this.optKugel_CheckedChanged);
            // 
            // lblDurchmesser
            // 
            this.lblDurchmesser.AutoSize = true;
            this.lblDurchmesser.Location = new System.Drawing.Point(11, 28);
            this.lblDurchmesser.Name = "lblDurchmesser";
            this.lblDurchmesser.Size = new System.Drawing.Size(104, 20);
            this.lblDurchmesser.TabIndex = 2;
            this.lblDurchmesser.Text = "Durchmesser";
            // 
            // lblHoehe
            // 
            this.lblHoehe.AutoSize = true;
            this.lblHoehe.Location = new System.Drawing.Point(220, 28);
            this.lblHoehe.Name = "lblHoehe";
            this.lblHoehe.Size = new System.Drawing.Size(48, 20);
            this.lblHoehe.TabIndex = 3;
            this.lblHoehe.Text = "Höhe";
            // 
            // txtHoehe
            // 
            this.txtHoehe.Location = new System.Drawing.Point(274, 25);
            this.txtHoehe.Name = "txtHoehe";
            this.txtHoehe.Size = new System.Drawing.Size(74, 26);
            this.txtHoehe.TabIndex = 4;
            this.txtHoehe.TextChanged += new System.EventHandler(this.txtHoehe_TextChanged);
            // 
            // txtDurchmesser
            // 
            this.txtDurchmesser.Location = new System.Drawing.Point(121, 25);
            this.txtDurchmesser.Name = "txtDurchmesser";
            this.txtDurchmesser.Size = new System.Drawing.Size(74, 26);
            this.txtDurchmesser.TabIndex = 5;
            this.txtDurchmesser.TextChanged += new System.EventHandler(this.txtDurchmesser_TextChanged);
            // 
            // grbSchoko
            // 
            this.grbSchoko.Controls.Add(this.label100);
            this.grbSchoko.Controls.Add(this.lbl0);
            this.grbSchoko.Controls.Add(this.trkSchoko);
            this.grbSchoko.Location = new System.Drawing.Point(230, 12);
            this.grbSchoko.Name = "grbSchoko";
            this.grbSchoko.Size = new System.Drawing.Size(318, 87);
            this.grbSchoko.TabIndex = 6;
            this.grbSchoko.TabStop = false;
            this.grbSchoko.Text = "Schokoladenanteil";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label100.Location = new System.Drawing.Point(264, 62);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(54, 20);
            this.label100.TabIndex = 2;
            this.label100.Text = "100 %";
            // 
            // lbl0
            // 
            this.lbl0.AutoSize = true;
            this.lbl0.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.lbl0.Location = new System.Drawing.Point(11, 62);
            this.lbl0.Name = "lbl0";
            this.lbl0.Size = new System.Drawing.Size(36, 20);
            this.lbl0.TabIndex = 1;
            this.lbl0.Text = "0 %";
            // 
            // trkSchoko
            // 
            this.trkSchoko.Location = new System.Drawing.Point(6, 31);
            this.trkSchoko.Maximum = 100;
            this.trkSchoko.Name = "trkSchoko";
            this.trkSchoko.Size = new System.Drawing.Size(289, 45);
            this.trkSchoko.TabIndex = 0;
            this.trkSchoko.TickFrequency = 10;
            this.trkSchoko.Value = 100;
            this.trkSchoko.Scroll += new System.EventHandler(this.trkSchoko_Scroll);
            // 
            // grbDim
            // 
            this.grbDim.Controls.Add(this.txtDurchmesser);
            this.grbDim.Controls.Add(this.lblDurchmesser);
            this.grbDim.Controls.Add(this.txtHoehe);
            this.grbDim.Controls.Add(this.lblHoehe);
            this.grbDim.Location = new System.Drawing.Point(12, 114);
            this.grbDim.Name = "grbDim";
            this.grbDim.Size = new System.Drawing.Size(536, 68);
            this.grbDim.TabIndex = 7;
            this.grbDim.TabStop = false;
            this.grbDim.Text = "Dimension(en) in mm";
            // 
            // btnErstellen
            // 
            this.btnErstellen.Location = new System.Drawing.Point(12, 199);
            this.btnErstellen.Name = "btnErstellen";
            this.btnErstellen.Size = new System.Drawing.Size(175, 33);
            this.btnErstellen.TabIndex = 8;
            this.btnErstellen.Text = "Praline erstellen";
            this.btnErstellen.UseVisualStyleBackColor = true;
            this.btnErstellen.Click += new System.EventHandler(this.btnErstellen_Click);
            // 
            // btnZeigen
            // 
            this.btnZeigen.Location = new System.Drawing.Point(12, 418);
            this.btnZeigen.Name = "btnZeigen";
            this.btnZeigen.Size = new System.Drawing.Size(175, 33);
            this.btnZeigen.TabIndex = 9;
            this.btnZeigen.Text = "Pralinen(feld) zeigen";
            this.btnZeigen.UseVisualStyleBackColor = true;
            this.btnZeigen.Click += new System.EventHandler(this.btnZeigen_Click);
            // 
            // btnAuswerten
            // 
            this.btnAuswerten.Location = new System.Drawing.Point(192, 418);
            this.btnAuswerten.Name = "btnAuswerten";
            this.btnAuswerten.Size = new System.Drawing.Size(175, 33);
            this.btnAuswerten.TabIndex = 10;
            this.btnAuswerten.Text = "Pralinen auswerten";
            this.btnAuswerten.UseVisualStyleBackColor = true;
            this.btnAuswerten.Click += new System.EventHandler(this.btnAuswerten_Click);
            // 
            // btnLoeschen
            // 
            this.btnLoeschen.Location = new System.Drawing.Point(373, 418);
            this.btnLoeschen.Name = "btnLoeschen";
            this.btnLoeschen.Size = new System.Drawing.Size(175, 33);
            this.btnLoeschen.TabIndex = 11;
            this.btnLoeschen.Text = "Pralinen(feld) löschen";
            this.btnLoeschen.UseVisualStyleBackColor = true;
            this.btnLoeschen.Click += new System.EventHandler(this.btnLoeschen_Click);
            // 
            // lstAnzeigen
            // 
            this.lstAnzeigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstAnzeigen.FormattingEnabled = true;
            this.lstAnzeigen.ItemHeight = 16;
            this.lstAnzeigen.Location = new System.Drawing.Point(12, 248);
            this.lstAnzeigen.Name = "lstAnzeigen";
            this.lstAnzeigen.Size = new System.Drawing.Size(536, 164);
            this.lstAnzeigen.TabIndex = 12;
            // 
            // frmTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(573, 463);
            this.Controls.Add(this.lstAnzeigen);
            this.Controls.Add(this.btnLoeschen);
            this.Controls.Add(this.btnAuswerten);
            this.Controls.Add(this.btnZeigen);
            this.Controls.Add(this.btnErstellen);
            this.Controls.Add(this.grbDim);
            this.Controls.Add(this.grbSchoko);
            this.Controls.Add(this.grbAuswahl);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "frmTest";
            this.Text = "Pralinen - Testformular";
            this.grbAuswahl.ResumeLayout(false);
            this.grbAuswahl.PerformLayout();
            this.grbSchoko.ResumeLayout(false);
            this.grbSchoko.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trkSchoko)).EndInit();
            this.grbDim.ResumeLayout(false);
            this.grbDim.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox grbAuswahl;
        private System.Windows.Forms.RadioButton optSpitze;
        private System.Windows.Forms.RadioButton optKugel;
        private System.Windows.Forms.Label lblDurchmesser;
        private System.Windows.Forms.Label lblHoehe;
        private System.Windows.Forms.TextBox txtHoehe;
        private System.Windows.Forms.TextBox txtDurchmesser;
        private System.Windows.Forms.GroupBox grbSchoko;
        private System.Windows.Forms.TrackBar trkSchoko;
        private System.Windows.Forms.GroupBox grbDim;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.Label lbl0;
        private System.Windows.Forms.Button btnErstellen;
        private System.Windows.Forms.Button btnZeigen;
        private System.Windows.Forms.Button btnAuswerten;
        private System.Windows.Forms.Button btnLoeschen;
        private System.Windows.Forms.ListBox lstAnzeigen;
    }
}

