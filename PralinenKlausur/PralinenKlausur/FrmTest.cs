﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PralinenKlausur
{
    public partial class frmTest : Form
    {
        //-----Formularattribute-----
        public string pralinenForm;
        public double durchmesser;
        public double hoehe;
        public int schokoanteil;

        public List<TPraline> pralinenListe = new List<TPraline>();
        
        
        //-----Formularmethoden------
        public frmTest() //ctor
        {
            InitializeComponent();
            //Start-GUI konfigurieren
            optKugel.Checked = true;
            grbSchoko.Text = "Schokoladenanteil " + trkSchoko.Value + " %";
            lblHoehe.Visible = false;
            txtHoehe.Visible = false;
            lstAnzeigen.Items.Clear();
        }

        private void trkSchoko_Scroll(object sender, EventArgs e)
        {
            grbSchoko.Text = "Schokoladenanteil " + trkSchoko.Value + " %";
            schokoanteil = trkSchoko.Value;
        }

        private void optKegel_CheckedChanged(object sender, EventArgs e)
        {
            //zusätzliche Eingabe der Höhe in mm
            lblHoehe.Visible = true;
            txtHoehe.Visible = true;
            pralinenForm = "Spitze";
        }

        private void optKugel_CheckedChanged(object sender, EventArgs e)
        {
            //nur Eingabe des Durchmessers in mm
            lblHoehe.Visible = false;
            txtHoehe.Visible = false;
            pralinenForm = "Kugel";
        }



        //-----Schaltflächenmethoden--
        private void btnErstellen_Click(object sender, EventArgs e)
        {
            switch (pralinenForm)
            {
                case "Spitze":
                    TSpitze spitze = new TSpitze(schokoanteil, durchmesser, hoehe);
                    pralinenListe.Add(spitze);
                    lstAnzeigen.Items.Clear();
                    lstAnzeigen.Items.Add(spitze.ausgeben());
                    break;

                case "Kugel":
                    TKugel kugel = new TKugel(schokoanteil, durchmesser);
                    pralinenListe.Add(kugel);
                    lstAnzeigen.Items.Clear();
                    lstAnzeigen.Items.Add(kugel.ausgeben());
                    break;

                default:
                    break;
            }
        }

        private void btnZeigen_Click(object sender, EventArgs e)
        {
            lstAnzeigen.Items.Clear();
            foreach (var Praline in pralinenListe)
            {
                lstAnzeigen.Items.Add(Praline.ausgeben());
            }
        }

        private void btnAuswerten_Click(object sender, EventArgs e)
        {
            string ausgabe = "";
            ausgabe += "Pralinenzahl: " + pralinenListe.Count()
                + "\n Schokolade: " + 18.8F + " ml";
        }

        private void btnLoeschen_Click(object sender, EventArgs e)
        {
            lstAnzeigen.Items.Clear();
            pralinenListe.Clear();
        }

        private void txtDurchmesser_TextChanged(object sender, EventArgs e)
        {
            durchmesser = Convert.ToDouble(txtDurchmesser.Text);
        }

        private void txtHoehe_TextChanged(object sender, EventArgs e)
        {
            hoehe = Convert.ToDouble(txtHoehe.Text);
        }
    }
}
