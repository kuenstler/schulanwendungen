﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PralinenKlausur
{
    class TKugel:TPraline
    {
        //-----Eigenschaften------
        protected double durchmesser; //mm

        //-----Methoden-----------
        public TKugel(int s, double d):base(s)
        {
            durchmesser = d;
        }
        
        //Verarbeitung
        public override void berechneVolumen()
        {
            volumen = Math.PI * Math.Pow(durchmesser, 3) / 6; //mm^3
        }

        //polymorphe Methode ausgeben()
        public override string ausgeben()
        {
            string ausgabe = "";

            ausgabe += "PralinenKlausur.TKugel"
                + " mit " + SchokoAnteil() + "% Schokoanteil"
                + ", "
                + "V: " + volumen + " cm^3"
                + ", "
                + "d: " + durchmesser + " mm";

            return ausgabe;
        }
    }
}
