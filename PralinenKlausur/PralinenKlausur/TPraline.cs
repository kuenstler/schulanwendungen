﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PralinenKlausur
{
    public abstract class TPraline
    {
        //-----Eigenschaften------
        private int schokoanteil; //Prozent
        protected double volumen; //mm^3

        //-----Methoden-----------
        //ctor
        public TPraline(int schoko)
        {
            schokoanteil = schoko;
        }
        public double SchokoAnteil()
        {
            return schokoanteil;
        }
        public double Volumen()
        {
            return volumen;
        }

        //get-Methode für Schokoladenvolumen
        public double schokoladenVolumen()
        {
            return volumen * schokoanteil / 100;
        }

        //Verarbeitung
        public abstract void berechneVolumen();
        
        //polymorphe Methode ausgeben()
        public virtual string ausgeben()
        {
            string ausgabe = "";

            ausgabe += "PralinenKlausur.TPraline"
                + " mit " + schokoanteil + "% Schokoanteil"
                + ", "
                + "V: " + volumen + " cm^3"
                + ", "
                + "d: " + " kein Durchmesser"
                + "h: " + " keine Höhe";

            return ausgabe;
        }
    }
}
