﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PralinenKlausur
{
    class TSpitze:TKugel
    {
        //-----Eigenschaften-----
        private double hoehe;
        
        //-----Methoden----------
        //ctor
        public TSpitze(int s, double d, double h): base(s, d)
        {
            hoehe = h;
        }
        
        //Verarbeitung
        public override void berechneVolumen()
        {
            volumen = Math.PI * Math.Pow(durchmesser, 2) * hoehe / 12;
        }

        //polymorphe Methode ausgeben()
        public override string ausgeben()
        {
            string ausgabe = "";

            ausgabe += "PralinenKlausur.TSpitze"
                + " mit " + SchokoAnteil() + "% Schokoanteil"
                + ", "
                + "V: " + volumen + " cm^3"
                + ", "
                + "d: " + durchmesser + " mm"
                + "h: " + hoehe + " mm";

            return ausgabe;
        }
    }
}

