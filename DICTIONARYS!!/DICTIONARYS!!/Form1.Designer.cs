﻿namespace DICTIONARYS__
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnDict = new System.Windows.Forms.Button();
            this.LstDict = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // BtnDict
            // 
            this.BtnDict.Location = new System.Drawing.Point(12, 12);
            this.BtnDict.Name = "BtnDict";
            this.BtnDict.Size = new System.Drawing.Size(108, 23);
            this.BtnDict.TabIndex = 0;
            this.BtnDict.Text = "DICTIONARY";
            this.BtnDict.UseVisualStyleBackColor = true;
            this.BtnDict.Click += new System.EventHandler(this.BtnDict_Click);
            // 
            // LstDict
            // 
            this.LstDict.FormattingEnabled = true;
            this.LstDict.Location = new System.Drawing.Point(12, 41);
            this.LstDict.Name = "LstDict";
            this.LstDict.Size = new System.Drawing.Size(390, 251);
            this.LstDict.TabIndex = 1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LstDict);
            this.Controls.Add(this.BtnDict);
            this.Name = "Form1";
            this.Text = "DICTIONARYS";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnDict;
        private System.Windows.Forms.ListBox LstDict;
    }
}

