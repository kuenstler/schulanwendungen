﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DICTIONARYS__
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void ausgabe_dict_keys(string s, Dictionary<string, string> d)
        {
            string ausgabe_string = ": ";
            foreach(string schleife in d.Keys)
            {
                ausgabe_string += schleife + "; ";
            }
            LstDict.Items.Add(s + ausgabe_string);
        }

        private void ausgabe_dict_werte(string s, Dictionary<string, string> d)
        {
            string ausgabe_string = ": ";
            foreach (string schleife in d.Values)
            {
                ausgabe_string += schleife + "; ";
            }
            LstDict.Items.Add(s + ausgabe_string);
        }

        private void ausgabe_dict_alles(string s, Dictionary<string, string> d)
        {
            string ausgabe_string = ": ";
            foreach (string schleife in d.Keys)
            {
                ausgabe_string += schleife + ":" + d[schleife] + "; ";
            }
            LstDict.Items.Add(s + ausgabe_string);
        }

        private void BtnDict_Click(object sender, EventArgs e)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();

            LstDict.Items.Clear();
            d["CH ".Trim()] = "Schweiz ".Trim();
            d["E".Trim()] = " Spanien ".Trim();

            LstDict.Items.Add("Anzahl: " + d.Count);
            ausgabe_dict_keys("Schlüssel", d);
            ausgabe_dict_werte("Werte", d);
            ausgabe_dict_alles("Zu Beginn", d);

            if (d.ContainsKey("CH"))
            {
                LstDict.Items.Add("Enthält Schlüssel CH");
            }
            if (d.ContainsValue("Spanien"))
            {
                LstDict.Items.Add("Enthält Wert Spanien");
            }

            d["E"] = "Equador";
            ausgabe_dict_alles("Nach ersetzen über schlüssel", d);

            d.Remove("E");
            ausgabe_dict_alles("Nach löschen über Key", d);
        }
    }
}
