﻿namespace Uebung_Fehler
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnA = new System.Windows.Forms.Button();
            this.LblA = new System.Windows.Forms.Label();
            this.LblB = new System.Windows.Forms.Label();
            this.TxbB = new System.Windows.Forms.TextBox();
            this.BtnAdd = new System.Windows.Forms.Button();
            this.BtnDiv = new System.Windows.Forms.Button();
            this.BtnDec = new System.Windows.Forms.Button();
            this.BtnName = new System.Windows.Forms.Button();
            this.LblGleichung = new System.Windows.Forms.Label();
            this.LblKlick = new System.Windows.Forms.Label();
            this.BtnBeenden = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BtnA
            // 
            this.BtnA.Location = new System.Drawing.Point(13, 13);
            this.BtnA.Name = "BtnA";
            this.BtnA.Size = new System.Drawing.Size(75, 23);
            this.BtnA.TabIndex = 0;
            this.BtnA.Text = "a laden";
            this.BtnA.UseVisualStyleBackColor = true;
            this.BtnA.Click += new System.EventHandler(this.BtnA_Click);
            // 
            // LblA
            // 
            this.LblA.AutoSize = true;
            this.LblA.Location = new System.Drawing.Point(94, 18);
            this.LblA.Name = "LblA";
            this.LblA.Size = new System.Drawing.Size(19, 13);
            this.LblA.TabIndex = 1;
            this.LblA.Text = "[a]";
            // 
            // LblB
            // 
            this.LblB.AutoSize = true;
            this.LblB.Location = new System.Drawing.Point(136, 18);
            this.LblB.Name = "LblB";
            this.LblB.Size = new System.Drawing.Size(60, 13);
            this.LblB.TabIndex = 2;
            this.LblB.Text = "b eingeben";
            // 
            // TxbB
            // 
            this.TxbB.Location = new System.Drawing.Point(203, 18);
            this.TxbB.Name = "TxbB";
            this.TxbB.Size = new System.Drawing.Size(100, 20);
            this.TxbB.TabIndex = 3;
            this.TxbB.TextChanged += new System.EventHandler(this.TxbB_TextChanged);
            this.TxbB.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.TxbB_KeyPress);
            // 
            // BtnAdd
            // 
            this.BtnAdd.Location = new System.Drawing.Point(13, 58);
            this.BtnAdd.Name = "BtnAdd";
            this.BtnAdd.Size = new System.Drawing.Size(75, 23);
            this.BtnAdd.TabIndex = 4;
            this.BtnAdd.Text = "Addition";
            this.BtnAdd.UseVisualStyleBackColor = true;
            this.BtnAdd.Click += new System.EventHandler(this.BtnAdd_Click);
            // 
            // BtnDiv
            // 
            this.BtnDiv.Location = new System.Drawing.Point(94, 58);
            this.BtnDiv.Name = "BtnDiv";
            this.BtnDiv.Size = new System.Drawing.Size(75, 23);
            this.BtnDiv.TabIndex = 5;
            this.BtnDiv.Text = "Division";
            this.BtnDiv.UseVisualStyleBackColor = true;
            this.BtnDiv.Click += new System.EventHandler(this.BtnDiv_Click);
            // 
            // BtnDec
            // 
            this.BtnDec.Location = new System.Drawing.Point(175, 58);
            this.BtnDec.Name = "BtnDec";
            this.BtnDec.Size = new System.Drawing.Size(75, 23);
            this.BtnDec.TabIndex = 6;
            this.BtnDec.Text = "Dekrement";
            this.BtnDec.UseVisualStyleBackColor = true;
            this.BtnDec.Click += new System.EventHandler(this.BtnDec_Click);
            // 
            // BtnName
            // 
            this.BtnName.Location = new System.Drawing.Point(256, 58);
            this.BtnName.Name = "BtnName";
            this.BtnName.Size = new System.Drawing.Size(100, 23);
            this.BtnName.TabIndex = 7;
            this.BtnName.Text = "Namenseingabe";
            this.BtnName.UseVisualStyleBackColor = true;
            this.BtnName.Click += new System.EventHandler(this.BtnName_Click);
            // 
            // LblGleichung
            // 
            this.LblGleichung.AutoSize = true;
            this.LblGleichung.Location = new System.Drawing.Point(13, 88);
            this.LblGleichung.Name = "LblGleichung";
            this.LblGleichung.Size = new System.Drawing.Size(166, 13);
            this.LblGleichung.TabIndex = 8;
            this.LblGleichung.Text = "[vollständige Gleichung anzeigen]";
            // 
            // LblKlick
            // 
            this.LblKlick.AutoSize = true;
            this.LblKlick.Location = new System.Drawing.Point(13, 167);
            this.LblKlick.Name = "LblKlick";
            this.LblKlick.Size = new System.Drawing.Size(224, 13);
            this.LblKlick.TabIndex = 9;
            this.LblKlick.Text = "[Ausgabe der Anzahl von Schaltflächenklicks]";
            // 
            // BtnBeenden
            // 
            this.BtnBeenden.Location = new System.Drawing.Point(13, 184);
            this.BtnBeenden.Name = "BtnBeenden";
            this.BtnBeenden.Size = new System.Drawing.Size(347, 23);
            this.BtnBeenden.TabIndex = 10;
            this.BtnBeenden.Text = "Formular schließen";
            this.BtnBeenden.UseVisualStyleBackColor = true;
            this.BtnBeenden.Click += new System.EventHandler(this.BtnBeenden_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(372, 216);
            this.Controls.Add(this.BtnBeenden);
            this.Controls.Add(this.LblKlick);
            this.Controls.Add(this.LblGleichung);
            this.Controls.Add(this.BtnName);
            this.Controls.Add(this.BtnDec);
            this.Controls.Add(this.BtnDiv);
            this.Controls.Add(this.BtnAdd);
            this.Controls.Add(this.TxbB);
            this.Controls.Add(this.LblB);
            this.Controls.Add(this.LblA);
            this.Controls.Add(this.BtnA);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnA;
        private System.Windows.Forms.Label LblA;
        private System.Windows.Forms.Label LblB;
        private System.Windows.Forms.TextBox TxbB;
        private System.Windows.Forms.Button BtnAdd;
        private System.Windows.Forms.Button BtnDiv;
        private System.Windows.Forms.Button BtnDec;
        private System.Windows.Forms.Button BtnName;
        private System.Windows.Forms.Label LblGleichung;
        private System.Windows.Forms.Label LblKlick;
        private System.Windows.Forms.Button BtnBeenden;
    }
}

