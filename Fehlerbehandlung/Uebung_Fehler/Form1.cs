﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.VisualBasic;

namespace Uebung_Fehler
{
    public partial class Form1 : Form
    {
        Random r = new Random();
        int a = 0, klickzaehler = 0, last_function = 0, b = 0;
        // last_function:
        // 0: Ungesetzt
        // 1: Addition
        // 2: Division
        // 3: Dekrement
        // 4: Namenseingabe
        public Form1()
        {
            InitializeComponent();
            a = r.Next(1, 1000);
            LblA.Text = "a: " + a;
        }

        private void BtnA_Click(object sender, EventArgs e)
        {
            Klickzaehler();
            a = r.Next(1, 1000);
            LblA.Text = "a: " + a;
            Calculate();
        }

        private void Klickzaehler()
        {
            klickzaehler++;
            LblKlick.Text = "" + klickzaehler + " Klicks";
        }

        private void TxbB_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsDigit(e.KeyChar) && !char.IsControl(e.KeyChar))
                e.Handled = true;
        }

        private void Calculate()
        {
            if (TxbB.Text != "")
            {
                b = Convert.ToInt32(TxbB.Text);
            }
            else b=0;
            try
            {
                if (last_function == 1)
                {
                    int ergebnis = b + a;
                    LblGleichung.Text = "Ergebnis der Addition: " + ergebnis;
                }
                else if (last_function == 2)
                {
                    int ganzzahl = a / b;
                    double gebrochen = (double)a / b;
                    int rest = a % b;
                    LblGleichung.Text = "Ganzzahl Ergebnis: " + ganzzahl + "; Rest: " + rest +
                        "\nGebrochen Ergebnis: " + gebrochen;
                }
                else if (last_function == 3)
                {
                    int dec_a = --a;
                    int dec_b = --b;
                    LblGleichung.Text = "Dekrement a: " + dec_a +
                        "\nDekrement b: " + dec_b;
                }
            }
            catch (DivideByZeroException ex)
            {
                LblGleichung.Text = "Fehler: Division durch null ist nicht erlaubt";
            }
            catch (FormatException ex)
            {
                LblGleichung.Text = "Fehler: Falsches Eingabeformat";
            }
            catch (Exception ex)
            {
                LblGleichung.Text = "Fehler: " + ex;
                System.Diagnostics.Process.Start("https://stackoverflow.com/search?q=C%23+" + ex.GetType());
            }
        }

        private void BtnAdd_Click(object sender, EventArgs e)
        {
            Klickzaehler();
            last_function = 1;
            Calculate();
        }

        private void TxbB_TextChanged(object sender, EventArgs e)
        {
            Calculate();
        }

        private void BtnDiv_Click(object sender, EventArgs e)
        {
            Klickzaehler();
            last_function = 2;
            Calculate();
        }

        private void BtnBeenden_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void BtnDec_Click(object sender, EventArgs e)
        {
            Klickzaehler();
            last_function = 3;
            Calculate();
        }

        private void BtnName_Click(object sender, EventArgs e)
        {
            Klickzaehler();
            last_function = 4;
            string vorname = Interaction.InputBox("Bitte Ihren Vornamen:", "Ihr Vorname", "Klaus");
            string name = Interaction.InputBox("Bitte Ihren Namen:", "Ihr Name", "Maier");
            LblGleichung.Text = "Name: " + vorname + " " + name;
        }
    }
}
