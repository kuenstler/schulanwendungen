﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FahrzeugKlausur
{
    class TAlternativfahrzeug:TFahrzeug
    {
        //----------Attribut----------------
        private int reichweite;
        //----------Methoden----------------
        //ctor
        public TAlternativfahrzeug(string b, double l, int r) : base(b, l)
        {
            reichweite = r;
        }
        //Accessor-Methode
        public int Reichweite
        {
            get
            {
                return reichweite;
            }
        }
        //Ausgabemethode
        public override string ausgeben()
        {
            string ausgabe = base.ausgeben() + " " + Reichweite + " km";
            return ausgabe;
        }
    }
}
