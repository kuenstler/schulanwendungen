﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FahrzeugKlausur
{
    class THybridfahrzeug:TFahrzeug
    {
        //----------Attribut----------------
        private double elektromotorleistung;
        //----------Methoden----------------
        //ctor
        public THybridfahrzeug(string b, double l, double el) : base(b, l)
        {
            elektromotorleistung = el;
        }
        //Accessor-Methode für Leistung
        public new double Motorleistung
        {
            get
            {
                return base.Motorleistung + elektromotorleistung;
            }
        }
    }
}
