﻿namespace FahrzeugKlausur
{
    partial class frmTest
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.grpArt = new System.Windows.Forms.GroupBox();
            this.optWasserstoff = new System.Windows.Forms.RadioButton();
            this.optHybrid = new System.Windows.Forms.RadioButton();
            this.optElektro = new System.Windows.Forms.RadioButton();
            this.optVerbrennung = new System.Windows.Forms.RadioButton();
            this.lblHantrieb = new System.Windows.Forms.Label();
            this.lblZAntrieb = new System.Windows.Forms.Label();
            this.lblReichweite = new System.Windows.Forms.Label();
            this.txtHAntrieb = new System.Windows.Forms.TextBox();
            this.txtZAntrieb = new System.Windows.Forms.TextBox();
            this.txtReichweite = new System.Windows.Forms.TextBox();
            this.cmdAnlegen = new System.Windows.Forms.Button();
            this.cmdAnzeigen = new System.Windows.Forms.Button();
            this.lstAnzeigen = new System.Windows.Forms.ListBox();
            this.cmdAuswerten = new System.Windows.Forms.Button();
            this.cmdLoeschen = new System.Windows.Forms.Button();
            this.grpArt.SuspendLayout();
            this.SuspendLayout();
            // 
            // grpArt
            // 
            this.grpArt.Controls.Add(this.optWasserstoff);
            this.grpArt.Controls.Add(this.optHybrid);
            this.grpArt.Controls.Add(this.optElektro);
            this.grpArt.Controls.Add(this.optVerbrennung);
            this.grpArt.Location = new System.Drawing.Point(34, 23);
            this.grpArt.Margin = new System.Windows.Forms.Padding(4);
            this.grpArt.Name = "grpArt";
            this.grpArt.Padding = new System.Windows.Forms.Padding(4);
            this.grpArt.Size = new System.Drawing.Size(307, 145);
            this.grpArt.TabIndex = 0;
            this.grpArt.TabStop = false;
            this.grpArt.Text = "Antriebsart";
            // 
            // optWasserstoff
            // 
            this.optWasserstoff.AutoSize = true;
            this.optWasserstoff.Location = new System.Drawing.Point(7, 108);
            this.optWasserstoff.Name = "optWasserstoff";
            this.optWasserstoff.Size = new System.Drawing.Size(151, 22);
            this.optWasserstoff.TabIndex = 3;
            this.optWasserstoff.Text = "Wasserstoffantrieb";
            this.optWasserstoff.UseVisualStyleBackColor = true;
            this.optWasserstoff.CheckedChanged += new System.EventHandler(this.optWasserstoff_CheckedChanged);
            // 
            // optHybrid
            // 
            this.optHybrid.AutoSize = true;
            this.optHybrid.Location = new System.Drawing.Point(7, 80);
            this.optHybrid.Name = "optHybrid";
            this.optHybrid.Size = new System.Drawing.Size(112, 22);
            this.optHybrid.TabIndex = 2;
            this.optHybrid.Text = "Hybridantrieb";
            this.optHybrid.UseVisualStyleBackColor = true;
            this.optHybrid.CheckedChanged += new System.EventHandler(this.optHybrid_CheckedChanged);
            // 
            // optElektro
            // 
            this.optElektro.AutoSize = true;
            this.optElektro.Location = new System.Drawing.Point(7, 52);
            this.optElektro.Name = "optElektro";
            this.optElektro.Size = new System.Drawing.Size(113, 22);
            this.optElektro.TabIndex = 1;
            this.optElektro.Text = "Elektromotor";
            this.optElektro.UseVisualStyleBackColor = true;
            this.optElektro.CheckedChanged += new System.EventHandler(this.optElektro_CheckedChanged);
            // 
            // optVerbrennung
            // 
            this.optVerbrennung.AutoSize = true;
            this.optVerbrennung.Checked = true;
            this.optVerbrennung.Location = new System.Drawing.Point(7, 24);
            this.optVerbrennung.Name = "optVerbrennung";
            this.optVerbrennung.Size = new System.Drawing.Size(157, 22);
            this.optVerbrennung.TabIndex = 0;
            this.optVerbrennung.TabStop = true;
            this.optVerbrennung.Text = "Verbrennungsmotor";
            this.optVerbrennung.UseVisualStyleBackColor = true;
            this.optVerbrennung.CheckedChanged += new System.EventHandler(this.optVerbrennung_CheckedChanged);
            // 
            // lblHantrieb
            // 
            this.lblHantrieb.AutoSize = true;
            this.lblHantrieb.Location = new System.Drawing.Point(31, 186);
            this.lblHantrieb.Name = "lblHantrieb";
            this.lblHantrieb.Size = new System.Drawing.Size(225, 18);
            this.lblHantrieb.TabIndex = 4;
            this.lblHantrieb.Text = "Leistung des Hauptantriebs [kW]:";
            // 
            // lblZAntrieb
            // 
            this.lblZAntrieb.AutoSize = true;
            this.lblZAntrieb.Location = new System.Drawing.Point(31, 221);
            this.lblZAntrieb.Name = "lblZAntrieb";
            this.lblZAntrieb.Size = new System.Drawing.Size(231, 18);
            this.lblZAntrieb.TabIndex = 5;
            this.lblZAntrieb.Text = "Leistung des Zusatzantriebs [kW]:";
            // 
            // lblReichweite
            // 
            this.lblReichweite.AutoSize = true;
            this.lblReichweite.Location = new System.Drawing.Point(31, 256);
            this.lblReichweite.Name = "lblReichweite";
            this.lblReichweite.Size = new System.Drawing.Size(117, 18);
            this.lblReichweite.TabIndex = 6;
            this.lblReichweite.Text = "Reichweite [km]:";
            // 
            // txtHAntrieb
            // 
            this.txtHAntrieb.Location = new System.Drawing.Point(262, 183);
            this.txtHAntrieb.Name = "txtHAntrieb";
            this.txtHAntrieb.Size = new System.Drawing.Size(79, 24);
            this.txtHAntrieb.TabIndex = 4;
            // 
            // txtZAntrieb
            // 
            this.txtZAntrieb.Location = new System.Drawing.Point(262, 218);
            this.txtZAntrieb.Name = "txtZAntrieb";
            this.txtZAntrieb.Size = new System.Drawing.Size(79, 24);
            this.txtZAntrieb.TabIndex = 5;
            // 
            // txtReichweite
            // 
            this.txtReichweite.Location = new System.Drawing.Point(262, 253);
            this.txtReichweite.Name = "txtReichweite";
            this.txtReichweite.Size = new System.Drawing.Size(79, 24);
            this.txtReichweite.TabIndex = 6;
            // 
            // cmdAnlegen
            // 
            this.cmdAnlegen.Location = new System.Drawing.Point(34, 296);
            this.cmdAnlegen.Name = "cmdAnlegen";
            this.cmdAnlegen.Size = new System.Drawing.Size(307, 35);
            this.cmdAnlegen.TabIndex = 7;
            this.cmdAnlegen.Text = "Fahrzeug erstellen";
            this.cmdAnlegen.UseVisualStyleBackColor = true;
            this.cmdAnlegen.Click += new System.EventHandler(this.cmdAnlegen_Click);
            // 
            // cmdAnzeigen
            // 
            this.cmdAnzeigen.Location = new System.Drawing.Point(369, 183);
            this.cmdAnzeigen.Name = "cmdAnzeigen";
            this.cmdAnzeigen.Size = new System.Drawing.Size(307, 35);
            this.cmdAnzeigen.TabIndex = 8;
            this.cmdAnzeigen.Text = "Fahrzeugfeld anzeigen";
            this.cmdAnzeigen.UseVisualStyleBackColor = true;
            this.cmdAnzeigen.Click += new System.EventHandler(this.cmdAnzeigen_Click);
            // 
            // lstAnzeigen
            // 
            this.lstAnzeigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstAnzeigen.FormattingEnabled = true;
            this.lstAnzeigen.HorizontalScrollbar = true;
            this.lstAnzeigen.ItemHeight = 16;
            this.lstAnzeigen.Location = new System.Drawing.Point(369, 20);
            this.lstAnzeigen.Name = "lstAnzeigen";
            this.lstAnzeigen.Size = new System.Drawing.Size(307, 148);
            this.lstAnzeigen.TabIndex = 11;
            // 
            // cmdAuswerten
            // 
            this.cmdAuswerten.Location = new System.Drawing.Point(369, 239);
            this.cmdAuswerten.Name = "cmdAuswerten";
            this.cmdAuswerten.Size = new System.Drawing.Size(307, 35);
            this.cmdAuswerten.TabIndex = 9;
            this.cmdAuswerten.Text = "Fahrzeugfeld auswerten";
            this.cmdAuswerten.UseVisualStyleBackColor = true;
            this.cmdAuswerten.Click += new System.EventHandler(this.cmdAuswerten_Click);
            // 
            // cmdLoeschen
            // 
            this.cmdLoeschen.Location = new System.Drawing.Point(369, 296);
            this.cmdLoeschen.Name = "cmdLoeschen";
            this.cmdLoeschen.Size = new System.Drawing.Size(307, 35);
            this.cmdLoeschen.TabIndex = 10;
            this.cmdLoeschen.Text = "Fahrzeugfeld löschen";
            this.cmdLoeschen.UseVisualStyleBackColor = true;
            this.cmdLoeschen.Click += new System.EventHandler(this.cmdLoeschen_Click);
            // 
            // frmTest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 18F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(712, 352);
            this.Controls.Add(this.cmdLoeschen);
            this.Controls.Add(this.cmdAuswerten);
            this.Controls.Add(this.lstAnzeigen);
            this.Controls.Add(this.cmdAnzeigen);
            this.Controls.Add(this.cmdAnlegen);
            this.Controls.Add(this.txtReichweite);
            this.Controls.Add(this.txtZAntrieb);
            this.Controls.Add(this.txtHAntrieb);
            this.Controls.Add(this.lblReichweite);
            this.Controls.Add(this.lblZAntrieb);
            this.Controls.Add(this.lblHantrieb);
            this.Controls.Add(this.grpArt);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmTest";
            this.Text = "Fahrzeuge - Testformular";
            this.grpArt.ResumeLayout(false);
            this.grpArt.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox grpArt;
        private System.Windows.Forms.RadioButton optWasserstoff;
        private System.Windows.Forms.RadioButton optHybrid;
        private System.Windows.Forms.RadioButton optElektro;
        private System.Windows.Forms.RadioButton optVerbrennung;
        private System.Windows.Forms.Label lblHantrieb;
        private System.Windows.Forms.Label lblZAntrieb;
        private System.Windows.Forms.Label lblReichweite;
        private System.Windows.Forms.TextBox txtHAntrieb;
        private System.Windows.Forms.TextBox txtZAntrieb;
        private System.Windows.Forms.TextBox txtReichweite;
        private System.Windows.Forms.Button cmdAnlegen;
        private System.Windows.Forms.Button cmdAnzeigen;
        private System.Windows.Forms.ListBox lstAnzeigen;
        private System.Windows.Forms.Button cmdAuswerten;
        private System.Windows.Forms.Button cmdLoeschen;
    }
}

