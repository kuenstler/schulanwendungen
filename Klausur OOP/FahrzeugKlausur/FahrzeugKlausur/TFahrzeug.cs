﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FahrzeugKlausur
{
    class TFahrzeug
    {        
        //----------Attribute---------------
        private string motorbezeichnung;
        private double motorleistung;
        //----------Methoden----------------
        //ctor
        public TFahrzeug(string b, double l)
        {
            motorbezeichnung = b;
            motorleistung = l;
        }
        //Accessor-Methoden
        public string Motorbezeichnung
        {
            get
            {
                return motorbezeichnung;
            }
        }
        public double Motorleistung
        {
            get
            {
                return motorleistung;
            }
        }
        //Ausgabemethode
        public virtual string ausgeben()
        {
            string ausgabe = GetType().ToString().Split('.')[1];
            ausgabe += " " + Motorbezeichnung;
            ausgabe += " " + Motorleistung.ToString("00.0") + " kW";
            return ausgabe;
        }
    }
}
