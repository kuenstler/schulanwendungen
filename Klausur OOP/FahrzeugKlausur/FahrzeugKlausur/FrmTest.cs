﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FahrzeugKlausur
{
    public partial class frmTest : Form
    {
        //--------------Formularattribut(e)------
        //Fahrzeugfeld
        TFahrzeug[] fahrzeuge = new TFahrzeug[0];
        //--------------Formularmethoden---------
        //ctor
        public frmTest()
        {
            InitializeComponent();
            //Start mit Verbrennungsmotor
            optVerbrennung.Checked = true;
            lblZAntrieb.Visible = false;
            txtZAntrieb.Visible = false;
            lblReichweite.Visible = false;
            txtReichweite.Visible = false;
            //Listbox leeren
            lstAnzeigen.Items.Clear();
        }
        //Eingabesteuerung für Verbrennungsmotor
        private void optVerbrennung_CheckedChanged(object sender, EventArgs e)
        {
            lblZAntrieb.Visible = false;
            txtZAntrieb.Visible = false;
            lblReichweite.Visible = false;
            txtReichweite.Visible = false;
        }
        //Eingabesteuerung für Elektromotor
        private void optElektro_CheckedChanged(object sender, EventArgs e)
        {
            lblZAntrieb.Visible = false;
            txtZAntrieb.Visible = false;
            lblReichweite.Visible = true;
            txtReichweite.Visible = true;
        }
        //Eingabesteuerung für Hybridfahrzeug
        private void optHybrid_CheckedChanged(object sender, EventArgs e)
        {
            lblZAntrieb.Visible = true;
            txtZAntrieb.Visible = true;
            lblReichweite.Visible = false;
            txtReichweite.Visible = false;
        }
        //Eingabesteuerung für Wasserstoffantrieb
        private void optWasserstoff_CheckedChanged(object sender, EventArgs e)
        {
            lblZAntrieb.Visible = false;
            txtZAntrieb.Visible = false;
            lblReichweite.Visible = true;
            txtReichweite.Visible = true;
        }
        //Schaltflächenmethoden
        //Ein neues Fahrzeug anlegen
        private void cmdAnlegen_Click(object sender, EventArgs e)
        {
            TFahrzeug f1;
            if (optElektro.Checked && txtHAntrieb.Text != "" && txtReichweite.Text != "")
            {
                //Formulatwerte
                double leistung = Convert.ToDouble(txtHAntrieb.Text);
                int reichweite = Convert.ToInt32(txtReichweite.Text);
                //Neues Fahrzeugobjekt erstellen
                f1 = new TAlternativfahrzeug("Elektromotor", leistung, reichweite);
            }
            else if (optWasserstoff.Checked && txtHAntrieb.Text != "" && txtReichweite.Text != "")
            {
                //Formulatwerte
                double leistung = Convert.ToDouble(txtHAntrieb.Text);
                int reichweite = Convert.ToInt32(txtReichweite.Text);
                //Neues Fahrzeugobjekt erstellen
                f1 = new TAlternativfahrzeug("Wasserstoffantrieb", leistung, reichweite);
            }
            else if (optVerbrennung.Checked && txtHAntrieb.Text != "")
            {
                //Formulatwerte
                double leistung = Convert.ToDouble(txtHAntrieb.Text);
                //Neues Fahrzeugobjekt erstellen
                f1 = new TFahrzeug("Verbrennungsmotor", leistung);
            }
            else if(optHybrid.Checked && txtHAntrieb.Text != "" && txtZAntrieb.Text != "")
            {
                //Formulatwerte
                double leistung = Convert.ToDouble(txtHAntrieb.Text);
                double leistung_el = Convert.ToDouble(txtZAntrieb.Text);
                //Neues Fahrzeugobjekt erstellen
                f1 = new THybridfahrzeug("Hybridfahrzeug", leistung, leistung_el);
            }
            //Wenn ein benötigtes Feld leer ist, Nutzer warnen und Methode beenden
            else
            {
                MessageBox.Show("Bitte bei allen Feldern etwas angeben");
                return;
            }
            //Fahrzeug dem Feld nur hinzufügen, wenn das Limit noch nicht erreicht wurde
            if (fahrzeuge.Length < 10)
            {
                //Objekt dem Feld hinzufügen
                Array.Resize(ref fahrzeuge, fahrzeuge.Length + 1);
                fahrzeuge[fahrzeuge.Length - 1] = f1;
                //Objekt in Liste anzeigen
                lstAnzeigen.Items.Clear();
                lstAnzeigen.Items.Add(f1.ausgeben());
            }
            else
            {
                //DEn Nutzer warnen, wenn das Fahrzeugfeld voll ist
                MessageBox.Show("Das Fahrzeugfeld ist voll");
            }
        }
        //Fahrzeugobjekte ausgeben
        private void cmdAnzeigen_Click(object sender, EventArgs e)
        {
            //Liste löschen
            lstAnzeigen.Items.Clear();
            foreach (TFahrzeug i in fahrzeuge)
            {
                //Eintrag hinzufügen
                lstAnzeigen.Items.Add(i.ausgeben());
            }
        }
        //Fahrzeugliste löschen
        private void cmdLoeschen_Click(object sender, EventArgs e)
        {
            //Feld löschen
            fahrzeuge = new TFahrzeug[0];
            //Liste löschen
            lstAnzeigen.Items.Clear();
        }
        //Fahrzeugobjekte auswerten
        private void cmdAuswerten_Click(object sender, EventArgs e)
        {
            //Zählvariablen initialisieren
            int standardfahrzeugobjekte = 0;
            int alternativfahrzeugobjekte = 0;
            int hybridfahrzeugobjekte = 0;
            //Objekte zählen
            foreach (TFahrzeug i in fahrzeuge)
            {
                if(i is THybridfahrzeug)
                {
                    hybridfahrzeugobjekte++;
                }
                else if (i is TAlternativfahrzeug)
                {
                    alternativfahrzeugobjekte++;
                }
                else
                {
                    standardfahrzeugobjekte++;
                }
            }
            //Liste löschen
            lstAnzeigen.Items.Clear();
            //Ausgabe
            lstAnzeigen.Items.Add("Autoliste enthält: " + fahrzeuge.Length + " Fahrzeugobjekt(e)");
            lstAnzeigen.Items.Add("davon");
            lstAnzeigen.Items.Add("" + standardfahrzeugobjekte + " Standardfahrzeugobjekt(e)");
            lstAnzeigen.Items.Add("" + alternativfahrzeugobjekte + " Alternativfahrzeugobjekt(e)");
            lstAnzeigen.Items.Add("" + hybridfahrzeugobjekte + " Hybridfahrzeugobjekt(e)");
        }
    }
}
