﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using DLLErstellen;

namespace DLLNutzen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Fahrzeug a = new Fahrzeug();
            label1.Text = a.ausgeben().ausgeben();
            a.beschleunigen(20);
            label1.Text += a.ausgeben().ausgeben();
            Fahrzeug b = new Fahrzeug("Schwalbe", 40);
            label1.Text += b.ausgeben().ausgeben();
        }
    }
}
