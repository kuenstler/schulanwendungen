﻿namespace Sportkurse
{
    partial class Sportkurse
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrpAddKurs = new System.Windows.Forms.GroupBox();
            this.BtnAddKursAdd = new System.Windows.Forms.Button();
            this.NumAddKursMax = new System.Windows.Forms.NumericUpDown();
            this.LblAddKursMax = new System.Windows.Forms.Label();
            this.NumAddKursMin = new System.Windows.Forms.NumericUpDown();
            this.LblAddKursMin = new System.Windows.Forms.Label();
            this.LblAddKursName = new System.Windows.Forms.Label();
            this.TxbAddKursName = new System.Windows.Forms.TextBox();
            this.GrpSchueler = new System.Windows.Forms.GroupBox();
            this.BtnSchuelerAdd = new System.Windows.Forms.Button();
            this.TxbVorname = new System.Windows.Forms.TextBox();
            this.TxbNachname = new System.Windows.Forms.TextBox();
            this.BtnCalc = new System.Windows.Forms.Button();
            this.BtnKursLoad = new System.Windows.Forms.Button();
            this.BtnKursWrite = new System.Windows.Forms.Button();
            this.BtnSchuelerAnz = new System.Windows.Forms.Button();
            this.LstBoxAusgabe = new System.Windows.Forms.ListBox();
            this.BtnZuordnungSpeichern = new System.Windows.Forms.Button();
            this.LblSchuelerVorname = new System.Windows.Forms.Label();
            this.LblSchuelerNachname = new System.Windows.Forms.Label();
            this.GrpAddKurs.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumAddKursMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAddKursMin)).BeginInit();
            this.GrpSchueler.SuspendLayout();
            this.SuspendLayout();
            // 
            // GrpAddKurs
            // 
            this.GrpAddKurs.Controls.Add(this.BtnAddKursAdd);
            this.GrpAddKurs.Controls.Add(this.NumAddKursMax);
            this.GrpAddKurs.Controls.Add(this.LblAddKursMax);
            this.GrpAddKurs.Controls.Add(this.NumAddKursMin);
            this.GrpAddKurs.Controls.Add(this.LblAddKursMin);
            this.GrpAddKurs.Controls.Add(this.LblAddKursName);
            this.GrpAddKurs.Controls.Add(this.TxbAddKursName);
            this.GrpAddKurs.Location = new System.Drawing.Point(13, 13);
            this.GrpAddKurs.Name = "GrpAddKurs";
            this.GrpAddKurs.Size = new System.Drawing.Size(201, 143);
            this.GrpAddKurs.TabIndex = 0;
            this.GrpAddKurs.TabStop = false;
            this.GrpAddKurs.Text = "Sportkurs hinzufügen";
            // 
            // BtnAddKursAdd
            // 
            this.BtnAddKursAdd.Location = new System.Drawing.Point(6, 107);
            this.BtnAddKursAdd.Name = "BtnAddKursAdd";
            this.BtnAddKursAdd.Size = new System.Drawing.Size(185, 23);
            this.BtnAddKursAdd.TabIndex = 7;
            this.BtnAddKursAdd.Text = "Sportkurs hinzufügen";
            this.BtnAddKursAdd.UseVisualStyleBackColor = true;
            this.BtnAddKursAdd.Click += new System.EventHandler(this.BtnAddKursAdd_Click);
            // 
            // NumAddKursMax
            // 
            this.NumAddKursMax.Location = new System.Drawing.Point(132, 81);
            this.NumAddKursMax.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumAddKursMax.Name = "NumAddKursMax";
            this.NumAddKursMax.Size = new System.Drawing.Size(59, 20);
            this.NumAddKursMax.TabIndex = 6;
            this.NumAddKursMax.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // LblAddKursMax
            // 
            this.LblAddKursMax.AutoSize = true;
            this.LblAddKursMax.Location = new System.Drawing.Point(6, 83);
            this.LblAddKursMax.Name = "LblAddKursMax";
            this.LblAddKursMax.Size = new System.Drawing.Size(121, 13);
            this.LblAddKursMax.TabIndex = 5;
            this.LblAddKursMax.Text = "maximal Teilnehmerzahl:";
            // 
            // NumAddKursMin
            // 
            this.NumAddKursMin.Location = new System.Drawing.Point(132, 52);
            this.NumAddKursMin.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumAddKursMin.Name = "NumAddKursMin";
            this.NumAddKursMin.Size = new System.Drawing.Size(59, 20);
            this.NumAddKursMin.TabIndex = 4;
            this.NumAddKursMin.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // LblAddKursMin
            // 
            this.LblAddKursMin.AutoSize = true;
            this.LblAddKursMin.Location = new System.Drawing.Point(6, 54);
            this.LblAddKursMin.Name = "LblAddKursMin";
            this.LblAddKursMin.Size = new System.Drawing.Size(118, 13);
            this.LblAddKursMin.TabIndex = 3;
            this.LblAddKursMin.Text = "minimal Teilnehmerzahl:";
            // 
            // LblAddKursName
            // 
            this.LblAddKursName.AutoSize = true;
            this.LblAddKursName.Location = new System.Drawing.Point(7, 26);
            this.LblAddKursName.Name = "LblAddKursName";
            this.LblAddKursName.Size = new System.Drawing.Size(57, 13);
            this.LblAddKursName.TabIndex = 1;
            this.LblAddKursName.Text = "Kursname:";
            // 
            // TxbAddKursName
            // 
            this.TxbAddKursName.Location = new System.Drawing.Point(91, 23);
            this.TxbAddKursName.Name = "TxbAddKursName";
            this.TxbAddKursName.Size = new System.Drawing.Size(100, 20);
            this.TxbAddKursName.TabIndex = 0;
            // 
            // GrpSchueler
            // 
            this.GrpSchueler.Controls.Add(this.LblSchuelerNachname);
            this.GrpSchueler.Controls.Add(this.LblSchuelerVorname);
            this.GrpSchueler.Controls.Add(this.BtnSchuelerAdd);
            this.GrpSchueler.Controls.Add(this.TxbVorname);
            this.GrpSchueler.Controls.Add(this.TxbNachname);
            this.GrpSchueler.Location = new System.Drawing.Point(13, 163);
            this.GrpSchueler.Name = "GrpSchueler";
            this.GrpSchueler.Size = new System.Drawing.Size(200, 100);
            this.GrpSchueler.TabIndex = 1;
            this.GrpSchueler.TabStop = false;
            this.GrpSchueler.Text = "Schüler hinzufügen";
            // 
            // BtnSchuelerAdd
            // 
            this.BtnSchuelerAdd.Location = new System.Drawing.Point(7, 70);
            this.BtnSchuelerAdd.Name = "BtnSchuelerAdd";
            this.BtnSchuelerAdd.Size = new System.Drawing.Size(184, 23);
            this.BtnSchuelerAdd.TabIndex = 2;
            this.BtnSchuelerAdd.Text = "Schüler hinzufügen";
            this.BtnSchuelerAdd.UseVisualStyleBackColor = true;
            this.BtnSchuelerAdd.Click += new System.EventHandler(this.BtnSchuelerAdd_Click);
            // 
            // TxbVorname
            // 
            this.TxbVorname.Location = new System.Drawing.Point(91, 19);
            this.TxbVorname.Name = "TxbVorname";
            this.TxbVorname.Size = new System.Drawing.Size(100, 20);
            this.TxbVorname.TabIndex = 0;
            // 
            // TxbNachname
            // 
            this.TxbNachname.Location = new System.Drawing.Point(91, 45);
            this.TxbNachname.Name = "TxbNachname";
            this.TxbNachname.Size = new System.Drawing.Size(100, 20);
            this.TxbNachname.TabIndex = 1;
            // 
            // BtnCalc
            // 
            this.BtnCalc.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnCalc.Location = new System.Drawing.Point(221, 13);
            this.BtnCalc.Name = "BtnCalc";
            this.BtnCalc.Size = new System.Drawing.Size(128, 30);
            this.BtnCalc.TabIndex = 2;
            this.BtnCalc.Text = "Kurse berechnen";
            this.BtnCalc.UseVisualStyleBackColor = true;
            this.BtnCalc.Click += new System.EventHandler(this.BtnCalc_Click);
            // 
            // BtnKursLoad
            // 
            this.BtnKursLoad.Location = new System.Drawing.Point(356, 13);
            this.BtnKursLoad.Name = "BtnKursLoad";
            this.BtnKursLoad.Size = new System.Drawing.Size(135, 23);
            this.BtnKursLoad.TabIndex = 4;
            this.BtnKursLoad.Text = "Kurse aus Datei laden";
            this.BtnKursLoad.UseVisualStyleBackColor = true;
            this.BtnKursLoad.Click += new System.EventHandler(this.BtnKursLoad_Click);
            // 
            // BtnKursWrite
            // 
            this.BtnKursWrite.Location = new System.Drawing.Point(356, 42);
            this.BtnKursWrite.Name = "BtnKursWrite";
            this.BtnKursWrite.Size = new System.Drawing.Size(135, 23);
            this.BtnKursWrite.TabIndex = 5;
            this.BtnKursWrite.Text = "Kurse in Datei schreiben";
            this.BtnKursWrite.UseVisualStyleBackColor = true;
            this.BtnKursWrite.Click += new System.EventHandler(this.BtnKursWrite_Click);
            // 
            // BtnSchuelerAnz
            // 
            this.BtnSchuelerAnz.Location = new System.Drawing.Point(497, 71);
            this.BtnSchuelerAnz.Name = "BtnSchuelerAnz";
            this.BtnSchuelerAnz.Size = new System.Drawing.Size(143, 23);
            this.BtnSchuelerAnz.TabIndex = 9;
            this.BtnSchuelerAnz.Text = "Schüler ausgeben";
            this.BtnSchuelerAnz.UseVisualStyleBackColor = true;
            this.BtnSchuelerAnz.Click += new System.EventHandler(this.BtnSchuelerAnz_Click);
            // 
            // LstBoxAusgabe
            // 
            this.LstBoxAusgabe.FormattingEnabled = true;
            this.LstBoxAusgabe.Location = new System.Drawing.Point(221, 101);
            this.LstBoxAusgabe.Name = "LstBoxAusgabe";
            this.LstBoxAusgabe.Size = new System.Drawing.Size(419, 329);
            this.LstBoxAusgabe.TabIndex = 10;
            // 
            // BtnZuordnungSpeichern
            // 
            this.BtnZuordnungSpeichern.Location = new System.Drawing.Point(221, 50);
            this.BtnZuordnungSpeichern.Name = "BtnZuordnungSpeichern";
            this.BtnZuordnungSpeichern.Size = new System.Drawing.Size(129, 23);
            this.BtnZuordnungSpeichern.TabIndex = 3;
            this.BtnZuordnungSpeichern.Text = "Zuordnung speichern";
            this.BtnZuordnungSpeichern.UseVisualStyleBackColor = true;
            this.BtnZuordnungSpeichern.Click += new System.EventHandler(this.BtnZuordnungSpeichern_Click);
            // 
            // LblSchuelerVorname
            // 
            this.LblSchuelerVorname.AutoSize = true;
            this.LblSchuelerVorname.Location = new System.Drawing.Point(7, 20);
            this.LblSchuelerVorname.Name = "LblSchuelerVorname";
            this.LblSchuelerVorname.Size = new System.Drawing.Size(49, 13);
            this.LblSchuelerVorname.TabIndex = 3;
            this.LblSchuelerVorname.Text = "Vorname";
            // 
            // LblSchuelerNachname
            // 
            this.LblSchuelerNachname.AutoSize = true;
            this.LblSchuelerNachname.Location = new System.Drawing.Point(7, 45);
            this.LblSchuelerNachname.Name = "LblSchuelerNachname";
            this.LblSchuelerNachname.Size = new System.Drawing.Size(59, 13);
            this.LblSchuelerNachname.TabIndex = 4;
            this.LblSchuelerNachname.Text = "Nachname";
            // 
            // Sportkurse
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(651, 450);
            this.Controls.Add(this.BtnZuordnungSpeichern);
            this.Controls.Add(this.LstBoxAusgabe);
            this.Controls.Add(this.BtnSchuelerAnz);
            this.Controls.Add(this.BtnKursWrite);
            this.Controls.Add(this.BtnKursLoad);
            this.Controls.Add(this.BtnCalc);
            this.Controls.Add(this.GrpSchueler);
            this.Controls.Add(this.GrpAddKurs);
            this.Name = "Sportkurse";
            this.Text = "Sportkursanwendung";
            this.GrpAddKurs.ResumeLayout(false);
            this.GrpAddKurs.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumAddKursMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAddKursMin)).EndInit();
            this.GrpSchueler.ResumeLayout(false);
            this.GrpSchueler.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpAddKurs;
        private System.Windows.Forms.NumericUpDown NumAddKursMin;
        private System.Windows.Forms.Label LblAddKursMin;
        private System.Windows.Forms.Label LblAddKursName;
        private System.Windows.Forms.TextBox TxbAddKursName;
        private System.Windows.Forms.Button BtnAddKursAdd;
        private System.Windows.Forms.NumericUpDown NumAddKursMax;
        private System.Windows.Forms.Label LblAddKursMax;
        private System.Windows.Forms.GroupBox GrpSchueler;
        private System.Windows.Forms.Button BtnSchuelerAdd;
        //private System.Windows.Forms.Button BtnSchuelerAktualisieren;
        private System.Windows.Forms.TextBox TxbVorname;
        private System.Windows.Forms.TextBox TxbNachname;
        private System.Windows.Forms.Button BtnCalc;
        private System.Windows.Forms.Button BtnKursLoad;
        private System.Windows.Forms.Button BtnKursWrite;
        private System.Windows.Forms.Button BtnSchuelerAnz;
        private System.Windows.Forms.ListBox LstBoxAusgabe;
        private System.Windows.Forms.Button BtnZuordnungSpeichern;
        private System.Windows.Forms.Label LblSchuelerNachname;
        private System.Windows.Forms.Label LblSchuelerVorname;
    }
}

