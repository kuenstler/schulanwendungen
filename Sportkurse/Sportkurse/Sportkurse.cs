﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Sportkurse
{
    public partial class Sportkurse : Form
    {
        // Formularvariablen
        Dictionary<string, Sportkurs> sportkurse = new Dictionary<string, Sportkurs>();
        Dictionary<string, Doppelkurs> doppelkurse = new Dictionary<string, Doppelkurs>();
        List<Schueler> schueler = new List<Schueler>();
        int originale_sportkurs_hoehe;
        Point originale_schueler_add_position;
        Dictionary<string, Doppelkurszuordnung> doppelkurszuordnungen;

        public Sportkurse()
        {
            InitializeComponent();
            originale_sportkurs_hoehe = GrpSchueler.Height;
            originale_schueler_add_position = BtnSchuelerAdd.Location;
        }

        private void BtnAddKursAdd_Click(object sender, EventArgs e)
        {
            string name = TxbAddKursName.Text;
            if (name == "")
            {
                MessageBox.Show("Bitte einen Namen für den Sportkurs angeben.");
                return;
            }
            int min = (int)NumAddKursMin.Value;
            int max = (int)NumAddKursMax.Value;
            if (min > max)
            {
                MessageBox.Show("Die Minimalanzahl der Teilnehmer muss kleiner als die Maximalteilnehmerzahl sein.");
                return;
            }
            if (schueler.Count > 0)
            {
                DialogResult msg = MessageBox.Show("Willst du wirklich einen neuen Sportkurs hinzufügen und alle Schüler löschen?", "Schüler löschen", MessageBoxButtons.YesNo);
                if (msg == DialogResult.Yes)
                {
                    schueler.Clear();
                }
                else
                {
                    return;
                }
            }
            Sportkurs sportkurs = new Sportkurs(name, max, min);
            sportkurse[name] = sportkurs;
            BtnSchuelerAktualisieren_Click();
            TxbAddKursName.Select();
        }

        private void BtnSchuelerAktualisieren_Click()
        {
            for (int i = GrpSchueler.Controls.Count - 1; i >= 0; --i)
            {
                if (GrpSchueler.Controls[i] is ExtendedLabel || GrpSchueler.Controls[i] is ExtendedNumUpDown)
                {
                    GrpSchueler.Controls.Remove(GrpSchueler.Controls[i]);
                }
            }
            int number_of_cycles = 0;
            GrpSchueler.Height = originale_sportkurs_hoehe;
            BtnSchuelerAdd.Location = originale_schueler_add_position;
            BtnSchuelerAdd.TabIndex = 3;
            foreach (string i in sportkurse.Keys)
            {
                BtnSchuelerAdd.Location = new Point(BtnSchuelerAdd.Location.X, BtnSchuelerAdd.Location.Y + 30);
                BtnSchuelerAdd.TabIndex += 1;
                ExtendedLabel label = new ExtendedLabel(i);
                label.Location = new Point(7, 70 + 30 * number_of_cycles);
                label.Text = i;
                label.Size = new Size(50, 15);
                ExtendedNumUpDown num_up_down = new ExtendedNumUpDown(i);
                num_up_down.Location=new Point(63, 70 + 30 * number_of_cycles);
                num_up_down.Size = new Size(50, 20);
                num_up_down.Maximum = sportkurse.Count();
                num_up_down.Minimum = 1;
                num_up_down.Value = num_up_down.Maximum;
                num_up_down.TabIndex = 3 + number_of_cycles;
                GrpSchueler.Controls.Add(label);
                GrpSchueler.Controls.Add(num_up_down);
                GrpSchueler.Height += 30;
                number_of_cycles++;
            }
        }

        private void BtnSchuelerAdd_Click(object sender, EventArgs e)
        {
            if (TxbVorname.Text == "")
            {
                MessageBox.Show("Bitte einen Vornamen angeben");
                return;
            }
            if (TxbNachname.Text == "")
            {
                MessageBox.Show("Bitte einen Nachnamen angeben");
                return;
            }
            string vorname = TxbVorname.Text;
            string nachname = TxbNachname.Text;
            Schueler schueler = new Schueler(vorname, nachname);
            foreach(Control i in GrpSchueler.Controls)
            {
                if (i is ExtendedNumUpDown)
                {
                    Sportkurs sportkurs = sportkurse[((ExtendedNumUpDown)i).get_kurs_id()];
                    Kurszuordnung kurszuordnung = new Kurszuordnung(ref schueler, ref sportkurs, (int)((ExtendedNumUpDown)i).Value);
                    schueler.kurs_hinzufuegen(kurszuordnung);
                }
            }
            this.schueler.Add(schueler);
            TxbVorname.Select();
        }

        private void BtnCalc_Click(object sender, EventArgs e)
        {
            if (sportkurse.Count <= 3)
            {
                MessageBox.Show("Bitte mindestens vier Sportkurse hinzufügen.");
                return;
            }
            doppelkurse = new Dictionary<string, Doppelkurs>();
            Sportkurs[] kurse = new Sportkurs[sportkurse.Count];
            sportkurse.Values.CopyTo(kurse, 0);
            for (int i = 0; i < sportkurse.Count; i++)
            {
                for (int j = i + 1; j < sportkurse.Count; j++)
                {
                    Doppelkurs doppelkurs = new Doppelkurs(kurse[i], kurse[j]);
                    doppelkurse[doppelkurs.Name] = doppelkurs;
                }
            }
            Doppelkurs[] d_kurse = new Doppelkurs[doppelkurse.Count];
            doppelkurse.Values.CopyTo(d_kurse, 0);
            doppelkurszuordnungen = new Dictionary<string, Doppelkurszuordnung>();
            List<KursKombination> kombis = new List<KursKombination>();
            double bester_wert = -1000;
            for (int i = 0; i < doppelkurse.Count; i++)
            {
                for (int j = i + 1; j < doppelkurse.Count; j++)
                {
                    foreach (Schueler k in schueler)
                    {
                        Doppelkurszuordnung doppelkurs = new Doppelkurszuordnung(d_kurse[i], d_kurse[j], k);
                        doppelkurszuordnungen[doppelkurs.Name] = doppelkurs;
                    }
                    kombis.Add(new KursKombination(ref doppelkurszuordnungen, d_kurse[i], d_kurse[j]));
                    if (kombis[kombis.Count - 1].durchschnitt_berechnen() > bester_wert) bester_wert = kombis[kombis.Count - 1].durchschnitt_berechnen();
                }
            }
            LstBoxAusgabe.Items.Clear();
            foreach (KursKombination i in kombis)
            {
                if (i.durchschnitt_berechnen() == bester_wert)
                {
                    foreach (Schueler j in schueler)
                    {
                        j.Bester_doppelkurs = i.Doppelkurse[j.name];
                        LstBoxAusgabe.Items.Add("| " + j.name + " | " + (j.Bester_doppelkurs.Kursentscheidung ? j.Bester_doppelkurs.Name_a : j.Bester_doppelkurs.Name_b));
                    }
                    break;
                }
            }
        }

        private void BtnKursLoad_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.ShowDialog();
            string[] text = File.ReadAllLines(dialog.FileName);
            sportkurse = new Dictionary<string, Sportkurs>();
            schueler = new List<Schueler>();
            foreach (string i in text)
            {
                string[] einzelheiten = i.Split('|');
                if (einzelheiten[0] == "Schueler")
                {
                    Schueler einzelner_schueler = new Schueler(einzelheiten[1], einzelheiten[2]);
                    for (int j = 3; j < einzelheiten.Length; j += 2)
                    {
                        Sportkurs sportkurs = sportkurse[einzelheiten[j]];
                        Kurszuordnung kurszuordnung = new Kurszuordnung(ref einzelner_schueler, ref sportkurs, Convert.ToInt32(einzelheiten[j + 1]));
                        einzelner_schueler.kurs_hinzufuegen(kurszuordnung);
                    }
                    schueler.Add(einzelner_schueler);
                }
                else if (einzelheiten[0] == "Sportkurs")
                {
                    Sportkurs sportkurs = new Sportkurs(einzelheiten[1], Convert.ToInt32(einzelheiten[2]), Convert.ToInt32(einzelheiten[3]));
                    sportkurse[sportkurs.Name] = sportkurs;
                }
            }
            BtnSchuelerAktualisieren_Click();
        }

        private void BtnKursWrite_Click(object sender, EventArgs e)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.ShowDialog();
            List<string> ausgabe = new List<string>();
            foreach (Sportkurs i in sportkurse.Values)
            {
                ausgabe.Add(i.dateizeile());
            }
            foreach (Schueler i in schueler)
            {
                ausgabe.Add(i.dateizeile());
            }
            File.WriteAllLines(dialog.FileName, ausgabe);
        }

        private void BtnSchuelerAnz_Click(object sender, EventArgs e)
        {
            LstBoxAusgabe.Items.Clear();
            foreach (Schueler i in schueler)
            {
                LstBoxAusgabe.Items.Add(i.ausgeben());
            }
        }

        private void BtnZuordnungSpeichern_Click(object sender, EventArgs e)
        {
            int laengster_name = 4;
            foreach (Schueler i in schueler)
            {
                if (i.name.Length > laengster_name) laengster_name = i.name.Length;
            }
            int laengster_kurs_name = 5;
            foreach (Doppelkurs i in doppelkurse.Values)
            {
                if (i.Name.Length > laengster_kurs_name) laengster_kurs_name = i.Name.Length;
            }
            List<string> ausgabe = new List<string>();
            string hinzufuegen = "|Name" + String.Concat(Enumerable.Repeat(" ", laengster_name - 4));
            hinzufuegen += "|" + "Kurse" + String.Concat(Enumerable.Repeat(" ", laengster_kurs_name - 5)) + "|";
            ausgabe.Add(hinzufuegen);
            Doppelkurszuordnung[] doppelkurszuordnungs = new Doppelkurszuordnung[doppelkurszuordnungen.Count];
            doppelkurszuordnungen.Values.CopyTo(doppelkurszuordnungs, 0);
            foreach(Schueler i in schueler)
            {
                string aus = "|" + i.name + String.Concat(Enumerable.Repeat(" ", laengster_name - i.name.Length)) + "|";
                int beste_prioritaet = 10000;
                Doppelkurszuordnung beste_doppelkurszuordnung = doppelkurszuordnungs[0];
                foreach(Doppelkurszuordnung j in doppelkurszuordnungen.Values)
                {
                    if (j.Schueler.name == i.name)
                    {
                        if (j.Prioritaet < beste_prioritaet) beste_prioritaet = (int)j.Prioritaet;
                    }
                }
                string kursname = beste_doppelkurszuordnung.Kursentscheidung ? beste_doppelkurszuordnung.Name_a : beste_doppelkurszuordnung.Name_b;
                aus += kursname + String.Concat(Enumerable.Repeat(" ", laengster_kurs_name - kursname.Length)) + "|";
            }
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.ShowDialog();
            File.WriteAllLines(dialog.FileName, ausgabe);
        }
    }
}
