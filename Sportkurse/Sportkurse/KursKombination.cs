﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace Sportkurse
{
    public class KursKombination
    {
        Dictionary<string, Doppelkurszuordnung> doppelkurse;
        Doppelkurs kurs_a;
        Doppelkurs kurs_b;
        bool zusammenstellung_berechnet;

        public KursKombination(ref Dictionary<string, Doppelkurszuordnung> doppelkurse, Doppelkurs k_a, Doppelkurs k_b)
        {
            this.doppelkurse = new Dictionary<string, Doppelkurszuordnung>();
            foreach(Doppelkurszuordnung i in doppelkurse.Values)
            {
                if (i.Name_a == k_a.Name && i.Name_b == k_b.Name) this.doppelkurse[i.Schueler.name] = i;
                else if(i.Name_a == k_b.Name && i.Name_b == k_a.Name)
                {
                    i.kurse_vertauschen();
                    this.doppelkurse[i.Schueler.name] = i;
                }
            }
            kurs_a = k_a;
            kurs_b = k_b;
            zusammenstellung_berechnet = false;
        }

        public Dictionary<string, Doppelkurszuordnung> zusammenstellen()
        {
            Dictionary<string, bool> zuordnung_zu_a = new Dictionary<string, bool>();
            int zu_a_zugeordnet = 0;
            int zu_b_zugeordnet = 0;
            foreach (Doppelkurszuordnung i in doppelkurse.Values)
            {
                zuordnung_zu_a[i.Schueler.name] = i.Prioritaet_a >= i.Prioritaet_b;
                if (zuordnung_zu_a[i.Schueler.name])
                {
                    zu_a_zugeordnet += 1;
                }
                else
                {
                    zu_b_zugeordnet += 1;
                }
            }
            bool a_daneben;
            Doppelkurszuordnung[] temp = new Doppelkurszuordnung[doppelkurse.Count];
            doppelkurse.Values.CopyTo(temp, 0);
            Doppelkurszuordnung doppelkurs = temp[0];
            int daneben = doppelkurs.in_begrenzung(zu_a_zugeordnet, zu_b_zugeordnet, out a_daneben);
            if (daneben == -1000)
            {
                //MessageBox.Show("Die Kurskombination aus " + doppelkurs.Name_a + " und " + doppelkurs.Name_b + " kann wegen der Maximal- und Minimalteilnehmerzahlen nicht Funktionieren.");
                return null;
            }
            while (a_daneben && daneben != 0)
            {
                double minimum = 1000000;
                Doppelkurszuordnung minimal_schueler = doppelkurs;
                foreach (Doppelkurszuordnung i in doppelkurse.Values)
                {
                    if (!zuordnung_zu_a[i.Schueler.name] && i.Prioritaet_b < minimum)
                    {
                        minimal_schueler = i;
                        minimum = i.Prioritaet_b;
                    }
                }
                zuordnung_zu_a[minimal_schueler.Schueler.name] = false;
                zu_a_zugeordnet += 1;
                zu_b_zugeordnet -= 1;
                daneben = doppelkurs.in_begrenzung(zu_a_zugeordnet, zu_b_zugeordnet, out a_daneben);
            }
            while (a_daneben && daneben != 0)
            {
                double minimum = 1000000;
                Doppelkurszuordnung minimal_schueler = doppelkurs;
                foreach (Doppelkurszuordnung i in doppelkurse.Values)
                {
                    if (zuordnung_zu_a[i.Schueler.name] && i.Prioritaet_a < minimum)
                    {
                        minimal_schueler = i;
                        minimum = i.Prioritaet_a;
                    }
                }
                zuordnung_zu_a[minimal_schueler.Schueler.name] = true;
                zu_a_zugeordnet -= 1;
                zu_b_zugeordnet += 1;
                daneben = doppelkurs.in_begrenzung(zu_a_zugeordnet, zu_b_zugeordnet, out a_daneben);
            }
            foreach (Doppelkurszuordnung i in doppelkurse.Values)
            {
                i.Kursentscheidung = zuordnung_zu_a[i.Schueler.name];
            }
            zusammenstellung_berechnet = true;
            return doppelkurse;
        }

        public double durchschnitt_berechnen()
        {
            if (!zusammenstellung_berechnet)
            {
                zusammenstellen();
            }
            double sum = 0;
            double count = 0;
            foreach(Doppelkurszuordnung i in doppelkurse.Values)
            {
                count++;
                sum += i.Schueler.Beste_prioritaet - i.Prioritaet;
            }
            return count / sum;
        }

        public Dictionary<string, Doppelkurszuordnung> Doppelkurse
        {
            get => doppelkurse;
        }
    }
}
