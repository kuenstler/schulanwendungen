﻿using System;
namespace Sportkurse
{
    public class Doppelkurszuordnung
    {
        private Doppelkurs kurs_a;
        private Kurszuordnung kurs_a_a;
        private Kurszuordnung kurs_a_b;
        private Doppelkurs kurs_b;
        private Kurszuordnung kurs_b_a;
        private Kurszuordnung kurs_b_b;
        private readonly Schueler schueler;
        private bool kursentscheidung;
        private double prioritaet_a;
        private double prioritaet_b;

        public Doppelkurszuordnung(Doppelkurs a, Doppelkurs b, Schueler s)
        {
            kurs_a = a;
            kurs_b = b;
            schueler = s;
            kurs_a_a = s.Kurszuordnungen[a.a.Name];
            kurs_a_b = s.Kurszuordnungen[a.b.Name];
            kurs_b_a = s.Kurszuordnungen[b.a.Name];
            kurs_b_b = s.Kurszuordnungen[b.b.Name];
            double summe = kurs_a_a.Prioritaet;
            summe += kurs_a_b.Prioritaet;
            prioritaet_a = summe / 2.0;
            summe = kurs_b_a.Prioritaet;
            summe += kurs_b_b.Prioritaet;
            prioritaet_b = summe / 2.0;
        }

        public int in_begrenzung(int a, int b, out bool a_daneben)
        {
            a_daneben = true;
            int daneben_a_a = kurs_a_a.Sportkurs.in_begrenzung(a);
            int daneben_a_b = kurs_a_b.Sportkurs.in_begrenzung(a);
            if (Math.Sign(daneben_a_a) != Math.Sign(daneben_a_b))
            {
                return -1000;
            }
            int daneben_a;
            if (daneben_a_a < 0) daneben_a = Math.Min(daneben_a_a, daneben_a_b);
            else daneben_a = Math.Max(daneben_a_a, daneben_a_b);
            int daneben_b_a = kurs_b_a.Sportkurs.in_begrenzung(b);
            int daneben_b_b = kurs_b_b.Sportkurs.in_begrenzung(b);
            if (Math.Sign(daneben_b_a) != Math.Sign(daneben_b_b))
            {
                return -1000;
            }
            int daneben_b;
            if (daneben_b_a < 0) daneben_b = Math.Min(daneben_b_a, daneben_b_b);
            else daneben_b = Math.Max(daneben_b_a, daneben_b_b);
            if (Math.Sign(daneben_a) == Math.Sign(daneben_b) && daneben_a != 0 && daneben_b != 0)
            {
                return -1000;
            }
            if (daneben_a != 0)
            {
                Tuple<int, int> potential_b_a = kurs_b_a.Sportkurs.potential(b);
                Tuple<int, int> potential_b_b = kurs_b_b.Sportkurs.potential(b);
                if (daneben_a < 0)//  && daneben_a + potential_b >= 0) 
                {
                    int potential_b_runter = Math.Min(potential_b_a.Item1, potential_b_b.Item1);
                    if (daneben_a + potential_b_runter >= 0) return daneben_a;
                    return -1000;
                }
                int potential_b_hoch = Math.Min(potential_b_a.Item2, potential_b_b.Item2);
                if (daneben_a - potential_b_hoch >= 0) return daneben_a;
                return -1000;
            }
            a_daneben = false;
            Tuple<int, int> potential_a_a = kurs_a_a.Sportkurs.potential(b);
            Tuple<int, int> potential_a_b = kurs_a_b.Sportkurs.potential(b);
            if (daneben_a < 0)//  && daneben_a + potential_b >= 0) 
            {
                int potential_a_runter = Math.Min(potential_a_a.Item1, potential_a_b.Item1);
                if (daneben_a + potential_a_runter >= 0) return daneben_a;
                return -1000;
            }
            int potential_a_hoch = Math.Min(potential_a_a.Item2, potential_a_b.Item2);
            if (daneben_a - potential_a_hoch >= 0) return daneben_a;
            return -1000;
        }

        public double Prioritaet_a
        {
            get => prioritaet_a;
        }

        public double Prioritaet_b
        {
            get => prioritaet_b;
        }

        public double Prioritaet
        {
            get => Kursentscheidung ? Prioritaet_a : Prioritaet_b;
        }

        public bool Kursentscheidung
        {
            get => kursentscheidung;
            set => kursentscheidung = value;
        }

        public string Name_a
        {
            get => kurs_a.Name;
        }

        public string Name_b
        {
            get => kurs_b.Name;
        }

        public string Name
        {
            get => Schueler.name + " in " + Name_a + " und " + Name_b;
        }

        public Schueler Schueler
        {
            get => schueler;
        }

        public void kurse_vertauschen()
        {
            Doppelkurs temp = kurs_a;
            kurs_a = kurs_b;
            kurs_b = temp;
            Kurszuordnung temp_a = kurs_a_a;
            Kurszuordnung temp_b = kurs_a_b;
            kurs_a_a = kurs_b_a;
            kurs_a_b = kurs_b_b;
            kurs_b_a = temp_a;
            kurs_b_b = temp_b;
            double temp_p = prioritaet_a;
            prioritaet_a = prioritaet_b;
            prioritaet_b = temp_p;
        }
    }
}
