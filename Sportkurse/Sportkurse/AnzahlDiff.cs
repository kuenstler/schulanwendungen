﻿using System;
namespace Sportkurse
{
    public class AnzahlDiff
    {
        private int fehlend;
        private int kapazitaet_hoch;
        private int kapazitaet_runter;

        public AnzahlDiff(int f, int h, int r)
        {
            fehlend = f;
            kapazitaet_hoch = h;
            kapazitaet_runter = r;
        }

        private int Fehlend
        {
            get => fehlend;
        }

        private int Kapazitaet_hoch
        {
            get => kapazitaet_hoch;
        }

        private int Kapazitaet_runter
        {
            get => kapazitaet_runter;
        }

        private bool Moeglich(AnzahlDiff value)
        {
            return (value.Fehlend < 0 ? kapazitaet_hoch >= value.Fehlend * -1 : kapazitaet_runter >= value.Fehlend) &&
            (fehlend < 0 ? value.Kapazitaet_hoch >= fehlend * -1 : value.Kapazitaet_runter >= fehlend);
        }

        private AnzahlDiff Merge(AnzahlDiff value)
        {
            if (!Moeglich(value))
            {
                throw new InvalidOperationException();
            }
            int fehlend = this.fehlend < 0 ? Math.Max(this.fehlend, value.Fehlend) : Math.Min(this.fehlend, value.Fehlend);
            int kapazitaet_hoch = Math.Min(this.kapazitaet_hoch, value.Kapazitaet_hoch);
            int kapazitaet_runter = Math.Min(this.kapazitaet_runter, value.Kapazitaet_runter);
            return new AnzahlDiff(fehlend, kapazitaet_hoch, kapazitaet_runter);
        }
    }
}
