﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sportkurse
{
    public class Kurszuordnung
    {
        private Schueler schueler;
        private Sportkurs sportkurs;
        private int prioritaet;

        public Kurszuordnung(ref Schueler s, ref Sportkurs sp, int p)
        {
            schueler = s;
            sportkurs = sp;
            prioritaet = p;
            s.kurs_hinzufuegen(this);
        }

        public string schueler_name
        {
            get => schueler.name;
        }

        public string kurs_name
        {
            get => sportkurs.Name;
        }

        public int Prioritaet
        {
            get => prioritaet;
        }

        public Sportkurs Sportkurs
        {
            get => sportkurs;
        }
    }
}