﻿using System;
using System.Windows.Forms;
namespace Sportkurse
{
    public class ExtendedNumUpDown: NumericUpDown
    {
        string kurs_id;

        public ExtendedNumUpDown(string kurs_id)
        {
            this.kurs_id = kurs_id;
        }

        public string get_kurs_id()
        {
            return kurs_id;
        }
    }
}
