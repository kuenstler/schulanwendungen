﻿using System;
using System.Collections.Generic;

namespace Sportkurse
{
    public class Doppelkurs
    {
        private Sportkurs kurs_a;
        private Sportkurs kurs_b;

        public Doppelkurs(Sportkurs kurs_a, Sportkurs kurs_b)
        {
            this.kurs_a = kurs_a;
            this.kurs_b = kurs_b;
        }

        public Sportkurs a
        {
            get => kurs_a;
            set => kurs_a = value;
        }

        public Sportkurs b
        {
            get => kurs_b;
            set => kurs_b = value;
        }

        public string Name
        {
            get => kurs_a.Name + " und " + kurs_b.Name;
        }
    }
}
