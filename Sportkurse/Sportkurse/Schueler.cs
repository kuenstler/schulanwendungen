﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sportkurse
{
    public class Schueler
    {
        private string vorname;
        private string nachname;
        private Dictionary<string, Kurszuordnung> kurse;
        private Doppelkurszuordnung bester_doppelkurs;
        private int bester_kurs;

        public Schueler(string v, string n)
        {
            vorname = v;
            nachname = n;
            kurse = new Dictionary<string, Kurszuordnung>();
            bester_kurs = 10000;
        }

        public void kurs_hinzufuegen(Kurszuordnung k)
        {
            kurse[k.kurs_name] = k;
            if (k.Prioritaet < bester_kurs) bester_kurs = k.Prioritaet;
        }

        public int Beste_prioritaet
        {
            get => bester_kurs;
        }

        public Dictionary<string, Kurszuordnung> Kurszuordnungen
        {
            get => kurse;
        }

        public string name
        {
            get => vorname + " " + nachname;
        }

        public string ausgeben()
        {
            string ausgabe = "";
            ausgabe += "Vorname: " + vorname;
            ausgabe += ", Name: " + nachname;
            foreach(Kurszuordnung i in kurse.Values)
            {
                ausgabe += ", Kurs: " + i.kurs_name;
            }
            return ausgabe;
        }

        public Doppelkurszuordnung Bester_doppelkurs
        {
            get => bester_doppelkurs;
            set => bester_doppelkurs = value;
        }

        public string dateizeile()
        {
            string aus = "Schueler|";
            aus += vorname + "|";
            aus += nachname;
            foreach(Kurszuordnung i in kurse.Values)
            {
                aus += "|" + i.kurs_name;
                aus += "|" + Convert.ToString(i.Prioritaet);
            }
            return aus;
        }
    }
}
