﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sportkurse
{
    public class Sportkurs
    {
        private string name;
        private int minimalteilnehmer;
        private int maximalteinehmer;
        private List<Kurszuordnung> schueler;
        private List<string> doppelkurse;

        public Sportkurs(string n, int max, int min)
        {
            name = n;
            maximalteinehmer = max;
            minimalteilnehmer = min;
            schueler = new List<Kurszuordnung>();
            doppelkurse = new List<string>();
        }

        public string ausgeben()
        {
            return "Name: " + name;
        }

        public string Name
        {
            get => name;
        }

        public List<string> Doppelkurse
        {
            get => doppelkurse;
            set => doppelkurse = value;
        }

        public int in_begrenzung(int zu_pruefend)
        {
            if (zu_pruefend < minimalteilnehmer)
            {
                return zu_pruefend - minimalteilnehmer;
            }
            if (zu_pruefend > maximalteinehmer)
            {
                return zu_pruefend - maximalteinehmer;
            }
            return 0;
        }

        public Tuple<int, int> potential(int anzahl)
        {
            int runter = anzahl - minimalteilnehmer;
            int hoch = maximalteinehmer - anzahl;
            return new Tuple<int, int>(runter, hoch);
        }

        public string dateizeile()
        {
            string aus = "Sportkurs|";
            aus += Name + "|";
            aus += Convert.ToString(maximalteinehmer) + "|";
            aus += Convert.ToString(minimalteilnehmer);
            return aus;
        }
    }
}