﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Telefon
{
    public struct telefon
    {
        public string vorwahl;
        public int nummer;

        public telefon(string v, int n)
        {
            vorwahl = v;
            nummer = n;
        }

        public string ausgeben()
        {
            return "(" + vorwahl + ") " + nummer;
        }
    }
}