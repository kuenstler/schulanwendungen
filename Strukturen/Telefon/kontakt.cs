﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Telefon
{
    public struct kontakt
    {
        public int plz;
        public string ort;
        public string strasse;
        public string hausnummer;
        public telefon tel;
        public telefon fax;

        public kontakt(int p, string o, string s, string h, telefon t, telefon f)
        {
            plz = p;
            ort = o;
            strasse = s;
            hausnummer = h;
            tel = t;
            fax = f;
        }

        public string ausgeben()
        {
            string aus = strasse + " " + hausnummer;
            aus += "\n" + plz.ToString("00000") + " " + ort;
            aus += "\nTel: " + tel.ausgeben();
            aus += "\nFax: " + fax.ausgeben();
            return aus;
        }
    }
}