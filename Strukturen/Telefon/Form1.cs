﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Telefon
{
    public partial class Form1 : Form
    {
        List<kontakt> list = new List<kontakt>();
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnAusgeben_Click(object sender, EventArgs e)
        {
            kontakt k1, k2;
            k1.plz = 43024;
            k1.ort = "Aachen";
            k1.strasse = "Hunsrückweg";
            k1.hausnummer = "104";
            k1.tel.vorwahl = "0466";
            k1.tel.nummer = 532626;
            k1.fax.vorwahl = "0466";
            k1.fax.nummer = 532627;

            k2 = k1;
            LblAusgabe.Text = k2.ausgeben();

            list.Add(k1);
            list.Add(k2);

            kontakt k3 = new kontakt(43035, "Düren", "Eifelweg", "12", new telefon("0463", 887743), new telefon("0463", 887744));
            LblAusgabe.Text += "\n\n" + k3.ausgeben();
            list.Add(k3);
            for (int i = 0; i < list.Count; i = i +2)
            {
                list.Add(list[i]);
                if (list.Count > 33000000)
                {
                    list.Clear();
                }
            }
            LblAusgabe.Text += "\n" + list.Count();
        }
    }
}
