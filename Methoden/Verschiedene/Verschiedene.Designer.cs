﻿namespace Verschiedene
{
    partial class Verschiedene
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrpBoxValue = new System.Windows.Forms.GroupBox();
            this.LblValue = new System.Windows.Forms.Label();
            this.BtnCallAnz2 = new System.Windows.Forms.Button();
            this.BtnValueAnz = new System.Windows.Forms.Button();
            this.GrpRef = new System.Windows.Forms.GroupBox();
            this.LblRef = new System.Windows.Forms.Label();
            this.BtnRefDouble = new System.Windows.Forms.Button();
            this.BtnRef = new System.Windows.Forms.Button();
            this.BtnRefValue = new System.Windows.Forms.Button();
            this.GrpOption = new System.Windows.Forms.GroupBox();
            this.NumOption = new System.Windows.Forms.NumericUpDown();
            this.LblOption = new System.Windows.Forms.Label();
            this.BtnOptMult = new System.Windows.Forms.Button();
            this.BtnOptAdd = new System.Windows.Forms.Button();
            this.GrpMany = new System.Windows.Forms.GroupBox();
            this.LblMany = new System.Windows.Forms.Label();
            this.LblOptionArg = new System.Windows.Forms.Label();
            this.NumMany = new System.Windows.Forms.NumericUpDown();
            this.BtnMany = new System.Windows.Forms.Button();
            this.GrpFak = new System.Windows.Forms.GroupBox();
            this.LblFak = new System.Windows.Forms.Label();
            this.NumFak = new System.Windows.Forms.NumericUpDown();
            this.button1 = new System.Windows.Forms.Button();
            this.GrpName = new System.Windows.Forms.GroupBox();
            this.LblName = new System.Windows.Forms.Label();
            this.BtnName3 = new System.Windows.Forms.Button();
            this.BtnName2 = new System.Windows.Forms.Button();
            this.BtnNameWirr = new System.Windows.Forms.Button();
            this.BtnNameDef = new System.Windows.Forms.Button();
            this.GrpGgT = new System.Windows.Forms.GroupBox();
            this.LstBoxBruchOperation = new System.Windows.Forms.ListBox();
            this.LblBruchN = new System.Windows.Forms.Label();
            this.LblBruchZ = new System.Windows.Forms.Label();
            this.LblBruchGanz = new System.Windows.Forms.Label();
            this.LblBruchMitte = new System.Windows.Forms.Label();
            this.NumBruchErgN = new System.Windows.Forms.NumericUpDown();
            this.NumBruchErgZ = new System.Windows.Forms.NumericUpDown();
            this.NumBruch2N = new System.Windows.Forms.NumericUpDown();
            this.NumBruch2Z = new System.Windows.Forms.NumericUpDown();
            this.NumBruch1N = new System.Windows.Forms.NumericUpDown();
            this.NumBruch1Z = new System.Windows.Forms.NumericUpDown();
            this.GrpBoxValue.SuspendLayout();
            this.GrpRef.SuspendLayout();
            this.GrpOption.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumOption)).BeginInit();
            this.GrpMany.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumMany)).BeginInit();
            this.GrpFak.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumFak)).BeginInit();
            this.GrpName.SuspendLayout();
            this.GrpGgT.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruchErgN)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruchErgZ)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruch2N)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruch2Z)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruch1N)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruch1Z)).BeginInit();
            this.SuspendLayout();
            // 
            // GrpBoxValue
            // 
            this.GrpBoxValue.Controls.Add(this.LblValue);
            this.GrpBoxValue.Controls.Add(this.BtnCallAnz2);
            this.GrpBoxValue.Controls.Add(this.BtnValueAnz);
            this.GrpBoxValue.Location = new System.Drawing.Point(20, 20);
            this.GrpBoxValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpBoxValue.Name = "GrpBoxValue";
            this.GrpBoxValue.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpBoxValue.Size = new System.Drawing.Size(510, 129);
            this.GrpBoxValue.TabIndex = 0;
            this.GrpBoxValue.TabStop = false;
            this.GrpBoxValue.Text = "Call by Value";
            // 
            // LblValue
            // 
            this.LblValue.AutoSize = true;
            this.LblValue.Location = new System.Drawing.Point(272, 46);
            this.LblValue.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblValue.Name = "LblValue";
            this.LblValue.Size = new System.Drawing.Size(76, 20);
            this.LblValue.TabIndex = 2;
            this.LblValue.Text = "Maximum";
            // 
            // BtnCallAnz2
            // 
            this.BtnCallAnz2.Location = new System.Drawing.Point(10, 77);
            this.BtnCallAnz2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnCallAnz2.Name = "BtnCallAnz2";
            this.BtnCallAnz2.Size = new System.Drawing.Size(250, 35);
            this.BtnCallAnz2.TabIndex = 1;
            this.BtnCallAnz2.Text = "Anzeigen2";
            this.BtnCallAnz2.UseVisualStyleBackColor = true;
            this.BtnCallAnz2.Click += new System.EventHandler(this.BtnCallAnz2_Click);
            // 
            // BtnValueAnz
            // 
            this.BtnValueAnz.Location = new System.Drawing.Point(10, 31);
            this.BtnValueAnz.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnValueAnz.Name = "BtnValueAnz";
            this.BtnValueAnz.Size = new System.Drawing.Size(250, 35);
            this.BtnValueAnz.TabIndex = 0;
            this.BtnValueAnz.Text = "Anzeigen";
            this.BtnValueAnz.UseVisualStyleBackColor = true;
            this.BtnValueAnz.Click += new System.EventHandler(this.BtnValueAnz_Click);
            // 
            // GrpRef
            // 
            this.GrpRef.Controls.Add(this.LblRef);
            this.GrpRef.Controls.Add(this.BtnRefDouble);
            this.GrpRef.Controls.Add(this.BtnRef);
            this.GrpRef.Controls.Add(this.BtnRefValue);
            this.GrpRef.Location = new System.Drawing.Point(20, 160);
            this.GrpRef.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpRef.Name = "GrpRef";
            this.GrpRef.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpRef.Size = new System.Drawing.Size(510, 198);
            this.GrpRef.TabIndex = 1;
            this.GrpRef.TabStop = false;
            this.GrpRef.Text = "Call by Reference";
            // 
            // LblRef
            // 
            this.LblRef.AutoSize = true;
            this.LblRef.Location = new System.Drawing.Point(272, 31);
            this.LblRef.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblRef.Name = "LblRef";
            this.LblRef.Size = new System.Drawing.Size(57, 20);
            this.LblRef.TabIndex = 3;
            this.LblRef.Text = "Vorher";
            // 
            // BtnRefDouble
            // 
            this.BtnRefDouble.Location = new System.Drawing.Point(10, 123);
            this.BtnRefDouble.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnRefDouble.Name = "BtnRefDouble";
            this.BtnRefDouble.Size = new System.Drawing.Size(250, 66);
            this.BtnRefDouble.TabIndex = 2;
            this.BtnRefDouble.Text = "Methode zum Verdoppeln der Feldelemente";
            this.BtnRefDouble.UseVisualStyleBackColor = true;
            this.BtnRefDouble.Click += new System.EventHandler(this.BtnRefDouble_Click);
            // 
            // BtnRef
            // 
            this.BtnRef.Location = new System.Drawing.Point(10, 77);
            this.BtnRef.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnRef.Name = "BtnRef";
            this.BtnRef.Size = new System.Drawing.Size(250, 35);
            this.BtnRef.TabIndex = 1;
            this.BtnRef.Text = "Methode mit Call by Reference";
            this.BtnRef.UseVisualStyleBackColor = true;
            this.BtnRef.Click += new System.EventHandler(this.BtnRef_Click);
            // 
            // BtnRefValue
            // 
            this.BtnRefValue.Location = new System.Drawing.Point(10, 31);
            this.BtnRefValue.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnRefValue.Name = "BtnRefValue";
            this.BtnRefValue.Size = new System.Drawing.Size(250, 35);
            this.BtnRefValue.TabIndex = 0;
            this.BtnRefValue.Text = "Methode mit Call by Value";
            this.BtnRefValue.UseVisualStyleBackColor = true;
            this.BtnRefValue.Click += new System.EventHandler(this.BtnRefValue_Click);
            // 
            // GrpOption
            // 
            this.GrpOption.Controls.Add(this.NumOption);
            this.GrpOption.Controls.Add(this.LblOption);
            this.GrpOption.Controls.Add(this.BtnOptMult);
            this.GrpOption.Controls.Add(this.BtnOptAdd);
            this.GrpOption.Location = new System.Drawing.Point(20, 369);
            this.GrpOption.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpOption.Name = "GrpOption";
            this.GrpOption.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpOption.Size = new System.Drawing.Size(510, 154);
            this.GrpOption.TabIndex = 2;
            this.GrpOption.TabStop = false;
            this.GrpOption.Text = "optionale Argumente";
            // 
            // NumOption
            // 
            this.NumOption.Location = new System.Drawing.Point(264, 35);
            this.NumOption.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumOption.Maximum = new decimal(new int[] {
            4,
            0,
            0,
            0});
            this.NumOption.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.NumOption.Name = "NumOption";
            this.NumOption.Size = new System.Drawing.Size(180, 26);
            this.NumOption.TabIndex = 3;
            this.NumOption.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // LblOption
            // 
            this.LblOption.AutoSize = true;
            this.LblOption.Location = new System.Drawing.Point(276, 77);
            this.LblOption.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblOption.Name = "LblOption";
            this.LblOption.Size = new System.Drawing.Size(52, 20);
            this.LblOption.TabIndex = 2;
            this.LblOption.Text = "Werte";
            // 
            // BtnOptMult
            // 
            this.BtnOptMult.Location = new System.Drawing.Point(10, 77);
            this.BtnOptMult.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnOptMult.Name = "BtnOptMult";
            this.BtnOptMult.Size = new System.Drawing.Size(250, 35);
            this.BtnOptMult.TabIndex = 1;
            this.BtnOptMult.Text = "Multiplizieren";
            this.BtnOptMult.UseVisualStyleBackColor = true;
            this.BtnOptMult.Click += new System.EventHandler(this.BtnOptMult_Click);
            // 
            // BtnOptAdd
            // 
            this.BtnOptAdd.Location = new System.Drawing.Point(10, 31);
            this.BtnOptAdd.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnOptAdd.Name = "BtnOptAdd";
            this.BtnOptAdd.Size = new System.Drawing.Size(250, 35);
            this.BtnOptAdd.TabIndex = 0;
            this.BtnOptAdd.Text = "Addieren";
            this.BtnOptAdd.UseVisualStyleBackColor = true;
            this.BtnOptAdd.Click += new System.EventHandler(this.BtnOptAdd_Click);
            // 
            // GrpMany
            // 
            this.GrpMany.Controls.Add(this.LblMany);
            this.GrpMany.Controls.Add(this.LblOptionArg);
            this.GrpMany.Controls.Add(this.NumMany);
            this.GrpMany.Controls.Add(this.BtnMany);
            this.GrpMany.Location = new System.Drawing.Point(20, 534);
            this.GrpMany.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpMany.Name = "GrpMany";
            this.GrpMany.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpMany.Size = new System.Drawing.Size(510, 154);
            this.GrpMany.TabIndex = 3;
            this.GrpMany.TabStop = false;
            this.GrpMany.Text = "beliebig viele Argumente";
            // 
            // LblMany
            // 
            this.LblMany.AutoSize = true;
            this.LblMany.Location = new System.Drawing.Point(10, 77);
            this.LblMany.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblMany.Name = "LblMany";
            this.LblMany.Size = new System.Drawing.Size(56, 20);
            this.LblMany.TabIndex = 3;
            this.LblMany.Text = "Werte:";
            // 
            // LblOptionArg
            // 
            this.LblOptionArg.AutoSize = true;
            this.LblOptionArg.Location = new System.Drawing.Point(222, 38);
            this.LblOptionArg.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblOptionArg.Name = "LblOptionArg";
            this.LblOptionArg.Size = new System.Drawing.Size(172, 20);
            this.LblOptionArg.TabIndex = 2;
            this.LblOptionArg.Text = "Anzahl der Argumente:";
            // 
            // NumMany
            // 
            this.NumMany.Location = new System.Drawing.Point(402, 35);
            this.NumMany.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumMany.Name = "NumMany";
            this.NumMany.Size = new System.Drawing.Size(82, 26);
            this.NumMany.TabIndex = 1;
            // 
            // BtnMany
            // 
            this.BtnMany.Location = new System.Drawing.Point(10, 31);
            this.BtnMany.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnMany.Name = "BtnMany";
            this.BtnMany.Size = new System.Drawing.Size(202, 35);
            this.BtnMany.TabIndex = 0;
            this.BtnMany.Text = "Mittelwert der Elemente";
            this.BtnMany.UseVisualStyleBackColor = true;
            this.BtnMany.Click += new System.EventHandler(this.BtnMany_Click);
            // 
            // GrpFak
            // 
            this.GrpFak.Controls.Add(this.LblFak);
            this.GrpFak.Controls.Add(this.NumFak);
            this.GrpFak.Controls.Add(this.button1);
            this.GrpFak.Location = new System.Drawing.Point(540, 534);
            this.GrpFak.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpFak.Name = "GrpFak";
            this.GrpFak.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpFak.Size = new System.Drawing.Size(387, 154);
            this.GrpFak.TabIndex = 4;
            this.GrpFak.TabStop = false;
            this.GrpFak.Text = "Rekursion mit Fakultät";
            // 
            // LblFak
            // 
            this.LblFak.AutoSize = true;
            this.LblFak.Location = new System.Drawing.Point(10, 77);
            this.LblFak.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblFak.Name = "LblFak";
            this.LblFak.Size = new System.Drawing.Size(199, 20);
            this.LblFak.TabIndex = 5;
            this.LblFak.Text = "Anzahl der Kombinationen:";
            // 
            // NumFak
            // 
            this.NumFak.Location = new System.Drawing.Point(272, 31);
            this.NumFak.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumFak.Maximum = new decimal(new int[] {
            65,
            0,
            0,
            0});
            this.NumFak.Name = "NumFak";
            this.NumFak.Size = new System.Drawing.Size(82, 26);
            this.NumFak.TabIndex = 4;
            this.NumFak.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(10, 31);
            this.button1.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(252, 35);
            this.button1.TabIndex = 0;
            this.button1.Text = "Anzahl der Bruchkombinationen";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // GrpName
            // 
            this.GrpName.Controls.Add(this.LblName);
            this.GrpName.Controls.Add(this.BtnName3);
            this.GrpName.Controls.Add(this.BtnName2);
            this.GrpName.Controls.Add(this.BtnNameWirr);
            this.GrpName.Controls.Add(this.BtnNameDef);
            this.GrpName.Location = new System.Drawing.Point(540, 20);
            this.GrpName.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpName.Name = "GrpName";
            this.GrpName.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpName.Size = new System.Drawing.Size(387, 505);
            this.GrpName.TabIndex = 5;
            this.GrpName.TabStop = false;
            this.GrpName.Text = "benannte Argumente";
            // 
            // LblName
            // 
            this.LblName.AutoSize = true;
            this.LblName.Location = new System.Drawing.Point(15, 231);
            this.LblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblName.Name = "LblName";
            this.LblName.Size = new System.Drawing.Size(51, 20);
            this.LblName.TabIndex = 4;
            this.LblName.Text = "Farbe";
            // 
            // BtnName3
            // 
            this.BtnName3.Location = new System.Drawing.Point(15, 168);
            this.BtnName3.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnName3.Name = "BtnName3";
            this.BtnName3.Size = new System.Drawing.Size(248, 57);
            this.BtnName3.TabIndex = 3;
            this.BtnName3.Text = "Reihenfolge mit 3 Argumenten und durcheinander";
            this.BtnName3.UseVisualStyleBackColor = true;
            this.BtnName3.Click += new System.EventHandler(this.BtnName3_Click);
            // 
            // BtnName2
            // 
            this.BtnName2.Location = new System.Drawing.Point(10, 122);
            this.BtnName2.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnName2.Name = "BtnName2";
            this.BtnName2.Size = new System.Drawing.Size(252, 35);
            this.BtnName2.TabIndex = 2;
            this.BtnName2.Text = "Reihenfolge mit 2 Ardumenten";
            this.BtnName2.UseVisualStyleBackColor = true;
            this.BtnName2.Click += new System.EventHandler(this.BtnName2_Click);
            // 
            // BtnNameWirr
            // 
            this.BtnNameWirr.Location = new System.Drawing.Point(10, 77);
            this.BtnNameWirr.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnNameWirr.Name = "BtnNameWirr";
            this.BtnNameWirr.Size = new System.Drawing.Size(252, 35);
            this.BtnNameWirr.TabIndex = 1;
            this.BtnNameWirr.Text = "Reihenfolge durcheinander";
            this.BtnNameWirr.UseVisualStyleBackColor = true;
            this.BtnNameWirr.Click += new System.EventHandler(this.BtnNameWirr_Click);
            // 
            // BtnNameDef
            // 
            this.BtnNameDef.Location = new System.Drawing.Point(10, 31);
            this.BtnNameDef.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.BtnNameDef.Name = "BtnNameDef";
            this.BtnNameDef.Size = new System.Drawing.Size(252, 35);
            this.BtnNameDef.TabIndex = 0;
            this.BtnNameDef.Text = "Reihenfolge Standard";
            this.BtnNameDef.UseVisualStyleBackColor = true;
            this.BtnNameDef.Click += new System.EventHandler(this.BtnNameDef_Click);
            // 
            // GrpGgT
            // 
            this.GrpGgT.Controls.Add(this.LstBoxBruchOperation);
            this.GrpGgT.Controls.Add(this.LblBruchN);
            this.GrpGgT.Controls.Add(this.LblBruchZ);
            this.GrpGgT.Controls.Add(this.LblBruchGanz);
            this.GrpGgT.Controls.Add(this.LblBruchMitte);
            this.GrpGgT.Controls.Add(this.NumBruchErgN);
            this.GrpGgT.Controls.Add(this.NumBruchErgZ);
            this.GrpGgT.Controls.Add(this.NumBruch2N);
            this.GrpGgT.Controls.Add(this.NumBruch2Z);
            this.GrpGgT.Controls.Add(this.NumBruch1N);
            this.GrpGgT.Controls.Add(this.NumBruch1Z);
            this.GrpGgT.Location = new System.Drawing.Point(540, 698);
            this.GrpGgT.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpGgT.Name = "GrpGgT";
            this.GrpGgT.Padding = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.GrpGgT.Size = new System.Drawing.Size(387, 182);
            this.GrpGgT.TabIndex = 6;
            this.GrpGgT.TabStop = false;
            this.GrpGgT.Text = "Größter gemeinsammer Teiler";
            // 
            // LstBoxBruchOperation
            // 
            this.LstBoxBruchOperation.FormattingEnabled = true;
            this.LstBoxBruchOperation.ItemHeight = 20;
            this.LstBoxBruchOperation.Items.AddRange(new object[] {
            "Addition",
            "Subtraktion",
            "Multiplikation",
            "Division"});
            this.LstBoxBruchOperation.Location = new System.Drawing.Point(272, 120);
            this.LstBoxBruchOperation.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.LstBoxBruchOperation.Name = "LstBoxBruchOperation";
            this.LstBoxBruchOperation.Size = new System.Drawing.Size(112, 44);
            this.LstBoxBruchOperation.TabIndex = 10;
            this.LstBoxBruchOperation.SelectedIndexChanged += new System.EventHandler(this.LstBoxBruchOperation_SelectedIndexChanged);
            // 
            // LblBruchN
            // 
            this.LblBruchN.AutoSize = true;
            this.LblBruchN.Location = new System.Drawing.Point(308, 95);
            this.LblBruchN.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblBruchN.Name = "LblBruchN";
            this.LblBruchN.Size = new System.Drawing.Size(0, 20);
            this.LblBruchN.TabIndex = 9;
            // 
            // LblBruchZ
            // 
            this.LblBruchZ.AutoSize = true;
            this.LblBruchZ.Location = new System.Drawing.Point(308, 46);
            this.LblBruchZ.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblBruchZ.Name = "LblBruchZ";
            this.LblBruchZ.Size = new System.Drawing.Size(0, 20);
            this.LblBruchZ.TabIndex = 8;
            // 
            // LblBruchGanz
            // 
            this.LblBruchGanz.AutoSize = true;
            this.LblBruchGanz.Location = new System.Drawing.Point(282, 66);
            this.LblBruchGanz.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblBruchGanz.Name = "LblBruchGanz";
            this.LblBruchGanz.Size = new System.Drawing.Size(0, 20);
            this.LblBruchGanz.TabIndex = 7;
            // 
            // LblBruchMitte
            // 
            this.LblBruchMitte.AutoSize = true;
            this.LblBruchMitte.Location = new System.Drawing.Point(9, 66);
            this.LblBruchMitte.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.LblBruchMitte.Name = "LblBruchMitte";
            this.LblBruchMitte.Size = new System.Drawing.Size(333, 20);
            this.LblBruchMitte.TabIndex = 6;
            this.LblBruchMitte.Text = "------------   +    ------------    =    ------------  =      -----";
            // 
            // NumBruchErgN
            // 
            this.NumBruchErgN.Location = new System.Drawing.Point(207, 95);
            this.NumBruchErgN.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumBruchErgN.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NumBruchErgN.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.NumBruchErgN.Name = "NumBruchErgN";
            this.NumBruchErgN.ReadOnly = true;
            this.NumBruchErgN.Size = new System.Drawing.Size(56, 26);
            this.NumBruchErgN.TabIndex = 5;
            // 
            // NumBruchErgZ
            // 
            this.NumBruchErgZ.InterceptArrowKeys = false;
            this.NumBruchErgZ.Location = new System.Drawing.Point(207, 31);
            this.NumBruchErgZ.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumBruchErgZ.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.NumBruchErgZ.Minimum = new decimal(new int[] {
            10000,
            0,
            0,
            -2147483648});
            this.NumBruchErgZ.Name = "NumBruchErgZ";
            this.NumBruchErgZ.ReadOnly = true;
            this.NumBruchErgZ.Size = new System.Drawing.Size(56, 26);
            this.NumBruchErgZ.TabIndex = 4;
            // 
            // NumBruch2N
            // 
            this.NumBruch2N.Location = new System.Drawing.Point(106, 95);
            this.NumBruch2N.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumBruch2N.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBruch2N.Name = "NumBruch2N";
            this.NumBruch2N.Size = new System.Drawing.Size(56, 26);
            this.NumBruch2N.TabIndex = 3;
            this.NumBruch2N.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBruch2N.ValueChanged += new System.EventHandler(this.NumBruch2N_ValueChanged);
            // 
            // NumBruch2Z
            // 
            this.NumBruch2Z.Location = new System.Drawing.Point(106, 31);
            this.NumBruch2Z.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumBruch2Z.Name = "NumBruch2Z";
            this.NumBruch2Z.Size = new System.Drawing.Size(56, 26);
            this.NumBruch2Z.TabIndex = 2;
            this.NumBruch2Z.ValueChanged += new System.EventHandler(this.NumBruch2Z_ValueChanged);
            // 
            // NumBruch1N
            // 
            this.NumBruch1N.Location = new System.Drawing.Point(10, 95);
            this.NumBruch1N.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumBruch1N.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBruch1N.Name = "NumBruch1N";
            this.NumBruch1N.Size = new System.Drawing.Size(56, 26);
            this.NumBruch1N.TabIndex = 1;
            this.NumBruch1N.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumBruch1N.ValueChanged += new System.EventHandler(this.NumBruch1N_ValueChanged);
            // 
            // NumBruch1Z
            // 
            this.NumBruch1Z.Location = new System.Drawing.Point(10, 31);
            this.NumBruch1Z.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.NumBruch1Z.Name = "NumBruch1Z";
            this.NumBruch1Z.Size = new System.Drawing.Size(56, 26);
            this.NumBruch1Z.TabIndex = 0;
            this.NumBruch1Z.ValueChanged += new System.EventHandler(this.NumBruch1Z_ValueChanged);
            // 
            // Verschiedene
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(945, 865);
            this.Controls.Add(this.GrpGgT);
            this.Controls.Add(this.GrpName);
            this.Controls.Add(this.GrpFak);
            this.Controls.Add(this.GrpMany);
            this.Controls.Add(this.GrpOption);
            this.Controls.Add(this.GrpRef);
            this.Controls.Add(this.GrpBoxValue);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Verschiedene";
            this.Text = "Verschiedene";
            this.GrpBoxValue.ResumeLayout(false);
            this.GrpBoxValue.PerformLayout();
            this.GrpRef.ResumeLayout(false);
            this.GrpRef.PerformLayout();
            this.GrpOption.ResumeLayout(false);
            this.GrpOption.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumOption)).EndInit();
            this.GrpMany.ResumeLayout(false);
            this.GrpMany.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumMany)).EndInit();
            this.GrpFak.ResumeLayout(false);
            this.GrpFak.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumFak)).EndInit();
            this.GrpName.ResumeLayout(false);
            this.GrpName.PerformLayout();
            this.GrpGgT.ResumeLayout(false);
            this.GrpGgT.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruchErgN)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruchErgZ)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruch2N)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruch2Z)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruch1N)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumBruch1Z)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpBoxValue;
        private System.Windows.Forms.Label LblValue;
        private System.Windows.Forms.Button BtnCallAnz2;
        private System.Windows.Forms.Button BtnValueAnz;
        private System.Windows.Forms.GroupBox GrpRef;
        private System.Windows.Forms.Label LblRef;
        private System.Windows.Forms.Button BtnRefDouble;
        private System.Windows.Forms.Button BtnRef;
        private System.Windows.Forms.Button BtnRefValue;
        private System.Windows.Forms.GroupBox GrpOption;
        private System.Windows.Forms.Button BtnOptMult;
        private System.Windows.Forms.Button BtnOptAdd;
        private System.Windows.Forms.NumericUpDown NumOption;
        private System.Windows.Forms.Label LblOption;
        private System.Windows.Forms.GroupBox GrpMany;
        private System.Windows.Forms.Label LblMany;
        private System.Windows.Forms.Label LblOptionArg;
        private System.Windows.Forms.NumericUpDown NumMany;
        private System.Windows.Forms.Button BtnMany;
        private System.Windows.Forms.GroupBox GrpFak;
        private System.Windows.Forms.Label LblFak;
        private System.Windows.Forms.NumericUpDown NumFak;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox GrpName;
        private System.Windows.Forms.Button BtnName3;
        private System.Windows.Forms.Button BtnName2;
        private System.Windows.Forms.Button BtnNameWirr;
        private System.Windows.Forms.Button BtnNameDef;
        private System.Windows.Forms.Label LblName;
        private System.Windows.Forms.GroupBox GrpGgT;
        private System.Windows.Forms.NumericUpDown NumBruchErgN;
        private System.Windows.Forms.NumericUpDown NumBruchErgZ;
        private System.Windows.Forms.NumericUpDown NumBruch2N;
        private System.Windows.Forms.NumericUpDown NumBruch2Z;
        private System.Windows.Forms.NumericUpDown NumBruch1N;
        private System.Windows.Forms.NumericUpDown NumBruch1Z;
        private System.Windows.Forms.Label LblBruchMitte;
        private System.Windows.Forms.Label LblBruchN;
        private System.Windows.Forms.Label LblBruchZ;
        private System.Windows.Forms.Label LblBruchGanz;
        private System.Windows.Forms.ListBox LstBoxBruchOperation;
    }
}

