﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Verschiedene
{
    public partial class Verschiedene : Form
    {
        Random r = new Random();

        public Verschiedene()
        {
            InitializeComponent();
            LstBoxBruchOperation.SelectedIndex = 0;
        }

        private void ZeigeMaximum(double x, double y)
        {
            LblValue.Text = "a: " + x + ", b: " + y;
            if (x > y)
            {
                LblValue.Text += "\nMaximum: " + x;
            }
            else
            {
                LblValue.Text += "\nMaximum: " + y;
            }
        }

        private void BtnValueAnz_Click(object sender, EventArgs e)
        {
            double a, b;
            a = (double)r.Next(0, 1000) / 10.0;
            b = (double)r.Next(0, 1000) / 10.0;
            ZeigeMaximum(a, b);
        }

        private void BtnCallAnz2_Click(object sender, EventArgs e)
        {
            double a, b;
            a = (double)r.Next(0, 1000) / 10.0;
            b = (double)r.Next(0, 1000) / 10.0;
            ZeigeMaximum(a, b);
        }

        private int[] tauscheVal(int x, int y)
        {
            int[] a = { y, x };
            return a;
        }

        private void BtnRefValue_Click(object sender, EventArgs e)
        {
            int x, y;
            x = r.Next(0, 100);
            y = r.Next(0, 100);
            LblRef.Text = "Vorher: x: " + x + ", y: " + y;
            // Tauschen
            int[] a = tauscheVal(x, y);
            x = a[0];
            y = a[1];
            LblRef.Text += "\nNachher: x: " + x + ", y: " + y;
        }

        private void tauscheRef(ref int a, ref int b)
        {
            int tmp;
            tmp = a;
            a = b;
            b = tmp;
        }

        private void BtnRef_Click(object sender, EventArgs e)
        {
            int x, y;
            x = r.Next(0, 100);
            y = r.Next(0, 100);
            LblRef.Text = "Vorher: x: " + x + ", y: " + y;
            // Tauschen
            tauscheRef(ref x, ref y);
            LblRef.Text += "\nNachher: x: " + x + ", y: " + y;
        }

        private void Verdoppeln(ref int[] x)
        {
            for (int i = 0; i < x.Length; i++)
            {
                x[i] *= 2;
            }
        }

        private void BtnRefDouble_Click(object sender, EventArgs e)
        {
            int x, y;
            x = r.Next(0, 100);
            y = r.Next(0, 100);
            LblRef.Text = "Vorher: x: " + x + ", y: " + y;
            // Verdoppeln
            int[] a = { x, y };
            Verdoppeln(ref a);
            LblRef.Text += "\nNachher: x: " + a[0] + ", y: " + a[1];
        }

        private ulong berechneFakultaet(ulong zahl)
        {
            ulong f; //Fakultät ist lokale Variable, long da schnell sehr groß
            if (zahl == 1 || zahl == 0) f = 1;//Reihenfolge beachten!!! - Abbruchbedingung
            else f = zahl * berechneFakultaet(zahl - 1); //rekursiver Aufruf
            return f; //Wertrückgabe
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ulong zahl, ergebnis;
            zahl = (ulong)NumFak.Value;
            ergebnis = berechneFakultaet(zahl);
            LblFak.Text = "Anzahl der Kombinationen:\n";
            LblFak.Text += ergebnis;
//            LblFak.Text += ergebnis.ToString("###,###,###,###,###,###,###,###,###,###,###,###,###,###,###,###,##0 ");
        }

        private double Addieren(double a, double b, double c = 0, double d = 0)
        {
            return a + b + c + d;
        }

        private void BtnOptAdd_Click(object sender, EventArgs e)
        {
            double a, b, c, d, ergebnis;
            a = (double)r.Next(1, 120) / 10.0;
            b = (double)r.Next(1, 120) / 10.0;
            c = (double)r.Next(1, 120) / 10.0;
            d = (double)r.Next(1, 120) / 10.0;
            switch ((int)NumOption.Value)
            {
                case 2:
                    ergebnis = Addieren(a, b);
                    LblOption.Text = "Die Summe aus " + a + " und " + b + "\nergibt " + ergebnis;
                    break;
                case 3:
                    ergebnis = Addieren(a, b, c);
                    LblOption.Text = "Die Summe aus " + a + ", " + b + "\nund " + c + " ergibt " + ergebnis;
                    break;
                case 4:
                    ergebnis = Addieren(a, b, c, d);
                    LblOption.Text = "Die Summe aus " + a + ", " + b + ",\n" + c + " und " + d + " ergibt " + ergebnis;
                    break;
            }
        }

        private double Multiplizieren(double a, double b, double c = 1, double d = 1)
        {
            return a * b * c * d;
        }

        private void BtnOptMult_Click(object sender, EventArgs e)
        {
            double a, b, c, d, ergebnis;
            a = (double)r.Next(1, 120) / 10.0;
            b = (double)r.Next(1, 120) / 10.0;
            c = (double)r.Next(1, 120) / 10.0;
            d = (double)r.Next(1, 120) / 10.0;
            switch ((int)NumOption.Value)
            {
                case 2:
                    ergebnis = Multiplizieren(a, b);
                    LblOption.Text = "Das Produkt aus " + a + " und " + b + "\nergibt " + ergebnis;
                    break;
                case 3:
                    ergebnis = Multiplizieren(a, b, c);
                    LblOption.Text = "Das Produkt aus " + a + ", " + b + "\nund " + c + " ergibt " + ergebnis;
                    break;
                case 4:
                    ergebnis = Multiplizieren(a, b, c, d);
                    LblOption.Text = "Das Produkt aus " + a + ", " + b + ",\n" + c + " und " + d + " ergibt " + ergebnis;
                    break;
            }
        }

        private double Mittelwert(params double[] x)
        {
            if (x.Length == 0)
            {
                return (double)0;
            }
            else
            {
                double summe = x.Sum();
                return summe / (double)x.Length;
            }
        }

        private void BtnMany_Click(object sender, EventArgs e)
        {
            double[] werte = new double[(int)NumMany.Value];
            for (int i = 0; i < werte.Length; i++)
            {
                werte[i] = (double)r.Next(0, 120) / 10.0;
            }
            double mittelwert = 0;
            switch (werte.Length)
            {
                case 0: mittelwert = Mittelwert(); break;
                case 1: mittelwert = Mittelwert(werte[0]); break;
                case 2: mittelwert = Mittelwert(werte[0], werte[1]); break;
                case 3: mittelwert = Mittelwert(werte[0], werte[1], werte[2]);break;
                case 4: mittelwert = Mittelwert(werte[0], werte[1], werte[2], werte[3]); break;
                case 5: mittelwert = Mittelwert(werte[0], werte[1], werte[2], werte[3], werte[4]); break;
                case 6: mittelwert = Mittelwert(werte[0], werte[1], werte[2], werte[3], werte[4], werte[5]); break;
                case 7: mittelwert = Mittelwert(werte[0], werte[1], werte[2], werte[3], werte[4], werte[5], werte[6]); break;
                case 8: mittelwert = Mittelwert(werte[0], werte[1], werte[2], werte[3], werte[4], werte[5], werte[6], werte[7]); break;
                case 9: mittelwert = Mittelwert(werte[0], werte[1], werte[2], werte[3], werte[4], werte[5], werte[6], werte[7], werte[8]); break;
                case 10: mittelwert = Mittelwert(werte[0], werte[1], werte[2], werte[3], werte[4], werte[5], werte[6], werte[7], werte[8], werte[9]); break;
                default: mittelwert = Mittelwert(werte); break;
            }
            LblMany.Text = "Werte: ";
            for (int i = 0; i < werte.Length - 1; i++)
            {
                LblMany.Text += werte[i] + ", ";
            }
            if (werte.Length != 0)
            {
                LblMany.Text += werte[werte.Length - 1] + " ";
            }
            LblMany.Text += "\nMittelwert: " + mittelwert;
        }

        private int GgT(int a, int b)
        {
            int rest;
            if (b == 0)
            {
                int z = a;
                a = b;
                b = z;
            }

            if (b != 0)
            {
                do
                {
                    rest = a % b;
                    a = b;
                    b = rest;
                } while (b != 0);
            }
//            if (a == 0) a = 1;

            return Math.Abs(a);
        }

        private int KgV(int a, int b)
        {
            return Math.Abs(a * b) / GgT(a, b);
        }

        private void Bruch_Addition(ref int ergz, ref int ergn, int az, int an, int bz, int bn)
        {
            // Nenner berechnen
            ergn = KgV(an, bn);
            // Zähler berechnen
            ergz = az * ergn / an + bz * ergn / bn;
        }

        private void Bruch_Subtraktion(ref int ergz, ref int ergn, int az, int an, int bz, int bn)
        {
            // Nenner berechnen
            ergn = KgV(an, bn);
            // Zähler berechnen
            ergz = az * ergn / an - bz * ergn / bn;
        }

        private void Bruch_Multiplikation(ref int ergz, ref int ergn, int az, int an, int bz, int bn)
        {
            // Nenner berechnen
            ergn = an * bn;
            // Zähler berechnen
            ergz = az * bz;
        }

        private void Bruch_Division(ref int ergz, ref int ergn, int az, int an, int bz, int bn)
        {
            // Multiplikation mit Kehrwert
            Bruch_Multiplikation(ref ergz, ref ergn, az, an, bn, bz);
        }

        private void Bruch_Kuerzen(ref int ergz, ref int ergn)
        {
            // ggT zum kürzen ermitteln
            int temp = GgT(ergn, ergz);
            // kürzen
            ergn /= temp;
            ergz /= temp;
        }

        private void Bruch_Gemischt(ref int gemz, ref int gemn, ref int gemg, int ergz, int ergn)
        {
            // Nenner von gemischten Bruch setzen
            gemn = ergn;
            // Zähler ermitteln
            gemz = Math.Abs(ergz % ergn);
            // ganze Zahl ermitteln
            if (ergz > 0) gemg = (ergz - gemz) / ergn;
            else gemg = (ergz + gemz) / ergn;
        }

        private void Brueche_Aktualisieren()
        {
            // Werte zu Variablen zuweisen
            int az = (int)NumBruch1Z.Value;
            int an = (int)NumBruch1N.Value;
            int bz = (int)NumBruch2Z.Value;
            int bn = (int)NumBruch2N.Value;
            // Rechenvariablen initialisieren
            int ergz = 0, ergn = 0;
            int gemn = 0, gemz = 0, gemg = 0;
            string operation = LstBoxBruchOperation.SelectedItem.ToString();
            switch (operation) { 
                case "Addition":
                    // Addieren
                    Bruch_Addition(ref ergz, ref ergn, az, an, bz, bn);
                    LblBruchMitte.Text = "------------   +    ------------    =    ------------  =      -----";
                break;
                case "Subtraktion":
                    // Subtrahieren
                    Bruch_Subtraktion(ref ergz, ref ergn, az, an, bz, bn);
                    LblBruchMitte.Text = "------------   -    ------------    =    ------------  =      -----";
                break;
                case "Multiplikation":
                    // Multiplizieren
                    Bruch_Multiplikation(ref ergz, ref ergn, az, an, bz, bn);
                    LblBruchMitte.Text = "------------   *    ------------    =    ------------  =      -----";
                    break;
                case "Division":
                    if (bz == 0)
                    {
                        NumBruchErgN.Value = 0;
                        NumBruchErgZ.Value = 0;
                        LblBruchGanz.Text = "";
                        LblBruchN.Text = "";
                        LblBruchZ.Text = "";
                        MessageBox.Show("Division durch 0 ist nicht erlaubt");
                        return;
                    }
                    // Dividieren
                    Bruch_Division(ref ergz, ref ergn, az, an, bz, bn);
                    LblBruchMitte.Text = "------------   /    ------------    =    ------------  =      -----";
                break;
                default:
                    // Addieren
                    Bruch_Addition(ref ergz, ref ergn, az, an, bz, bn);
                break;
            }
            // Kürzen
            Bruch_Kuerzen(ref ergz, ref ergn);
            // gemischten Bruch ermitteln
            Bruch_Gemischt(ref gemz, ref gemn, ref gemg, ergz, ergn);
            // errechnete Werte den Feldern zuweisen
            NumBruchErgN.Value = ergn;
            NumBruchErgZ.Value = ergz;
            LblBruchN.Text = "" + gemn;
            LblBruchZ.Text = "" + gemz;
            if (ergz < 0 && gemg == 0) LblBruchGanz.Text = "-";
            else if (gemg != 0) LblBruchGanz.Text = "" + gemg;
            // falls Bruch kleiner als eins: Feld leeren
            else LblBruchGanz.Text = "";

        }

        private void NumBruch1Z_ValueChanged(object sender, EventArgs e)
        {
            Brueche_Aktualisieren();
        }

        private void NumBruch1N_ValueChanged(object sender, EventArgs e)
        {
            Brueche_Aktualisieren();
        }

        private void NumBruch2Z_ValueChanged(object sender, EventArgs e)
        {
            Brueche_Aktualisieren();
        }

        private void NumBruch2N_ValueChanged(object sender, EventArgs e)
        {
            Brueche_Aktualisieren();
        }

        private void LstBoxBruchOperation_SelectedIndexChanged(object sender, EventArgs e)
        {
            Brueche_Aktualisieren();
        }

        private int[] Rechteck(string farbe, int laenge = 1, int breite = 1, string rand = "Linie")
        {
            LblName.Text = "Farbe: " + farbe + ", Länge: " + laenge;
            LblName.Text += ",\nBreite: " + breite + ", Rand: " + rand;
            int[] raus = new int[2];
            raus[0] = laenge * breite;
            raus[1] = laenge * 2 + breite * 2;
            return raus;
        }

        private void BtnNameDef_Click(object sender, EventArgs e)
        {
            int[] back = Rechteck("rot", 4, 6, "Punkte");
            LblName.Text += ",\nFlächeninhalt: " + back[0] + ", Umfang: " + back[1];
        }

        private void BtnNameWirr_Click(object sender, EventArgs e)
        {
            int[] back = Rechteck("rot", rand: "Striche", breite: 2, laenge: 5);
            LblName.Text += ",\nFlächeninhalt: " + back[0] + ", Umfang: " + back[1];
        }

        private void BtnName2_Click(object sender, EventArgs e)
        {
            int[] back = Rechteck("gelb", 7);
            LblName.Text += ",\nFlächeninhalt: " + back[0] + ", Umfang: " + back[1];
        }

        private void BtnName3_Click(object sender, EventArgs e)
        {
            int[] back = Rechteck("blau", rand: "Haarlinie");
            LblName.Text += ",\nFlächeninhalt: " + back[0] + ", Umfang: " + back[1];
        }
    }
}
