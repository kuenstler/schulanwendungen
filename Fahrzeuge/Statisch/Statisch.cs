﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Statisch
{
    public partial class Statisch : Form
    {
        public Statisch()
        {
            InitializeComponent();
        }

        private void BtnStatic_Click(object sender, EventArgs e)
        {
            TZahl x = new TZahl(2.5);
            TZahl p = new TZahl(5);
            double y, r;

            x.maldrei();
            LblAus.Text = x.ausgeben() + "\n" + p.ausgeben();
            y = 4;
            LblAus.Text += "\nZahl: " + y + "\nNach verdopplung: " + TZahl.verdoppeln(y);
            r = 6;
            LblAus.Text += "\nRadius: " + r + "\nFläche: " + r * r * TZahl.pi;
        }
    }
}
