﻿namespace Statisch
{
    partial class Statisch
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnStatic = new System.Windows.Forms.Button();
            this.LblAus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnStatic
            // 
            this.BtnStatic.Location = new System.Drawing.Point(13, 13);
            this.BtnStatic.Name = "BtnStatic";
            this.BtnStatic.Size = new System.Drawing.Size(227, 23);
            this.BtnStatic.TabIndex = 0;
            this.BtnStatic.Text = "Statische Attribute und Methoden anzeigen";
            this.BtnStatic.UseVisualStyleBackColor = true;
            this.BtnStatic.Click += new System.EventHandler(this.BtnStatic_Click);
            // 
            // LblAus
            // 
            this.LblAus.AutoSize = true;
            this.LblAus.Location = new System.Drawing.Point(13, 43);
            this.LblAus.Name = "LblAus";
            this.LblAus.Size = new System.Drawing.Size(16, 13);
            this.LblAus.TabIndex = 1;
            this.LblAus.Text = "...";
            // 
            // Statisch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.LblAus);
            this.Controls.Add(this.BtnStatic);
            this.Name = "Statisch";
            this.Text = "Statisch";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnStatic;
        private System.Windows.Forms.Label LblAus;
    }
}

