﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Statisch
{
    class TZahl
    {
        private double wert;
        private int nummer;
        private static int anzahl = 0;
        public static double pi = 3.1415926;

        public TZahl(double z)
        {
            anzahl += 1;
            nummer = anzahl;
            wert = z;
        }

        public void maldrei()
        {
            wert *= 3;
        }

        public static double verdoppeln(double z)
        {
            return z * 2;
        }

        public string ausgeben()
        {
            return "Objekt Nr. " + nummer + ",\nWert: " + wert;
        }
    }
}
