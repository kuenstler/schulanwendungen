﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Delegates
{
    public partial class Delegates : Form
    {
        private int YPos = 44;
        private int Nr = 1;
        private int Xpos = 12;

        public Delegates()
        {
            InitializeComponent();
        }

        private void BtnNeuerBtn_Click(object sender, EventArgs e)
        {
            Button neuer = new Button();
            neuer.Location = new Point(Xpos, YPos);
            neuer.Size = new Size(26, 26);
            neuer.Text = "" + Nr;

            neuer.Click += new EventHandler(NeuerBtn_Click);
            Controls.Add(neuer);

            YPos += neuer.Size.Height + 6;
            Nr += 1;
            if (YPos + neuer.Size.Height > this.Size.Height - 20)
            {
                Xpos += neuer.Size.Width + 6;
                YPos = 44;
            }
        }
        private void NeuerBtn_Click(object sender, EventArgs e)
        {
            Button senderButton;
            senderButton = sender as Button;
            Controls.Remove(senderButton);

        }
    }
}
