﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fahrzeuge
{
    class TFahrzeug
    {
        //----------Eigenschaften----------
        private int v; //Geschwindigkeit
        private string b; //Bezeichnung
        //----------Methoden---------------

        //ctor4
        public TFahrzeug(string n, int g)
        {
            v = g;
            b = n;
        }
        
        //ctor1
        public TFahrzeug():this("nur Blech",0) //nutzen des vorhandenen Konstruktors ctor 4
        {
           
            MessageBox.Show("lebe ;)");
        }

        //ctor2
        public TFahrzeug(string n)
        {
            v = 0;
            b = n;
            //MessageBox.Show(n+ " lebt");
        }

        //ctor3
        public TFahrzeug(int g)
        {
            v = g;
            b = "nur Blech";
        }

/*        //dtor Aufruf wird vom Carbage Collector gemanagt
        ~TFahrzeug()
        {
           MessageBox.Show("sterbe :o");
        }*/

        //Eigenschaftsmethode Accessor-Methoden
        public int V //für Geschwindigkeit
        {
            get
            {
                return v;
            }
            private set
            {
                if (value > 100) v = 100;
                else if (value < 0) v = 0;
                else v = value;
            }
        }

        //Verarbeitung
        public void beschleunigen(int wert)
        {
            V += wert; //Aufruf der Accessor-Methode set
        }

        public bool Equals(TFahrzeug x)
        {
            bool vergleich = false;
            if (this.b == x.b && this.v == x.v) vergleich = true;
            return vergleich;
        }

        //Ausgabe              
        public virtual string ausgeben()
        {
            string s = "Bezeichnung: " + b +
                       "\nGeschwindigkeit " + v + " km/h\n";
            return s;
        }

        public override string ToString()
        {
            string s = "Bezeichnung: " + b +
                       "\nGeschwindigkeit " + v + " km/h\n";
            return s;
        }

    }

    class TPKW : TFahrzeug
    {
        private int insassen;

        public TPKW()
        {
            insassen = 0;
        }

        public TPKW(string b, int g) : base(b, g)
        {
            if (g == 0) insassen = 0;
            else insassen = 1;
        }

        public TPKW(string b) : base(b)
        {
            insassen = 0;
        }

        public TPKW(int i)
        {
            insassen = i;
        }

        public TPKW(string b, int g, int i) : base(b, g)
        {
            insassen = i;
        }

        public void einsteigen(int anzahl)
        {
            insassen += anzahl;
            if (insassen < 0) insassen = 0;
        }

        public override string ausgeben()
        {
            return "Insassen: " + insassen + "\n" + base.ausgeben();
        }

        public override string ToString()
        {
            return "Insassen: " + insassen + "\n" + base.ausgeben();
        }
    }
}
