﻿namespace Fahrzeuge
{
    partial class Frm_Fahrzeuge
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.btn_FahrzeugeAnzeigen = new System.Windows.Forms.Button();
            this.label_FahrzeugeAnzeigen = new System.Windows.Forms.Label();
            this.btn_VespaAnzeigen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btn_FahrzeugeAnzeigen
            // 
            this.btn_FahrzeugeAnzeigen.Location = new System.Drawing.Point(236, 32);
            this.btn_FahrzeugeAnzeigen.Name = "btn_FahrzeugeAnzeigen";
            this.btn_FahrzeugeAnzeigen.Size = new System.Drawing.Size(168, 34);
            this.btn_FahrzeugeAnzeigen.TabIndex = 0;
            this.btn_FahrzeugeAnzeigen.Text = "Fahrzeuge anzeigen";
            this.btn_FahrzeugeAnzeigen.UseVisualStyleBackColor = true;
            this.btn_FahrzeugeAnzeigen.Click += new System.EventHandler(this.btn_FahrzeugeAnzeigen_Click);
            // 
            // label_FahrzeugeAnzeigen
            // 
            this.label_FahrzeugeAnzeigen.AutoSize = true;
            this.label_FahrzeugeAnzeigen.Location = new System.Drawing.Point(58, 141);
            this.label_FahrzeugeAnzeigen.Name = "label_FahrzeugeAnzeigen";
            this.label_FahrzeugeAnzeigen.Size = new System.Drawing.Size(29, 20);
            this.label_FahrzeugeAnzeigen.TabIndex = 1;
            this.label_FahrzeugeAnzeigen.Text = "[...]";
            // 
            // btn_VespaAnzeigen
            // 
            this.btn_VespaAnzeigen.Location = new System.Drawing.Point(33, 32);
            this.btn_VespaAnzeigen.Name = "btn_VespaAnzeigen";
            this.btn_VespaAnzeigen.Size = new System.Drawing.Size(168, 34);
            this.btn_VespaAnzeigen.TabIndex = 2;
            this.btn_VespaAnzeigen.Text = "Vespa anzeigen";
            this.btn_VespaAnzeigen.UseVisualStyleBackColor = true;
            this.btn_VespaAnzeigen.Click += new System.EventHandler(this.btn_VespaAnzeigen_Click);
            // 
            // Frm_Fahrzeuge
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(433, 477);
            this.Controls.Add(this.btn_VespaAnzeigen);
            this.Controls.Add(this.label_FahrzeugeAnzeigen);
            this.Controls.Add(this.btn_FahrzeugeAnzeigen);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Frm_Fahrzeuge";
            this.Text = "Fahrzeuge";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_FahrzeugeAnzeigen;
        private System.Windows.Forms.Label label_FahrzeugeAnzeigen;
        private System.Windows.Forms.Button btn_VespaAnzeigen;
    }
}

