﻿namespace Fahrzeuge
{
    partial class FrmStart
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnAnzeigenVespa = new System.Windows.Forms.Button();
            this.lblAusgabe = new System.Windows.Forms.Label();
            this.btnAnzeigenFahrzeuge = new System.Windows.Forms.Button();
            this.BtnReferenz = new System.Windows.Forms.Button();
            this.BtnReferenzVergleichen = new System.Windows.Forms.Button();
            this.BtnVergleich = new System.Windows.Forms.Button();
            this.BtnKlasseErmitteln = new System.Windows.Forms.Button();
            this.BtnKlasseVergleichen = new System.Windows.Forms.Button();
            this.BtnPKW = new System.Windows.Forms.Button();
            this.BtnPKWBen = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnAnzeigenVespa
            // 
            this.btnAnzeigenVespa.Location = new System.Drawing.Point(12, 12);
            this.btnAnzeigenVespa.Name = "btnAnzeigenVespa";
            this.btnAnzeigenVespa.Size = new System.Drawing.Size(166, 41);
            this.btnAnzeigenVespa.TabIndex = 0;
            this.btnAnzeigenVespa.Text = "Vespa anzeigen";
            this.btnAnzeigenVespa.UseVisualStyleBackColor = true;
            this.btnAnzeigenVespa.Click += new System.EventHandler(this.btnAnzeigen_Click);
            // 
            // lblAusgabe
            // 
            this.lblAusgabe.AutoSize = true;
            this.lblAusgabe.Location = new System.Drawing.Point(12, 102);
            this.lblAusgabe.Name = "lblAusgabe";
            this.lblAusgabe.Size = new System.Drawing.Size(29, 20);
            this.lblAusgabe.TabIndex = 1;
            this.lblAusgabe.Text = "[...]";
            // 
            // btnAnzeigenFahrzeuge
            // 
            this.btnAnzeigenFahrzeuge.Location = new System.Drawing.Point(184, 11);
            this.btnAnzeigenFahrzeuge.Name = "btnAnzeigenFahrzeuge";
            this.btnAnzeigenFahrzeuge.Size = new System.Drawing.Size(180, 41);
            this.btnAnzeigenFahrzeuge.TabIndex = 2;
            this.btnAnzeigenFahrzeuge.Text = "Fahrzeuge anzeigen";
            this.btnAnzeigenFahrzeuge.UseVisualStyleBackColor = true;
            this.btnAnzeigenFahrzeuge.Click += new System.EventHandler(this.btnAnzeigenFahrzeuge_Click);
            // 
            // BtnReferenz
            // 
            this.BtnReferenz.Location = new System.Drawing.Point(370, 12);
            this.BtnReferenz.Name = "BtnReferenz";
            this.BtnReferenz.Size = new System.Drawing.Size(113, 40);
            this.BtnReferenz.TabIndex = 3;
            this.BtnReferenz.Text = "Referenzen";
            this.BtnReferenz.UseVisualStyleBackColor = true;
            this.BtnReferenz.Click += new System.EventHandler(this.BtnReferenz_Click);
            // 
            // BtnReferenzVergleichen
            // 
            this.BtnReferenzVergleichen.Location = new System.Drawing.Point(489, 13);
            this.BtnReferenzVergleichen.Name = "BtnReferenzVergleichen";
            this.BtnReferenzVergleichen.Size = new System.Drawing.Size(175, 40);
            this.BtnReferenzVergleichen.TabIndex = 4;
            this.BtnReferenzVergleichen.Text = "Referenz vergleichen";
            this.BtnReferenzVergleichen.UseVisualStyleBackColor = true;
            this.BtnReferenzVergleichen.Click += new System.EventHandler(this.BtnReferenzVergleichen_Click);
            // 
            // BtnVergleich
            // 
            this.BtnVergleich.Location = new System.Drawing.Point(12, 59);
            this.BtnVergleich.Name = "BtnVergleich";
            this.BtnVergleich.Size = new System.Drawing.Size(166, 40);
            this.BtnVergleich.TabIndex = 5;
            this.BtnVergleich.Text = "Objekte vergleichen";
            this.BtnVergleich.UseVisualStyleBackColor = true;
            this.BtnVergleich.Click += new System.EventHandler(this.BtnVergleich_Click);
            // 
            // BtnKlasseErmitteln
            // 
            this.BtnKlasseErmitteln.Location = new System.Drawing.Point(185, 59);
            this.BtnKlasseErmitteln.Name = "BtnKlasseErmitteln";
            this.BtnKlasseErmitteln.Size = new System.Drawing.Size(140, 40);
            this.BtnKlasseErmitteln.TabIndex = 6;
            this.BtnKlasseErmitteln.Text = "Klasse ermitteln";
            this.BtnKlasseErmitteln.UseVisualStyleBackColor = true;
            this.BtnKlasseErmitteln.Click += new System.EventHandler(this.BtnKlasseErmitteln_Click);
            // 
            // BtnKlasseVergleichen
            // 
            this.BtnKlasseVergleichen.Location = new System.Drawing.Point(332, 59);
            this.BtnKlasseVergleichen.Name = "BtnKlasseVergleichen";
            this.BtnKlasseVergleichen.Size = new System.Drawing.Size(151, 40);
            this.BtnKlasseVergleichen.TabIndex = 7;
            this.BtnKlasseVergleichen.Text = "Klasse vergleichen";
            this.BtnKlasseVergleichen.UseVisualStyleBackColor = true;
            this.BtnKlasseVergleichen.Click += new System.EventHandler(this.BtnKlasseVergleichen_Click);
            // 
            // BtnPKW
            // 
            this.BtnPKW.Location = new System.Drawing.Point(490, 60);
            this.BtnPKW.Name = "BtnPKW";
            this.BtnPKW.Size = new System.Drawing.Size(75, 39);
            this.BtnPKW.TabIndex = 8;
            this.BtnPKW.Text = "PKW";
            this.BtnPKW.UseVisualStyleBackColor = true;
            this.BtnPKW.Click += new System.EventHandler(this.BtnPKW_Click);
            // 
            // BtnPKWBen
            // 
            this.BtnPKWBen.Location = new System.Drawing.Point(572, 59);
            this.BtnPKWBen.Name = "BtnPKWBen";
            this.BtnPKWBen.Size = new System.Drawing.Size(115, 40);
            this.BtnPKWBen.TabIndex = 9;
            this.BtnPKWBen.Text = "PKW benannt";
            this.BtnPKWBen.UseVisualStyleBackColor = true;
            this.BtnPKWBen.Click += new System.EventHandler(this.BtnPKWBen_Click);
            // 
            // FrmStart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(691, 380);
            this.Controls.Add(this.BtnPKWBen);
            this.Controls.Add(this.BtnPKW);
            this.Controls.Add(this.BtnKlasseVergleichen);
            this.Controls.Add(this.BtnKlasseErmitteln);
            this.Controls.Add(this.BtnVergleich);
            this.Controls.Add(this.BtnReferenzVergleichen);
            this.Controls.Add(this.BtnReferenz);
            this.Controls.Add(this.btnAnzeigenFahrzeuge);
            this.Controls.Add(this.lblAusgabe);
            this.Controls.Add(this.btnAnzeigenVespa);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "FrmStart";
            this.Text = "Fahrzeuganwendung";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnAnzeigenVespa;
        private System.Windows.Forms.Label lblAusgabe;
        private System.Windows.Forms.Button btnAnzeigenFahrzeuge;
        private System.Windows.Forms.Button BtnReferenz;
        private System.Windows.Forms.Button BtnReferenzVergleichen;
        private System.Windows.Forms.Button BtnVergleich;
        private System.Windows.Forms.Button BtnKlasseErmitteln;
        private System.Windows.Forms.Button BtnKlasseVergleichen;
        private System.Windows.Forms.Button BtnPKW;
        private System.Windows.Forms.Button BtnPKWBen;
    }
}

