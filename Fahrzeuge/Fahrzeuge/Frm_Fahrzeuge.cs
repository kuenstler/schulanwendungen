﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fahrzeuge
{
    public partial class Frm_Fahrzeuge : Form
    {
        public Frm_Fahrzeuge()
        {
            InitializeComponent();
        }

        private void btn_FahrzeugeAnzeigen_Click(object sender, EventArgs e)
        {
            //Fahrzeugobjekte erstellen
            TFahrzeug vespa; //Objekt deklariert
            vespa = new TFahrzeug(); //Objekt definiert
            TFahrzeug sperling = new TFahrzeug();
            TFahrzeug simson = new TFahrzeug("Simse");
            TFahrzeug schwalbe = new TFahrzeug("Moped");
            TFahrzeug yamaha = new TFahrzeug(50);
            TFahrzeug honda = new TFahrzeug("Motorrad", 75);

            //Ausgabe
            label_FahrzeugeAnzeigen.Text = "Fahrzeuge\n";
            label_FahrzeugeAnzeigen.Text += sperling.ausgeben() + "\n";
            label_FahrzeugeAnzeigen.Text += simson.ausgeben() + "\n";
            label_FahrzeugeAnzeigen.Text += schwalbe.ausgeben() + "\n";
            label_FahrzeugeAnzeigen.Text += yamaha.ausgeben() + "\n";
            label_FahrzeugeAnzeigen.Text += honda.ausgeben() + "\n";
        }

        private void btn_VespaAnzeigen_Click(object sender, EventArgs e)
        {
            //Fahrzeugobjekte erstellen
            TFahrzeug vespa; //Objekt deklariert
            vespa = new TFahrzeug(); //Objekt definiert
            
            label_FahrzeugeAnzeigen.Text = "Vespa:\n" + vespa.ausgeben();
            vespa.beschleunigen(30); //Gas geben
            label_FahrzeugeAnzeigen.Text += "\n" + vespa.ausgeben();
            vespa.beschleunigen(-20); //bremsen
            label_FahrzeugeAnzeigen.Text += "\n" + vespa.ausgeben();
            vespa.beschleunigen(130); //Gas geben
            label_FahrzeugeAnzeigen.Text += "\n" + vespa.ausgeben();
            vespa.beschleunigen(-40); //bremsen
            label_FahrzeugeAnzeigen.Text += "\n" + vespa.V + "km/h";
        }
    }
}
