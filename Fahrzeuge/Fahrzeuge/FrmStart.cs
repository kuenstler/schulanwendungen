﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Fahrzeuge
{
    public partial class FrmStart : Form
    {
        public FrmStart()
        {
            InitializeComponent();
        }

        private void btnAnzeigen_Click(object sender, EventArgs e)
        {
            //Fahrzeugobjekt(e) erstellen
            TFahrzeug vespa; //Objekt deklariert
            vespa = new TFahrzeug(); //Objekt definiert

            lblAusgabe.Text = "Vespa\n"+vespa.ausgeben();
            vespa.beschleunigen(30); //Gas geben
            lblAusgabe.Text += "\n" + vespa.ausgeben();
            vespa.beschleunigen(-20);//bremsen
            lblAusgabe.Text += "\n" + vespa.ausgeben();
            vespa.beschleunigen(130); //Gas geben
            lblAusgabe.Text += "\n" + vespa.ausgeben();
            vespa.beschleunigen(-40); //bremsen
            lblAusgabe.Text += "\n" + vespa.V + " km/h";

        }

        private void btnAnzeigenFahrzeuge_Click(object sender, EventArgs e)
        {
            //Fahrzeugobjekt(e) erstellen
            TFahrzeug sperling = new TFahrzeug();
            TFahrzeug simson = new TFahrzeug("Simse");
            TFahrzeug schwalbe = new TFahrzeug("Moped");
            TFahrzeug yamaha = new TFahrzeug(50);
            TFahrzeug honda = new TFahrzeug("Motorrad", 75);

            //Ausgabe
            lblAusgabe.Text="Fahrzeuge\n";
            lblAusgabe.Text += sperling.ausgeben() + "\n";
            lblAusgabe.Text += simson.ausgeben() + "\n";
            lblAusgabe.Text += schwalbe.ausgeben() + "\n";
            lblAusgabe.Text += yamaha.ausgeben() + "\n";
            lblAusgabe.Text += honda.ausgeben() + "\n";
        }

        private void BtnReferenz_Click(object sender, EventArgs e)
        {
            TFahrzeug f1 = new TFahrzeug("Moped", 50);
            TFahrzeug f2;
            f2 = f1;
            lblAusgabe.Text = f1.ausgeben() + " / " + f2.ausgeben();
            f1.beschleunigen(35);
            lblAusgabe.Text += f1.ausgeben() + " / " + f2.ausgeben();
        }

        private void BtnReferenzVergleichen_Click(object sender, EventArgs e)
        {
            TFahrzeug f1 = new TFahrzeug("Moped", 35);
            TFahrzeug f2 = new TFahrzeug("Moped", 35);

            if (f1 == f2) lblAusgabe.Text = "Die beiden Objektverweise zeigen auf das selbe Objekt";
            else lblAusgabe.Text = "Die beiden Objektverweise zeigen nicht auf das selbe Objekt";

            f1 = f2;

            if (f1 == f2) lblAusgabe.Text += "\nDie beiden Objektverweise zeigen auf das selbe Objekt";
            else lblAusgabe.Text += "\nDie beiden Objektverweise zeigen nicht auf das selbe Objekt";
        }

        private void BtnVergleich_Click(object sender, EventArgs e)
        {
            TFahrzeug f1 = new TFahrzeug("Moped", 35);
            TFahrzeug f2 = new TFahrzeug("Moped", 35);

            if (f1.Equals(f2)) lblAusgabe.Text = "Die beiden Objekte sind gleich";
            else lblAusgabe.Text = "Die beiden Objekte sind nicht gleich";
        }

        private void BtnKlasseErmitteln_Click(object sender, EventArgs e)
        {
            TFahrzeug f1 = new TFahrzeug("Moped", 35);
            lblAusgabe.Text = "Objekt f1 ist vom Typ " + f1.GetType().ToString();
            lblAusgabe.Text += "\nDie Klasse heißt " + typeof(TFahrzeug);

            lblAusgabe.Text += "\nDer Button ist vom Typ " + BtnKlasseErmitteln.GetType().ToString();
            lblAusgabe.Text += "\nDie Klasse heißt " + typeof(Button);
        }

        private void BtnKlasseVergleichen_Click(object sender, EventArgs e)
        {
            TFahrzeug f1 = new TFahrzeug("Moped", 35);
            if (f1 is TFahrzeug) lblAusgabe.Text = "f1 ist ein Fahrzeug";
        }

        private void BtnPKW_Click(object sender, EventArgs e)
        {
            TFahrzeug vespa = new TFahrzeug();
            TPKW fiat = new TPKW();

            vespa.beschleunigen(35);
            lblAusgabe.Text = vespa.ausgeben();

            lblAusgabe.Text += "\n" + fiat.ausgeben();
            fiat.einsteigen(3);
            fiat.beschleunigen(30);
            lblAusgabe.Text += "\n" + fiat.ausgeben();
        }

        private void BtnPKWBen_Click(object sender, EventArgs e)
        {
            TPKW fiat = new TPKW("Limousine", 50, 2);
            TPKW peugeot = new TPKW();
            lblAusgabe.Text = fiat.ausgeben() + peugeot.ausgeben();
            fiat.beschleunigen(130);
            peugeot.beschleunigen(400);
            lblAusgabe.Text += fiat.ausgeben() + peugeot.ausgeben();
        }
    }
}
