﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace FahrzeugeKlasse
{
    public partial class FahrzeugVerwaltung : Form
    {
        ManualResetEvent warteAufButton = new ManualResetEvent(false);
        TFahrzeug[] fahrzeuge = new TFahrzeug[0];
        TFahrzeug[] ausgewaehlt = new TFahrzeug[0];
        TFahrzeug aktuell;
        int anzahl = 0;

        public FahrzeugVerwaltung()
        {
            InitializeComponent();
        }

        private void AnzahlAktualisieren()
        {
            LblInfo.Text = "Anzahl der Fahrzeuge gesammt: " + anzahl;
        }

        private void BtnFahrAnlegen_Click(object sender, EventArgs e)
        {
            TFahrzeug fahrzeug = new TFahrzeug(TxtBoxFah.Text, (int)NumFahrGesch.Value);
            Array.Resize(ref fahrzeuge, fahrzeuge.Length + 1);
            fahrzeuge[fahrzeuge.Length - 1] = fahrzeug;
            anzahl += 1;
            AnzahlAktualisieren();
        }

        private void BtnPKWAnlegen_Click(object sender, EventArgs e)
        {
            TPKW fahrzeug = new TPKW(TxbPkw.Text, (int)NumPKWGesch.Value, (int)NumInsassen.Value);
            Array.Resize(ref fahrzeuge, fahrzeuge.Length + 1);
            fahrzeuge[fahrzeuge.Length - 1] = fahrzeug;
            anzahl += 1;
            AnzahlAktualisieren();
        }

        private void BtnAuswahl_Click(object sender, EventArgs e)
        {
            for (int i = GrpAus.Controls.Count - 1; i >= 0; --i)
            {
                GrpAus.Controls.Remove(GrpAus.Controls[i]);
            }

            ausgewaehlt = new TFahrzeug[0];

            GrpAus.Visible = true;
            GrpAus.Height = 20;

            int hoehe = 13;
            foreach (TFahrzeug fahrzeug in fahrzeuge)
            {
                TCheckBox neu = new TCheckBox(fahrzeug);
                neu.Location = new Point(18, hoehe);
                neu.Size = new Size(200, 45);
                neu.Text = fahrzeug.ausgeben().ausgeben();
                neu.Checked = false;

                neu.CheckedChanged += new EventHandler(ChkFahrzeug_Click);
                GrpAus.Controls.Add(neu);
                GrpAus.Height += neu.Size.Height + 5;

                hoehe += neu.Size.Height + 5;
            }
        }

        private void ChkFahrzeug_Click(object sender, EventArgs e)
        {
            TCheckBox sender_check;
            sender_check = sender as TCheckBox;
            if (sender_check.Checked)
            {
                Array.Resize(ref ausgewaehlt, ausgewaehlt.Length + 1);
                ausgewaehlt[ausgewaehlt.Length - 1] = sender_check.getFahrzeug();
            }
            else
            {
                int position = Array.IndexOf(ausgewaehlt, sender_check.getFahrzeug());
                for (int i = position; i < ausgewaehlt.Length; i++)
                {
                    ausgewaehlt[i] = ausgewaehlt[i + 1];
                }
                Array.Resize(ref ausgewaehlt, ausgewaehlt.Length - 1);
            }
        }

        private void BtnAnz_Click(object sender, EventArgs e)
        {
            foreach(TFahrzeug fahrzeug in ausgewaehlt)
            {
                BtnAnz.Enabled = false;
                GrpAnders.Visible = true;
                TFahrzeugType a = fahrzeug.ausgeben();
                if (a.insassen >= 0)
                {
                    LblAndersIns.Visible = true;
                    NumAndersIns.Visible = true;
                    NumAndersIns.Value = a.insassen;
                }
                else
                {
                    LblAndersIns.Visible = false;
                    NumAndersIns.Visible = false;
                }
                NumAndersGesch.Value = a.geschwindigkeit;
                TxtBoxAnders.Text = a.bezeichnung;
                aktuell = fahrzeug;
            }
        }

        private void BtnWeg_Click(object sender, EventArgs e)
        {
            if (aktuell != null)
            {
                int index = Array.IndexOf(fahrzeuge, aktuell);
                for (int i = index; i < fahrzeuge.Length - 1; i++)
                {
                    fahrzeuge[i] = fahrzeuge[i + 1];
                }
                Array.Resize(ref fahrzeuge, fahrzeuge.Length - 1);
                GrpAnders.Visible = false;
                GrpAus.Visible = false;
                aktuell = null;
                anzahl -= 1;
                AnzahlAktualisieren();
            }
            BtnAnz.Enabled = true;
        }

        private void BtnAnders_Click(object sender, EventArgs e)
        {
            if (aktuell != null)
            {
                TFahrzeugType ist = aktuell.ausgeben();
                aktuell.beschleunigen((int)NumAndersGesch.Value - ist.geschwindigkeit);
                aktuell.benennen(TxtBoxAnders.Text);
                if (ist.insassen >= 0)
                {
                    aktuell.einsteigen((int)NumAndersIns.Value - ist.insassen);
                }
                GrpAnders.Visible = false;
                GrpAus.Visible = false;
                aktuell = null;
            }
            BtnAnz.Enabled = true;
        }
    }
}
