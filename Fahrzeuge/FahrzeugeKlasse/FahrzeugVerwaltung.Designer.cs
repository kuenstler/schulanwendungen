﻿namespace FahrzeugeKlasse
{
    partial class FahrzeugVerwaltung
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.GrpFahr = new System.Windows.Forms.GroupBox();
            this.BtnFahrAnlegen = new System.Windows.Forms.Button();
            this.NumFahrGesch = new System.Windows.Forms.NumericUpDown();
            this.LblFahrGesch = new System.Windows.Forms.Label();
            this.TxtBoxFah = new System.Windows.Forms.TextBox();
            this.LblFahrBez = new System.Windows.Forms.Label();
            this.GrpPkw = new System.Windows.Forms.GroupBox();
            this.BtnPKWAnlegen = new System.Windows.Forms.Button();
            this.NumInsassen = new System.Windows.Forms.NumericUpDown();
            this.LblPKWIns = new System.Windows.Forms.Label();
            this.NumPKWGesch = new System.Windows.Forms.NumericUpDown();
            this.TxbPkw = new System.Windows.Forms.TextBox();
            this.LblPKWGesch = new System.Windows.Forms.Label();
            this.LblPKWBez = new System.Windows.Forms.Label();
            this.GrpInfo = new System.Windows.Forms.GroupBox();
            this.LblInfo = new System.Windows.Forms.Label();
            this.BtnAuswahl = new System.Windows.Forms.Button();
            this.BtnAnz = new System.Windows.Forms.Button();
            this.GrpAnders = new System.Windows.Forms.GroupBox();
            this.LblAndersBez = new System.Windows.Forms.Label();
            this.LblAndersIns = new System.Windows.Forms.Label();
            this.LblAndersGesch = new System.Windows.Forms.Label();
            this.BtnWeg = new System.Windows.Forms.Button();
            this.BtnAnders = new System.Windows.Forms.Button();
            this.TxtBoxAnders = new System.Windows.Forms.TextBox();
            this.NumAndersGesch = new System.Windows.Forms.NumericUpDown();
            this.NumAndersIns = new System.Windows.Forms.NumericUpDown();
            this.GrpAus = new System.Windows.Forms.GroupBox();
            this.GrpFahr.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumFahrGesch)).BeginInit();
            this.GrpPkw.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumInsassen)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPKWGesch)).BeginInit();
            this.GrpInfo.SuspendLayout();
            this.GrpAnders.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumAndersGesch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAndersIns)).BeginInit();
            this.SuspendLayout();
            // 
            // GrpFahr
            // 
            this.GrpFahr.Controls.Add(this.BtnFahrAnlegen);
            this.GrpFahr.Controls.Add(this.NumFahrGesch);
            this.GrpFahr.Controls.Add(this.LblFahrGesch);
            this.GrpFahr.Controls.Add(this.TxtBoxFah);
            this.GrpFahr.Controls.Add(this.LblFahrBez);
            this.GrpFahr.Location = new System.Drawing.Point(13, 13);
            this.GrpFahr.Name = "GrpFahr";
            this.GrpFahr.Size = new System.Drawing.Size(255, 83);
            this.GrpFahr.TabIndex = 0;
            this.GrpFahr.TabStop = false;
            this.GrpFahr.Text = "Fahrzeug";
            // 
            // BtnFahrAnlegen
            // 
            this.BtnFahrAnlegen.Location = new System.Drawing.Point(165, 20);
            this.BtnFahrAnlegen.Name = "BtnFahrAnlegen";
            this.BtnFahrAnlegen.Size = new System.Drawing.Size(75, 52);
            this.BtnFahrAnlegen.TabIndex = 4;
            this.BtnFahrAnlegen.Text = "Anlegen";
            this.BtnFahrAnlegen.UseVisualStyleBackColor = true;
            this.BtnFahrAnlegen.Click += new System.EventHandler(this.BtnFahrAnlegen_Click);
            // 
            // NumFahrGesch
            // 
            this.NumFahrGesch.Location = new System.Drawing.Point(102, 52);
            this.NumFahrGesch.Name = "NumFahrGesch";
            this.NumFahrGesch.Size = new System.Drawing.Size(43, 20);
            this.NumFahrGesch.TabIndex = 3;
            this.NumFahrGesch.Value = new decimal(new int[] {
            50,
            0,
            0,
            0});
            // 
            // LblFahrGesch
            // 
            this.LblFahrGesch.AutoSize = true;
            this.LblFahrGesch.Location = new System.Drawing.Point(7, 54);
            this.LblFahrGesch.Name = "LblFahrGesch";
            this.LblFahrGesch.Size = new System.Drawing.Size(88, 13);
            this.LblFahrGesch.TabIndex = 2;
            this.LblFahrGesch.Text = "Geschwindigkeit:";
            // 
            // TxtBoxFah
            // 
            this.TxtBoxFah.Location = new System.Drawing.Point(45, 20);
            this.TxtBoxFah.Name = "TxtBoxFah";
            this.TxtBoxFah.Size = new System.Drawing.Size(100, 20);
            this.TxtBoxFah.TabIndex = 1;
            // 
            // LblFahrBez
            // 
            this.LblFahrBez.AutoSize = true;
            this.LblFahrBez.Location = new System.Drawing.Point(7, 20);
            this.LblFahrBez.Name = "LblFahrBez";
            this.LblFahrBez.Size = new System.Drawing.Size(31, 13);
            this.LblFahrBez.TabIndex = 0;
            this.LblFahrBez.Text = "Bez.:";
            // 
            // GrpPkw
            // 
            this.GrpPkw.Controls.Add(this.BtnPKWAnlegen);
            this.GrpPkw.Controls.Add(this.NumInsassen);
            this.GrpPkw.Controls.Add(this.LblPKWIns);
            this.GrpPkw.Controls.Add(this.NumPKWGesch);
            this.GrpPkw.Controls.Add(this.TxbPkw);
            this.GrpPkw.Controls.Add(this.LblPKWGesch);
            this.GrpPkw.Controls.Add(this.LblPKWBez);
            this.GrpPkw.Location = new System.Drawing.Point(13, 103);
            this.GrpPkw.Name = "GrpPkw";
            this.GrpPkw.Size = new System.Drawing.Size(255, 84);
            this.GrpPkw.TabIndex = 1;
            this.GrpPkw.TabStop = false;
            this.GrpPkw.Text = "PKW";
            // 
            // BtnPKWAnlegen
            // 
            this.BtnPKWAnlegen.Location = new System.Drawing.Point(165, 53);
            this.BtnPKWAnlegen.Name = "BtnPKWAnlegen";
            this.BtnPKWAnlegen.Size = new System.Drawing.Size(75, 23);
            this.BtnPKWAnlegen.TabIndex = 11;
            this.BtnPKWAnlegen.Text = "Anlegen";
            this.BtnPKWAnlegen.UseVisualStyleBackColor = true;
            this.BtnPKWAnlegen.Click += new System.EventHandler(this.BtnPKWAnlegen_Click);
            // 
            // NumInsassen
            // 
            this.NumInsassen.Location = new System.Drawing.Point(208, 18);
            this.NumInsassen.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.NumInsassen.Name = "NumInsassen";
            this.NumInsassen.Size = new System.Drawing.Size(47, 20);
            this.NumInsassen.TabIndex = 10;
            // 
            // LblPKWIns
            // 
            this.LblPKWIns.AutoSize = true;
            this.LblPKWIns.Location = new System.Drawing.Point(152, 20);
            this.LblPKWIns.Name = "LblPKWIns";
            this.LblPKWIns.Size = new System.Drawing.Size(49, 13);
            this.LblPKWIns.TabIndex = 9;
            this.LblPKWIns.Text = "Insassen";
            // 
            // NumPKWGesch
            // 
            this.NumPKWGesch.Location = new System.Drawing.Point(102, 51);
            this.NumPKWGesch.Maximum = new decimal(new int[] {
            280,
            0,
            0,
            0});
            this.NumPKWGesch.Name = "NumPKWGesch";
            this.NumPKWGesch.Size = new System.Drawing.Size(43, 20);
            this.NumPKWGesch.TabIndex = 8;
            this.NumPKWGesch.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // TxbPkw
            // 
            this.TxbPkw.Location = new System.Drawing.Point(45, 19);
            this.TxbPkw.Name = "TxbPkw";
            this.TxbPkw.Size = new System.Drawing.Size(100, 20);
            this.TxbPkw.TabIndex = 6;
            // 
            // LblPKWGesch
            // 
            this.LblPKWGesch.AutoSize = true;
            this.LblPKWGesch.Location = new System.Drawing.Point(7, 53);
            this.LblPKWGesch.Name = "LblPKWGesch";
            this.LblPKWGesch.Size = new System.Drawing.Size(88, 13);
            this.LblPKWGesch.TabIndex = 7;
            this.LblPKWGesch.Text = "Geschwindigkeit:";
            // 
            // LblPKWBez
            // 
            this.LblPKWBez.AutoSize = true;
            this.LblPKWBez.Location = new System.Drawing.Point(7, 19);
            this.LblPKWBez.Name = "LblPKWBez";
            this.LblPKWBez.Size = new System.Drawing.Size(31, 13);
            this.LblPKWBez.TabIndex = 5;
            this.LblPKWBez.Text = "Bez.:";
            // 
            // GrpInfo
            // 
            this.GrpInfo.Controls.Add(this.LblInfo);
            this.GrpInfo.Location = new System.Drawing.Point(275, 13);
            this.GrpInfo.Name = "GrpInfo";
            this.GrpInfo.Size = new System.Drawing.Size(200, 72);
            this.GrpInfo.TabIndex = 2;
            this.GrpInfo.TabStop = false;
            this.GrpInfo.Text = "Info";
            // 
            // LblInfo
            // 
            this.LblInfo.AutoSize = true;
            this.LblInfo.Location = new System.Drawing.Point(7, 20);
            this.LblInfo.Name = "LblInfo";
            this.LblInfo.Size = new System.Drawing.Size(161, 13);
            this.LblInfo.TabIndex = 0;
            this.LblInfo.Text = "Anzahl der Fahrzeuge gesammt: ";
            // 
            // BtnAuswahl
            // 
            this.BtnAuswahl.Location = new System.Drawing.Point(13, 194);
            this.BtnAuswahl.Name = "BtnAuswahl";
            this.BtnAuswahl.Size = new System.Drawing.Size(145, 23);
            this.BtnAuswahl.TabIndex = 3;
            this.BtnAuswahl.Text = "Fahrzeuge auswählen";
            this.BtnAuswahl.UseVisualStyleBackColor = true;
            this.BtnAuswahl.Click += new System.EventHandler(this.BtnAuswahl_Click);
            // 
            // BtnAnz
            // 
            this.BtnAnz.Location = new System.Drawing.Point(178, 193);
            this.BtnAnz.Name = "BtnAnz";
            this.BtnAnz.Size = new System.Drawing.Size(265, 23);
            this.BtnAnz.TabIndex = 4;
            this.BtnAnz.Text = "ausgewählte Fahrzeuge übernehmen und anzeigen";
            this.BtnAnz.UseVisualStyleBackColor = true;
            this.BtnAnz.Click += new System.EventHandler(this.BtnAnz_Click);
            // 
            // GrpAnders
            // 
            this.GrpAnders.Controls.Add(this.LblAndersBez);
            this.GrpAnders.Controls.Add(this.LblAndersIns);
            this.GrpAnders.Controls.Add(this.LblAndersGesch);
            this.GrpAnders.Controls.Add(this.BtnWeg);
            this.GrpAnders.Controls.Add(this.BtnAnders);
            this.GrpAnders.Controls.Add(this.TxtBoxAnders);
            this.GrpAnders.Controls.Add(this.NumAndersGesch);
            this.GrpAnders.Controls.Add(this.NumAndersIns);
            this.GrpAnders.Location = new System.Drawing.Point(250, 240);
            this.GrpAnders.Name = "GrpAnders";
            this.GrpAnders.Size = new System.Drawing.Size(245, 85);
            this.GrpAnders.TabIndex = 2;
            this.GrpAnders.TabStop = false;
            this.GrpAnders.Text = "Ändern";
            this.GrpAnders.Visible = false;
            // 
            // LblAndersBez
            // 
            this.LblAndersBez.AutoSize = true;
            this.LblAndersBez.Location = new System.Drawing.Point(7, 20);
            this.LblAndersBez.Name = "LblAndersBez";
            this.LblAndersBez.Size = new System.Drawing.Size(28, 13);
            this.LblAndersBez.TabIndex = 0;
            this.LblAndersBez.Text = "Bez:";
            // 
            // LblAndersIns
            // 
            this.LblAndersIns.AutoSize = true;
            this.LblAndersIns.Location = new System.Drawing.Point(7, 60);
            this.LblAndersIns.Name = "LblAndersIns";
            this.LblAndersIns.Size = new System.Drawing.Size(52, 13);
            this.LblAndersIns.TabIndex = 0;
            this.LblAndersIns.Text = "Insassen:";
            // 
            // LblAndersGesch
            // 
            this.LblAndersGesch.AutoSize = true;
            this.LblAndersGesch.Location = new System.Drawing.Point(7, 40);
            this.LblAndersGesch.Name = "LblAndersGesch";
            this.LblAndersGesch.Size = new System.Drawing.Size(88, 13);
            this.LblAndersGesch.TabIndex = 0;
            this.LblAndersGesch.Text = "Geschwindigkeit:";
            // 
            // BtnWeg
            // 
            this.BtnWeg.Location = new System.Drawing.Point(190, 7);
            this.BtnWeg.Name = "BtnWeg";
            this.BtnWeg.Size = new System.Drawing.Size(50, 23);
            this.BtnWeg.TabIndex = 5;
            this.BtnWeg.Text = "Löschen";
            this.BtnWeg.UseVisualStyleBackColor = true;
            this.BtnWeg.Click += new System.EventHandler(this.BtnWeg_Click);
            // 
            // BtnAnders
            // 
            this.BtnAnders.Location = new System.Drawing.Point(190, 30);
            this.BtnAnders.Name = "BtnAnders";
            this.BtnAnders.Size = new System.Drawing.Size(50, 23);
            this.BtnAnders.TabIndex = 6;
            this.BtnAnders.Text = "Ändern";
            this.BtnAnders.UseVisualStyleBackColor = true;
            this.BtnAnders.Click += new System.EventHandler(this.BtnAnders_Click);
            // 
            // TxtBoxAnders
            // 
            this.TxtBoxAnders.Location = new System.Drawing.Point(45, 20);
            this.TxtBoxAnders.Name = "TxtBoxAnders";
            this.TxtBoxAnders.Size = new System.Drawing.Size(100, 20);
            this.TxtBoxAnders.TabIndex = 1;
            // 
            // NumAndersGesch
            // 
            this.NumAndersGesch.Location = new System.Drawing.Point(102, 40);
            this.NumAndersGesch.Maximum = new decimal(new int[] {
            280,
            0,
            0,
            0});
            this.NumAndersGesch.Name = "NumAndersGesch";
            this.NumAndersGesch.Size = new System.Drawing.Size(43, 20);
            this.NumAndersGesch.TabIndex = 8;
            this.NumAndersGesch.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // NumAndersIns
            // 
            this.NumAndersIns.Location = new System.Drawing.Point(60, 60);
            this.NumAndersIns.Maximum = new decimal(new int[] {
            9,
            0,
            0,
            0});
            this.NumAndersIns.Name = "NumAndersIns";
            this.NumAndersIns.Size = new System.Drawing.Size(43, 20);
            this.NumAndersIns.TabIndex = 8;
            this.NumAndersIns.Value = new decimal(new int[] {
            9,
            0,
            0,
            0});
            // 
            // GrpAus
            // 
            this.GrpAus.Location = new System.Drawing.Point(13, 240);
            this.GrpAus.Name = "GrpAus";
            this.GrpAus.Size = new System.Drawing.Size(230, 20);
            this.GrpAus.TabIndex = 2;
            this.GrpAus.TabStop = false;
            this.GrpAus.Text = "Ausgabe";
            this.GrpAus.Visible = false;
            // 
            // FahrzeugVerwaltung
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(500, 400);
            this.Controls.Add(this.BtnAnz);
            this.Controls.Add(this.BtnAuswahl);
            this.Controls.Add(this.GrpInfo);
            this.Controls.Add(this.GrpPkw);
            this.Controls.Add(this.GrpFahr);
            this.Controls.Add(this.GrpAnders);
            this.Controls.Add(this.GrpAus);
            this.Name = "FahrzeugVerwaltung";
            this.Text = "Fahrzeug Verwaltung";
            this.GrpFahr.ResumeLayout(false);
            this.GrpFahr.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumFahrGesch)).EndInit();
            this.GrpPkw.ResumeLayout(false);
            this.GrpPkw.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumInsassen)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPKWGesch)).EndInit();
            this.GrpInfo.ResumeLayout(false);
            this.GrpInfo.PerformLayout();
            this.GrpAnders.ResumeLayout(false);
            this.GrpAnders.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.NumAndersGesch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumAndersIns)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GrpFahr;
        private System.Windows.Forms.Button BtnFahrAnlegen;
        private System.Windows.Forms.NumericUpDown NumFahrGesch;
        private System.Windows.Forms.Label LblFahrGesch;
        private System.Windows.Forms.TextBox TxtBoxFah;
        private System.Windows.Forms.Label LblFahrBez;
        private System.Windows.Forms.GroupBox GrpPkw;
        private System.Windows.Forms.Button BtnPKWAnlegen;
        private System.Windows.Forms.NumericUpDown NumInsassen;
        private System.Windows.Forms.Label LblPKWIns;
        private System.Windows.Forms.NumericUpDown NumPKWGesch;
        private System.Windows.Forms.TextBox TxbPkw;
        private System.Windows.Forms.Label LblPKWGesch;
        private System.Windows.Forms.Label LblPKWBez;
        private System.Windows.Forms.GroupBox GrpInfo;
        private System.Windows.Forms.Label LblInfo;
        private System.Windows.Forms.Button BtnAuswahl;
        private System.Windows.Forms.Button BtnAnz;
        private System.Windows.Forms.GroupBox GrpAnders;
        private System.Windows.Forms.Label LblAndersGesch;
        private System.Windows.Forms.NumericUpDown NumAndersGesch;
        private System.Windows.Forms.Label LblAndersIns;
        private System.Windows.Forms.NumericUpDown NumAndersIns;
        private System.Windows.Forms.Label LblAndersBez;
        private System.Windows.Forms.TextBox TxtBoxAnders;
        private System.Windows.Forms.Button BtnWeg;
        private System.Windows.Forms.Button BtnAnders;
        private System.Windows.Forms.GroupBox GrpAus;
    }
}

