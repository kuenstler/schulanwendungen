﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FahrzeugeKlasse
{
    class TFahrzeugType
    {
        public int geschwindigkeit;
        public int insassen;
        public string bezeichnung;

        public TFahrzeugType(string b, int g)
        {
            geschwindigkeit = g;
            insassen = -1;
            bezeichnung = b;
        }

        public string ausgeben()
        {
            string ausgabe = "";
            if (insassen != -1)
            {
                ausgabe = "Insassen: " + insassen + "\n";
            }
            ausgabe += "Bezeichnung: " + bezeichnung;
            ausgabe += "\nGeschwindigkeit: " + geschwindigkeit + "km/h\n";
            return ausgabe;
        }

        public void Insassen(int i)
        {
            insassen = i;
        }
    }

    class TFahrzeug
    {
        //----------Eigenschaften----------
        private int v; //Geschwindigkeit
        private string b; //Bezeichnung
        private static int a; //Anzahl
        //----------Methoden---------------

        //ctor4
        public TFahrzeug(string n, int g)
        {
            v = g;
            b = n;
            a += 1;
        }

        //dtor Aufruf wird vom Carbage Collector gemanagt
        ~TFahrzeug()
        {
            a -= 1;
        }

        //Eigenschaftsmethode Accessor-Methoden
        public int V //für Geschwindigkeit
        {
            get
            {
                return v;
            }
            private set
            {
                if (value > 280) v = 280;
                else if (value < 0) v = 0;
                else v = value;
            }
        }

        //Verarbeitung
        public void beschleunigen(int wert)
        {
            V += wert; //Aufruf der Accessor-Methode set
        }

        public bool Equals(TFahrzeug x)
        {
            bool vergleich = false;
            if (this.b == x.b && this.v == x.v) vergleich = true;
            return vergleich;
        }

        public void benennen(string n)
        {
            b = n;
        }

        public virtual void einsteigen(int i)
        {

        }

        //Ausgabe              
        public virtual TFahrzeugType ausgeben()
        {
            TFahrzeugType a = new TFahrzeugType(b, v);
            return a;
        }
    }

    class TPKW : TFahrzeug
    {
        private int insassen;

        public TPKW(string b, int g, int i) : base(b, g)
        {
            insassen = i;
        }

        public override void einsteigen(int anzahl)
        {
            insassen += anzahl;
            if (insassen < 0) insassen = 0;
        }

        public override TFahrzeugType ausgeben()
        {
            TFahrzeugType a = base.ausgeben();
            a.Insassen(insassen);
            return a;
        }
    }

    class TCheckBox : CheckBox
    {
        TFahrzeug fahrzeug;
        public TCheckBox(TFahrzeug fahrzeug)
        {
            this.fahrzeug = fahrzeug;
        }

        public TFahrzeug getFahrzeug()
        {
            return fahrzeug;
        }
    }
}
