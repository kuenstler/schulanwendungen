﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Flaechen
{
    public partial class Form1 : Form
    {
        TFlaechen[] flaechen = new TFlaechen[0];// Feld zum Speichern der Geoobjekte
        TKoerper[] koerper = new TKoerper[0];// Feld zum Speichern der Geoobjekte
        // Wenn Eintrag eingefügt werden soll, wird das Feld vergrößert
        public Form1()
        {
            InitializeComponent();
        }

        private void RadBtnKreis_CheckedChanged(object sender, EventArgs e)
        {
            LblE1.Text = "Radius in cm";
            LblE1.Visible = true;
            TxbE1.Visible = true;
            LblE2.Visible = false;
            TxbE2.Visible = false;
            LblE3.Visible = false;
            TxbE3.Visible = false;
            LblE4.Visible = false;
            TxbE4.Visible = false;
        }

        private void RadBtnDreieck_CheckedChanged(object sender, EventArgs e)
        {
            LblE1.Text = "Seite 1 in cm";
            LblE1.Visible = true;
            TxbE1.Visible = true;
            LblE2.Text = "Seite 2 in cm";
            LblE2.Visible = true;
            TxbE2.Visible = true;
            LblE3.Text = "Seite 3 in cm";
            LblE3.Visible = true;
            TxbE3.Visible = true;
            LblE4.Visible = false;
            TxbE4.Visible = false;
        }

        private void RadBtnQuadrat_CheckedChanged(object sender, EventArgs e)
        {
            LblE1.Text = "Seite 1 in cm";
            LblE1.Visible = true;
            TxbE1.Visible = true;
            LblE2.Visible = false;
            TxbE2.Visible = false;
            LblE3.Visible = false;
            TxbE3.Visible = false;
            LblE4.Visible = false;
            TxbE4.Visible = false;
        }

        private void RadBtnRechteck_CheckedChanged(object sender, EventArgs e)
        {
            LblE1.Text = "Seite 1 in cm";
            LblE1.Visible = true;
            TxbE1.Visible = true;
            LblE2.Text = "Seite 2 in cm";
            LblE2.Visible = true;
            TxbE2.Visible = true;
            LblE3.Visible = false;
            TxbE3.Visible = false;
            LblE4.Visible = false;
            TxbE4.Visible = false;
        }

        private void RadBtnZylinder_CheckedChanged(object sender, EventArgs e)
        {
            LblE1.Text = "Radius in cm";
            LblE1.Visible = true;
            TxbE1.Visible = true;
            LblE2.Text = "Höhe in cm";
            LblE2.Visible = true;
            TxbE2.Visible = true;
            LblE3.Visible = false;
            TxbE3.Visible = false;
            LblE4.Visible = false;
            TxbE4.Visible = false;
        }

        private void RadBtnDreiecksaeule_CheckedChanged(object sender, EventArgs e)
        {
            LblE1.Text = "Seite 1 in cm";
            LblE1.Visible = true;
            TxbE1.Visible = true;
            LblE2.Text = "Seite 2 in cm";
            LblE2.Visible = true;
            TxbE2.Visible = true;
            LblE3.Text = "Seite 3 in cm";
            LblE3.Visible = true;
            TxbE3.Visible = true;
            LblE4.Text = "Höhe in cm";
            LblE4.Visible = true;
            TxbE4.Visible = true;
        }

        private void RadBtnQuadratsaeule_CheckedChanged(object sender, EventArgs e)
        {
            LblE1.Text = "Seite 1 in cm";
            LblE1.Visible = true;
            TxbE1.Visible = true;
            LblE2.Text = "Höhe in cm";
            LblE2.Visible = true;
            TxbE2.Visible = true;
            LblE3.Visible = false;
            TxbE3.Visible = false;
            LblE4.Visible = false;
            TxbE4.Visible = false;
        }

        private void RadBtnRechtecksaeule_CheckedChanged(object sender, EventArgs e)
        {
            LblE1.Text = "Seite 1 in cm";
            LblE1.Visible = true;
            TxbE1.Visible = true;
            LblE2.Text = "Seite 2 in cm";
            LblE2.Visible = true;
            TxbE2.Visible = true;
            LblE3.Text = "Höhe in cm";
            LblE3.Visible = true;
            TxbE3.Visible = true;
            LblE4.Visible = false;
            TxbE4.Visible = false;
        }

        private void RadBtnWuerfel_CheckedChanged(object sender, EventArgs e)
        {
            LblE1.Text = "Seite 1 in cm";
            LblE1.Visible = true;
            TxbE1.Visible = true;
            LblE2.Visible = false;
            TxbE2.Visible = false;
            LblE3.Visible = false;
            TxbE3.Visible = false;
            LblE4.Visible = false;
            TxbE4.Visible = false;
        }

        private void BtnFertig_Click(object sender, EventArgs e)
        {
            bool flaeche = true;
            TFlaechen f;
            TKoerper k = new TWuerfel(new TQuadrat(1));
            try
            {
                if (RadBtnDreieck.Checked && TxbE1.Text != "" && TxbE2.Text != "" && TxbE3.Text != "")
                {
                    double a = Convert.ToDouble(TxbE1.Text);
                    double b = Convert.ToDouble(TxbE2.Text);
                    double c = Convert.ToDouble(TxbE3.Text);
                    if (a + b > c && a + c > b && b + c > a)
                    {
                        f = new TDreieck(a, b, c);
                    }
                    else
                    {
                        MessageBox.Show("Die angegebenen Seiten ergeben kein Dreieck");
                        return;
                    }
                }

                else if (RadBtnDreiecksaeule.Checked && TxbE1.Text != "" && TxbE2.Text != "" && TxbE3.Text != "" && TxbE4.Text != "")
                {
                    double a = Convert.ToDouble(TxbE1.Text);
                    double b = Convert.ToDouble(TxbE2.Text);
                    double c = Convert.ToDouble(TxbE3.Text);
                    double h = Convert.ToDouble(TxbE4.Text);
                    if (a + b > c && a + c > b && b + c > a)
                    {
                        TDreieck g = new TDreieck(a, b, c);
                        k = new TDreiecksaeule(g, h);
                        flaeche = false;
                        f = g;
                    }
                    else
                    {
                        MessageBox.Show("Die angegebenen Seiten ergeben kein Dreieck");
                        return;
                    }
                }

                else if (RadBtnKreis.Checked && TxbE1.Text != "")
                {
                    double r = Convert.ToDouble(TxbE1.Text);
                    f = new TKreis(r);
                }

                else if (RadBtnZylinder.Checked && TxbE1.Text != "" && TxbE2.Text != "")
                {
                    double r = Convert.ToDouble(TxbE1.Text);
                    double h = Convert.ToDouble(TxbE2.Text);
                    TKreis g = new TKreis(r);
                    k = new TZylinder(g, h);
                    f = g;
                    flaeche = false;
                }

                else if (RadBtnQuadrat.Checked && TxbE1.Text != "")
                {
                    double a = Convert.ToDouble(TxbE1.Text);
                    f = new TQuadrat(a);
                }

                else if (RadBtnQuadratsaeule.Checked && TxbE1.Text != "" && TxbE2.Text != "")
                {
                    double a = Convert.ToDouble(TxbE1.Text);
                    double h = Convert.ToDouble(TxbE2.Text);
                    TQuadrat g = new TQuadrat(a);
                    k = new TQuadratsaeule(g, h);
                    flaeche = false;
                    f = g;
                }

                else if (RadBtnWuerfel.Checked && TxbE1.Text != "")
                {
                    double a = Convert.ToDouble(TxbE1.Text);
                    TQuadrat g = new TQuadrat(a);
                    k = new TWuerfel(g);
                    flaeche = false;
                    f = g;
                }

                else if (RadBtnRechteck.Checked && TxbE1.Text != "" && TxbE2.Text != "")
                {
                    double a = Convert.ToDouble(TxbE1.Text);
                    double b = Convert.ToDouble(TxbE2.Text);
                    f = new TRechteck(a, b);
                }

                else if (RadBtnRechtecksaeule.Checked && TxbE1.Text != "" && TxbE2.Text != "" && TxbE3.Text != "")
                {
                    double a = Convert.ToDouble(TxbE1.Text);
                    double b = Convert.ToDouble(TxbE2.Text);
                    double h = Convert.ToDouble(TxbE3.Text);
                    TRechteck g = new TRechteck(a, b);
                    k = new TRechtecksaeule(g, h);
                    flaeche = false;
                    f = g;
                }

                else
                {
                    MessageBox.Show("Bitte in allen Feldern etwas angeben");
                    return;
                }
            }
            catch (FormatException){
                MessageBox.Show("Bitte nur Zahlen angeben");
                return;
            }
            if (flaeche)
            {
                string ausgabe = f.ausgeben() + ", " + f.GetType().ToString().Split('.')[1];
                LstBoxAus.Items.Add(ausgabe);
                Array.Resize(ref flaechen, flaechen.Length + 1);
                flaechen[flaechen.Length - 1] = f;
            }
            else
            {
                string ausgabe = k.ausgeben() + ", " + k.GetType().ToString().Split('.')[1];
                LstBoxAus.Items.Add(ausgabe);
                Array.Resize(ref koerper, koerper.Length + 1);
                koerper[koerper.Length - 1] = k;
            }
        }
    }
}
