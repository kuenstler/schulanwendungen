﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flaechen
{
    public class TDreieck : TFlaechen
    {
        private double seite_a;
        private double seite_b;
        private double seite_c;

        public TDreieck(double a, double b, double c)
        {
            seite_a = a;
            seite_b = b;
            seite_c = c;
        }

        public override void flaecheBerechnen()
        {
            double beta = Math.Acos((Math.Pow(seite_a, 2) + Math.Pow(seite_b, 2) - Math.Pow(seite_c, 2)) / (2 * seite_a * seite_b));
            flaeche = seite_b * seite_a * Math.Sin(beta) * 2;
        }

        public override void umfangBerechnen()
        {
            umfang = seite_a + seite_b + seite_c;
        }
    }
}