﻿namespace Flaechen
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.Lbl2D = new System.Windows.Forms.Label();
            this.RadBtnKreis = new System.Windows.Forms.RadioButton();
            this.RadBtnDreieck = new System.Windows.Forms.RadioButton();
            this.RadBtnQuadrat = new System.Windows.Forms.RadioButton();
            this.RadBtnRechteck = new System.Windows.Forms.RadioButton();
            this.Lbl3D = new System.Windows.Forms.Label();
            this.RadBtnZylinder = new System.Windows.Forms.RadioButton();
            this.RadBtnDreiecksaeule = new System.Windows.Forms.RadioButton();
            this.RadBtnQuadratsaeule = new System.Windows.Forms.RadioButton();
            this.RadBtnRechtecksaeule = new System.Windows.Forms.RadioButton();
            this.LblE1 = new System.Windows.Forms.Label();
            this.LblEingabe = new System.Windows.Forms.Label();
            this.TxbE1 = new System.Windows.Forms.TextBox();
            this.TxbE2 = new System.Windows.Forms.TextBox();
            this.LblE2 = new System.Windows.Forms.Label();
            this.TxbE3 = new System.Windows.Forms.TextBox();
            this.LblE3 = new System.Windows.Forms.Label();
            this.LstBoxAus = new System.Windows.Forms.ListBox();
            this.RadBtnWuerfel = new System.Windows.Forms.RadioButton();
            this.BtnFertig = new System.Windows.Forms.Button();
            this.TxbE4 = new System.Windows.Forms.TextBox();
            this.LblE4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // Lbl2D
            // 
            this.Lbl2D.AutoSize = true;
            this.Lbl2D.Location = new System.Drawing.Point(13, 13);
            this.Lbl2D.Name = "Lbl2D";
            this.Lbl2D.Size = new System.Drawing.Size(47, 13);
            this.Lbl2D.TabIndex = 0;
            this.Lbl2D.Text = "2D-Form";
            // 
            // RadBtnKreis
            // 
            this.RadBtnKreis.AutoSize = true;
            this.RadBtnKreis.Location = new System.Drawing.Point(13, 30);
            this.RadBtnKreis.Name = "RadBtnKreis";
            this.RadBtnKreis.Size = new System.Drawing.Size(48, 17);
            this.RadBtnKreis.TabIndex = 1;
            this.RadBtnKreis.TabStop = true;
            this.RadBtnKreis.Text = "Kreis";
            this.RadBtnKreis.UseVisualStyleBackColor = true;
            this.RadBtnKreis.CheckedChanged += new System.EventHandler(this.RadBtnKreis_CheckedChanged);
            // 
            // RadBtnDreieck
            // 
            this.RadBtnDreieck.AutoSize = true;
            this.RadBtnDreieck.Location = new System.Drawing.Point(13, 54);
            this.RadBtnDreieck.Name = "RadBtnDreieck";
            this.RadBtnDreieck.Size = new System.Drawing.Size(62, 17);
            this.RadBtnDreieck.TabIndex = 2;
            this.RadBtnDreieck.TabStop = true;
            this.RadBtnDreieck.Text = "Dreieck";
            this.RadBtnDreieck.UseVisualStyleBackColor = true;
            this.RadBtnDreieck.CheckedChanged += new System.EventHandler(this.RadBtnDreieck_CheckedChanged);
            // 
            // RadBtnQuadrat
            // 
            this.RadBtnQuadrat.AutoSize = true;
            this.RadBtnQuadrat.Location = new System.Drawing.Point(13, 78);
            this.RadBtnQuadrat.Name = "RadBtnQuadrat";
            this.RadBtnQuadrat.Size = new System.Drawing.Size(63, 17);
            this.RadBtnQuadrat.TabIndex = 3;
            this.RadBtnQuadrat.TabStop = true;
            this.RadBtnQuadrat.Text = "Quadrat";
            this.RadBtnQuadrat.UseVisualStyleBackColor = true;
            this.RadBtnQuadrat.CheckedChanged += new System.EventHandler(this.RadBtnQuadrat_CheckedChanged);
            // 
            // RadBtnRechteck
            // 
            this.RadBtnRechteck.AutoSize = true;
            this.RadBtnRechteck.Location = new System.Drawing.Point(13, 102);
            this.RadBtnRechteck.Name = "RadBtnRechteck";
            this.RadBtnRechteck.Size = new System.Drawing.Size(72, 17);
            this.RadBtnRechteck.TabIndex = 4;
            this.RadBtnRechteck.TabStop = true;
            this.RadBtnRechteck.Text = "Rechteck";
            this.RadBtnRechteck.UseVisualStyleBackColor = true;
            this.RadBtnRechteck.CheckedChanged += new System.EventHandler(this.RadBtnRechteck_CheckedChanged);
            // 
            // Lbl3D
            // 
            this.Lbl3D.AutoSize = true;
            this.Lbl3D.Location = new System.Drawing.Point(130, 12);
            this.Lbl3D.Name = "Lbl3D";
            this.Lbl3D.Size = new System.Drawing.Size(47, 13);
            this.Lbl3D.TabIndex = 5;
            this.Lbl3D.Text = "3D-Form";
            // 
            // RadBtnZylinder
            // 
            this.RadBtnZylinder.AutoSize = true;
            this.RadBtnZylinder.Location = new System.Drawing.Point(133, 30);
            this.RadBtnZylinder.Name = "RadBtnZylinder";
            this.RadBtnZylinder.Size = new System.Drawing.Size(62, 17);
            this.RadBtnZylinder.TabIndex = 6;
            this.RadBtnZylinder.TabStop = true;
            this.RadBtnZylinder.Text = "Zylinder";
            this.RadBtnZylinder.UseVisualStyleBackColor = true;
            this.RadBtnZylinder.CheckedChanged += new System.EventHandler(this.RadBtnZylinder_CheckedChanged);
            // 
            // RadBtnDreiecksaeule
            // 
            this.RadBtnDreiecksaeule.AutoSize = true;
            this.RadBtnDreiecksaeule.Location = new System.Drawing.Point(133, 54);
            this.RadBtnDreiecksaeule.Name = "RadBtnDreiecksaeule";
            this.RadBtnDreiecksaeule.Size = new System.Drawing.Size(87, 17);
            this.RadBtnDreiecksaeule.TabIndex = 7;
            this.RadBtnDreiecksaeule.TabStop = true;
            this.RadBtnDreiecksaeule.Text = "Dreiecksäule";
            this.RadBtnDreiecksaeule.UseVisualStyleBackColor = true;
            this.RadBtnDreiecksaeule.CheckedChanged += new System.EventHandler(this.RadBtnDreiecksaeule_CheckedChanged);
            // 
            // RadBtnQuadratsaeule
            // 
            this.RadBtnQuadratsaeule.AutoSize = true;
            this.RadBtnQuadratsaeule.Location = new System.Drawing.Point(133, 78);
            this.RadBtnQuadratsaeule.Name = "RadBtnQuadratsaeule";
            this.RadBtnQuadratsaeule.Size = new System.Drawing.Size(88, 17);
            this.RadBtnQuadratsaeule.TabIndex = 8;
            this.RadBtnQuadratsaeule.TabStop = true;
            this.RadBtnQuadratsaeule.Text = "Quadratsäule";
            this.RadBtnQuadratsaeule.UseVisualStyleBackColor = true;
            this.RadBtnQuadratsaeule.CheckedChanged += new System.EventHandler(this.RadBtnQuadratsaeule_CheckedChanged);
            // 
            // RadBtnRechtecksaeule
            // 
            this.RadBtnRechtecksaeule.AutoSize = true;
            this.RadBtnRechtecksaeule.Location = new System.Drawing.Point(133, 102);
            this.RadBtnRechtecksaeule.Name = "RadBtnRechtecksaeule";
            this.RadBtnRechtecksaeule.Size = new System.Drawing.Size(97, 17);
            this.RadBtnRechtecksaeule.TabIndex = 9;
            this.RadBtnRechtecksaeule.TabStop = true;
            this.RadBtnRechtecksaeule.Text = "Rechtecksäule";
            this.RadBtnRechtecksaeule.UseVisualStyleBackColor = true;
            this.RadBtnRechtecksaeule.CheckedChanged += new System.EventHandler(this.RadBtnRechtecksaeule_CheckedChanged);
            // 
            // LblE1
            // 
            this.LblE1.AutoSize = true;
            this.LblE1.Location = new System.Drawing.Point(394, 37);
            this.LblE1.Name = "LblE1";
            this.LblE1.Size = new System.Drawing.Size(24, 13);
            this.LblE1.TabIndex = 10;
            this.LblE1.Text = "test";
            this.LblE1.Visible = false;
            // 
            // LblEingabe
            // 
            this.LblEingabe.AutoSize = true;
            this.LblEingabe.Location = new System.Drawing.Point(285, 11);
            this.LblEingabe.Name = "LblEingabe";
            this.LblEingabe.Size = new System.Drawing.Size(46, 13);
            this.LblEingabe.TabIndex = 11;
            this.LblEingabe.Text = "Eingabe";
            // 
            // TxbE1
            // 
            this.TxbE1.Location = new System.Drawing.Point(288, 30);
            this.TxbE1.Name = "TxbE1";
            this.TxbE1.Size = new System.Drawing.Size(100, 20);
            this.TxbE1.TabIndex = 12;
            this.TxbE1.Visible = false;
            // 
            // TxbE2
            // 
            this.TxbE2.Location = new System.Drawing.Point(288, 56);
            this.TxbE2.Name = "TxbE2";
            this.TxbE2.Size = new System.Drawing.Size(100, 20);
            this.TxbE2.TabIndex = 14;
            this.TxbE2.Visible = false;
            // 
            // LblE2
            // 
            this.LblE2.AutoSize = true;
            this.LblE2.Location = new System.Drawing.Point(394, 63);
            this.LblE2.Name = "LblE2";
            this.LblE2.Size = new System.Drawing.Size(24, 13);
            this.LblE2.TabIndex = 13;
            this.LblE2.Text = "test";
            this.LblE2.Visible = false;
            // 
            // TxbE3
            // 
            this.TxbE3.Location = new System.Drawing.Point(288, 82);
            this.TxbE3.Name = "TxbE3";
            this.TxbE3.Size = new System.Drawing.Size(100, 20);
            this.TxbE3.TabIndex = 16;
            this.TxbE3.Visible = false;
            // 
            // LblE3
            // 
            this.LblE3.AutoSize = true;
            this.LblE3.Location = new System.Drawing.Point(394, 89);
            this.LblE3.Name = "LblE3";
            this.LblE3.Size = new System.Drawing.Size(24, 13);
            this.LblE3.TabIndex = 15;
            this.LblE3.Text = "test";
            this.LblE3.Visible = false;
            // 
            // LstBoxAus
            // 
            this.LstBoxAus.FormattingEnabled = true;
            this.LstBoxAus.Location = new System.Drawing.Point(16, 169);
            this.LstBoxAus.Name = "LstBoxAus";
            this.LstBoxAus.Size = new System.Drawing.Size(405, 238);
            this.LstBoxAus.TabIndex = 19;
            // 
            // RadBtnWuerfel
            // 
            this.RadBtnWuerfel.AutoSize = true;
            this.RadBtnWuerfel.Location = new System.Drawing.Point(133, 126);
            this.RadBtnWuerfel.Name = "RadBtnWuerfel";
            this.RadBtnWuerfel.Size = new System.Drawing.Size(56, 17);
            this.RadBtnWuerfel.TabIndex = 10;
            this.RadBtnWuerfel.TabStop = true;
            this.RadBtnWuerfel.Text = "Würfel";
            this.RadBtnWuerfel.UseVisualStyleBackColor = true;
            this.RadBtnWuerfel.CheckedChanged += new System.EventHandler(this.RadBtnWuerfel_CheckedChanged);
            // 
            // BtnFertig
            // 
            this.BtnFertig.Location = new System.Drawing.Point(273, 140);
            this.BtnFertig.Name = "BtnFertig";
            this.BtnFertig.Size = new System.Drawing.Size(148, 23);
            this.BtnFertig.TabIndex = 18;
            this.BtnFertig.Text = "Übernehmen";
            this.BtnFertig.UseVisualStyleBackColor = true;
            this.BtnFertig.Click += new System.EventHandler(this.BtnFertig_Click);
            // 
            // TxbE4
            // 
            this.TxbE4.Location = new System.Drawing.Point(288, 108);
            this.TxbE4.Name = "TxbE4";
            this.TxbE4.Size = new System.Drawing.Size(100, 20);
            this.TxbE4.TabIndex = 17;
            this.TxbE4.Visible = false;
            // 
            // LblE4
            // 
            this.LblE4.AutoSize = true;
            this.LblE4.Location = new System.Drawing.Point(394, 115);
            this.LblE4.Name = "LblE4";
            this.LblE4.Size = new System.Drawing.Size(24, 13);
            this.LblE4.TabIndex = 20;
            this.LblE4.Text = "test";
            this.LblE4.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.TxbE4);
            this.Controls.Add(this.LblE4);
            this.Controls.Add(this.BtnFertig);
            this.Controls.Add(this.RadBtnWuerfel);
            this.Controls.Add(this.LstBoxAus);
            this.Controls.Add(this.TxbE3);
            this.Controls.Add(this.LblE3);
            this.Controls.Add(this.TxbE2);
            this.Controls.Add(this.LblE2);
            this.Controls.Add(this.TxbE1);
            this.Controls.Add(this.LblEingabe);
            this.Controls.Add(this.LblE1);
            this.Controls.Add(this.RadBtnRechtecksaeule);
            this.Controls.Add(this.RadBtnQuadratsaeule);
            this.Controls.Add(this.RadBtnDreiecksaeule);
            this.Controls.Add(this.RadBtnZylinder);
            this.Controls.Add(this.Lbl3D);
            this.Controls.Add(this.RadBtnRechteck);
            this.Controls.Add(this.RadBtnQuadrat);
            this.Controls.Add(this.RadBtnDreieck);
            this.Controls.Add(this.RadBtnKreis);
            this.Controls.Add(this.Lbl2D);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Lbl2D;
        private System.Windows.Forms.RadioButton RadBtnKreis;
        private System.Windows.Forms.RadioButton RadBtnDreieck;
        private System.Windows.Forms.RadioButton RadBtnQuadrat;
        private System.Windows.Forms.RadioButton RadBtnRechteck;
        private System.Windows.Forms.Label Lbl3D;
        private System.Windows.Forms.RadioButton RadBtnZylinder;
        private System.Windows.Forms.RadioButton RadBtnDreiecksaeule;
        private System.Windows.Forms.RadioButton RadBtnQuadratsaeule;
        private System.Windows.Forms.RadioButton RadBtnRechtecksaeule;
        private System.Windows.Forms.Label LblE1;
        private System.Windows.Forms.Label LblEingabe;
        private System.Windows.Forms.TextBox TxbE1;
        private System.Windows.Forms.TextBox TxbE2;
        private System.Windows.Forms.Label LblE2;
        private System.Windows.Forms.TextBox TxbE3;
        private System.Windows.Forms.Label LblE3;
        private System.Windows.Forms.ListBox LstBoxAus;
        private System.Windows.Forms.RadioButton RadBtnWuerfel;
        private System.Windows.Forms.Button BtnFertig;
        private System.Windows.Forms.TextBox TxbE4;
        private System.Windows.Forms.Label LblE4;
    }
}

