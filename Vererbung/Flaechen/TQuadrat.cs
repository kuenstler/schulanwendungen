﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flaechen
{
    public class TQuadrat : TFlaechen
    {
        public double seite_a;

        public TQuadrat(double a)
        {
            seite_a = a;
        }

        public override void flaecheBerechnen()
        {
            flaeche = Math.Pow(seite_a, 2);
        }

        public override void umfangBerechnen()
        {
            umfang = 4 * seite_a;
        }
    }
}