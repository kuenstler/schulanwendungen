﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flaechen
{
    public class TRechteck : TQuadrat
    {
        private double seite_b;

        public TRechteck(double a, double b) : base(a)
        {
            seite_b = b;
        }

        public override void flaecheBerechnen()
        {
            flaeche = seite_a * seite_b;
        }

        public override void umfangBerechnen()
        {
            umfang = 2 * seite_a + 2 * seite_b;
        }
    }
}