﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Flaechen
{
    public abstract class TFlaechen
    {
        protected double umfang;
        protected double flaeche;

        public TFlaechen()
        {
            umfang = 0;
            flaeche = 0;
        }

        public abstract void umfangBerechnen();

        public double umfangAusgeben()
        {
            return umfang;
        }

        public abstract void flaecheBerechnen();

        public double flaecheAusgeben()
        {
            return flaeche;
        }

        public string ausgeben()
        {
            flaecheBerechnen();
            umfangBerechnen();
            string ausgabe = "";
            ausgabe += "Flächeninhalt: " + flaecheAusgeben();
            ausgabe += ", Umfang: " + umfangAusgeben();
            return ausgabe;
        }
    }
}
