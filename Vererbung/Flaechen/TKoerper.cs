﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flaechen
{
    public abstract class TKoerper
    {
        protected double oberflaeche;
        protected double volumen;
        protected double hoehe;
        protected TFlaechen boden;

        public TKoerper(TFlaechen b, double h)
        {
            boden = b;
            hoehe = h;
        }

        public void volumenBerechnen()
        {
            boden.flaecheBerechnen();
            volumen = boden.flaecheAusgeben() * hoehe;
        }

        public void oberflaecheBerechnen()
        {
            boden.umfangBerechnen();
            oberflaeche = 2 * boden.flaecheAusgeben() + boden.umfangAusgeben() * hoehe;
        }

        public string ausgeben()
        {
            volumenBerechnen();
            oberflaecheBerechnen();
            return "Oberfläche: " + oberflaeche + ", Volumen: " + volumen;
        }
    }
}