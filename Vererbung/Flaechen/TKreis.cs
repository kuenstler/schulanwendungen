﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Flaechen
{
    public class TKreis : TFlaechen
    {
        private double radius;

        public TKreis(double r)
        {
            radius = r;
        }

        public override void umfangBerechnen()
        {
            umfang = 2 * Math.PI * radius;
        }

        public override void flaecheBerechnen()
        {
            flaeche = Math.PI * Math.Pow(radius, 2);
        }
    }
}