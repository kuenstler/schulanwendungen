﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mehrfachvererbung
{
    interface IAenderbar
    {
        void faerben(string farbe);
        void vergroessern(double faktor);
    }
}