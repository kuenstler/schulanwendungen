﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mehrfachvererbung
{
    public partial class Form1 : Form
    {
        private Graphics z;
        private Pen stift = new Pen(Color.Red);
        public Form1()
        {
            InitializeComponent();
            z = CreateGraphics();
        }

        private void BtnKreis_Click(object sender, EventArgs e)
        {
            TKreis k1 = new TKreis("red", 20);
            LblAusgabe.Text = k1.ausgeben() + "\n";
            k1.malen(z, 15, 100);

            k1.faerben("yellow");
            k1.vergroessern(Math.PI);
            LblAusgabe.Text += k1.ausgeben() + "\n";

            TKreis k2 = (TKreis)k1.Clone();
            LblAusgabe.Text += k2.ausgeben() + "\n";
            k2.malen(z, 15, 100);
        }
    }
}
