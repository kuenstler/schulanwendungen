﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mehrfachvererbung
{
    public class TQuadrat : T2DKlasse
    {
        private double seite;

        public double Seitenlaenge
        {
            get => seite;
        }

        public override int Flaecheninhalt()
        {
            flaecheninhalt = seite * seite;
            return (int)flaecheninhalt;
        }

        public void setSeite(double s)
        {
            seite = s;
        }
    }
}