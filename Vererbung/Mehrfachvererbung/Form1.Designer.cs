﻿namespace Mehrfachvererbung
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblAusgabe = new System.Windows.Forms.Label();
            this.BtnKreis = new System.Windows.Forms.Button();
            this.BtnQuadrat = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LblAusgabe
            // 
            this.LblAusgabe.AutoSize = true;
            this.LblAusgabe.Location = new System.Drawing.Point(13, 13);
            this.LblAusgabe.Name = "LblAusgabe";
            this.LblAusgabe.Size = new System.Drawing.Size(49, 13);
            this.LblAusgabe.TabIndex = 0;
            this.LblAusgabe.Text = "Ausgabe";
            // 
            // BtnKreis
            // 
            this.BtnKreis.Location = new System.Drawing.Point(254, 3);
            this.BtnKreis.Name = "BtnKreis";
            this.BtnKreis.Size = new System.Drawing.Size(103, 23);
            this.BtnKreis.TabIndex = 1;
            this.BtnKreis.Text = "Ausgabe Kreis";
            this.BtnKreis.UseVisualStyleBackColor = true;
            this.BtnKreis.Click += new System.EventHandler(this.BtnKreis_Click);
            // 
            // BtnQuadrat
            // 
            this.BtnQuadrat.Location = new System.Drawing.Point(254, 32);
            this.BtnQuadrat.Name = "BtnQuadrat";
            this.BtnQuadrat.Size = new System.Drawing.Size(103, 23);
            this.BtnQuadrat.TabIndex = 2;
            this.BtnQuadrat.Text = "Ausgabe Quadrat";
            this.BtnQuadrat.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(364, 450);
            this.Controls.Add(this.BtnQuadrat);
            this.Controls.Add(this.BtnKreis);
            this.Controls.Add(this.LblAusgabe);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblAusgabe;
        private System.Windows.Forms.Button BtnKreis;
        private System.Windows.Forms.Button BtnQuadrat;
    }
}

