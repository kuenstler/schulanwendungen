﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Mehrfachvererbung
{
    public abstract class T2DKlasse
    {
        protected double flaecheninhalt;

        public abstract int Flaecheninhalt();
    }
}