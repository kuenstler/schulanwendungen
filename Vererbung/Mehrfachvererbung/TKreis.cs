﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Drawing;


namespace Mehrfachvererbung
{
    public class TKreis : IAenderbar, ICloneable
    {
        private string farbe;
        private double radius;

        public TKreis(string f, double r)
        {
            farbe = f;
            radius = r;
        }

        public double Durchmesser
        {
            get => radius * 2;
        }

        public string Farbe
        {
            get => farbe;
        }

        public string ausgeben()
        {
            string aus = "";
            aus += "Farbe: " + farbe + ", Radius: " + radius;
            return aus;
        }

        public object Clone()
        {
            return new TKreis(farbe, radius);
        }

        public void faerben(string farbe)
        {
            this.farbe = farbe;
        }

        public void vergroessern(double faktor)
        {
            radius *= faktor;
        }

        public void malen(Graphics z, int x, int y)
        {
            Pen stift = new Pen(Color.FromName(Farbe));
            z.DrawEllipse(stift, x, y, (float)Durchmesser, (float)Durchmesser);
        }
    }
}