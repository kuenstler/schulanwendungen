﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sportfest
{
    class TPerson
    {
        private string Name;
        private string Vorname;

        public TPerson(string n, string v)
        {
            Name = n;
            Vorname = v;
        }

        public string[] getNamen()
        {
            string[] aus = { Name, Vorname };
            return aus;
        }

        public virtual string ausgeben()
        {
            return "Name: " + Name + "\nVorname: " + Vorname;
        }
    }

    class TSchueler : TPerson
    {
        private string SchuelerIdent;

        public TSchueler(string n, string v, string s) : base(n, v)
        {
            SchuelerIdent = s;
        }

        public string getIdent()
        {
            return SchuelerIdent;
        }

        public override string ausgeben()
        {
            return base.ausgeben() + "\nSchuelerIdent: " + SchuelerIdent;
        }
    }

    class TSportler : TSchueler
    {
        private DateTime Geburtsdatum;
        private string Geschlecht;

        public TSportler(string n,string v,string s,DateTime g, string ge) : base(n, v, s)
        {
            Geburtsdatum = g;
            Geschlecht = ge;
        }

        public string getGeschlecht()
        {
            return Geschlecht;
        }

        public int getAlter()
        {
            int alter = alter = DateTime.Today.Year - Geburtsdatum.Year;
            if (DateTime.Today.Month == Geburtsdatum.Month)
            {
                if (DateTime.Today.Day < Geburtsdatum.Day)
                {
                    --alter;
                }
            }
            else if (DateTime.Today.Month < Geburtsdatum.Month)
            {
                --alter;
            }
            return alter;
        }

        public override string ausgeben()
        {
            return base.ausgeben() + "\nGeschlecht: " + Geschlecht + "\nAlter: " + getAlter() + " Jahre";
        }
    }

    class THelfer : TSchueler
    {
        private DateTime Befreiung_bis;
        private int Disziplin;
        public THelfer(string n, string v, string s, DateTime b, int d) : base(n, v, s)
        {
            Befreiung_bis = b;
            Disziplin = d;
        }

        public TDisziplin TDisziplin
        {
            get => default(TDisziplin);
            set
            {
            }
        }

        public bool getBefreiung_gueltig()
        {
            bool aus = false;
            if (DateTime.Today.CompareTo(Befreiung_bis) <= 0) aus = true;
            return aus;
        }

        public int getDisziplin()
        {
            return Disziplin;
        }

        public override string ausgeben()
        {
            return base.ausgeben() + "\nDisziplin: " + Disziplin + "\nDie Befreiung ist" + (getBefreiung_gueltig() ? "" : " nicht") + " gültig.";
        }
    }

    class TLehrer : TPerson
    {
        private string LehrerIdent;

        public TLehrer(string n, string v, string i) : base(n, v)
        {
            LehrerIdent = i;
        }

        public string getLehrerIdent()
        {
            return LehrerIdent;
        }

        public override string ausgeben()
        {
            return base.ausgeben() + "\nLehrerIdent: " + LehrerIdent;
        }
    }

    class TKampfrichter : TLehrer
    {
        private int Disziplin;
        public TKampfrichter(string n,string v, string i, int d) : base(n, v, i)
        {
            Disziplin = d;
        }

        public TDisziplin TDisziplin
        {
            get => default(TDisziplin);
            set
            {
            }
        }

        public int getDisziplin()
        {
            return Disziplin;
        }
        public override string ausgeben()
        {
            return base.ausgeben() + "\nDisziplin: " + Disziplin;
        }
    }

    public class TDisziplin
    {
        private int Nummer;
        private string Name;
        private string Bezeichnung;

        public TDisziplin(int nr, string b, string n)
        {
            Nummer = nr;
            Name = n;
            Bezeichnung = b;
        }

        public int getNummer()
        {
            return Nummer;
        }
    }

    public class TLeistung
    {
        int SchuelerID;
        int Disziplin;
        int Punktwert;

        public TLeistung(int id,int d, int p)
        {
            SchuelerID = id;
            Disziplin = d;
            Punktwert = p;
        }

        internal TSportler TSportler
        {
            get => default(TSportler);
            set
            {
            }
        }

        public TDisziplin TDisziplin
        {
            get => default(TDisziplin);
            set
            {
            }
        }

        public int getDisziplin()
        {
            return Disziplin;
        }

        public int getPunktwert()
        {
            return Punktwert;
        }

        public int getSchuelerID()
        {
            return SchuelerID;
        }
    }
}
