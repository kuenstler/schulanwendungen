﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Sportfest
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnTest_Click(object sender, EventArgs e)
        {
            DateTime d1 = new DateTime(2001, 4, 15);
            DateTime d2 = new DateTime(2019, 10, 20);
            TPerson t1 = new TPerson("Mustermann", "Max");
            TSchueler s1 = new TSchueler("Musk", "Elon", "IG18-20");
            TSportler s2 = new TSportler("Mustermann", "Erika", "IG19-10", d1, "Weiblich");
            THelfer h1 = new THelfer("Wunsch", "Fabian", "IG17-14", d2, 3);
            LblAusgabe.Text = t1.ausgeben();
            LblAusgabe.Text += "\n" + s1.ausgeben();
            LblAusgabe.Text += "\n" + s2.ausgeben();
            LblAusgabe.Text += "\n" + h1.ausgeben();
        }
    }
}
