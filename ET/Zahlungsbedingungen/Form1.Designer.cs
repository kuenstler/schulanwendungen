﻿namespace Zahlungsbedingungen
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.ChkNeu = new System.Windows.Forms.CheckBox();
            this.LblNeu = new System.Windows.Forms.Label();
            this.LblVorjahr = new System.Windows.Forms.Label();
            this.TrkVorjahr = new System.Windows.Forms.TrackBar();
            this.TxbVorjahr = new System.Windows.Forms.TextBox();
            this.PnlZahl = new System.Windows.Forms.Panel();
            this.RadBtnZahlSchlecht = new System.Windows.Forms.RadioButton();
            this.RadBtnZahlGut = new System.Windows.Forms.RadioButton();
            this.LblZahl = new System.Windows.Forms.Label();
            this.LblAuftag = new System.Windows.Forms.Label();
            this.TxbAuftrag = new System.Windows.Forms.TextBox();
            this.BtnBestimmen = new System.Windows.Forms.Button();
            this.BtnLeeren = new System.Windows.Forms.Button();
            this.LblErg = new System.Windows.Forms.Label();
            this.TxbErg = new System.Windows.Forms.RichTextBox();
            ((System.ComponentModel.ISupportInitialize)(this.TrkVorjahr)).BeginInit();
            this.PnlZahl.SuspendLayout();
            this.SuspendLayout();
            // 
            // ChkNeu
            // 
            this.ChkNeu.AutoSize = true;
            this.ChkNeu.Location = new System.Drawing.Point(91, 13);
            this.ChkNeu.Name = "ChkNeu";
            this.ChkNeu.Size = new System.Drawing.Size(15, 14);
            this.ChkNeu.TabIndex = 0;
            this.ChkNeu.UseVisualStyleBackColor = true;
            this.ChkNeu.CheckedChanged += new System.EventHandler(this.ChkNeu_CheckedChanged);
            // 
            // LblNeu
            // 
            this.LblNeu.AutoSize = true;
            this.LblNeu.Location = new System.Drawing.Point(13, 13);
            this.LblNeu.Name = "LblNeu";
            this.LblNeu.Size = new System.Drawing.Size(60, 13);
            this.LblNeu.TabIndex = 1;
            this.LblNeu.Text = "Neukunde:";
            // 
            // LblVorjahr
            // 
            this.LblVorjahr.AutoSize = true;
            this.LblVorjahr.Location = new System.Drawing.Point(13, 47);
            this.LblVorjahr.Name = "LblVorjahr";
            this.LblVorjahr.Size = new System.Drawing.Size(86, 13);
            this.LblVorjahr.TabIndex = 2;
            this.LblVorjahr.Text = "Aufträge Vorjahr:";
            // 
            // TrkVorjahr
            // 
            this.TrkVorjahr.Location = new System.Drawing.Point(16, 63);
            this.TrkVorjahr.Maximum = 80;
            this.TrkVorjahr.Name = "TrkVorjahr";
            this.TrkVorjahr.Size = new System.Drawing.Size(224, 45);
            this.TrkVorjahr.TabIndex = 3;
            this.TrkVorjahr.TickFrequency = 10;
            this.TrkVorjahr.Value = 10;
            this.TrkVorjahr.Scroll += new System.EventHandler(this.TrkVorjahr_Scroll);
            // 
            // TxbVorjahr
            // 
            this.TxbVorjahr.Location = new System.Drawing.Point(246, 63);
            this.TxbVorjahr.Name = "TxbVorjahr";
            this.TxbVorjahr.Size = new System.Drawing.Size(33, 20);
            this.TxbVorjahr.TabIndex = 4;
            this.TxbVorjahr.Text = "10";
            this.TxbVorjahr.TextChanged += new System.EventHandler(this.TxbVorjahr_TextChanged);
            // 
            // PnlZahl
            // 
            this.PnlZahl.Controls.Add(this.RadBtnZahlSchlecht);
            this.PnlZahl.Controls.Add(this.RadBtnZahlGut);
            this.PnlZahl.Controls.Add(this.LblZahl);
            this.PnlZahl.Location = new System.Drawing.Point(16, 124);
            this.PnlZahl.Name = "PnlZahl";
            this.PnlZahl.Size = new System.Drawing.Size(124, 44);
            this.PnlZahl.TabIndex = 5;
            // 
            // RadBtnZahlSchlecht
            // 
            this.RadBtnZahlSchlecht.AutoSize = true;
            this.RadBtnZahlSchlecht.Location = new System.Drawing.Point(53, 17);
            this.RadBtnZahlSchlecht.Name = "RadBtnZahlSchlecht";
            this.RadBtnZahlSchlecht.Size = new System.Drawing.Size(65, 17);
            this.RadBtnZahlSchlecht.TabIndex = 2;
            this.RadBtnZahlSchlecht.TabStop = true;
            this.RadBtnZahlSchlecht.Text = "schlecht";
            this.RadBtnZahlSchlecht.UseVisualStyleBackColor = true;
            // 
            // RadBtnZahlGut
            // 
            this.RadBtnZahlGut.AutoSize = true;
            this.RadBtnZahlGut.Checked = true;
            this.RadBtnZahlGut.Location = new System.Drawing.Point(4, 17);
            this.RadBtnZahlGut.Name = "RadBtnZahlGut";
            this.RadBtnZahlGut.Size = new System.Drawing.Size(40, 17);
            this.RadBtnZahlGut.TabIndex = 1;
            this.RadBtnZahlGut.TabStop = true;
            this.RadBtnZahlGut.Text = "gut";
            this.RadBtnZahlGut.UseVisualStyleBackColor = true;
            // 
            // LblZahl
            // 
            this.LblZahl.AutoSize = true;
            this.LblZahl.Location = new System.Drawing.Point(20, 0);
            this.LblZahl.Name = "LblZahl";
            this.LblZahl.Size = new System.Drawing.Size(79, 13);
            this.LblZahl.TabIndex = 0;
            this.LblZahl.Text = "Zahlungsmoral:";
            // 
            // LblAuftag
            // 
            this.LblAuftag.AutoSize = true;
            this.LblAuftag.Location = new System.Drawing.Point(13, 202);
            this.LblAuftag.Name = "LblAuftag";
            this.LblAuftag.Size = new System.Drawing.Size(84, 13);
            this.LblAuftag.TabIndex = 6;
            this.LblAuftag.Text = "Auftragswert [€]:";
            // 
            // TxbAuftrag
            // 
            this.TxbAuftrag.Location = new System.Drawing.Point(104, 202);
            this.TxbAuftrag.Name = "TxbAuftrag";
            this.TxbAuftrag.Size = new System.Drawing.Size(67, 20);
            this.TxbAuftrag.TabIndex = 7;
            this.TxbAuftrag.Text = "100";
            this.TxbAuftrag.TextChanged += new System.EventHandler(this.TxbAuftrag_TextChanged);
            // 
            // BtnBestimmen
            // 
            this.BtnBestimmen.Location = new System.Drawing.Point(13, 257);
            this.BtnBestimmen.Name = "BtnBestimmen";
            this.BtnBestimmen.Size = new System.Drawing.Size(127, 43);
            this.BtnBestimmen.TabIndex = 8;
            this.BtnBestimmen.Text = "Zahlungsbedingungen\r\nbestimmen";
            this.BtnBestimmen.UseVisualStyleBackColor = true;
            this.BtnBestimmen.Click += new System.EventHandler(this.BtnBestimmen_Click);
            // 
            // BtnLeeren
            // 
            this.BtnLeeren.Location = new System.Drawing.Point(165, 257);
            this.BtnLeeren.Name = "BtnLeeren";
            this.BtnLeeren.Size = new System.Drawing.Size(98, 43);
            this.BtnLeeren.TabIndex = 9;
            this.BtnLeeren.Text = "Eingaben\r\nleeren";
            this.BtnLeeren.UseVisualStyleBackColor = true;
            this.BtnLeeren.Click += new System.EventHandler(this.BtnLeeren_Click);
            // 
            // LblErg
            // 
            this.LblErg.AutoSize = true;
            this.LblErg.Location = new System.Drawing.Point(13, 330);
            this.LblErg.Name = "LblErg";
            this.LblErg.Size = new System.Drawing.Size(116, 13);
            this.LblErg.TabIndex = 10;
            this.LblErg.Text = "Zahlungsbedingungen:";
            // 
            // TxbErg
            // 
            this.TxbErg.Location = new System.Drawing.Point(13, 347);
            this.TxbErg.Name = "TxbErg";
            this.TxbErg.ReadOnly = true;
            this.TxbErg.Size = new System.Drawing.Size(227, 96);
            this.TxbErg.TabIndex = 11;
            this.TxbErg.Text = "kein Rabatt";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(384, 470);
            this.Controls.Add(this.TxbErg);
            this.Controls.Add(this.LblErg);
            this.Controls.Add(this.BtnLeeren);
            this.Controls.Add(this.BtnBestimmen);
            this.Controls.Add(this.TxbAuftrag);
            this.Controls.Add(this.LblAuftag);
            this.Controls.Add(this.PnlZahl);
            this.Controls.Add(this.TxbVorjahr);
            this.Controls.Add(this.TrkVorjahr);
            this.Controls.Add(this.LblVorjahr);
            this.Controls.Add(this.LblNeu);
            this.Controls.Add(this.ChkNeu);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.TrkVorjahr)).EndInit();
            this.PnlZahl.ResumeLayout(false);
            this.PnlZahl.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox ChkNeu;
        private System.Windows.Forms.Label LblNeu;
        private System.Windows.Forms.Label LblVorjahr;
        private System.Windows.Forms.TrackBar TrkVorjahr;
        private System.Windows.Forms.TextBox TxbVorjahr;
        private System.Windows.Forms.Panel PnlZahl;
        private System.Windows.Forms.RadioButton RadBtnZahlSchlecht;
        private System.Windows.Forms.RadioButton RadBtnZahlGut;
        private System.Windows.Forms.Label LblZahl;
        private System.Windows.Forms.Label LblAuftag;
        private System.Windows.Forms.TextBox TxbAuftrag;
        private System.Windows.Forms.Button BtnBestimmen;
        private System.Windows.Forms.Button BtnLeeren;
        private System.Windows.Forms.Label LblErg;
        private System.Windows.Forms.RichTextBox TxbErg;
    }
}

