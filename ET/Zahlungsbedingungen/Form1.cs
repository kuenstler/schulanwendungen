﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Zahlungsbedingungen
{
    public partial class Form1 : Form
    {
        bool neukunde = false, zahlungsmoral = true;
        int auftraege = 0;
        string auftragswert = "0";
        public Form1()
        {
            InitializeComponent();
            neukunde = ChkNeu.Checked;
            zahlungsmoral = RadBtnZahlGut.Checked;
            auftraege = TrkVorjahr.Value;
            auftragswert = TxbAuftrag.Text;
        }

        private void bedingungen_ermitteln()
        {
            bool stammkunde = !ChkNeu.Checked;
            bool zmoral = RadBtnZahlGut.Checked;
            bool auftrag_oft = TrkVorjahr.Value > 48 ? true : false;
            bool ueber_500 = Convert.ToInt32(TxbAuftrag.Text) > 500 ? true : false;
            // Zahlungsbedingung bestimmen
            string zahlungsbedingungen;
            if (stammkunde && auftrag_oft && zmoral && ueber_500) zahlungsbedingungen = "15% Rabat\nohne Vorkasse";
            else if (stammkunde && auftrag_oft && zmoral && !ueber_500) zahlungsbedingungen = "10% Rabat\nohne Vorkasse";
            else if (stammkunde && auftrag_oft && !zmoral) zahlungsbedingungen = "5% Rabat\nohne Vorkasse";
            else if (stammkunde && !auftrag_oft && !zmoral && !ueber_500) zahlungsbedingungen = "kein Rabat\nohne Vorkasse";
            else if (stammkunde && !auftrag_oft && !zmoral && ueber_500) zahlungsbedingungen = "5% Rabat\nohne Vorkasse";
            else if (stammkunde && !auftrag_oft && zmoral) zahlungsbedingungen = "10% Rabat\nohne Vorkasse";
            else if (!stammkunde && ueber_500) zahlungsbedingungen = "5% Rabat\nmit Vorkasse";
            else if (!stammkunde && !ueber_500) zahlungsbedingungen = "kein Rabat\nohne Vorkasse";
            else zahlungsbedingungen = "nicht möglich";
            TxbErg.Text = zahlungsbedingungen;
        }

        private void ChkNeu_CheckedChanged(object sender, EventArgs e)
        {
            if (ChkNeu.Checked)
            {
                TrkVorjahr.Enabled = false;
                TxbVorjahr.Enabled = false;
                LblVorjahr.Enabled = false;
                PnlZahl.Enabled = false;
            }
            else
            {
                TrkVorjahr.Enabled = true;
                TxbVorjahr.Enabled = true;
                LblVorjahr.Enabled = true;
                PnlZahl.Enabled = true;
            }
            bedingungen_ermitteln();
        }

        private void TrkVorjahr_Scroll(object sender, EventArgs e)
        {
            TxbVorjahr.Text = Convert.ToString(TrkVorjahr.Value);
            bedingungen_ermitteln();
        }

        private void BtnLeeren_Click(object sender, EventArgs e)
        {
            Form1 NewForm = new Form1();
            NewForm.Show();
            this.Dispose(false);
//            ChkNeu.Checked = neukunde;
//            RadBtnZahlGut.Checked = zahlungsmoral;
//            RadBtnZahlSchlecht.Checked = !zahlungsmoral;
//            TrkVorjahr.Value = auftraege;
//            TxbAuftrag.Text = auftragswert;
//            TxbVorjahr.Text = Convert.ToString(auftraege);
            bedingungen_ermitteln();
        }

        private void TxbVorjahr_TextChanged(object sender, EventArgs e)
        {
            if (TxbVorjahr.Text == "")
            {
                MessageBox.Show("Bitte Zahl eintragen");
                TxbVorjahr.Text = "0";
            }
            else if (Convert.ToInt32(TxbVorjahr.Text) > TrkVorjahr.Maximum)
            {
                TrkVorjahr.Value = TrkVorjahr.Maximum;
                TxbVorjahr.Text = Convert.ToString(TrkVorjahr.Maximum);
            }
            else if (Convert.ToInt32(TxbVorjahr.Text) < TrkVorjahr.Minimum)
            {
                TrkVorjahr.Value = TrkVorjahr.Minimum;
                TxbVorjahr.Text = Convert.ToString(TrkVorjahr.Minimum);
            }
            else
            {
                TrkVorjahr.Value = Convert.ToInt32(TxbVorjahr.Text);
            }
            bedingungen_ermitteln();
        }

        private void BtnBestimmen_Click(object sender, EventArgs e)
        {
            bedingungen_ermitteln();
        }

        private void TxbAuftrag_TextChanged(object sender, EventArgs e)
        {
            if (TxbAuftrag.Text == "")
            {
                MessageBox.Show("Bitte Zahl eintragen");
                TxbAuftrag.Text = "0";
            }
            bedingungen_ermitteln();
        }
    }
}
