﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Barbesuch
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        private void Algorithmus()
        {
            bool schoene_augen = RadbtnAugenTrue.Checked;
            bool mag_bier = RadbtnBierTrue.Checked;
            bool sportlich = RadbtnSportTrue.Checked;
            string ausgabe;
            if ((schoene_augen && !sportlich && !mag_bier) || (!schoene_augen && !sportlich))
            {
                ausgabe = "Ich sag Tschüss.";
            }
            else if ((schoene_augen && sportlich && !mag_bier) || (!schoene_augen && sportlich && mag_bier) || (schoene_augen && !sportlich && mag_bier))
            {
                ausgabe = "Ich lasse mir die Handynummer geben.";
            }
            else if (schoene_augen && sportlich && mag_bier)
            {
                ausgabe = "Ich lasse mir die Handynummer und Feisbukkontakt geben.";
            }
            else if (!schoene_augen && sportlich && !mag_bier)
            {
                ausgabe = "Ich lasse mir die Handynummer geben usd sage tschüss.";
            }
            else ausgabe = "Programm zerstört";
            TxbAusgabe.Text = ausgabe;
        }

        private void RadbtnAugenTrue_CheckedChanged(object sender, EventArgs e)
        {
            Algorithmus();
        }

        private void RadbtnSportTrue_CheckedChanged(object sender, EventArgs e)
        {
            Algorithmus();
        }

        private void RadbtnBierTrue_CheckedChanged(object sender, EventArgs e)
        {
            Algorithmus();
        }
    }
}
