﻿namespace Barbesuch
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.RadbtnAugenTrue = new System.Windows.Forms.RadioButton();
            this.RadbtnAugenFalse = new System.Windows.Forms.RadioButton();
            this.RadbtnSportTrue = new System.Windows.Forms.RadioButton();
            this.RadbtnSportFalse = new System.Windows.Forms.RadioButton();
            this.panel3 = new System.Windows.Forms.Panel();
            this.RadbtnBierTrue = new System.Windows.Forms.RadioButton();
            this.RadbtnBierFalse = new System.Windows.Forms.RadioButton();
            this.label1 = new System.Windows.Forms.Label();
            this.TxbAusgabe = new System.Windows.Forms.RichTextBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadbtnAugenFalse);
            this.panel1.Controls.Add(this.RadbtnAugenTrue);
            this.panel1.Location = new System.Drawing.Point(13, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 50);
            this.panel1.TabIndex = 0;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.RadbtnSportFalse);
            this.panel2.Controls.Add(this.RadbtnSportTrue);
            this.panel2.Location = new System.Drawing.Point(13, 101);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(200, 51);
            this.panel2.TabIndex = 1;
            // 
            // RadbtnAugenTrue
            // 
            this.RadbtnAugenTrue.AutoSize = true;
            this.RadbtnAugenTrue.Checked = true;
            this.RadbtnAugenTrue.Location = new System.Drawing.Point(3, 3);
            this.RadbtnAugenTrue.Name = "RadbtnAugenTrue";
            this.RadbtnAugenTrue.Size = new System.Drawing.Size(112, 17);
            this.RadbtnAugenTrue.TabIndex = 0;
            this.RadbtnAugenTrue.TabStop = true;
            this.RadbtnAugenTrue.Text = "hat schöne Augen";
            this.RadbtnAugenTrue.UseVisualStyleBackColor = true;
            this.RadbtnAugenTrue.CheckedChanged += new System.EventHandler(this.RadbtnAugenTrue_CheckedChanged);
            // 
            // RadbtnAugenFalse
            // 
            this.RadbtnAugenFalse.AutoSize = true;
            this.RadbtnAugenFalse.Location = new System.Drawing.Point(3, 26);
            this.RadbtnAugenFalse.Name = "RadbtnAugenFalse";
            this.RadbtnAugenFalse.Size = new System.Drawing.Size(147, 17);
            this.RadbtnAugenFalse.TabIndex = 1;
            this.RadbtnAugenFalse.TabStop = true;
            this.RadbtnAugenFalse.Text = "hat keine schönen Augen";
            this.RadbtnAugenFalse.UseVisualStyleBackColor = true;
            // 
            // RadbtnSportTrue
            // 
            this.RadbtnSportTrue.AutoSize = true;
            this.RadbtnSportTrue.Checked = true;
            this.RadbtnSportTrue.Location = new System.Drawing.Point(4, 4);
            this.RadbtnSportTrue.Name = "RadbtnSportTrue";
            this.RadbtnSportTrue.Size = new System.Drawing.Size(77, 17);
            this.RadbtnSportTrue.TabIndex = 0;
            this.RadbtnSportTrue.TabStop = true;
            this.RadbtnSportTrue.Text = "ist sportlich";
            this.RadbtnSportTrue.UseVisualStyleBackColor = true;
            this.RadbtnSportTrue.CheckedChanged += new System.EventHandler(this.RadbtnSportTrue_CheckedChanged);
            // 
            // RadbtnSportFalse
            // 
            this.RadbtnSportFalse.AutoSize = true;
            this.RadbtnSportFalse.Location = new System.Drawing.Point(3, 28);
            this.RadbtnSportFalse.Name = "RadbtnSportFalse";
            this.RadbtnSportFalse.Size = new System.Drawing.Size(89, 17);
            this.RadbtnSportFalse.TabIndex = 1;
            this.RadbtnSportFalse.TabStop = true;
            this.RadbtnSportFalse.Text = "ist unsportlich";
            this.RadbtnSportFalse.UseVisualStyleBackColor = true;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.RadbtnBierFalse);
            this.panel3.Controls.Add(this.RadbtnBierTrue);
            this.panel3.Location = new System.Drawing.Point(13, 158);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(200, 49);
            this.panel3.TabIndex = 2;
            // 
            // RadbtnBierTrue
            // 
            this.RadbtnBierTrue.AutoSize = true;
            this.RadbtnBierTrue.Checked = true;
            this.RadbtnBierTrue.Location = new System.Drawing.Point(4, 4);
            this.RadbtnBierTrue.Name = "RadbtnBierTrue";
            this.RadbtnBierTrue.Size = new System.Drawing.Size(66, 17);
            this.RadbtnBierTrue.TabIndex = 0;
            this.RadbtnBierTrue.TabStop = true;
            this.RadbtnBierTrue.Text = "mag Bier";
            this.RadbtnBierTrue.UseVisualStyleBackColor = true;
            this.RadbtnBierTrue.CheckedChanged += new System.EventHandler(this.RadbtnBierTrue_CheckedChanged);
            // 
            // RadbtnBierFalse
            // 
            this.RadbtnBierFalse.AutoSize = true;
            this.RadbtnBierFalse.Location = new System.Drawing.Point(4, 28);
            this.RadbtnBierFalse.Name = "RadbtnBierFalse";
            this.RadbtnBierFalse.Size = new System.Drawing.Size(92, 17);
            this.RadbtnBierFalse.TabIndex = 1;
            this.RadbtnBierFalse.TabStop = true;
            this.RadbtnBierFalse.Text = "mag Bier nicht";
            this.RadbtnBierFalse.UseVisualStyleBackColor = true;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Das Mädel";
            // 
            // TxbAusgabe
            // 
            this.TxbAusgabe.Location = new System.Drawing.Point(13, 214);
            this.TxbAusgabe.Name = "TxbAusgabe";
            this.TxbAusgabe.ReadOnly = true;
            this.TxbAusgabe.Size = new System.Drawing.Size(200, 47);
            this.TxbAusgabe.TabIndex = 4;
            this.TxbAusgabe.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(230, 280);
            this.Controls.Add(this.TxbAusgabe);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton RadbtnAugenFalse;
        private System.Windows.Forms.RadioButton RadbtnAugenTrue;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.RadioButton RadbtnSportFalse;
        private System.Windows.Forms.RadioButton RadbtnSportTrue;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.RadioButton RadbtnBierFalse;
        private System.Windows.Forms.RadioButton RadbtnBierTrue;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.RichTextBox TxbAusgabe;
    }
}

