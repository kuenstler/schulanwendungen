﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Datenfelder
{
    public partial class Form1 : Form
    {
        int[] a = new int[15];
        int[] b = new int[15];
        bool sortiert = false;
        Random r = new Random();
        public Form1()
        {
            InitializeComponent();
            NeueListe();
            Sortieren();
        }

        private void NeueListe()
        {
            LstBoxUnsortiert.Items.Clear();
            LstBoxSortiert.Items.Clear();
            b.Initialize();
            sortiert = false;
            LblStatistik.Text = "Statistik";
            for (int i = 0; i < 15; i++)
            {
                a[i] = r.Next(-999, 1000);
                LstBoxUnsortiert.Items.Add(a[i]);
            }
        }
        
        private void Sortieren()
        {
            LstBoxSortiert.Items.Clear();
            b.Initialize();
            foreach (int i in LstBoxUnsortiert.Items)
            {
                int index = 0;
                foreach (int j in LstBoxSortiert.Items)
                {
                    if (j > i)
                    {
                        break;
                    }
                    else
                    {
                        index++;
                    }
                }
                LstBoxSortiert.Items.Insert(index, i);
            }
            for (int i = 0; i < LstBoxSortiert.Items.Count; i++)
            {
                b[i] = (int)LstBoxSortiert.Items[i];
            }
            sortiert = true;
        }

        public static int IntArrayLinearSearch(int[] data, int item)
        {
            int N = data.Length;
            for (int i = 0; i < N; i++)
                if (data[i] == item)
                    return i;
            return -1;
        }

        private void BtnErzeugen_Click(object sender, EventArgs e)
        {
            NeueListe();
            Statistik();
        }

        private void Statistik()
        {
            LblStatistik.Text = "Statistik\n************************************\n";
            int minimum = 10001;
            int maximum = -10001;
            double mittelwert = 0;
            int summe = 0;
            bool zweiundvirzig = false;
            foreach (int i in LstBoxUnsortiert.Items)
            {
                summe += i;
                if (i < minimum) minimum = i;
                if (i > maximum) maximum = i;
                if (i == 42) zweiundvirzig = true;
            }
            mittelwert = (double)summe / (double)15;
            LblStatistik.Text += "Summe: " + summe;
            LblStatistik.Text += "\nMittelwert: " + mittelwert;
            LblStatistik.Text += "\nMinimum: " + minimum;
            LblStatistik.Text += "\nMaximum: " + maximum;
            if (zweiundvirzig) LblStatistik.Text += "\nDie Zahl 42 kommt in der Liste vor";
        }

        private void BtnStat_Click(object sender, EventArgs e)
        {
            Statistik();
        }

        private void BtnSort_Click(object sender, EventArgs e)
        {
            Sortieren();
        }

        private void BtnBuble_Click(object sender, EventArgs e)
        {
            LstBoxSortiert.Items.Clear();
            b.Initialize();
            int[] arr = new int[15];
            a.CopyTo(arr, 0);
            for (int write = 0; write < arr.Length; write++)
            {
                for (int sort = 0; sort < arr.Length - 1; sort++)
                {
                    if (arr[sort] > arr[sort + 1])
                    {
                        int temp = arr[sort + 1];
                        arr[sort + 1] = arr[sort];
                        arr[sort] = temp;
                    }
                }
            }
            arr.CopyTo(b, 0);
            foreach (int i in arr)
            {
                LstBoxSortiert.Items.Add(i);
            }
        }

        private void BtnSelection_Click(object sender, EventArgs e)
        {
            int[] feld = new int[15];
            a.CopyTo(feld, 0);
            for (int i = feld.Length - 1; i > 0; --i)
            {
                // Position min der kleinsten Zahl ab Position i suchen
                int min = i;
                for (int j = i - 1; j >= 0; --j)
                    if (feld[j] < feld[min])
                        min = j;

                // Zahl an Position i mit der kleinsten Zahl vertauschen
                int tmp = feld[min];
                feld[min] = feld[i];
                feld[i] = tmp;
            }
            LstBoxSortiert.Items.Clear();
            foreach (int i in feld)
            {
                LstBoxSortiert.Items.Add(i);
            }
            sortiert = true;
        }

        public static int BinSearch(int[] x, int searchValue)
        {
            // Returns index of searchValue in sorted array x, or -1 if not found
            int left = 0;
            int right = x.Length - 1;
            return binarySearch(ref x, searchValue, left, right);
        }

        public static int binarySearch(ref int[] x, int searchValue, int left, int right)
        {
            if (right < left)
            {
                return -1;
            }
            int mid = (left + right) >> 1;
            if (searchValue > x[mid])
            {
                return binarySearch(ref x, searchValue, mid + 1, right);
            }
            else if (searchValue < x[mid])
            {
                return binarySearch(ref x, searchValue, left, mid - 1);
            }
            else
            {
                return mid;
            }
        }

        private void BtnLinear_Click(object sender, EventArgs e)
        {
            int zu_suchend = 0;
            string text = TxbZahl.Text;
            if (text.Replace("-", "").All(char.IsDigit))
            {
                try
                {
                    zu_suchend = Convert.ToInt32(text);
                    int eintrag = IntArrayLinearSearch(a, zu_suchend);
                    if (eintrag == -1)
                    {
                        LblSuchenAusgabe.Text = "Die Zahl wurde nicht gefunden";
                    }
                    else
                    {
                        LblSuchenAusgabe.Text = "Die Zahl " + text + " wurde an der Stelle " + (eintrag + 1) + " in der unsortierten Liste gefunden";
                        LstBoxUnsortiert.SelectedIndex = eintrag;
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Bitte nur zahlen eingeben");
                }
            }
            else
            {
                MessageBox.Show("Bitte nur zahlen eingeben");

            }
        }

        private void BtnSuchen_Click(object sender, EventArgs e)
        {
            int zu_suchend = 0;
            string text = TxbZahl.Text;
            if (text.Replace("-", "").All(char.IsDigit) && sortiert)
            {
                try
                {
                    zu_suchend = Convert.ToInt32(text);
                    int eintrag = BinSearch(b, zu_suchend);
                    if (eintrag == -1)
                    {
                        LblSuchenAusgabe.Text = "Die Zahl wurde nicht gefunden";
                    }
                    else
                    {
                        LblSuchenAusgabe.Text = "Die Zahl " + text + " wurde an der Stelle " + (eintrag + 1) + " in der sortierten Liste gefunden";
                        LstBoxSortiert.SelectedIndex = eintrag;
                    }
                }
                catch (FormatException)
                {
                    MessageBox.Show("Bitte nur zahlen eingeben");
                }
            }
            else if (!sortiert)
            {
                MessageBox.Show("Zum verwenden von Binary search müssen sie die Liste zuerst sortieren");
            }
            else
            {
                MessageBox.Show("Bitte nur zahlen eingeben");

            }
        }
    }
}
