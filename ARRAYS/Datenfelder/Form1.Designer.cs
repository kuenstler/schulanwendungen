﻿namespace Datenfelder
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.LstBoxUnsortiert = new System.Windows.Forms.ListBox();
            this.LblUnsortiert = new System.Windows.Forms.Label();
            this.LblSortiert = new System.Windows.Forms.Label();
            this.LstBoxSortiert = new System.Windows.Forms.ListBox();
            this.LblStatistik = new System.Windows.Forms.Label();
            this.GrpBoxSuchen = new System.Windows.Forms.GroupBox();
            this.LblSuchenAusgabe = new System.Windows.Forms.Label();
            this.BtnSuchen = new System.Windows.Forms.Button();
            this.BtnLinear = new System.Windows.Forms.Button();
            this.TxbZahl = new System.Windows.Forms.TextBox();
            this.BtnErzeugen = new System.Windows.Forms.Button();
            this.BtnStat = new System.Windows.Forms.Button();
            this.BtnSort = new System.Windows.Forms.Button();
            this.BtnBuble = new System.Windows.Forms.Button();
            this.BtnSelection = new System.Windows.Forms.Button();
            this.GrpBoxSuchen.SuspendLayout();
            this.SuspendLayout();
            // 
            // LstBoxUnsortiert
            // 
            this.LstBoxUnsortiert.FormattingEnabled = true;
            this.LstBoxUnsortiert.Location = new System.Drawing.Point(12, 29);
            this.LstBoxUnsortiert.Name = "LstBoxUnsortiert";
            this.LstBoxUnsortiert.Size = new System.Drawing.Size(80, 199);
            this.LstBoxUnsortiert.TabIndex = 0;
            // 
            // LblUnsortiert
            // 
            this.LblUnsortiert.AutoSize = true;
            this.LblUnsortiert.Location = new System.Drawing.Point(13, 13);
            this.LblUnsortiert.Name = "LblUnsortiert";
            this.LblUnsortiert.Size = new System.Drawing.Size(52, 13);
            this.LblUnsortiert.TabIndex = 1;
            this.LblUnsortiert.Text = "Unsortiert";
            // 
            // LblSortiert
            // 
            this.LblSortiert.AutoSize = true;
            this.LblSortiert.Location = new System.Drawing.Point(124, 13);
            this.LblSortiert.Name = "LblSortiert";
            this.LblSortiert.Size = new System.Drawing.Size(40, 13);
            this.LblSortiert.TabIndex = 2;
            this.LblSortiert.Text = "Sortiert";
            // 
            // LstBoxSortiert
            // 
            this.LstBoxSortiert.FormattingEnabled = true;
            this.LstBoxSortiert.Location = new System.Drawing.Point(127, 29);
            this.LstBoxSortiert.Name = "LstBoxSortiert";
            this.LstBoxSortiert.Size = new System.Drawing.Size(80, 199);
            this.LstBoxSortiert.TabIndex = 3;
            // 
            // LblStatistik
            // 
            this.LblStatistik.AutoSize = true;
            this.LblStatistik.Location = new System.Drawing.Point(214, 38);
            this.LblStatistik.Name = "LblStatistik";
            this.LblStatistik.Size = new System.Drawing.Size(44, 13);
            this.LblStatistik.TabIndex = 4;
            this.LblStatistik.Text = "Statistik";
            // 
            // GrpBoxSuchen
            // 
            this.GrpBoxSuchen.Controls.Add(this.LblSuchenAusgabe);
            this.GrpBoxSuchen.Controls.Add(this.BtnSuchen);
            this.GrpBoxSuchen.Controls.Add(this.BtnLinear);
            this.GrpBoxSuchen.Controls.Add(this.TxbZahl);
            this.GrpBoxSuchen.Location = new System.Drawing.Point(12, 235);
            this.GrpBoxSuchen.Name = "GrpBoxSuchen";
            this.GrpBoxSuchen.Size = new System.Drawing.Size(365, 83);
            this.GrpBoxSuchen.TabIndex = 5;
            this.GrpBoxSuchen.TabStop = false;
            this.GrpBoxSuchen.Text = "Zahl suchen";
            // 
            // LblSuchenAusgabe
            // 
            this.LblSuchenAusgabe.AutoSize = true;
            this.LblSuchenAusgabe.Location = new System.Drawing.Point(7, 59);
            this.LblSuchenAusgabe.Name = "LblSuchenAusgabe";
            this.LblSuchenAusgabe.Size = new System.Drawing.Size(72, 13);
            this.LblSuchenAusgabe.TabIndex = 3;
            this.LblSuchenAusgabe.Text = "Suchergebnis";
            // 
            // BtnSuchen
            // 
            this.BtnSuchen.Location = new System.Drawing.Point(186, 16);
            this.BtnSuchen.Name = "BtnSuchen";
            this.BtnSuchen.Size = new System.Drawing.Size(75, 23);
            this.BtnSuchen.TabIndex = 2;
            this.BtnSuchen.Text = "Suchen";
            this.BtnSuchen.UseVisualStyleBackColor = true;
            this.BtnSuchen.Click += new System.EventHandler(this.BtnSuchen_Click);
            // 
            // BtnLinear
            // 
            this.BtnLinear.Location = new System.Drawing.Point(95, 16);
            this.BtnLinear.Name = "BtnLinear";
            this.BtnLinear.Size = new System.Drawing.Size(85, 23);
            this.BtnLinear.TabIndex = 1;
            this.BtnLinear.Text = "Linear suchen";
            this.BtnLinear.UseVisualStyleBackColor = true;
            this.BtnLinear.Click += new System.EventHandler(this.BtnLinear_Click);
            // 
            // TxbZahl
            // 
            this.TxbZahl.Location = new System.Drawing.Point(7, 20);
            this.TxbZahl.Name = "TxbZahl";
            this.TxbZahl.Size = new System.Drawing.Size(81, 20);
            this.TxbZahl.TabIndex = 0;
            // 
            // BtnErzeugen
            // 
            this.BtnErzeugen.Location = new System.Drawing.Point(464, 13);
            this.BtnErzeugen.Name = "BtnErzeugen";
            this.BtnErzeugen.Size = new System.Drawing.Size(83, 23);
            this.BtnErzeugen.TabIndex = 6;
            this.BtnErzeugen.Text = "Erzeugen";
            this.BtnErzeugen.UseVisualStyleBackColor = true;
            this.BtnErzeugen.Click += new System.EventHandler(this.BtnErzeugen_Click);
            // 
            // BtnStat
            // 
            this.BtnStat.Location = new System.Drawing.Point(464, 42);
            this.BtnStat.Name = "BtnStat";
            this.BtnStat.Size = new System.Drawing.Size(83, 23);
            this.BtnStat.TabIndex = 7;
            this.BtnStat.Text = "Statistik";
            this.BtnStat.UseVisualStyleBackColor = true;
            this.BtnStat.Click += new System.EventHandler(this.BtnStat_Click);
            // 
            // BtnSort
            // 
            this.BtnSort.Location = new System.Drawing.Point(464, 71);
            this.BtnSort.Name = "BtnSort";
            this.BtnSort.Size = new System.Drawing.Size(83, 23);
            this.BtnSort.TabIndex = 8;
            this.BtnSort.Text = "Sortieren";
            this.BtnSort.UseVisualStyleBackColor = true;
            this.BtnSort.Click += new System.EventHandler(this.BtnSort_Click);
            // 
            // BtnBuble
            // 
            this.BtnBuble.Location = new System.Drawing.Point(464, 100);
            this.BtnBuble.Name = "BtnBuble";
            this.BtnBuble.Size = new System.Drawing.Size(83, 37);
            this.BtnBuble.TabIndex = 9;
            this.BtnBuble.Text = "Bubble Sort\r\naufsteigend";
            this.BtnBuble.UseVisualStyleBackColor = true;
            this.BtnBuble.Click += new System.EventHandler(this.BtnBuble_Click);
            // 
            // BtnSelection
            // 
            this.BtnSelection.Location = new System.Drawing.Point(464, 143);
            this.BtnSelection.Name = "BtnSelection";
            this.BtnSelection.Size = new System.Drawing.Size(83, 37);
            this.BtnSelection.TabIndex = 10;
            this.BtnSelection.Text = "Selection Sort\r\nabsteigend";
            this.BtnSelection.UseVisualStyleBackColor = true;
            this.BtnSelection.Click += new System.EventHandler(this.BtnSelection_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(559, 327);
            this.Controls.Add(this.BtnSelection);
            this.Controls.Add(this.BtnBuble);
            this.Controls.Add(this.BtnSort);
            this.Controls.Add(this.BtnStat);
            this.Controls.Add(this.BtnErzeugen);
            this.Controls.Add(this.GrpBoxSuchen);
            this.Controls.Add(this.LblStatistik);
            this.Controls.Add(this.LstBoxSortiert);
            this.Controls.Add(this.LblSortiert);
            this.Controls.Add(this.LblUnsortiert);
            this.Controls.Add(this.LstBoxUnsortiert);
            this.Name = "Form1";
            this.Text = "Form1";
            this.GrpBoxSuchen.ResumeLayout(false);
            this.GrpBoxSuchen.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LstBoxUnsortiert;
        private System.Windows.Forms.Label LblUnsortiert;
        private System.Windows.Forms.Label LblSortiert;
        private System.Windows.Forms.ListBox LstBoxSortiert;
        private System.Windows.Forms.Label LblStatistik;
        private System.Windows.Forms.GroupBox GrpBoxSuchen;
        private System.Windows.Forms.Label LblSuchenAusgabe;
        private System.Windows.Forms.Button BtnSuchen;
        private System.Windows.Forms.Button BtnLinear;
        private System.Windows.Forms.TextBox TxbZahl;
        private System.Windows.Forms.Button BtnErzeugen;
        private System.Windows.Forms.Button BtnStat;
        private System.Windows.Forms.Button BtnSort;
        private System.Windows.Forms.Button BtnBuble;
        private System.Windows.Forms.Button BtnSelection;
    }
}

