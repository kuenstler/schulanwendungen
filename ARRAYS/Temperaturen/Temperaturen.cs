﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Temperaturen
{
    public partial class Temperaturen : Form
    {
        // Formularvariaable
        double[,] a = new double[24, 3];
        
        public Temperaturen()
        {
            InitializeComponent();
            NeueWerte();
        }

        private void NeueWerte()
        {
            // Zuffalszahlengeneratorobjekt
            Random r = new Random();

            LstBoxCh.Items.Clear();
            LstBoxDD.Items.Clear();
            LstBoxL.Items.Clear();

            // for-Schleife für die Uhrzeiten-indicies
            for (int i = 0; i <= a.GetUpperBound(0); i++)
            {
                // for-Schleife für die Städte-indicies
                for (int j = 0; j <= a.GetUpperBound(1); j++)
                {
                    a[i, j] = (double)r.Next(-50, 151) / 10.0;
                }
                LstBoxDD.Items.Add(a[i, 0]);
                LstBoxL.Items.Add(a[i, 1]);
                LstBoxCh.Items.Add(a[i, 2]);
            }
        }

        private void BtnNeu_Click(object sender, EventArgs e)
        {
            NeueWerte();
        }

        private void BtnAuswert_Click(object sender, EventArgs e)
        {
            double max = a[0, 0];
            int[] max_cord = { 0, 0 };
            double min = a[0, 0];
            double sum = 0;
            double avg = 0;
            for (int i = 0; i <= a.GetUpperBound(1); i++)
            {
                for (int j = 0; j <= a.GetUpperBound(0); j++)
                {
                    if (a[j, i] > max)
                    {
                        max = a[j, i];
                        max_cord[0] = j;
                        max_cord[1] = i;
                    }
                    if (a[j, i] < min) min = a[j, i];
                    sum += a[j, i];
                }
            }
            avg = sum / (double)a.Length;
            LblAuswert.Text = "Anzahl der Werte: " + a.Length +
                "\nMaximalwert: " + max +
                "\nMinimalwert: " + min +
                "\nDurchschnitt:" + avg;
            switch (max_cord[1])
            {
                case 0:
                    LstBoxDD.SelectedIndex = max_cord[0];
                    break;
                case 1:
                    LstBoxL.SelectedIndex = max_cord[0];
                    break;
                case 2:
                    LstBoxCh.SelectedIndex = max_cord[0];
                    break;
            }
        }
    }
}
