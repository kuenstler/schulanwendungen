﻿namespace Temperaturen
{
    partial class Temperaturen
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.LstBoxUhr = new System.Windows.Forms.ListBox();
            this.LblUhr = new System.Windows.Forms.Label();
            this.LblDD = new System.Windows.Forms.Label();
            this.LstBoxDD = new System.Windows.Forms.ListBox();
            this.LstBoxL = new System.Windows.Forms.ListBox();
            this.LblL = new System.Windows.Forms.Label();
            this.LstBoxCh = new System.Windows.Forms.ListBox();
            this.LblCh = new System.Windows.Forms.Label();
            this.BtnNeu = new System.Windows.Forms.Button();
            this.BtnAuswert = new System.Windows.Forms.Button();
            this.LblAuswert = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LstBoxUhr
            // 
            this.LstBoxUhr.FormattingEnabled = true;
            this.LstBoxUhr.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12",
            "13",
            "14",
            "15",
            "16",
            "17",
            "18",
            "19",
            "20",
            "21",
            "22",
            "23",
            "24"});
            this.LstBoxUhr.Location = new System.Drawing.Point(15, 29);
            this.LstBoxUhr.Name = "LstBoxUhr";
            this.LstBoxUhr.Size = new System.Drawing.Size(47, 316);
            this.LstBoxUhr.TabIndex = 0;
            // 
            // LblUhr
            // 
            this.LblUhr.AutoSize = true;
            this.LblUhr.Location = new System.Drawing.Point(12, 13);
            this.LblUhr.Name = "LblUhr";
            this.LblUhr.Size = new System.Drawing.Size(40, 13);
            this.LblUhr.TabIndex = 1;
            this.LblUhr.Text = "Uhrzeit";
            // 
            // LblDD
            // 
            this.LblDD.AutoSize = true;
            this.LblDD.Location = new System.Drawing.Point(78, 13);
            this.LblDD.Name = "LblDD";
            this.LblDD.Size = new System.Drawing.Size(48, 13);
            this.LblDD.TabIndex = 2;
            this.LblDD.Text = "°C in DD";
            // 
            // LstBoxDD
            // 
            this.LstBoxDD.FormattingEnabled = true;
            this.LstBoxDD.Location = new System.Drawing.Point(81, 30);
            this.LstBoxDD.Name = "LstBoxDD";
            this.LstBoxDD.Size = new System.Drawing.Size(45, 316);
            this.LstBoxDD.TabIndex = 3;
            // 
            // LstBoxL
            // 
            this.LstBoxL.FormattingEnabled = true;
            this.LstBoxL.Location = new System.Drawing.Point(132, 30);
            this.LstBoxL.Name = "LstBoxL";
            this.LstBoxL.Size = new System.Drawing.Size(45, 316);
            this.LstBoxL.TabIndex = 5;
            // 
            // LblL
            // 
            this.LblL.AutoSize = true;
            this.LblL.Location = new System.Drawing.Point(129, 13);
            this.LblL.Name = "LblL";
            this.LblL.Size = new System.Drawing.Size(38, 13);
            this.LblL.TabIndex = 4;
            this.LblL.Text = "°C in L";
            // 
            // LstBoxCh
            // 
            this.LstBoxCh.FormattingEnabled = true;
            this.LstBoxCh.Location = new System.Drawing.Point(183, 30);
            this.LstBoxCh.Name = "LstBoxCh";
            this.LstBoxCh.Size = new System.Drawing.Size(45, 316);
            this.LstBoxCh.TabIndex = 7;
            // 
            // LblCh
            // 
            this.LblCh.AutoSize = true;
            this.LblCh.Location = new System.Drawing.Point(180, 13);
            this.LblCh.Name = "LblCh";
            this.LblCh.Size = new System.Drawing.Size(45, 13);
            this.LblCh.TabIndex = 6;
            this.LblCh.Text = "°C in Ch";
            // 
            // BtnNeu
            // 
            this.BtnNeu.Location = new System.Drawing.Point(234, 29);
            this.BtnNeu.Name = "BtnNeu";
            this.BtnNeu.Size = new System.Drawing.Size(138, 36);
            this.BtnNeu.TabIndex = 8;
            this.BtnNeu.Text = "Neue Temperaturwerte simulieren";
            this.BtnNeu.UseVisualStyleBackColor = true;
            this.BtnNeu.Click += new System.EventHandler(this.BtnNeu_Click);
            // 
            // BtnAuswert
            // 
            this.BtnAuswert.Location = new System.Drawing.Point(234, 71);
            this.BtnAuswert.Name = "BtnAuswert";
            this.BtnAuswert.Size = new System.Drawing.Size(138, 36);
            this.BtnAuswert.TabIndex = 9;
            this.BtnAuswert.Text = "Temperaturwerte Auswerten";
            this.BtnAuswert.UseVisualStyleBackColor = true;
            this.BtnAuswert.Click += new System.EventHandler(this.BtnAuswert_Click);
            // 
            // LblAuswert
            // 
            this.LblAuswert.AutoSize = true;
            this.LblAuswert.Location = new System.Drawing.Point(234, 114);
            this.LblAuswert.Name = "LblAuswert";
            this.LblAuswert.Size = new System.Drawing.Size(63, 13);
            this.LblAuswert.TabIndex = 10;
            this.LblAuswert.Text = "Auswertung";
            // 
            // Temperaturen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(416, 363);
            this.Controls.Add(this.LblAuswert);
            this.Controls.Add(this.BtnAuswert);
            this.Controls.Add(this.BtnNeu);
            this.Controls.Add(this.LstBoxCh);
            this.Controls.Add(this.LblCh);
            this.Controls.Add(this.LstBoxL);
            this.Controls.Add(this.LblL);
            this.Controls.Add(this.LstBoxDD);
            this.Controls.Add(this.LblDD);
            this.Controls.Add(this.LblUhr);
            this.Controls.Add(this.LstBoxUhr);
            this.Name = "Temperaturen";
            this.Text = "Temperaturen";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox LstBoxUhr;
        private System.Windows.Forms.Label LblUhr;
        private System.Windows.Forms.Label LblDD;
        private System.Windows.Forms.ListBox LstBoxDD;
        private System.Windows.Forms.ListBox LstBoxL;
        private System.Windows.Forms.Label LblL;
        private System.Windows.Forms.ListBox LstBoxCh;
        private System.Windows.Forms.Label LblCh;
        private System.Windows.Forms.Button BtnNeu;
        private System.Windows.Forms.Button BtnAuswert;
        private System.Windows.Forms.Label LblAuswert;
    }
}

