﻿namespace dynamischeDatenfelder
{
    partial class dynamischeDatenfelder
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.NumDim3 = new System.Windows.Forms.NumericUpDown();
            this.BtnDim1 = new System.Windows.Forms.Button();
            this.BtnDim2 = new System.Windows.Forms.Button();
            this.BtnDim3 = new System.Windows.Forms.Button();
            this.LstBoxListe = new System.Windows.Forms.ListBox();
            this.NumPriem = new System.Windows.Forms.NumericUpDown();
            this.BtnPriem = new System.Windows.Forms.Button();
            this.LblPriemZeit = new System.Windows.Forms.Label();
            this.ChkMehrPriem = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.NumDim3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPriem)).BeginInit();
            this.SuspendLayout();
            // 
            // NumDim3
            // 
            this.NumDim3.Location = new System.Drawing.Point(251, 180);
            this.NumDim3.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.NumDim3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.NumDim3.Name = "NumDim3";
            this.NumDim3.Size = new System.Drawing.Size(120, 20);
            this.NumDim3.TabIndex = 3;
            this.NumDim3.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // BtnDim1
            // 
            this.BtnDim1.Location = new System.Drawing.Point(251, 13);
            this.BtnDim1.Name = "BtnDim1";
            this.BtnDim1.Size = new System.Drawing.Size(120, 23);
            this.BtnDim1.TabIndex = 1;
            this.BtnDim1.Text = "Diemension 1";
            this.BtnDim1.UseVisualStyleBackColor = true;
            this.BtnDim1.Click += new System.EventHandler(this.BtnDim1_Click);
            // 
            // BtnDim2
            // 
            this.BtnDim2.Location = new System.Drawing.Point(251, 42);
            this.BtnDim2.Name = "BtnDim2";
            this.BtnDim2.Size = new System.Drawing.Size(120, 23);
            this.BtnDim2.TabIndex = 2;
            this.BtnDim2.Text = "Diemension 2";
            this.BtnDim2.UseVisualStyleBackColor = true;
            this.BtnDim2.Click += new System.EventHandler(this.BtnDim2_Click);
            // 
            // BtnDim3
            // 
            this.BtnDim3.Location = new System.Drawing.Point(251, 206);
            this.BtnDim3.Name = "BtnDim3";
            this.BtnDim3.Size = new System.Drawing.Size(120, 23);
            this.BtnDim3.TabIndex = 4;
            this.BtnDim3.Text = "Diemension 3";
            this.BtnDim3.UseVisualStyleBackColor = true;
            this.BtnDim3.Click += new System.EventHandler(this.BtnDim3_Click);
            // 
            // LstBoxListe
            // 
            this.LstBoxListe.FormattingEnabled = true;
            this.LstBoxListe.Location = new System.Drawing.Point(13, 13);
            this.LstBoxListe.Name = "LstBoxListe";
            this.LstBoxListe.Size = new System.Drawing.Size(120, 342);
            this.LstBoxListe.TabIndex = 7;
            // 
            // NumPriem
            // 
            this.NumPriem.Location = new System.Drawing.Point(251, 236);
            this.NumPriem.Maximum = new decimal(new int[] {
            268435454,
            0,
            0,
            0});
            this.NumPriem.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.NumPriem.Name = "NumPriem";
            this.NumPriem.Size = new System.Drawing.Size(120, 20);
            this.NumPriem.TabIndex = 5;
            this.NumPriem.Value = new decimal(new int[] {
            1000000,
            0,
            0,
            0});
            // 
            // BtnPriem
            // 
            this.BtnPriem.Location = new System.Drawing.Point(251, 263);
            this.BtnPriem.Name = "BtnPriem";
            this.BtnPriem.Size = new System.Drawing.Size(120, 23);
            this.BtnPriem.TabIndex = 6;
            this.BtnPriem.Text = "Priemzahlen";
            this.BtnPriem.UseVisualStyleBackColor = true;
            this.BtnPriem.Click += new System.EventHandler(this.BtnPriem_Click);
            // 
            // LblPriemZeit
            // 
            this.LblPriemZeit.AutoSize = true;
            this.LblPriemZeit.Location = new System.Drawing.Point(251, 293);
            this.LblPriemZeit.Name = "LblPriemZeit";
            this.LblPriemZeit.Size = new System.Drawing.Size(25, 13);
            this.LblPriemZeit.TabIndex = 8;
            this.LblPriemZeit.Text = "Zeit";
            // 
            // ChkMehrPriem
            // 
            this.ChkMehrPriem.AutoSize = true;
            this.ChkMehrPriem.Location = new System.Drawing.Point(251, 341);
            this.ChkMehrPriem.Name = "ChkMehrPriem";
            this.ChkMehrPriem.Size = new System.Drawing.Size(94, 17);
            this.ChkMehrPriem.TabIndex = 9;
            this.ChkMehrPriem.Text = "PriemOverflow";
            this.ChkMehrPriem.UseVisualStyleBackColor = true;
            // 
            // dynamischeDatenfelder
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(462, 370);
            this.Controls.Add(this.ChkMehrPriem);
            this.Controls.Add(this.LblPriemZeit);
            this.Controls.Add(this.BtnPriem);
            this.Controls.Add(this.NumPriem);
            this.Controls.Add(this.LstBoxListe);
            this.Controls.Add(this.BtnDim3);
            this.Controls.Add(this.BtnDim2);
            this.Controls.Add(this.BtnDim1);
            this.Controls.Add(this.NumDim3);
            this.Name = "dynamischeDatenfelder";
            this.Text = "dynamische Datenfelder";
            ((System.ComponentModel.ISupportInitialize)(this.NumDim3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NumPriem)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.NumericUpDown NumDim3;
        private System.Windows.Forms.Button BtnDim1;
        private System.Windows.Forms.Button BtnDim2;
        private System.Windows.Forms.Button BtnDim3;
        private System.Windows.Forms.ListBox LstBoxListe;
        private System.Windows.Forms.NumericUpDown NumPriem;
        private System.Windows.Forms.Button BtnPriem;
        private System.Windows.Forms.Label LblPriemZeit;
        private System.Windows.Forms.CheckBox ChkMehrPriem;
    }
}

