﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace dynamischeDatenfelder
{
    public partial class dynamischeDatenfelder : Form
    {
        Random r = new Random();
        int[] a;

        public dynamischeDatenfelder()
        {
            InitializeComponent();
        }

        private void ListeFuellenAusgeben(int zero)
        {
            LstBoxListe.Items.Clear();

            for (int i = 0; i < a.Length; i++)
            {
                if (i >= zero) 
                {
                    int number;
                    do
                    {
                        number = r.Next(10, 99);
                    }
                    while (Array.IndexOf(a, number) != -1);
                    a[i] = number;
                }
                LstBoxListe.Items.Add(a[i]);
            }
        }

        private void BtnDim1_Click(object sender, EventArgs e)
        {
            a = new int[4];
            ListeFuellenAusgeben(0);
        }

        private void BtnDim2_Click(object sender, EventArgs e)
        {
            int zero = a.Length;
            Array.Resize(ref a, 6);
            ListeFuellenAusgeben(zero);
        }

        private void BtnDim3_Click(object sender, EventArgs e)
        {
            int zero = a.Length;
            Array.Resize(ref a, (int)NumDim3.Value);
            ListeFuellenAusgeben(zero);
        }

        private void BtnPriem_Click(object sender, EventArgs e)
        {
            int[] b = new int[(int)NumPriem.Value + 1];
            for (int i = 2; i < b.Length; i++)
            {
                b[i] = i;
            }

            double wurzel = Math.Sqrt(b.Length);
            List<int> tempList = new List<int>();
            DateTime p = DateTime.Now;
            for (int i = 2; i < b.Length; i++)
            {
                if (b[i] != 0)
                {
                    if (i <= wurzel)
                    {
                        int j = b[i] * 2;
                        while (j < b.Length)
                        {
                            b[j] = 0;
                            j += b[i];
                        }
                    }
                    tempList.Add(b[i]);
                }
            }
            int[] a = tempList.ToArray();
            TimeSpan diff = DateTime.Now - p;
            LblPriemZeit.Text = "Zeit: " + diff.Milliseconds + " Millisekunden";
            LblPriemZeit.Text += "\nEs wurden " + a.Length + " Priemzahlen gefunden";
            LstBoxListe.Items.Clear();
            for (int i = 0; i < a.Length; i++)
            {
                if (i < 10000 || ChkMehrPriem.Checked) LstBoxListe.Items.Add(a[i]);
            }
        }
    }
}
