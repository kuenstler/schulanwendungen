﻿namespace DatenfeldbVerzweigt
{
    partial class DatenfeldVerzweigt
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnAnz = new System.Windows.Forms.Button();
            this.LblAus = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // BtnAnz
            // 
            this.BtnAnz.Location = new System.Drawing.Point(13, 13);
            this.BtnAnz.Name = "BtnAnz";
            this.BtnAnz.Size = new System.Drawing.Size(75, 23);
            this.BtnAnz.TabIndex = 0;
            this.BtnAnz.Text = "Anzeigen";
            this.BtnAnz.UseVisualStyleBackColor = true;
            this.BtnAnz.Click += new System.EventHandler(this.BtnAnz_Click);
            // 
            // LblAus
            // 
            this.LblAus.AutoSize = true;
            this.LblAus.Location = new System.Drawing.Point(13, 43);
            this.LblAus.Name = "LblAus";
            this.LblAus.Size = new System.Drawing.Size(49, 13);
            this.LblAus.TabIndex = 1;
            this.LblAus.Text = "Ausgabe";
            // 
            // DatenfeldVerzweigt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(400, 346);
            this.Controls.Add(this.LblAus);
            this.Controls.Add(this.BtnAnz);
            this.Name = "DatenfeldVerzweigt";
            this.Text = "Datenfeld, verzweigt";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnAnz;
        private System.Windows.Forms.Label LblAus;
    }
}

