﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DatenfeldbVerzweigt
{
    public partial class DatenfeldVerzweigt : Form
    {
        public DatenfeldVerzweigt()
        {
            InitializeComponent();
        }

        private void BtnAnz_Click(object sender, EventArgs e)
        {
            Random r = new Random();

            double[][] a = new double[r.Next(1, 10)][];
            int anz = 0;

            for (int i = 0; i <= a.GetUpperBound(0); i++)
            {
                a[i] = new double[r.Next(1, 10)];
            }

            LblAus.Text = "";

            for (int i = 0; i < a.Length; i++)
            {
                for (int j = 0; j < a[i].Length; j++)
                {
                    a[i][j] = Math.Round(r.NextDouble(), 3);
                    LblAus.Text += a[i][j].ToString("0.000") + " ";
                }
                anz += a[i].Length;
                LblAus.Text += "\n";
            }
            LblAus.Text += "Anzahl: " + anz;
        }
    }
}
