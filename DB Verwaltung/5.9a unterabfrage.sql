SELECT mnr Mieternummer, nachname Nachname, ort Ort, mietkosten Mietkosten
FROM mieter
WHERE mietkosten = ANY (SELECT mietkosten FROM mieter WHERE ort LIKE 'Berlin')