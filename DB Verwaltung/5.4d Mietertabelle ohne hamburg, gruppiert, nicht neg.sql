SELECT ort Ort, SUM(mietkosten) 'Mietkosten'
FROM mieter
WHERE ort NOT LIKE 'Hamburg'
GROUP BY ort
HAVING SUM(mietkosten) >= 0;