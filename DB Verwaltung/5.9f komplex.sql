SELECT *
FROM niederlassung 
WHERE ort = (
	SELECT ort
	FROM mieter
	WHERE mietkosten = (
		SELECT MIN(mietkosten)
		FROM mieter));