-- Tabelle l�schen
DROP TABLE kunden
-- Tabelle Kunden anlegen
CREATE TABLE kunden
(
	knr		INT NOT NULL CONSTRAINT knr_const CHECK ( knr BETWEEN 1000 AND 9999 ),
	name1	CHAR(20),
	name2	CHAR(20),
	strasse	CHAR(20),
	bezirk	INT,
	beginn	DATE NOT NULL,
	typ		CHAR(2) DEFAULT 'EH' CHECK ( typ IN ('EH', 'GH', 'SH', 'IN', 'SO') ),
	umsatz	DEC(10,2) DEFAULT 0,
	PRIMARY KEY (knr),
);