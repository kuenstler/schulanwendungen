SELECT SUM(mietkosten) 'Summe der Mietkosten', MIN(mietkosten) 'Minimale Mietkosten', MAX(mietkosten) 'Maximale Mietkosten', AVG(mietkosten) 'Durchschnitt der Mietkosten', Count(*) 'Anzahl der Mieter'
FROM mieter
WHERE ort LIKE 'Berlin';