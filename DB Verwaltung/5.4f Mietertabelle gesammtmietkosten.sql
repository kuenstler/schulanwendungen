SELECT ort Ort, SUM(mietkosten) 'Mietkosten'
FROM mieter
GROUP BY ort
HAVING COUNT(*) >= 2 AND SUM(mietkosten) <= -200;