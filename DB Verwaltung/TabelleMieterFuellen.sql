DELETE FROM mieter;
--
INSERT mieter VALUES (1200, 'Herr', 'Schwin', 'Heinrich', '81739', 'M�nchen', -45.75);
--
INSERT mieter VALUES (2100, 'Frau', 'Schwan', 'Ursula', '60389', 'Frankfurt a. M.', 120);
--
INSERT mieter VALUES (2400, 'Herr', 'Wei�', 'Werner', '10785', 'Berlin', 210.50);
--
INSERT mieter VALUES (2500, 'Herr', 'Klemm', 'Friedrich', '70569', 'Stuttgart', 0);
--
INSERT mieter VALUES (3000, 'Firma', 'SOAG', NULL, '22525', 'Hamburg', -5423.25);
--
INSERT mieter VALUES (3200, 'Herr', 'Weitzel', 'Michael', '40233', 'D�sseldorf', 750);
--
INSERT mieter VALUES (3700, 'Herr', 'Wahle', 'Eberhard', '10787', 'Berlin', 0);
--
INSERT mieter VALUES (4000, 'Frau', 'Baad', 'Susanne', '50933', 'K�ln', 0);
--
INSERT mieter VALUES (4400, 'Herr', 'Schneider', 'Ralph', '89079', 'Ulm', 2110.9);
--
INSERT mieter VALUES (4700, 'Herr', 'Bank', 'Anton', '52070', 'Aachen', 0);
--
INSERT mieter VALUES (5000, 'Herr', 'Gauss', 'Manfred', '15236', 'Frankfurt/Oder', 210.50);
--
INSERT mieter VALUES (5300, 'Firma', 'SOFT_KG', NULL , '13355', 'Berlin', - 315.50);
--
INSERT mieter VALUES (6000, 'Frau', 'Rasch', 'Regine', '60313', 'Frankfurt a. M.', -385);
--
INSERT mieter VALUES (1000, 'Frau', 'Bauer', 'Sandra', '40233', 'D�sseldorf', 10000);
--
INSERT mieter VALUES (1100, 'Herr', 'Geyer', 'Peter', '13599', 'Berlin', 0);
