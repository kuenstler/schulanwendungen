SELECT ort Ort, MIN(mietkosten) 'Minimale Mietkosten', AVG(mietkosten) 'Durchschnitt der Mietkosten', MAX(mietkosten) 'Maximale Mietkosten'
FROM mieter
GROUP BY ort
HAVING MIN(mietkosten) < 0 AND COUNT(*) >= 2;