SELECT nachname Nachname, ort Ort, mietkosten Mietkosten
from mieter
ORDER BY ort ASC, mietkosten DESC;