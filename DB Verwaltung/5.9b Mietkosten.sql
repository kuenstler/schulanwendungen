SELECT ort Ort, AVG(mietkosten) 'durchschnittliche Mietkosten'
FROM mieter
WHERE mietkosten > (SELECT AVG(mietkosten) FROM mieter)
GROUP BY ort