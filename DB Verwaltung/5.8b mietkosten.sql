SELECT nachname Nachname, CONCAT(ROUND(mietkosten, 2), ' EUR') Mietkosten
FROM mieter
ORDER BY nachname ASC;