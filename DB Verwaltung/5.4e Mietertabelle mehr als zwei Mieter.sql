SELECT ort Ort, COUNT(*) 'Anzahl der Mieter', SUM(mietkosten) 'Mietkosten'
FROM mieter
GROUP BY ort
HAVING COUNT(*) > 2;