SELECT b.bnr Buchungsnummer, m.nachname Nachname, b.beginn Mietbeginn, b.ende Mietende
FROM buchung b
INNER JOIN mieter m ON b.mnr = m.mnr;