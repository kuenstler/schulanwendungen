SELECT
	m.nachname Nachname,
	m.ort Mieterort,
	b.bnr Buchungsnummer,
	n.name Niederlassungsname,
	n.ort Niederlassungsort
FROM mieter m
INNER JOIN buchung b ON m.mnr = b.mnr
INNER JOIN niederlassung n ON b.nnr = n.nnr
WHERE m.nachname LIKE 'Schneider'