IF OBJECT_ID('zwoelfa', 'V') IS NOT NULL
    DROP VIEW zwoelfa

GO

CREATE VIEW zwoelfa AS
SELECT
	m.nachname Nachname,
	n.name Niederlassungsname
FROM mieter m
INNER JOIN buchung b ON m.mnr = b.mnr
INNER JOIN niederlassung n ON b.nnr = n.nnr

GO

IF OBJECT_ID('zwoelfb', 'V') IS NOT NULL
    DROP VIEW zwoelfb

GO

CREATE VIEW zwoelfb AS
SELECT
	b.bnr Buchungsnummer,
	m.nachname 'Nachname des Mieters',
	m.ort 'Ort des Mieters',
	n.name Niederlassungsname,
	n.ort Niederlassungsstandort,
	b.gruppe Fahrzeugtyp,
	CONCAT(ROUND(t.kosten, 2), ' EUR') 'Mietpreis pro Tag',
	b.beginn Mietbeginn,
	b.ende Mietende,
	CONCAT(ROUND((DATEDIFF(day, b.beginn,b.ende) + 1) * t.kosten, 2), ' EUR') Gesammtmietkosten
FROM buchung b
LEFT JOIN mieter m ON m.mnr = b.mnr
LEFT JOIN niederlassung n ON b.nnr = n.nnr
LEFT JOIN typ t ON t.nnr = b.nnr AND t.gruppe = b.gruppe
