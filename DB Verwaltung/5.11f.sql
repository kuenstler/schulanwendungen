SELECT
	n.nnr Niederlassungsnummer,
	m.nachname Name,
	b.bnr Buchungsnummer
FROM niederlassung n
RIGHT JOIN buchung b ON b.nnr = n.nnr
INNER JOIN mieter m ON m.mnr = b.mnr
WHERE n.ort LIKE 'Hamburg';