SELECT
	m.nachname Nachname,
	m.ort Mietort,
	b.beginn Mietbeginn,
	n.ort Niederlassungsort
FROM mieter m
INNER JOIN buchung b ON b.mnr = m.mnr
INNER JOIN niederlassung n on b.nnr = n.nnr;