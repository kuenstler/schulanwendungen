-- Mietertabelle erstellen
DROP TABLE mieter;
CREATE TABLE mieter
(
	mnr			INT			NOT NULL CONSTRAINT mnr_const CHECK ( mnr BETWEEN 1000 AND 9999 ),
	anrede		CHAR(6)		CHECK ( anrede IN ('Herr', 'Frau', 'Firma', 'Divers')),
	nachname	CHAR(20)	NOT NULL,
	vorname		CHAR(20)	,
	plz			CHAR(5)		CHECK (plz LIKE '[0-9][0-9][0-9][0-9][0-9]'),
	ort			CHAR(20)	NOT NULL,
	mietkosten	DEC(7,2)	CHECK ( mietkosten >= -10000 AND mietkosten <= 10000)
);