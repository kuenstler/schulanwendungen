SELECT CONCAT(SUBSTRING(vorname, 1, 1), '. ', nachname) Name, ort Ort
FROM mieter
WHERE vorname IS NOT NULL
ORDER BY nachname;