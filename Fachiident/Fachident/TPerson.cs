﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fachident
{
    class TPerson
    {
        private string name;
        private string vorname;
        private string anrede;
        
        public TPerson(string n, string v, string a) {
           name = n;
           vorname = v;
           anrede = a;
        }
        
        public string getPerson() {
            return $"Name: {name}\nVorname: {vorname}\nAnrede: {anrede}\n";
        }
    }
}
