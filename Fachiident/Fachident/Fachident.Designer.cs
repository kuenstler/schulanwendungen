﻿
namespace Fachident
{
    partial class Fachident
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.GtpBoxFach = new System.Windows.Forms.GroupBox();
            this.CmbBoxFach = new System.Windows.Forms.ComboBox();
            this.GrpAusbildung = new System.Windows.Forms.GroupBox();
            this.CmbAusbildung = new System.Windows.Forms.ComboBox();
            this.BtnIdent = new System.Windows.Forms.Button();
            this.BtnAusfuehren = new System.Windows.Forms.Button();
            this.TxbSQL = new System.Windows.Forms.RichTextBox();
            this.GrpDatei = new System.Windows.Forms.GroupBox();
            this.BtnTxtLeeren = new System.Windows.Forms.Button();
            this.BtnLoeschen = new System.Windows.Forms.Button();
            this.BtnExistenzPruefen = new System.Windows.Forms.Button();
            this.BtnSpeichern = new System.Windows.Forms.Button();
            this.BtnLaden = new System.Windows.Forms.Button();
            this.GrpVerzeichnis = new System.Windows.Forms.GroupBox();
            this.BtnDetails = new System.Windows.Forms.Button();
            this.TxbDateiname = new System.Windows.Forms.TextBox();
            this.LblDateiname = new System.Windows.Forms.Label();
            this.BtnCreateFile = new System.Windows.Forms.Button();
            this.BtnDelete = new System.Windows.Forms.Button();
            this.BtnCreate = new System.Windows.Forms.Button();
            this.TxbDatei = new System.Windows.Forms.RichTextBox();
            this.GrpXML = new System.Windows.Forms.GroupBox();
            this.BtnXMLSchreiben = new System.Windows.Forms.Button();
            this.BtnOpenDialog = new System.Windows.Forms.Button();
            this.ChkSource = new System.Windows.Forms.CheckBox();
            this.GtpBoxFach.SuspendLayout();
            this.GrpAusbildung.SuspendLayout();
            this.GrpDatei.SuspendLayout();
            this.GrpVerzeichnis.SuspendLayout();
            this.GrpXML.SuspendLayout();
            this.SuspendLayout();
            // 
            // GtpBoxFach
            // 
            this.GtpBoxFach.Controls.Add(this.CmbBoxFach);
            this.GtpBoxFach.Location = new System.Drawing.Point(13, 13);
            this.GtpBoxFach.Name = "GtpBoxFach";
            this.GtpBoxFach.Size = new System.Drawing.Size(142, 54);
            this.GtpBoxFach.TabIndex = 0;
            this.GtpBoxFach.TabStop = false;
            this.GtpBoxFach.Text = "Fach auswählen";
            // 
            // CmbBoxFach
            // 
            this.CmbBoxFach.FormattingEnabled = true;
            this.CmbBoxFach.Items.AddRange(new object[] {
            "Informatik",
            "Mathematik",
            "Englisch",
            "Biologie",
            "Wirtschaftslehe/Recht",
            "Informatik-Systeme"});
            this.CmbBoxFach.Location = new System.Drawing.Point(7, 20);
            this.CmbBoxFach.Name = "CmbBoxFach";
            this.CmbBoxFach.Size = new System.Drawing.Size(121, 21);
            this.CmbBoxFach.TabIndex = 0;
            // 
            // GrpAusbildung
            // 
            this.GrpAusbildung.Controls.Add(this.CmbAusbildung);
            this.GrpAusbildung.Location = new System.Drawing.Point(13, 74);
            this.GrpAusbildung.Name = "GrpAusbildung";
            this.GrpAusbildung.Size = new System.Drawing.Size(142, 59);
            this.GrpAusbildung.TabIndex = 1;
            this.GrpAusbildung.TabStop = false;
            this.GrpAusbildung.Text = "Ausbildungsart wählen";
            // 
            // CmbAusbildung
            // 
            this.CmbAusbildung.FormattingEnabled = true;
            this.CmbAusbildung.Items.AddRange(new object[] {
            "Grundkurs",
            "Leistungskurs",
            "Berufsschule",
            "Fachoberschule",
            "Berufsvorbereitung"});
            this.CmbAusbildung.Location = new System.Drawing.Point(7, 20);
            this.CmbAusbildung.Name = "CmbAusbildung";
            this.CmbAusbildung.Size = new System.Drawing.Size(121, 21);
            this.CmbAusbildung.TabIndex = 0;
            // 
            // BtnIdent
            // 
            this.BtnIdent.Location = new System.Drawing.Point(13, 140);
            this.BtnIdent.Name = "BtnIdent";
            this.BtnIdent.Size = new System.Drawing.Size(142, 23);
            this.BtnIdent.TabIndex = 2;
            this.BtnIdent.Text = "Ident erstellen";
            this.BtnIdent.UseVisualStyleBackColor = true;
            this.BtnIdent.Click += new System.EventHandler(this.BtnIdent_Click);
            // 
            // BtnAusfuehren
            // 
            this.BtnAusfuehren.Location = new System.Drawing.Point(13, 170);
            this.BtnAusfuehren.Name = "BtnAusfuehren";
            this.BtnAusfuehren.Size = new System.Drawing.Size(142, 23);
            this.BtnAusfuehren.TabIndex = 3;
            this.BtnAusfuehren.Text = "SQL ausführen";
            this.BtnAusfuehren.UseVisualStyleBackColor = true;
            this.BtnAusfuehren.Click += new System.EventHandler(this.BtnAusfuehren_Click);
            // 
            // TxbSQL
            // 
            this.TxbSQL.Location = new System.Drawing.Point(13, 200);
            this.TxbSQL.Name = "TxbSQL";
            this.TxbSQL.ReadOnly = true;
            this.TxbSQL.Size = new System.Drawing.Size(309, 96);
            this.TxbSQL.TabIndex = 4;
            this.TxbSQL.Text = "";
            // 
            // GrpDatei
            // 
            this.GrpDatei.Controls.Add(this.BtnTxtLeeren);
            this.GrpDatei.Controls.Add(this.BtnLoeschen);
            this.GrpDatei.Controls.Add(this.BtnExistenzPruefen);
            this.GrpDatei.Controls.Add(this.BtnSpeichern);
            this.GrpDatei.Controls.Add(this.BtnLaden);
            this.GrpDatei.Controls.Add(this.ChkSource);
            this.GrpDatei.Location = new System.Drawing.Point(162, 13);
            this.GrpDatei.Name = "GrpDatei";
            this.GrpDatei.Size = new System.Drawing.Size(160, 180);
            this.GrpDatei.TabIndex = 5;
            this.GrpDatei.TabStop = false;
            this.GrpDatei.Text = "Dateiarbeit";
            // 
            // BtnTxtLeeren
            // 
            this.BtnTxtLeeren.Location = new System.Drawing.Point(6, 136);
            this.BtnTxtLeeren.Name = "BtnTxtLeeren";
            this.BtnTxtLeeren.Size = new System.Drawing.Size(142, 23);
            this.BtnTxtLeeren.TabIndex = 7;
            this.BtnTxtLeeren.Text = "Textfeld leeren";
            this.BtnTxtLeeren.UseVisualStyleBackColor = true;
            this.BtnTxtLeeren.Click += new System.EventHandler(this.BtnTxtLeeren_Click);
            // 
            // BtnLoeschen
            // 
            this.BtnLoeschen.Location = new System.Drawing.Point(6, 107);
            this.BtnLoeschen.Name = "BtnLoeschen";
            this.BtnLoeschen.Size = new System.Drawing.Size(142, 23);
            this.BtnLoeschen.TabIndex = 6;
            this.BtnLoeschen.Text = "Datei {placeholder} Löschen DO NOT EDIT";
            this.BtnLoeschen.UseVisualStyleBackColor = true;
            this.BtnLoeschen.Click += new System.EventHandler(this.BtnLoeschen_Click);
            // 
            // BtnExistenzPruefen
            // 
            this.BtnExistenzPruefen.Location = new System.Drawing.Point(6, 78);
            this.BtnExistenzPruefen.Name = "BtnExistenzPruefen";
            this.BtnExistenzPruefen.Size = new System.Drawing.Size(142, 23);
            this.BtnExistenzPruefen.TabIndex = 5;
            this.BtnExistenzPruefen.Text = "Dateiexistenz prüfen";
            this.BtnExistenzPruefen.UseVisualStyleBackColor = true;
            this.BtnExistenzPruefen.Click += new System.EventHandler(this.BtnExistenzPruefen_Click);
            // 
            // BtnSpeichern
            // 
            this.BtnSpeichern.Location = new System.Drawing.Point(6, 49);
            this.BtnSpeichern.Name = "BtnSpeichern";
            this.BtnSpeichern.Size = new System.Drawing.Size(142, 23);
            this.BtnSpeichern.TabIndex = 4;
            this.BtnSpeichern.Text = "In {placeholder} speichern DO NOT EDIT";
            this.BtnSpeichern.UseVisualStyleBackColor = true;
            this.BtnSpeichern.Click += new System.EventHandler(this.BtnSpeichern_Click);
            // 
            // BtnLaden
            // 
            this.BtnLaden.Location = new System.Drawing.Point(6, 20);
            this.BtnLaden.Name = "BtnLaden";
            this.BtnLaden.Size = new System.Drawing.Size(142, 23);
            this.BtnLaden.TabIndex = 3;
            this.BtnLaden.Text = "{placeholder} laden DO NOT EDIT";
            this.BtnLaden.UseVisualStyleBackColor = true;
            this.BtnLaden.Click += new System.EventHandler(this.BtnLaden_Click);
            // 
            // ChkSource
            // 
            this.ChkSource.Location = new System.Drawing.Point(6, 165);
            this.ChkSource.Name = "ChkSource";
            this.ChkSource.Size = new System.Drawing.Size(142, 15);
            this.ChkSource.Text = "Von Fächern speichern";
            this.ChkSource.UseVisualStyleBackColor = true;
            this.ChkSource.Checked = true;
            // 
            // GrpVerzeichnis
            // 
            this.GrpVerzeichnis.Controls.Add(this.BtnDetails);
            this.GrpVerzeichnis.Controls.Add(this.TxbDateiname);
            this.GrpVerzeichnis.Controls.Add(this.LblDateiname);
            this.GrpVerzeichnis.Controls.Add(this.BtnCreateFile);
            this.GrpVerzeichnis.Controls.Add(this.BtnDelete);
            this.GrpVerzeichnis.Controls.Add(this.BtnCreate);
            this.GrpVerzeichnis.Location = new System.Drawing.Point(329, 13);
            this.GrpVerzeichnis.Name = "GrpVerzeichnis";
            this.GrpVerzeichnis.Size = new System.Drawing.Size(174, 195);
            this.GrpVerzeichnis.TabIndex = 6;
            this.GrpVerzeichnis.TabStop = false;
            this.GrpVerzeichnis.Text = "Verzeichnisarbeit";
            // 
            // BtnDetails
            // 
            this.BtnDetails.Location = new System.Drawing.Point(6, 165);
            this.BtnDetails.Name = "BtnDetails";
            this.BtnDetails.Size = new System.Drawing.Size(162, 23);
            this.BtnDetails.TabIndex = 13;
            this.BtnDetails.Text = "Datailiste anzeigen";
            this.BtnDetails.UseVisualStyleBackColor = true;
            this.BtnDetails.Click += new System.EventHandler(this.BtnDetails_Click);
            // 
            // TxbDateiname
            // 
            this.TxbDateiname.Location = new System.Drawing.Point(6, 91);
            this.TxbDateiname.Name = "TxbDateiname";
            this.TxbDateiname.Size = new System.Drawing.Size(100, 20);
            this.TxbDateiname.TabIndex = 12;
            // 
            // LblDateiname
            // 
            this.LblDateiname.AutoSize = true;
            this.LblDateiname.Location = new System.Drawing.Point(6, 75);
            this.LblDateiname.Name = "LblDateiname";
            this.LblDateiname.Size = new System.Drawing.Size(61, 13);
            this.LblDateiname.TabIndex = 11;
            this.LblDateiname.Text = "Dateiname:";
            // 
            // BtnCreateFile
            // 
            this.BtnCreateFile.Location = new System.Drawing.Point(6, 107);
            this.BtnCreateFile.Name = "BtnCreateFile";
            this.BtnCreateFile.Size = new System.Drawing.Size(162, 52);
            this.BtnCreateFile.TabIndex = 10;
            this.BtnCreateFile.Text = $"Datei mit Namen im Verzeichnis\r\n{verzeichnis} speichnern";
            this.BtnCreateFile.UseVisualStyleBackColor = true;
            this.BtnCreateFile.Click += new System.EventHandler(this.BtnCreateFile_Click);
            // 
            // BtnDelete
            // 
            this.BtnDelete.Location = new System.Drawing.Point(6, 49);
            this.BtnDelete.Name = "BtnDelete";
            this.BtnDelete.Size = new System.Drawing.Size(162, 23);
            this.BtnDelete.TabIndex = 9;
            this.BtnDelete.Text = $"Verzeichnis {verzeichnis} löschen";
            this.BtnDelete.UseVisualStyleBackColor = true;
            this.BtnDelete.Click += new System.EventHandler(this.BtnDelete_Click);
            // 
            // BtnCreate
            // 
            this.BtnCreate.Location = new System.Drawing.Point(6, 20);
            this.BtnCreate.Name = "BtnCreate";
            this.BtnCreate.Size = new System.Drawing.Size(162, 23);
            this.BtnCreate.TabIndex = 8;
            this.BtnCreate.Text = $"Verzeichnis {verzeichnis} erstellen";
            this.BtnCreate.UseVisualStyleBackColor = true;
            this.BtnCreate.Click += new System.EventHandler(this.BtnCreate_Click);
            // 
            // TxbDatei
            // 
            this.TxbDatei.Location = new System.Drawing.Point(329, 214);
            this.TxbDatei.Name = "TxbDatei";
            this.TxbDatei.Size = new System.Drawing.Size(350, 82);
            this.TxbDatei.TabIndex = 7;
            this.TxbDatei.Text = "";
            // 
            // GrpXML
            // 
            this.GrpXML.Controls.Add(this.BtnXMLSchreiben);
            this.GrpXML.Location = new System.Drawing.Point(509, 60);
            this.GrpXML.Name = "GrpXML";
            this.GrpXML.Size = new System.Drawing.Size(158, 54);
            this.GrpXML.TabIndex = 8;
            this.GrpXML.TabStop = false;
            this.GrpXML.Text = "XML Arbeit";
            // 
            // BtnXMLSchreiben
            // 
            this.BtnXMLSchreiben.Location = new System.Drawing.Point(6, 19);
            this.BtnXMLSchreiben.Name = "BtnXMLSchreiben";
            this.BtnXMLSchreiben.Size = new System.Drawing.Size(142, 23);
            this.BtnXMLSchreiben.TabIndex = 9;
            this.BtnXMLSchreiben.Text = "XML Datei schreiben";
            this.BtnXMLSchreiben.UseVisualStyleBackColor = true;
            // 
            // BtnOpenDialog
            // 
            this.BtnOpenDialog.Location = new System.Drawing.Point(509, 31);
            this.BtnOpenDialog.Name = "BtnOpenDialog";
            this.BtnOpenDialog.Size = new System.Drawing.Size(142, 23);
            this.BtnOpenDialog.TabIndex = 8;
            this.BtnOpenDialog.Text = "Datei Öffnen mit Dialog";
            this.BtnOpenDialog.UseVisualStyleBackColor = true;
            this.BtnOpenDialog.Click += new System.EventHandler(this.BtnOpenDialog_Click);
            // 
            // Fachident
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(685, 312);
            this.Controls.Add(this.BtnOpenDialog);
            this.Controls.Add(this.GrpXML);
            this.Controls.Add(this.TxbDatei);
            this.Controls.Add(this.GrpVerzeichnis);
            this.Controls.Add(this.GrpDatei);
            this.Controls.Add(this.TxbSQL);
            this.Controls.Add(this.BtnAusfuehren);
            this.Controls.Add(this.BtnIdent);
            this.Controls.Add(this.GrpAusbildung);
            this.Controls.Add(this.GtpBoxFach);
            this.Name = "Fachident";
            this.Text = "Fachident";
            this.GtpBoxFach.ResumeLayout(false);
            this.GrpAusbildung.ResumeLayout(false);
            this.GrpDatei.ResumeLayout(false);
            this.GrpVerzeichnis.ResumeLayout(false);
            this.GrpVerzeichnis.PerformLayout();
            this.GrpXML.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox GtpBoxFach;
        private System.Windows.Forms.ComboBox CmbBoxFach;
        private System.Windows.Forms.GroupBox GrpAusbildung;
        private System.Windows.Forms.ComboBox CmbAusbildung;
        private System.Windows.Forms.Button BtnIdent;
        private System.Windows.Forms.Button BtnAusfuehren;
        private System.Windows.Forms.RichTextBox TxbSQL;
        private System.Windows.Forms.GroupBox GrpDatei;
        private System.Windows.Forms.Button BtnTxtLeeren;
        private System.Windows.Forms.Button BtnLoeschen;
        private System.Windows.Forms.Button BtnExistenzPruefen;
        private System.Windows.Forms.Button BtnSpeichern;
        private System.Windows.Forms.Button BtnLaden;
        private System.Windows.Forms.GroupBox GrpVerzeichnis;
        private System.Windows.Forms.Button BtnCreate;
        private System.Windows.Forms.Button BtnCreateFile;
        private System.Windows.Forms.Button BtnDelete;
        private System.Windows.Forms.TextBox TxbDateiname;
        private System.Windows.Forms.Label LblDateiname;
        private System.Windows.Forms.Button BtnDetails;
        private System.Windows.Forms.RichTextBox TxbDatei;
        private System.Windows.Forms.GroupBox GrpXML;
        private System.Windows.Forms.Button BtnOpenDialog;
        private System.Windows.Forms.Button BtnXMLSchreiben;
        private System.Windows.Forms.CheckBox ChkSource;
    }
}

