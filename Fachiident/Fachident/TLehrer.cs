using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fachident
{
    class TLehrer : TPerson {
        private string lehrerkuerzel;
        private string titel;
        
        public TLehrer(string n, string v, string a, string lk, string t): base(n, v, a) {
            lehrerkuerzel = lk;
            titel = t;
        }

        public string getPerson() {
            return $"{base.getPerson()}Lehrerkürzel: {lehrerkuerzel}\nTitel: {titel}";
        }
    }
}
