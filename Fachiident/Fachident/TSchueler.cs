using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fachident
{
    class TSchueler : TPerson {
        private string schuelerIdent;
        private DateTime geburtsdatum;
        private string klassenbezeichnung;
        
        public TSchueler(string n, string v, string a, DateTime g, string sid): base(n, v, a) {
            schuelerIdent = sid;
            geburtsdatum = g;
        }

        public TSchueler(string n, string v, string a, DateTime g, string sid, string kb): base(n, v, a) {
            schuelerIdent = sid;
            geburtsdatum = g;
            klassenbezeichnung = kb;
        }

        public string getPerson() {
            return $"{base.getPerson()}Schüler ident: {schuelerIdent}\nGeburtsdatum: {geburtsdatum}\nKlassenbezeichnung: {klassenbezeichnung}";
        }
        
        public void setKlassenbezeichnung(string kb) {
            klassenbezeichnung = kb;
        }
    }
}
