﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Fachident
{
    class TFach
    {
        private string fachbezeichnung;
        private string fachident;

        public TFach(string fb) {
            // Ein Kommentar
            fachbezeichnung = fb;
            string[] teile = fb.Split(';');
            string[] fb_teile = teile[0].Replace('/', '-').Split('-');
            if (fb_teile.Length > 1) {
                fachident = fb_teile[0].Substring(0, 2) + 
                    fb_teile[1].Substring(0, 2);
            } else { 
                fachident = fb_teile[0].Substring(0, 4);
            }
            switch (teile[1]) {
                case "Leistungskurs": fachident += "LK"; break;
                case "Berufsschule": fachident += "BS"; break;
                case "Fachoberschule": fachident += "FO"; break;
                case "Berufsvorbereitung": fachident += "BV"; break;
                default: fachident += "GK"; break;
            }
        }

        public string getFach() {
            return $"FachIdent: {fachident}";
        }

        public string DateiSpeichern() {
            return $"Fachident: '{fachident}', Fachbezeichnung: '{fachbezeichnung}'";
        }

        public string Speichern() {
            return $"INSERT INTO Fach VALUES ('{fachident}', '{fachbezeichnung}');";
        }

        public bool Gleichheit(TFach obj)
        {
            return obj.DateiSpeichern() == this.DateiSpeichern();
        }
    }
}
