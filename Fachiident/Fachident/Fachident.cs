﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Fachident
{
    public partial class Fachident : Form {
        private List<TFach> faecher;
        private TFach fach;
        string path;
        string verzeichnis;

        public Fachident() {
            // Die Standarddatei, betrifft auch UI
			// Weiterer Kommentar
            path = "text.txt";
            // Das Standardverzeichniss, betrifft auch UI
            verzeichnis = "Temp";
            // Liste aller bisherigen Fächer
            faecher = new List<TFach>();
            // Initialisierung des GUI
            InitializeComponent();
            // Pfadangaben des UI aktualisieren
            update_path();
            // Erstes Element in Comboboxen auswählen
            CmbAusbildung.SelectedIndex = 0;
            CmbBoxFach.SelectedIndex = 0;
        }

        private void update_path()
        {
            // Aktualisiere Pfadangaben in Dateiarbeit
            // Überschreibt die Angaben des Designers
            BtnLoeschen.Text = $"Datei {path} Löschen";
            BtnSpeichern.Text = $"In {path} speichern";
            BtnLaden.Text = $"{path} laden";
        }

        private void update_fach() {
            // Das fach aktualisieren
            fach = new TFach($"{CmbBoxFach.SelectedItem};{CmbAusbildung.SelectedItem}");
            // Wenn kein Duplikat mit letztem Eintrag, füg in die Liste der Fächer ein
            if (faecher.Count == 0 || !faecher.Last().Gleichheit(fach)) {
                faecher.Add(fach);
            }
        }

        private void BtnAusfuehren_Click(object sender, EventArgs e) {
            update_fach();
            TxbSQL.Text += fach.Speichern() + Environment.NewLine;
        }

        private void BtnIdent_Click(object sender, EventArgs e) {
            // Ident erstellen und in Textbox ausgeben
            update_fach();
            TxbSQL.Text += fach.getFach() + Environment.NewLine;
        }

        private void BtnLaden_Click(object sender, EventArgs e) {
            // Textdatei aus path laden
            // Prüfen, ob die Datei existiert
            if (File.Exists(path)) {
                // Wenn ja, dann lese sie und gib sie anschließend in der Textbox aus
                TxbDatei.Text = File.ReadAllText(path);
            } else {
                // Zeige eine Fehlermeldung
                MessageBox.Show($"Datei {path} wurde nicht gefunden", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void BtnSpeichern_Click(object sender, EventArgs e) {
            // Erstelle den zu speichernden String
            string text = "";
            // Prüfe die Quelle
            if (ChkSource.Checked) {
                // Generiere zu speichernden String aus Fächern
                foreach (TFach fach in faecher) {
                    text += fach.DateiSpeichern() + Environment.NewLine;
                }
            } else {
                // Lies zu speichernden Text von Textbox
                text = TxbDatei.Text;
            }
            // Warne wenn die Datei existiert
            if (File.Exists(path)) {
                var msg = MessageBox.Show($"Die Datei {path} existiert bereits,\nÜberschreiben?", "Überschreiben", MessageBoxButtons.YesNo);
                if (msg != DialogResult.No) {
                    // Schreiblogik
                    File.WriteAllText(path, text);
                }
            } else {
                // Schreiblogik
                File.WriteAllText(path, text);
            }
        }

        private void BtnExistenzPruefen_Click(object sender, EventArgs e) {
            // Zeige dem Nutzer, ob die Datei existiert
            if (File.Exists(path)) {
                MessageBox.Show($"Die Datei {path} existiert");
            } else {
                MessageBox.Show($"Die Datei {path} existiert nicht");
            }
        }

        private void BtnLoeschen_Click(object sender, EventArgs e) {
            // Lösche die Datei
            if (!File.Exists(path)) {
                // Warne, wenn sie nicht existiert
                MessageBox.Show($"Die Datei {path} existiert nicht", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else {
                File.Delete(path);
            }
        }

        private void BtnTxtLeeren_Click(object sender, EventArgs e) {
            // Leere das Bearbeitungstextfeld
            TxbDatei.Clear();
        }

        private void BtnDetails_Click(object sender, EventArgs e) {
            // Zeige eine Liste von Dateien im verzeichnis an
            if (!Directory.Exists(verzeichnis)) {
                // Warne, wenn das Verzeichnis nicht existiert
                MessageBox.Show($"Das Verzeicnis {verzeichnis} existiert nicht", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else {
                // Liste der Dateinamen aller Dateien des Verzeichnisses
                string[] dateien = Directory.GetFiles(verzeichnis);
                // Initialisiere Ausgabestring
                string output = "";
                // Konvertiere die Dateinamen in FileInfo Objekte und iteriere darüber
                foreach (FileInfo datei in dateien.Select((arg) => new FileInfo(arg))) {
                    // Stelle Ausgabe zusammen
                    output += $"Name:{datei.Name}, Länge: {datei.Length}{Environment.NewLine}";
                }
                // Zeige Dateiinfo
                MessageBox.Show(output, "Dateiinfo");
            }
        }

        private void BtnCreateFile_Click(object sender, EventArgs e) {
            // Erstelle eine gegebene Datei
            // Prüfe ob das Textfeld leer ist
            if (TxbDateiname.Text == "") {
                // Warne den Nutzer
                MessageBox.Show($"Kein Name angegeben", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Prüfe, ob das Verzeichnis existiert
            } else if (!Directory.Exists(verzeichnis)) {
                // Warne den Nutzer
                MessageBox.Show($"Das Verzeicnis {verzeichnis} existiert nicht", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else {
                // Aktualisiere path auf verzeichnis/TxbDateiname.Text
                path = $"{verzeichnis}{Path.DirectorySeparatorChar}{TxbDateiname.Text}";
                // Aktualisiere die UI Elemente
                update_path();
                // Wenn die Datei bereits existiert, warne den Nutzer
                if (File.Exists(path)) {
                    MessageBox.Show($"Die Datei {path} existiert bereits", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                } else {
                    // Erstell die Datei leer
                    File.Create(path).Close();
                }
            }
        }

        private void BtnDelete_Click(object sender, EventArgs e) {
            // Lösche ein Verzeichnis
            // Wenn es nicht existiert, warne den Nutzer
            if (!Directory.Exists(verzeichnis)) {
                MessageBox.Show($"Das Verzeicnis {verzeichnis} existiert nicht", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                // Wenn das Verzeichnis Dateien enthält, warne den Nutzer
            } else if (Directory.GetFiles(verzeichnis).Length > 0) {
                var msg = MessageBox.Show($"Das Verzeichnis {verzeichnis} enthält Dateien,\nLöschen?", "Löschen", MessageBoxButtons.YesNo);
                // Lösche rekursiv
                if (msg == DialogResult.Yes) {
                    Directory.Delete(verzeichnis, true);
                }
            } else {
                // Lösche rekursiv, falls das Verzeichniss leere Verzeichnisse enthält
                Directory.Delete(verzeichnis, true);
            }
        }

        private void BtnCreate_Click(object sender, EventArgs e) {
            // Erstelle das Verzeichneis
            // Warne den Nutzer, falls das Verzeichnis existiert
            if (Directory.Exists(verzeichnis)) {
                MessageBox.Show($"Das Verzeicnis {verzeichnis} existiert bereits", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            } else {
                // Erstelle Verzeichnis
                Directory.CreateDirectory(verzeichnis);
            }
        }

        private void BtnOpenDialog_Click(object sender, EventArgs e) {
            // Datei öffnen Dialog
            var dialog = new OpenFileDialog();
            if (dialog.ShowDialog() == DialogResult.OK) {
                // aktualisiere path
                path = dialog.FileName;
                // aktualisiere UI
                update_path();
            }
        }
    }
}
