﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace RNW
{
    public partial class Form1 : Form
    {
        //Globale Formularvariablen
        private double rg = 0;
        public Form1()
        {
            InitializeComponent();
            rg = Convert.ToDouble(TxbRG.Text);
        }

        private void BtnBeenden_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void Berechnung()
        {
            // Variablen deklarieren
            double r1, r2, rg, g;
            // Eingaben prüfen und verarbeiten
            if (TxbR1.Text == "")
            {
                MessageBox.Show("Bitte etwas eingeben");
                r1 = 0;
            }
            else r1 = Convert.ToDouble(TxbR1.Text);
            if (TxbR2.Text == "")
            {
                if (TxbR1.Text != "") MessageBox.Show("Bitte etwas eingeben");
                r2 = 0;
            }
            else r2 = Convert.ToDouble(TxbR2.Text);
            // Berechnung mit Fallprüfung
            if (RadParallel.Checked) rg = r1 * r2 / (r1 + r2);
            else rg = r1 + r2;
            g = 1 / rg;
            // Anzeige mit kOhm überprüfung
            if (rg >= 1000)
            {
                LblRg.Text = "RG [kOhm] =";
                LblG.Text = "G [mSimens] =";
                TxbRG.Text = string.Format("{0:0.000}", rg/1000);
                TxbG.Text = string.Format("{0:0.000}", g*1000);
            }
            else
            {
                LblRg.Text = "RG [Ohm] =";
                LblG.Text = "G [Simens] =";
                TxbRG.Text = string.Format("{0:0.000}", rg);
                TxbG.Text = string.Format("{0:0.000}", g);
            }
            if (r1 != 0 && r2 != 0) this.rg = rg;
        }

        private void TxbR1_TextChanged(object sender, EventArgs e)
        {
            Berechnung();
        }

        private void TxbR2_TextChanged(object sender, EventArgs e)
        {
            Berechnung();
        }

        private void RadReihe_CheckedChanged(object sender, EventArgs e)
        {
            Berechnung();
        }

        private void BtnWeiter_Click(object sender, EventArgs e)
        {
            TxbR1.Text = string.Format("{0:0.000}", rg);
            TxbR2.Clear();
        }

        private void BtnNeu_Click(object sender, EventArgs e)
        {
            rg = 0;
            TxbR1.Clear();
            TxbR2.Clear();
            TxbRG.Clear();
            TxbG.Clear();
        }

    }
}
