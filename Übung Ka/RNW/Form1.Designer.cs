﻿namespace RNW
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblR1 = new System.Windows.Forms.Label();
            this.LblR2 = new System.Windows.Forms.Label();
            this.TxbR1 = new System.Windows.Forms.TextBox();
            this.TxbR2 = new System.Windows.Forms.TextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.LblSchaltart = new System.Windows.Forms.Label();
            this.RadReihe = new System.Windows.Forms.RadioButton();
            this.RadParallel = new System.Windows.Forms.RadioButton();
            this.LblRg = new System.Windows.Forms.Label();
            this.LblG = new System.Windows.Forms.Label();
            this.BtnBeenden = new System.Windows.Forms.Button();
            this.TxbRG = new System.Windows.Forms.TextBox();
            this.TxbG = new System.Windows.Forms.TextBox();
            this.BtnWeiter = new System.Windows.Forms.Button();
            this.BtnNeu = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // LblR1
            // 
            this.LblR1.AutoSize = true;
            this.LblR1.Location = new System.Drawing.Point(13, 59);
            this.LblR1.Name = "LblR1";
            this.LblR1.Size = new System.Drawing.Size(52, 13);
            this.LblR1.TabIndex = 0;
            this.LblR1.Text = "R1 [Ohm]";
            // 
            // LblR2
            // 
            this.LblR2.AutoSize = true;
            this.LblR2.Location = new System.Drawing.Point(13, 94);
            this.LblR2.Name = "LblR2";
            this.LblR2.Size = new System.Drawing.Size(52, 13);
            this.LblR2.TabIndex = 1;
            this.LblR2.Text = "R2 [Ohm]";
            // 
            // TxbR1
            // 
            this.TxbR1.Location = new System.Drawing.Point(72, 59);
            this.TxbR1.Name = "TxbR1";
            this.TxbR1.Size = new System.Drawing.Size(100, 20);
            this.TxbR1.TabIndex = 2;
            this.TxbR1.Text = "680";
            this.TxbR1.TextChanged += new System.EventHandler(this.TxbR1_TextChanged);
            // 
            // TxbR2
            // 
            this.TxbR2.Location = new System.Drawing.Point(72, 94);
            this.TxbR2.Name = "TxbR2";
            this.TxbR2.Size = new System.Drawing.Size(100, 20);
            this.TxbR2.TabIndex = 3;
            this.TxbR2.Text = "220";
            this.TxbR2.TextChanged += new System.EventHandler(this.TxbR2_TextChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.RadParallel);
            this.panel1.Controls.Add(this.RadReihe);
            this.panel1.Controls.Add(this.LblSchaltart);
            this.panel1.Location = new System.Drawing.Point(13, 138);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(225, 55);
            this.panel1.TabIndex = 4;
            // 
            // LblSchaltart
            // 
            this.LblSchaltart.AutoSize = true;
            this.LblSchaltart.Location = new System.Drawing.Point(3, 0);
            this.LblSchaltart.Name = "LblSchaltart";
            this.LblSchaltart.Size = new System.Drawing.Size(126, 13);
            this.LblSchaltart.TabIndex = 0;
            this.LblSchaltart.Text = "Schaltungsart auswählen";
            // 
            // RadReihe
            // 
            this.RadReihe.AutoSize = true;
            this.RadReihe.Checked = true;
            this.RadReihe.Location = new System.Drawing.Point(4, 26);
            this.RadReihe.Name = "RadReihe";
            this.RadReihe.Size = new System.Drawing.Size(105, 17);
            this.RadReihe.TabIndex = 1;
            this.RadReihe.TabStop = true;
            this.RadReihe.Text = "Reihenschaltung";
            this.RadReihe.UseVisualStyleBackColor = true;
            this.RadReihe.CheckedChanged += new System.EventHandler(this.RadReihe_CheckedChanged);
            // 
            // RadParallel
            // 
            this.RadParallel.AutoSize = true;
            this.RadParallel.Location = new System.Drawing.Point(116, 26);
            this.RadParallel.Name = "RadParallel";
            this.RadParallel.Size = new System.Drawing.Size(105, 17);
            this.RadParallel.TabIndex = 2;
            this.RadParallel.Text = "Parallelschaltung";
            this.RadParallel.UseVisualStyleBackColor = true;
            // 
            // LblRg
            // 
            this.LblRg.AutoSize = true;
            this.LblRg.Location = new System.Drawing.Point(13, 233);
            this.LblRg.Name = "LblRg";
            this.LblRg.Size = new System.Drawing.Size(63, 13);
            this.LblRg.TabIndex = 5;
            this.LblRg.Text = "RG [Ohm] =";
            // 
            // LblG
            // 
            this.LblG.AutoSize = true;
            this.LblG.Location = new System.Drawing.Point(13, 262);
            this.LblG.Name = "LblG";
            this.LblG.Size = new System.Drawing.Size(67, 13);
            this.LblG.TabIndex = 6;
            this.LblG.Text = "G [Simens] =";
            // 
            // BtnBeenden
            // 
            this.BtnBeenden.Location = new System.Drawing.Point(13, 306);
            this.BtnBeenden.Name = "BtnBeenden";
            this.BtnBeenden.Size = new System.Drawing.Size(75, 23);
            this.BtnBeenden.TabIndex = 7;
            this.BtnBeenden.Text = "Beenden";
            this.BtnBeenden.UseVisualStyleBackColor = true;
            this.BtnBeenden.Click += new System.EventHandler(this.BtnBeenden_Click);
            // 
            // TxbRG
            // 
            this.TxbRG.Location = new System.Drawing.Point(129, 230);
            this.TxbRG.Name = "TxbRG";
            this.TxbRG.ReadOnly = true;
            this.TxbRG.Size = new System.Drawing.Size(100, 20);
            this.TxbRG.TabIndex = 8;
            this.TxbRG.Text = "900,000";
            // 
            // TxbG
            // 
            this.TxbG.Location = new System.Drawing.Point(129, 262);
            this.TxbG.Name = "TxbG";
            this.TxbG.ReadOnly = true;
            this.TxbG.Size = new System.Drawing.Size(100, 20);
            this.TxbG.TabIndex = 9;
            this.TxbG.Text = "0,001";
            // 
            // BtnWeiter
            // 
            this.BtnWeiter.Location = new System.Drawing.Point(95, 305);
            this.BtnWeiter.Name = "BtnWeiter";
            this.BtnWeiter.Size = new System.Drawing.Size(102, 23);
            this.BtnWeiter.TabIndex = 10;
            this.BtnWeiter.Text = "Weiterrechnen";
            this.BtnWeiter.UseVisualStyleBackColor = true;
            this.BtnWeiter.Click += new System.EventHandler(this.BtnWeiter_Click);
            // 
            // BtnNeu
            // 
            this.BtnNeu.Location = new System.Drawing.Point(204, 304);
            this.BtnNeu.Name = "BtnNeu";
            this.BtnNeu.Size = new System.Drawing.Size(97, 23);
            this.BtnNeu.TabIndex = 11;
            this.BtnNeu.Text = "Neuberechnen";
            this.BtnNeu.UseVisualStyleBackColor = true;
            this.BtnNeu.Click += new System.EventHandler(this.BtnNeu_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 480);
            this.Controls.Add(this.BtnNeu);
            this.Controls.Add(this.BtnWeiter);
            this.Controls.Add(this.TxbG);
            this.Controls.Add(this.TxbRG);
            this.Controls.Add(this.BtnBeenden);
            this.Controls.Add(this.LblG);
            this.Controls.Add(this.LblRg);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.TxbR2);
            this.Controls.Add(this.TxbR1);
            this.Controls.Add(this.LblR2);
            this.Controls.Add(this.LblR1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblR1;
        private System.Windows.Forms.Label LblR2;
        private System.Windows.Forms.TextBox TxbR1;
        private System.Windows.Forms.TextBox TxbR2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.RadioButton RadParallel;
        private System.Windows.Forms.RadioButton RadReihe;
        private System.Windows.Forms.Label LblSchaltart;
        private System.Windows.Forms.Label LblRg;
        private System.Windows.Forms.Label LblG;
        private System.Windows.Forms.Button BtnBeenden;
        private System.Windows.Forms.TextBox TxbRG;
        private System.Windows.Forms.TextBox TxbG;
        private System.Windows.Forms.Button BtnWeiter;
        private System.Windows.Forms.Button BtnNeu;
    }
}

