﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KA_Langweilig
{
    public partial class Form1 : Form
    {
        List<FloatingPanel> liste = new List<FloatingPanel>();
        public Form1()
        {
            InitializeComponent();
            
        }

        private void tick_Tick(object sender, EventArgs e)
        {
            for(int i = liste.Count - 1; i >= 0; --i)
            {
                bool keep_alive = liste[i].on_tick(Size);
                if (!keep_alive)
                {
                    Controls.Remove(liste[i]);
                    liste.RemoveAt(i);
                }
            }
            Point mouse = MousePosition;
            Point position = PointToClient(mouse);
            liste.Add(new FloatingPanel(position.X, position.Y));
            Controls.Add(liste[liste.Count - 1]);
            if(MouseButtons == MouseButtons.Left)
            {
                liste.Add(new FloatingPanel(position.X, position.Y));
                Controls.Add(liste[liste.Count - 1]);
            }
        }

        private void Form1_MouseClick(object sender, MouseEventArgs e)
        {
            Point mouse = MousePosition;
            Point position = PointToClient(mouse);
            liste.Add(new FloatingPanel(position.X, position.Y));
            Controls.Add(liste[liste.Count - 1]);
        }
    }
}
