﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace KA_Langweilig
{
    public class FloatingPanel: Panel
    {
        private bool pos_x, pos_y;
        private int speed;
        private Random r = new Random();

        public FloatingPanel(bool x_pos, bool y_pos, int x, int y, Color c, int s)
        {
            pos_x = x_pos;
            pos_y = y_pos;
            Location = new Point(x, y);
            Size = new Size(10, 10);
            BackColor = c;
            speed = s;
        }

        public FloatingPanel(int x, int y)
        {
            Color c = Color.FromArgb(r.Next(256), r.Next(256), r.Next(256));
            Location = new Point(x, y);
            Size = new Size(10, 10);
            BackColor = c;
            speed = r.Next(5, 10);
            pos_x = r.Next(2) == 1 ? true : false;
            pos_y = r.Next(2) == 1 ? true : false;
        }

        public bool on_tick(Size form)
        {
            Point location = Location;
            location.X += pos_x ? speed : speed * (-1);
            location.Y += pos_y ? speed : speed * (-1);
            Location = location;
            int links = Location.X;
            int rechts = links + Size.Width;
            int oben = Location.Y;
            int unten = oben + Size.Height;
            bool oberer_rand = unten > 0;
            bool unterer_rand = oben < form.Height;
            bool linker_rand = rechts > 0;
            bool rechter_rand = links < form.Width;
            bool innen = oberer_rand && unterer_rand && linker_rand && rechter_rand;
            return innen;
        }
    }
}