﻿namespace Ereignisse
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxbNachName = new System.Windows.Forms.TextBox();
            this.TxbVorName = new System.Windows.Forms.TextBox();
            this.ChkNeukunde = new System.Windows.Forms.CheckBox();
            this.LblLogin = new System.Windows.Forms.Label();
            this.TxbLogin = new System.Windows.Forms.TextBox();
            this.GrpZahlung = new System.Windows.Forms.GroupBox();
            this.RadBtnPaypal = new System.Windows.Forms.RadioButton();
            this.RadBtnUeberweisung = new System.Windows.Forms.RadioButton();
            this.RadBtnLast = new System.Windows.Forms.RadioButton();
            this.RadBtnKredit = new System.Windows.Forms.RadioButton();
            this.LstBxVersand = new System.Windows.Forms.ListBox();
            this.LblTip = new System.Windows.Forms.Label();
            this.BtnAusgabe = new System.Windows.Forms.Button();
            this.GrpZahlung.SuspendLayout();
            this.SuspendLayout();
            // 
            // TxbNachName
            // 
            this.TxbNachName.Location = new System.Drawing.Point(13, 13);
            this.TxbNachName.Name = "TxbNachName";
            this.TxbNachName.Size = new System.Drawing.Size(100, 20);
            this.TxbNachName.TabIndex = 0;
            this.TxbNachName.TextChanged += new System.EventHandler(this.TxbNachName_TextChanged);
            this.TxbNachName.Enter += new System.EventHandler(this.TxbNachName_Enter);
            // 
            // TxbVorName
            // 
            this.TxbVorName.Location = new System.Drawing.Point(131, 13);
            this.TxbVorName.Name = "TxbVorName";
            this.TxbVorName.Size = new System.Drawing.Size(100, 20);
            this.TxbVorName.TabIndex = 1;
            this.TxbVorName.TextChanged += new System.EventHandler(this.TxbVorName_TextChanged);
            this.TxbVorName.Enter += new System.EventHandler(this.TxbVorName_Enter);
            // 
            // ChkNeukunde
            // 
            this.ChkNeukunde.AutoSize = true;
            this.ChkNeukunde.Location = new System.Drawing.Point(248, 13);
            this.ChkNeukunde.Name = "ChkNeukunde";
            this.ChkNeukunde.Size = new System.Drawing.Size(97, 17);
            this.ChkNeukunde.TabIndex = 2;
            this.ChkNeukunde.Text = "bereits &Kunde?";
            this.ChkNeukunde.UseVisualStyleBackColor = true;
            this.ChkNeukunde.Visible = false;
            this.ChkNeukunde.Enter += new System.EventHandler(this.ChkNeukunde_Enter);
            this.ChkNeukunde.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChkNeukunde_KeyDown);
            // 
            // LblLogin
            // 
            this.LblLogin.AutoSize = true;
            this.LblLogin.Location = new System.Drawing.Point(13, 40);
            this.LblLogin.Name = "LblLogin";
            this.LblLogin.Size = new System.Drawing.Size(62, 13);
            this.LblLogin.TabIndex = 3;
            this.LblLogin.Text = "Loginname:";
            // 
            // TxbLogin
            // 
            this.TxbLogin.Location = new System.Drawing.Point(131, 40);
            this.TxbLogin.Name = "TxbLogin";
            this.TxbLogin.ReadOnly = true;
            this.TxbLogin.Size = new System.Drawing.Size(185, 20);
            this.TxbLogin.TabIndex = 4;
            this.TxbLogin.TabStop = false;
            // 
            // GrpZahlung
            // 
            this.GrpZahlung.Controls.Add(this.RadBtnPaypal);
            this.GrpZahlung.Controls.Add(this.RadBtnUeberweisung);
            this.GrpZahlung.Controls.Add(this.RadBtnLast);
            this.GrpZahlung.Controls.Add(this.RadBtnKredit);
            this.GrpZahlung.Location = new System.Drawing.Point(13, 76);
            this.GrpZahlung.Name = "GrpZahlung";
            this.GrpZahlung.Size = new System.Drawing.Size(200, 117);
            this.GrpZahlung.TabIndex = 5;
            this.GrpZahlung.TabStop = false;
            this.GrpZahlung.Text = "Zahlungsweise";
            this.GrpZahlung.Enter += new System.EventHandler(this.GrpZahlung_Enter);
            // 
            // RadBtnPaypal
            // 
            this.RadBtnPaypal.AutoSize = true;
            this.RadBtnPaypal.Location = new System.Drawing.Point(7, 92);
            this.RadBtnPaypal.Name = "RadBtnPaypal";
            this.RadBtnPaypal.Size = new System.Drawing.Size(57, 17);
            this.RadBtnPaypal.TabIndex = 3;
            this.RadBtnPaypal.TabStop = true;
            this.RadBtnPaypal.Text = "&Paypal";
            this.RadBtnPaypal.UseVisualStyleBackColor = true;
            // 
            // RadBtnUeberweisung
            // 
            this.RadBtnUeberweisung.AutoSize = true;
            this.RadBtnUeberweisung.Location = new System.Drawing.Point(7, 68);
            this.RadBtnUeberweisung.Name = "RadBtnUeberweisung";
            this.RadBtnUeberweisung.Size = new System.Drawing.Size(87, 17);
            this.RadBtnUeberweisung.TabIndex = 2;
            this.RadBtnUeberweisung.TabStop = true;
            this.RadBtnUeberweisung.Text = "&Überweisung";
            this.RadBtnUeberweisung.UseVisualStyleBackColor = true;
            // 
            // RadBtnLast
            // 
            this.RadBtnLast.AutoSize = true;
            this.RadBtnLast.Location = new System.Drawing.Point(7, 44);
            this.RadBtnLast.Name = "RadBtnLast";
            this.RadBtnLast.Size = new System.Drawing.Size(73, 17);
            this.RadBtnLast.TabIndex = 1;
            this.RadBtnLast.TabStop = true;
            this.RadBtnLast.Text = "&Lastschrift";
            this.RadBtnLast.UseVisualStyleBackColor = true;
            // 
            // RadBtnKredit
            // 
            this.RadBtnKredit.AutoSize = true;
            this.RadBtnKredit.Location = new System.Drawing.Point(7, 20);
            this.RadBtnKredit.Name = "RadBtnKredit";
            this.RadBtnKredit.Size = new System.Drawing.Size(76, 17);
            this.RadBtnKredit.TabIndex = 0;
            this.RadBtnKredit.TabStop = true;
            this.RadBtnKredit.Text = "K&reditkarte";
            this.RadBtnKredit.UseVisualStyleBackColor = true;
            // 
            // LstBxVersand
            // 
            this.LstBxVersand.FormattingEnabled = true;
            this.LstBxVersand.Items.AddRange(new object[] {
            "DHL",
            "Hermes",
            "UPS"});
            this.LstBxVersand.Location = new System.Drawing.Point(13, 200);
            this.LstBxVersand.Name = "LstBxVersand";
            this.LstBxVersand.Size = new System.Drawing.Size(120, 43);
            this.LstBxVersand.TabIndex = 6;
            this.LstBxVersand.Enter += new System.EventHandler(this.LstBxVersand_Enter);
            // 
            // LblTip
            // 
            this.LblTip.AutoSize = true;
            this.LblTip.BackColor = System.Drawing.Color.Yellow;
            this.LblTip.Location = new System.Drawing.Point(13, 250);
            this.LblTip.Name = "LblTip";
            this.LblTip.Size = new System.Drawing.Size(230, 13);
            this.LblTip.TabIndex = 7;
            this.LblTip.Text = "Hier erscheinen Tips wenn sie Felder anwählen";
            // 
            // BtnAusgabe
            // 
            this.BtnAusgabe.Enabled = false;
            this.BtnAusgabe.Location = new System.Drawing.Point(13, 267);
            this.BtnAusgabe.Name = "BtnAusgabe";
            this.BtnAusgabe.Size = new System.Drawing.Size(332, 23);
            this.BtnAusgabe.TabIndex = 8;
            this.BtnAusgabe.Text = "&Angaben anzeigen";
            this.BtnAusgabe.UseVisualStyleBackColor = true;
            this.BtnAusgabe.Click += new System.EventHandler(this.BtnAusgabe_Click);
            this.BtnAusgabe.Enter += new System.EventHandler(this.BtnAusgabe_Enter);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(352, 297);
            this.Controls.Add(this.BtnAusgabe);
            this.Controls.Add(this.LblTip);
            this.Controls.Add(this.LstBxVersand);
            this.Controls.Add(this.GrpZahlung);
            this.Controls.Add(this.TxbLogin);
            this.Controls.Add(this.LblLogin);
            this.Controls.Add(this.ChkNeukunde);
            this.Controls.Add(this.TxbVorName);
            this.Controls.Add(this.TxbNachName);
            this.Name = "Form1";
            this.Text = "Form1";
            this.GrpZahlung.ResumeLayout(false);
            this.GrpZahlung.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxbNachName;
        private System.Windows.Forms.TextBox TxbVorName;
        private System.Windows.Forms.CheckBox ChkNeukunde;
        private System.Windows.Forms.Label LblLogin;
        private System.Windows.Forms.TextBox TxbLogin;
        private System.Windows.Forms.GroupBox GrpZahlung;
        private System.Windows.Forms.RadioButton RadBtnPaypal;
        private System.Windows.Forms.RadioButton RadBtnUeberweisung;
        private System.Windows.Forms.RadioButton RadBtnLast;
        private System.Windows.Forms.RadioButton RadBtnKredit;
        private System.Windows.Forms.ListBox LstBxVersand;
        private System.Windows.Forms.Label LblTip;
        private System.Windows.Forms.Button BtnAusgabe;
    }
}

