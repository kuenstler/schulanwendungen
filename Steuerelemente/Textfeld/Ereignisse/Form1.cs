﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Ereignisse
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void TxbNachName_Enter(object sender, EventArgs e)
        {
            LblTip.Text = "Bitte geben sie ihren Nachnamen ein.";
        }

        private void TxbVorName_Enter(object sender, EventArgs e)
        {
            LblTip.Text = "Bitte geben sie ihren vollständigen Vornamen ein.";
        }

        private void ChkNeukunde_Enter(object sender, EventArgs e)
        {
            LblTip.Text = "Bitte wählen sie an, ob sie bereits ein Kunde sind.";
        }

        private void GrpZahlung_Enter(object sender, EventArgs e)
        {
            LblTip.Text = "Bitte wählen sie die Zahlungsweise aus.";
        }

        private void LstBxVersand_Enter(object sender, EventArgs e)
        {
            LblTip.Text = "Bitte wählen sie die Versandart aus.";
        }

        private void BtnAusgabe_Enter(object sender, EventArgs e)
        {
            LblTip.Text = "Wählen sie das aus wenn sie meinen, dass sie fertig sind.";
        }

        private void Pruefen()
        {
            if (TxbNachName.Text != "")
            {
                ChkNeukunde.Visible = true;
            }
            else
            {
                ChkNeukunde.Visible = false;
            }
            TxbLogin.Text = TxbNachName.Text.ToLower().Trim() + TxbVorName.Text.ToLower().Trim();
            if (TxbNachName.Text != "" && TxbVorName.Text != "")
            {
                BtnAusgabe.Enabled = true;
            }
            else
            {
                BtnAusgabe.Enabled = false;
            }
        }

        private void TxbNachName_TextChanged(object sender, EventArgs e)
        {
            Pruefen();
        }

        private void TxbVorName_TextChanged(object sender, EventArgs e)
        {
            Pruefen();
        }

        private void BtnAusgabe_Click(object sender, EventArgs e)
        {
            bool zahlungsmittel = RadBtnKredit.Checked || RadBtnLast.Checked || RadBtnPaypal.Checked || RadBtnUeberweisung.Checked;
            bool versand = LstBxVersand.SelectedIndex != -1;
            if (zahlungsmittel && versand)
            {
                MessageBox.Show("Sie heißen " + TxbVorName.Text + " " + TxbNachName.Text +
                    "\n" + (ChkNeukunde.Checked ? "sie sind bereits Kunde" : "sie sind noch kein Kunde") + " und" +
                    "\nzahlen mit " + (RadBtnKredit.Checked ? "Kreditkarte" : RadBtnLast.Checked ? "Lastschrift" : RadBtnPaypal.Checked ? "Paypal" : "Überweisung") +
                    ".\nSie erhalten ihre Ware durch " + LstBxVersand.SelectedItem + ".");
            }
            else if (zahlungsmittel)
            {
                MessageBox.Show("Bitte geben sie eine Versandart an.");
            }
            else if (versand)
            {
                MessageBox.Show("Bitte geben sie eine Zahlungsart an");
            }
            else
            {
                MessageBox.Show("Bitte geben sie eine Versand- und eine Zahlungsart an.");
            }
        }

        private void ChkNeukunde_KeyDown(object sender, KeyEventArgs e)
        {
            ChkNeukunde.Checked = !ChkNeukunde.Checked;
        }
    }
}
