﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Kombinationsfelder
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            update_lists();
        }

        private void update_lists()
        {
            foreach (string s in CmbboxDrop.Items)
            {
                CmbboxDropList.Items.Add(s);
                CmbboxSimple.Items.Add(s);
            }
        }

        private void CmbboxDrop_SelectedIndexChanged(object sender, EventArgs e)
        {
            LblDrop.Text = "Auswahl: " + CmbboxDrop.Text +
                "\nIndex: " + CmbboxDrop.SelectedIndex +
                "\nStyle: " + CmbboxDrop.DropDownStyle;
        }

        private void CmbboxDropList_SelectedIndexChanged(object sender, EventArgs e)
        {
            LblDropList.Text = "Auswahl: " + CmbboxDropList.Text +
                "\nIndex: " + CmbboxDropList.SelectedIndex +
                "\nStyle: " + CmbboxDropList.DropDownStyle;
        }

        private void CmbboxSimple_SelectedIndexChanged(object sender, EventArgs e)
        {
            LblSimple.Text = "Auswahl: " + CmbboxSimple.Text +
                "\nIndex: " + CmbboxSimple.SelectedIndex +
                "\nStyle: " + CmbboxSimple.DropDownStyle;
        }

        private void BtnDropTake_Click(object sender, EventArgs e)
        {
            if (CmbboxDrop.Text != "")
            {
                CmbboxDrop.Items.Add(CmbboxDrop.Text);
                CmbboxDropList.Items.Add(CmbboxDrop.Text);
                CmbboxSimple.Items.Add(CmbboxDrop.Text);
                CmbboxDrop.Text = "";
                CmbboxDrop.SelectedIndex = CmbboxDrop.Items.Count - 1;
            }
            else
            {
                MessageBox.Show("Bitte etwas eingeben");
            }
        }

        private void CmbboxSimple_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13 && CmbboxSimple.Text != "")
            {
                CmbboxSimple.Items.Add(CmbboxSimple.Text);
                CmbboxDropList.Items.Add(CmbboxSimple.Text);
                CmbboxDrop.Items.Add(CmbboxSimple.Text);
                CmbboxSimple.Text = "";
                CmbboxSimple.SelectedIndex = CmbboxSimple.Items.Count - 1;
            }
            else if (e.KeyChar == (char)13)
            {
                MessageBox.Show("Bitte etwas eingeben");
            }
        }

        private void BtnIn_Click(object sender, EventArgs e)
        {
            FileStream fs = new FileStream("ein.txt", FileMode.Open);
            StreamReader sr = new StreamReader(fs);
            CmbboxDrop.Items.Clear();
            string zeile;
            while (sr.Peek() != -1)
            {
                zeile = sr.ReadLine();
                CmbboxDrop.Items.Add(zeile);
            }
        }
    }
}
