﻿namespace Kombinationsfelder
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.CmbboxDrop = new System.Windows.Forms.ComboBox();
            this.CmbboxDropList = new System.Windows.Forms.ComboBox();
            this.CmbboxSimple = new System.Windows.Forms.ComboBox();
            this.BtnDropShow = new System.Windows.Forms.Button();
            this.BtnDropTake = new System.Windows.Forms.Button();
            this.BtnDropListShow = new System.Windows.Forms.Button();
            this.BtnSimpleShow = new System.Windows.Forms.Button();
            this.LblDrop = new System.Windows.Forms.Label();
            this.LblDropList = new System.Windows.Forms.Label();
            this.LblSimple = new System.Windows.Forms.Label();
            this.BtnIn = new System.Windows.Forms.Button();
            this.BtnOut = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // CmbboxDrop
            // 
            this.CmbboxDrop.FormattingEnabled = true;
            this.CmbboxDrop.Items.AddRange(new object[] {
            "Zange",
            "Hammer",
            "Bohrmaschine",
            "Schraubendreher"});
            this.CmbboxDrop.Location = new System.Drawing.Point(13, 13);
            this.CmbboxDrop.Name = "CmbboxDrop";
            this.CmbboxDrop.Size = new System.Drawing.Size(121, 21);
            this.CmbboxDrop.TabIndex = 0;
            this.CmbboxDrop.SelectedIndexChanged += new System.EventHandler(this.CmbboxDrop_SelectedIndexChanged);
            // 
            // CmbboxDropList
            // 
            this.CmbboxDropList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CmbboxDropList.FormattingEnabled = true;
            this.CmbboxDropList.Location = new System.Drawing.Point(13, 86);
            this.CmbboxDropList.Name = "CmbboxDropList";
            this.CmbboxDropList.Size = new System.Drawing.Size(121, 21);
            this.CmbboxDropList.TabIndex = 1;
            this.CmbboxDropList.SelectedIndexChanged += new System.EventHandler(this.CmbboxDropList_SelectedIndexChanged);
            // 
            // CmbboxSimple
            // 
            this.CmbboxSimple.DropDownStyle = System.Windows.Forms.ComboBoxStyle.Simple;
            this.CmbboxSimple.FormattingEnabled = true;
            this.CmbboxSimple.Location = new System.Drawing.Point(13, 136);
            this.CmbboxSimple.Name = "CmbboxSimple";
            this.CmbboxSimple.Size = new System.Drawing.Size(121, 150);
            this.CmbboxSimple.TabIndex = 2;
            this.CmbboxSimple.SelectedIndexChanged += new System.EventHandler(this.CmbboxSimple_SelectedIndexChanged);
            this.CmbboxSimple.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CmbboxSimple_KeyPress);
            // 
            // BtnDropShow
            // 
            this.BtnDropShow.Location = new System.Drawing.Point(160, 13);
            this.BtnDropShow.Name = "BtnDropShow";
            this.BtnDropShow.Size = new System.Drawing.Size(111, 23);
            this.BtnDropShow.TabIndex = 3;
            this.BtnDropShow.Text = "Eintrag anzeigen";
            this.BtnDropShow.UseVisualStyleBackColor = true;
            // 
            // BtnDropTake
            // 
            this.BtnDropTake.Location = new System.Drawing.Point(160, 43);
            this.BtnDropTake.Name = "BtnDropTake";
            this.BtnDropTake.Size = new System.Drawing.Size(111, 23);
            this.BtnDropTake.TabIndex = 4;
            this.BtnDropTake.Text = "Eintrag übenehmen";
            this.BtnDropTake.UseVisualStyleBackColor = true;
            this.BtnDropTake.Click += new System.EventHandler(this.BtnDropTake_Click);
            // 
            // BtnDropListShow
            // 
            this.BtnDropListShow.Location = new System.Drawing.Point(160, 84);
            this.BtnDropListShow.Name = "BtnDropListShow";
            this.BtnDropListShow.Size = new System.Drawing.Size(111, 23);
            this.BtnDropListShow.TabIndex = 5;
            this.BtnDropListShow.Text = "Eintrag anzeigen";
            this.BtnDropListShow.UseVisualStyleBackColor = true;
            // 
            // BtnSimpleShow
            // 
            this.BtnSimpleShow.Location = new System.Drawing.Point(160, 136);
            this.BtnSimpleShow.Name = "BtnSimpleShow";
            this.BtnSimpleShow.Size = new System.Drawing.Size(111, 23);
            this.BtnSimpleShow.TabIndex = 6;
            this.BtnSimpleShow.Text = "Eintrag anzeigen";
            this.BtnSimpleShow.UseVisualStyleBackColor = true;
            // 
            // LblDrop
            // 
            this.LblDrop.AutoSize = true;
            this.LblDrop.Location = new System.Drawing.Point(278, 13);
            this.LblDrop.Name = "LblDrop";
            this.LblDrop.Size = new System.Drawing.Size(81, 13);
            this.LblDrop.TabIndex = 7;
            this.LblDrop.Text = "[Style/Auswahl]";
            // 
            // LblDropList
            // 
            this.LblDropList.AutoSize = true;
            this.LblDropList.Location = new System.Drawing.Point(278, 86);
            this.LblDropList.Name = "LblDropList";
            this.LblDropList.Size = new System.Drawing.Size(81, 13);
            this.LblDropList.TabIndex = 8;
            this.LblDropList.Text = "[Style/Auswahl]";
            // 
            // LblSimple
            // 
            this.LblSimple.AutoSize = true;
            this.LblSimple.Location = new System.Drawing.Point(278, 136);
            this.LblSimple.Name = "LblSimple";
            this.LblSimple.Size = new System.Drawing.Size(81, 13);
            this.LblSimple.TabIndex = 9;
            this.LblSimple.Text = "[Style/Auswahl]";
            // 
            // BtnIn
            // 
            this.BtnIn.Location = new System.Drawing.Point(160, 166);
            this.BtnIn.Name = "BtnIn";
            this.BtnIn.Size = new System.Drawing.Size(128, 23);
            this.BtnIn.TabIndex = 10;
            this.BtnIn.Text = "Aus Datei importieren";
            this.BtnIn.UseVisualStyleBackColor = true;
            this.BtnIn.Click += new System.EventHandler(this.BtnIn_Click);
            // 
            // BtnOut
            // 
            this.BtnOut.Location = new System.Drawing.Point(160, 196);
            this.BtnOut.Name = "BtnOut";
            this.BtnOut.Size = new System.Drawing.Size(128, 23);
            this.BtnOut.TabIndex = 11;
            this.BtnOut.Text = "In Datei exportieren";
            this.BtnOut.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(539, 287);
            this.Controls.Add(this.BtnOut);
            this.Controls.Add(this.BtnIn);
            this.Controls.Add(this.LblSimple);
            this.Controls.Add(this.LblDropList);
            this.Controls.Add(this.LblDrop);
            this.Controls.Add(this.BtnSimpleShow);
            this.Controls.Add(this.BtnDropListShow);
            this.Controls.Add(this.BtnDropTake);
            this.Controls.Add(this.BtnDropShow);
            this.Controls.Add(this.CmbboxSimple);
            this.Controls.Add(this.CmbboxDropList);
            this.Controls.Add(this.CmbboxDrop);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox CmbboxDrop;
        private System.Windows.Forms.ComboBox CmbboxDropList;
        private System.Windows.Forms.ComboBox CmbboxSimple;
        private System.Windows.Forms.Button BtnDropShow;
        private System.Windows.Forms.Button BtnDropTake;
        private System.Windows.Forms.Button BtnDropListShow;
        private System.Windows.Forms.Button BtnSimpleShow;
        private System.Windows.Forms.Label LblDrop;
        private System.Windows.Forms.Label LblDropList;
        private System.Windows.Forms.Label LblSimple;
        private System.Windows.Forms.Button BtnIn;
        private System.Windows.Forms.Button BtnOut;
    }
}

