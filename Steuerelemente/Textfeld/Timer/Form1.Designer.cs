﻿namespace Timer
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnStart = new System.Windows.Forms.Button();
            this.BtnStop = new System.Windows.Forms.Button();
            this.BtnSetz = new System.Windows.Forms.Button();
            this.TrkGeschwindigkeit = new System.Windows.Forms.TrackBar();
            this.LblGeschwindigkeit = new System.Windows.Forms.Label();
            this.CkEin = new System.Windows.Forms.CheckBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Pnl5 = new System.Windows.Forms.Panel();
            this.Pnl3 = new System.Windows.Forms.Panel();
            this.Pnl2 = new System.Windows.Forms.Panel();
            this.Pnl1 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.RadbtnStart = new System.Windows.Forms.RadioButton();
            this.RadbtnRand = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.TrkGeschwindigkeit)).BeginInit();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnStart
            // 
            this.BtnStart.Location = new System.Drawing.Point(13, 13);
            this.BtnStart.Name = "BtnStart";
            this.BtnStart.Size = new System.Drawing.Size(75, 23);
            this.BtnStart.TabIndex = 0;
            this.BtnStart.Text = "Starten";
            this.BtnStart.UseVisualStyleBackColor = true;
            this.BtnStart.Click += new System.EventHandler(this.BtnStart_Click);
            // 
            // BtnStop
            // 
            this.BtnStop.Enabled = false;
            this.BtnStop.Location = new System.Drawing.Point(95, 13);
            this.BtnStop.Name = "BtnStop";
            this.BtnStop.Size = new System.Drawing.Size(75, 23);
            this.BtnStop.TabIndex = 1;
            this.BtnStop.Text = "Stoppen";
            this.BtnStop.UseVisualStyleBackColor = true;
            this.BtnStop.Click += new System.EventHandler(this.BtnStop_Click);
            // 
            // BtnSetz
            // 
            this.BtnSetz.Location = new System.Drawing.Point(177, 13);
            this.BtnSetz.Name = "BtnSetz";
            this.BtnSetz.Size = new System.Drawing.Size(75, 23);
            this.BtnSetz.TabIndex = 2;
            this.BtnSetz.Text = "Setzen";
            this.BtnSetz.UseVisualStyleBackColor = true;
            this.BtnSetz.Click += new System.EventHandler(this.BtnSetz_Click);
            // 
            // TrkGeschwindigkeit
            // 
            this.TrkGeschwindigkeit.Location = new System.Drawing.Point(13, 43);
            this.TrkGeschwindigkeit.Maximum = 200;
            this.TrkGeschwindigkeit.Minimum = 10;
            this.TrkGeschwindigkeit.Name = "TrkGeschwindigkeit";
            this.TrkGeschwindigkeit.Size = new System.Drawing.Size(330, 45);
            this.TrkGeschwindigkeit.SmallChange = 5;
            this.TrkGeschwindigkeit.TabIndex = 4;
            this.TrkGeschwindigkeit.TickFrequency = 10;
            this.TrkGeschwindigkeit.Value = 100;
            this.TrkGeschwindigkeit.Scroll += new System.EventHandler(this.TrkGeschwindigkeit_Scroll);
            // 
            // LblGeschwindigkeit
            // 
            this.LblGeschwindigkeit.AutoSize = true;
            this.LblGeschwindigkeit.Location = new System.Drawing.Point(350, 43);
            this.LblGeschwindigkeit.Name = "LblGeschwindigkeit";
            this.LblGeschwindigkeit.Size = new System.Drawing.Size(62, 13);
            this.LblGeschwindigkeit.TabIndex = 4;
            this.LblGeschwindigkeit.Text = "aktuell: 100";
            // 
            // CkEin
            // 
            this.CkEin.AutoSize = true;
            this.CkEin.Location = new System.Drawing.Point(353, 13);
            this.CkEin.Name = "CkEin";
            this.CkEin.Size = new System.Drawing.Size(74, 17);
            this.CkEin.TabIndex = 3;
            this.CkEin.Text = "Einfangen";
            this.CkEin.UseVisualStyleBackColor = true;
            this.CkEin.CheckedChanged += new System.EventHandler(this.CkEin_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.SystemColors.ControlDark;
            this.panel1.Controls.Add(this.Pnl5);
            this.panel1.Controls.Add(this.Pnl3);
            this.panel1.Controls.Add(this.Pnl2);
            this.panel1.Controls.Add(this.Pnl1);
            this.panel1.Location = new System.Drawing.Point(13, 107);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(458, 287);
            this.panel1.TabIndex = 6;
            // 
            // Pnl5
            // 
            this.Pnl5.BackColor = System.Drawing.Color.Lime;
            this.Pnl5.Location = new System.Drawing.Point(170, 146);
            this.Pnl5.Name = "Pnl5";
            this.Pnl5.Size = new System.Drawing.Size(30, 30);
            this.Pnl5.TabIndex = 3;
            // 
            // Pnl3
            // 
            this.Pnl3.BackColor = System.Drawing.Color.Yellow;
            this.Pnl3.Location = new System.Drawing.Point(206, 146);
            this.Pnl3.Name = "Pnl3";
            this.Pnl3.Size = new System.Drawing.Size(30, 30);
            this.Pnl3.TabIndex = 2;
            // 
            // Pnl2
            // 
            this.Pnl2.BackColor = System.Drawing.Color.Red;
            this.Pnl2.Location = new System.Drawing.Point(206, 110);
            this.Pnl2.Name = "Pnl2";
            this.Pnl2.Size = new System.Drawing.Size(30, 30);
            this.Pnl2.TabIndex = 1;
            // 
            // Pnl1
            // 
            this.Pnl1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Pnl1.Location = new System.Drawing.Point(170, 110);
            this.Pnl1.Name = "Pnl1";
            this.Pnl1.Size = new System.Drawing.Size(30, 30);
            this.Pnl1.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 91);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 13);
            this.label1.TabIndex = 7;
            this.label1.Text = "Langsam";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(308, 91);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 8;
            this.label2.Text = "Schnell";
            // 
            // RadbtnStart
            // 
            this.RadbtnStart.AutoSize = true;
            this.RadbtnStart.Checked = true;
            this.RadbtnStart.Location = new System.Drawing.Point(353, 60);
            this.RadbtnStart.Name = "RadbtnStart";
            this.RadbtnStart.Size = new System.Drawing.Size(149, 17);
            this.RadbtnStart.TabIndex = 9;
            this.RadbtnStart.TabStop = true;
            this.RadbtnStart.Text = "An Startposition umkehren";
            this.RadbtnStart.UseVisualStyleBackColor = true;
            this.RadbtnStart.CheckedChanged += new System.EventHandler(this.RadbtnStart_CheckedChanged);
            // 
            // RadbtnRand
            // 
            this.RadbtnRand.AutoSize = true;
            this.RadbtnRand.Location = new System.Drawing.Point(353, 84);
            this.RadbtnRand.Name = "RadbtnRand";
            this.RadbtnRand.Size = new System.Drawing.Size(119, 17);
            this.RadbtnRand.TabIndex = 10;
            this.RadbtnRand.TabStop = true;
            this.RadbtnRand.Text = "Am Rand umkehren";
            this.RadbtnRand.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(497, 406);
            this.Controls.Add(this.RadbtnRand);
            this.Controls.Add(this.RadbtnStart);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.CkEin);
            this.Controls.Add(this.LblGeschwindigkeit);
            this.Controls.Add(this.TrkGeschwindigkeit);
            this.Controls.Add(this.BtnSetz);
            this.Controls.Add(this.BtnStop);
            this.Controls.Add(this.BtnStart);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.TrkGeschwindigkeit)).EndInit();
            this.panel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnStart;
        private System.Windows.Forms.Button BtnStop;
        private System.Windows.Forms.Button BtnSetz;
        private System.Windows.Forms.TrackBar TrkGeschwindigkeit;
        private System.Windows.Forms.Label LblGeschwindigkeit;
        private System.Windows.Forms.CheckBox CkEin;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel Pnl5;
        private System.Windows.Forms.Panel Pnl3;
        private System.Windows.Forms.Panel Pnl2;
        private System.Windows.Forms.Panel Pnl1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton RadbtnStart;
        private System.Windows.Forms.RadioButton RadbtnRand;
    }
}

