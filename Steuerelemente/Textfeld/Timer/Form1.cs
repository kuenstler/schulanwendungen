﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Timer
{
    public partial class Form1 : Form
    {
        Point pnl1 = new Point(0, 0);
        Point pnl2 = new Point(0, 0);
        Point pnl3 = new Point(0, 0);
        Point pnl5 = new Point(0, 0);
        int speed = 0;
        System.Windows.Forms.Timer Hauptschleife = new System.Windows.Forms.Timer();
        bool bewegungsrichtung = true;
        bool pruefen = false;
        int startposition = 0;
        bool startposition_rand = true;// Zwischen umkehren an Startposition und umkehren an Rand umschalten, true ist startposition

        public Form1()
        {
            InitializeComponent();
            pnl1 = Pnl1.Location;
            pnl2 = Pnl2.Location;
            pnl3 = Pnl3.Location;
            pnl5 = Pnl5.Location;
            speed = TrkGeschwindigkeit.Value;
            Hauptschleife.Interval = 201 - speed;
            Hauptschleife.Tick += new EventHandler(timer_Tick);
            startposition = Pnl2.Location.X;
        }

        void timer_Tick(object sender, EventArgs e)
        {
            bool local_bewegungsrichtung = true;
            if (pruefen)
            {
                bewegungsrichtung = Pnl2.Location.Y <= 0 || Pnl2.Location.X + Pnl2.Size.Width >= panel1.Size.Width ? false :
                    startposition_rand ? startposition >= Pnl2.Location.X ? true : bewegungsrichtung :
                    Pnl2.Location.Y + Pnl2.Size.Height >= panel1.Size.Height || Pnl2.Location.X <= 0 ? true : bewegungsrichtung;
                local_bewegungsrichtung = bewegungsrichtung;
            }
            int pixel_speed = speed /50+1;
            if (local_bewegungsrichtung)
            {
                Pnl1.Location = new Point(Pnl1.Location.X - pixel_speed, Pnl1.Location.Y - pixel_speed);
                Pnl2.Location = new Point(Pnl2.Location.X + pixel_speed, Pnl2.Location.Y - pixel_speed);
                Pnl3.Location = new Point(Pnl3.Location.X + pixel_speed, Pnl3.Location.Y + pixel_speed);
                Pnl5.Location = new Point(Pnl5.Location.X - pixel_speed, Pnl5.Location.Y + pixel_speed);
            }
            else
            {
                Pnl1.Location = new Point(Pnl1.Location.X + pixel_speed, Pnl1.Location.Y + pixel_speed);
                Pnl2.Location = new Point(Pnl2.Location.X - pixel_speed, Pnl2.Location.Y + pixel_speed);
                Pnl3.Location = new Point(Pnl3.Location.X - pixel_speed, Pnl3.Location.Y - pixel_speed);
                Pnl5.Location = new Point(Pnl5.Location.X + pixel_speed, Pnl5.Location.Y - pixel_speed);
            }
        }

        private void TrkGeschwindigkeit_Scroll(object sender, EventArgs e)
        {
            LblGeschwindigkeit.Text = "aktuell: " + TrkGeschwindigkeit.Value;
            bool zahl = LblGeschwindigkeit.Text.All(char.IsDigit);
            speed = TrkGeschwindigkeit.Value;
            Hauptschleife.Interval = 201 - speed;
        }

        private void BtnSetz_Click(object sender, EventArgs e)
        {
            Pnl1.Location = pnl1;
            Pnl2.Location = pnl2;
            Pnl3.Location = pnl3;
            Pnl5.Location = pnl5;
            Hauptschleife.Enabled = false;
            BtnStart.Enabled = true;
            BtnStop.Enabled = false;
            bewegungsrichtung = true;
        }

        private void BtnStart_Click(object sender, EventArgs e)
        {
            Hauptschleife.Enabled = true;
            BtnStart.Enabled = false;
            BtnStop.Enabled = true;
        }

        private void BtnStop_Click(object sender, EventArgs e)
        {
            Hauptschleife.Enabled = false;
            BtnStart.Enabled = true;
            BtnStop.Enabled = false;
        }

        private void CkEin_CheckedChanged(object sender, EventArgs e)
        {
            pruefen = CkEin.Checked;
        }

        private void RadbtnStart_CheckedChanged(object sender, EventArgs e)
        {
            startposition_rand = RadbtnStart.Checked;
        }
    }
}
