﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Loesung
{
    public partial class Form1 : Form
    {
        Random r = new Random();
        public Form1()
        {
            InitializeComponent();
            // weitere Listeneinträge
            LstEssen.Items.Add("Makkaroni");
            LstEssen.Items.Add("Salat");
            LstEssen.Items.Add("Gnocchi");
            LstEssen.Items.Add("Bürger");
        }

        private void Ausgabe_Aktualisieren()
        {
            LblAnz.Text = "Anzahl: " + LstEssen.Items.Count + " Items";
            LblIndex.Text = "Index: " + LstEssen.SelectedIndex;
            LblAuswahl.Text = "Ausgewählt: " + LstEssen.SelectedItem;
            LblEintraege.Text = "Listeneinträge:\n";
            int i;
            for (i = 0; i < LstEssen.Items.Count; i++)
            {
                LblEintraege.Text += LstEssen.Items[i] + "\n";
            }
            LblMehrfachAusgabe.Text = "Ausgewählte Einträge:\n";
            foreach (string s in LstEssen.SelectedItems) LblMehrfachAusgabe.Text += s + "\n";
        }

        private void LstEssen_SelectedIndexChanged(object sender, EventArgs e)
        {
            Ausgabe_Aktualisieren();
        }

        private void BtnZufall_Click(object sender, EventArgs e)
        {
            if (LstEssen.Items.Count != 0)
            {
                LstEssen.SelectedIndex = r.Next(0, LstEssen.Items.Count);
            }
            else
            {
                MessageBox.Show("Bitte Listeneinträge hinzufügen", "Listeneinträge", MessageBoxButtons.OK);
            }
        }

        private void BntEintrLoeschen_Click(object sender, EventArgs e)
        {
            if (LstEssen.SelectedIndex != -1) LstEssen.Items.RemoveAt(LstEssen.SelectedIndex);
        }

        private void BtnLstLoeschen_Click(object sender, EventArgs e)
        {
            LstEssen.Items.Clear();
        }

        private void BtnErsetzen_Click(object sender, EventArgs e)
        {
            if (LstEssen.SelectedIndex != -1) LstEssen.Items[LstEssen.SelectedIndex] = TxbErsetzen.Text;
            TxbErsetzen.Clear();
        }

        private void BtnEinfuegen_Click(object sender, EventArgs e)
        {
            if (RadBtnAnfang.Checked)
            {
                LstEssen.Items.Insert(0, TxbNeuerEintrag.Text);
                LstEssen.SelectedIndex=0;
            }
            else if (RadBtnAuswahl.Checked && LstEssen.SelectedIndex != -1)
            {
                LstEssen.Items.Insert(LstEssen.SelectedIndex, TxbNeuerEintrag.Text);
                LstEssen.SelectedIndex -= 1;
            }
            else
            {
                LstEssen.Items.Add(TxbNeuerEintrag.Text);
                LstEssen.SelectedIndex = LstEssen.Items.Count - 1;
            }
            TxbNeuerEintrag.Clear();
        }

        private void ChkMehrfach_CheckedChanged(object sender, EventArgs e)
        {
            LstEssen.SelectionMode = ChkMehrfach.Checked ? SelectionMode.MultiExtended : SelectionMode.One;
            Ausgabe_Aktualisieren();
        }

    }
}
