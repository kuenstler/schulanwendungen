﻿namespace Loesung
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnZufall = new System.Windows.Forms.Button();
            this.LblAnz = new System.Windows.Forms.Label();
            this.LblAuswahl = new System.Windows.Forms.Label();
            this.LblIndex = new System.Windows.Forms.Label();
            this.LblEintraege = new System.Windows.Forms.Label();
            this.LstEssen = new System.Windows.Forms.ListBox();
            this.PnlErweitern = new System.Windows.Forms.Panel();
            this.BtnEinfuegen = new System.Windows.Forms.Button();
            this.RadBtnEnde = new System.Windows.Forms.RadioButton();
            this.RadBtnAuswahl = new System.Windows.Forms.RadioButton();
            this.LblNeuerEintragPosition = new System.Windows.Forms.Label();
            this.RadBtnAnfang = new System.Windows.Forms.RadioButton();
            this.TxbNeuerEintrag = new System.Windows.Forms.TextBox();
            this.LblEintrgName = new System.Windows.Forms.Label();
            this.LblErweitern = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.BtnLstLoeschen = new System.Windows.Forms.Button();
            this.BntEintrLoeschen = new System.Windows.Forms.Button();
            this.BtnErsetzen = new System.Windows.Forms.Button();
            this.TxbErsetzen = new System.Windows.Forms.TextBox();
            this.LblMit = new System.Windows.Forms.Label();
            this.LblAendern = new System.Windows.Forms.Label();
            this.LblMehrfach = new System.Windows.Forms.Label();
            this.ChkMehrfach = new System.Windows.Forms.CheckBox();
            this.LblMehrfachAusgabe = new System.Windows.Forms.Label();
            this.PnlErweitern.SuspendLayout();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // BtnZufall
            // 
            this.BtnZufall.Location = new System.Drawing.Point(140, 42);
            this.BtnZufall.Name = "BtnZufall";
            this.BtnZufall.Size = new System.Drawing.Size(156, 23);
            this.BtnZufall.TabIndex = 1;
            this.BtnZufall.Text = "Zufällig auswählen";
            this.BtnZufall.UseVisualStyleBackColor = true;
            this.BtnZufall.Click += new System.EventHandler(this.BtnZufall_Click);
            // 
            // LblAnz
            // 
            this.LblAnz.AutoSize = true;
            this.LblAnz.Location = new System.Drawing.Point(12, 128);
            this.LblAnz.Name = "LblAnz";
            this.LblAnz.Size = new System.Drawing.Size(42, 13);
            this.LblAnz.TabIndex = 5;
            this.LblAnz.Text = "Anzahl:";
            // 
            // LblAuswahl
            // 
            this.LblAuswahl.AutoSize = true;
            this.LblAuswahl.Location = new System.Drawing.Point(12, 164);
            this.LblAuswahl.Name = "LblAuswahl";
            this.LblAuswahl.Size = new System.Drawing.Size(62, 13);
            this.LblAuswahl.TabIndex = 6;
            this.LblAuswahl.Text = "Ausgewählt";
            // 
            // LblIndex
            // 
            this.LblIndex.AutoSize = true;
            this.LblIndex.Location = new System.Drawing.Point(12, 200);
            this.LblIndex.Name = "LblIndex";
            this.LblIndex.Size = new System.Drawing.Size(36, 13);
            this.LblIndex.TabIndex = 7;
            this.LblIndex.Text = "Index:";
            // 
            // LblEintraege
            // 
            this.LblEintraege.AutoSize = true;
            this.LblEintraege.Location = new System.Drawing.Point(12, 236);
            this.LblEintraege.Name = "LblEintraege";
            this.LblEintraege.Size = new System.Drawing.Size(76, 13);
            this.LblEintraege.TabIndex = 8;
            this.LblEintraege.Text = "Listeneinträge:";
            // 
            // LstEssen
            // 
            this.LstEssen.FormattingEnabled = true;
            this.LstEssen.Items.AddRange(new object[] {
            "Spagetti",
            "Nudeln",
            "Pasta",
            "Lasange",
            "Brötchen"});
            this.LstEssen.Location = new System.Drawing.Point(12, 12);
            this.LstEssen.Name = "LstEssen";
            this.LstEssen.Size = new System.Drawing.Size(120, 108);
            this.LstEssen.TabIndex = 0;
            this.LstEssen.SelectedIndexChanged += new System.EventHandler(this.LstEssen_SelectedIndexChanged);
            // 
            // PnlErweitern
            // 
            this.PnlErweitern.Controls.Add(this.BtnEinfuegen);
            this.PnlErweitern.Controls.Add(this.RadBtnEnde);
            this.PnlErweitern.Controls.Add(this.RadBtnAuswahl);
            this.PnlErweitern.Controls.Add(this.LblNeuerEintragPosition);
            this.PnlErweitern.Controls.Add(this.RadBtnAnfang);
            this.PnlErweitern.Controls.Add(this.TxbNeuerEintrag);
            this.PnlErweitern.Controls.Add(this.LblEintrgName);
            this.PnlErweitern.Controls.Add(this.LblErweitern);
            this.PnlErweitern.Location = new System.Drawing.Point(319, 12);
            this.PnlErweitern.Name = "PnlErweitern";
            this.PnlErweitern.Size = new System.Drawing.Size(200, 165);
            this.PnlErweitern.TabIndex = 2;
            // 
            // BtnEinfuegen
            // 
            this.BtnEinfuegen.Location = new System.Drawing.Point(6, 132);
            this.BtnEinfuegen.Name = "BtnEinfuegen";
            this.BtnEinfuegen.Size = new System.Drawing.Size(191, 23);
            this.BtnEinfuegen.TabIndex = 4;
            this.BtnEinfuegen.Text = "Einfügen";
            this.BtnEinfuegen.UseVisualStyleBackColor = true;
            this.BtnEinfuegen.Click += new System.EventHandler(this.BtnEinfuegen_Click);
            // 
            // RadBtnEnde
            // 
            this.RadBtnEnde.AutoSize = true;
            this.RadBtnEnde.Checked = true;
            this.RadBtnEnde.Location = new System.Drawing.Point(9, 109);
            this.RadBtnEnde.Name = "RadBtnEnde";
            this.RadBtnEnde.Size = new System.Drawing.Size(112, 17);
            this.RadBtnEnde.TabIndex = 3;
            this.RadBtnEnde.TabStop = true;
            this.RadBtnEnde.Text = "an das Listenende";
            this.RadBtnEnde.UseVisualStyleBackColor = true;
            // 
            // RadBtnAuswahl
            // 
            this.RadBtnAuswahl.AutoSize = true;
            this.RadBtnAuswahl.Location = new System.Drawing.Point(9, 86);
            this.RadBtnAuswahl.Name = "RadBtnAuswahl";
            this.RadBtnAuswahl.Size = new System.Drawing.Size(132, 17);
            this.RadBtnAuswahl.TabIndex = 3;
            this.RadBtnAuswahl.Text = "an ausgewählter Stelle";
            this.RadBtnAuswahl.UseVisualStyleBackColor = true;
            // 
            // LblNeuerEintragPosition
            // 
            this.LblNeuerEintragPosition.AutoSize = true;
            this.LblNeuerEintragPosition.Location = new System.Drawing.Point(9, 45);
            this.LblNeuerEintragPosition.Name = "LblNeuerEintragPosition";
            this.LblNeuerEintragPosition.Size = new System.Drawing.Size(81, 13);
            this.LblNeuerEintragPosition.TabIndex = 4;
            this.LblNeuerEintragPosition.Text = "Position wählen";
            // 
            // RadBtnAnfang
            // 
            this.RadBtnAnfang.AutoSize = true;
            this.RadBtnAnfang.Location = new System.Drawing.Point(9, 61);
            this.RadBtnAnfang.Name = "RadBtnAnfang";
            this.RadBtnAnfang.Size = new System.Drawing.Size(123, 17);
            this.RadBtnAnfang.TabIndex = 3;
            this.RadBtnAnfang.Text = "An den Listenanfang";
            this.RadBtnAnfang.UseVisualStyleBackColor = true;
            // 
            // TxbNeuerEintrag
            // 
            this.TxbNeuerEintrag.Location = new System.Drawing.Point(83, 14);
            this.TxbNeuerEintrag.Name = "TxbNeuerEintrag";
            this.TxbNeuerEintrag.Size = new System.Drawing.Size(100, 20);
            this.TxbNeuerEintrag.TabIndex = 2;
            // 
            // LblEintrgName
            // 
            this.LblEintrgName.AutoSize = true;
            this.LblEintrgName.Location = new System.Drawing.Point(6, 14);
            this.LblEintrgName.Name = "LblEintrgName";
            this.LblEintrgName.Size = new System.Drawing.Size(70, 13);
            this.LblEintrgName.TabIndex = 1;
            this.LblEintrgName.Text = "neuer Eintrag";
            // 
            // LblErweitern
            // 
            this.LblErweitern.AutoSize = true;
            this.LblErweitern.Location = new System.Drawing.Point(3, -3);
            this.LblErweitern.Name = "LblErweitern";
            this.LblErweitern.Size = new System.Drawing.Size(75, 13);
            this.LblErweitern.TabIndex = 0;
            this.LblErweitern.Text = "Liste erweitern";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.BtnLstLoeschen);
            this.panel1.Controls.Add(this.BntEintrLoeschen);
            this.panel1.Controls.Add(this.BtnErsetzen);
            this.panel1.Controls.Add(this.TxbErsetzen);
            this.panel1.Controls.Add(this.LblMit);
            this.panel1.Controls.Add(this.LblAendern);
            this.panel1.Location = new System.Drawing.Point(319, 184);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(200, 100);
            this.panel1.TabIndex = 5;
            // 
            // BtnLstLoeschen
            // 
            this.BtnLstLoeschen.Location = new System.Drawing.Point(4, 71);
            this.BtnLstLoeschen.Name = "BtnLstLoeschen";
            this.BtnLstLoeschen.Size = new System.Drawing.Size(193, 23);
            this.BtnLstLoeschen.TabIndex = 8;
            this.BtnLstLoeschen.Text = "Liste Löschen";
            this.BtnLstLoeschen.UseVisualStyleBackColor = true;
            this.BtnLstLoeschen.Click += new System.EventHandler(this.BtnLstLoeschen_Click);
            // 
            // BntEintrLoeschen
            // 
            this.BntEintrLoeschen.Location = new System.Drawing.Point(4, 41);
            this.BntEintrLoeschen.Name = "BntEintrLoeschen";
            this.BntEintrLoeschen.Size = new System.Drawing.Size(193, 23);
            this.BntEintrLoeschen.TabIndex = 7;
            this.BntEintrLoeschen.Text = "Eintrag Löschen";
            this.BntEintrLoeschen.UseVisualStyleBackColor = true;
            this.BntEintrLoeschen.Click += new System.EventHandler(this.BntEintrLoeschen_Click);
            // 
            // BtnErsetzen
            // 
            this.BtnErsetzen.Location = new System.Drawing.Point(137, 16);
            this.BtnErsetzen.Name = "BtnErsetzen";
            this.BtnErsetzen.Size = new System.Drawing.Size(63, 23);
            this.BtnErsetzen.TabIndex = 6;
            this.BtnErsetzen.Text = "Ersetzen";
            this.BtnErsetzen.UseVisualStyleBackColor = true;
            this.BtnErsetzen.Click += new System.EventHandler(this.BtnErsetzen_Click);
            // 
            // TxbErsetzen
            // 
            this.TxbErsetzen.Location = new System.Drawing.Point(31, 17);
            this.TxbErsetzen.Name = "TxbErsetzen";
            this.TxbErsetzen.Size = new System.Drawing.Size(100, 20);
            this.TxbErsetzen.TabIndex = 5;
            // 
            // LblMit
            // 
            this.LblMit.AutoSize = true;
            this.LblMit.Location = new System.Drawing.Point(4, 17);
            this.LblMit.Name = "LblMit";
            this.LblMit.Size = new System.Drawing.Size(20, 13);
            this.LblMit.TabIndex = 1;
            this.LblMit.Text = "mit";
            // 
            // LblAendern
            // 
            this.LblAendern.AutoSize = true;
            this.LblAendern.Location = new System.Drawing.Point(3, 0);
            this.LblAendern.Name = "LblAendern";
            this.LblAendern.Size = new System.Drawing.Size(143, 13);
            this.LblAendern.TabIndex = 0;
            this.LblAendern.Text = "Einträge ändern und löschen";
            // 
            // LblMehrfach
            // 
            this.LblMehrfach.AutoSize = true;
            this.LblMehrfach.Location = new System.Drawing.Point(526, 13);
            this.LblMehrfach.Name = "LblMehrfach";
            this.LblMehrfach.Size = new System.Drawing.Size(91, 13);
            this.LblMehrfach.TabIndex = 12;
            this.LblMehrfach.Text = "Mehrfachauswahl";
            // 
            // ChkMehrfach
            // 
            this.ChkMehrfach.AutoSize = true;
            this.ChkMehrfach.Location = new System.Drawing.Point(526, 28);
            this.ChkMehrfach.Name = "ChkMehrfach";
            this.ChkMehrfach.Size = new System.Drawing.Size(154, 17);
            this.ChkMehrfach.TabIndex = 9;
            this.ChkMehrfach.Text = "Mehrfachauswahl zulassen";
            this.ChkMehrfach.UseVisualStyleBackColor = true;
            this.ChkMehrfach.CheckedChanged += new System.EventHandler(this.ChkMehrfach_CheckedChanged);
            // 
            // LblMehrfachAusgabe
            // 
            this.LblMehrfachAusgabe.AutoSize = true;
            this.LblMehrfachAusgabe.Location = new System.Drawing.Point(526, 57);
            this.LblMehrfachAusgabe.Name = "LblMehrfachAusgabe";
            this.LblMehrfachAusgabe.Size = new System.Drawing.Size(113, 13);
            this.LblMehrfachAusgabe.TabIndex = 15;
            this.LblMehrfachAusgabe.Text = "Ausgewählte Einträge:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(716, 292);
            this.Controls.Add(this.LblMehrfachAusgabe);
            this.Controls.Add(this.ChkMehrfach);
            this.Controls.Add(this.LblMehrfach);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.PnlErweitern);
            this.Controls.Add(this.LstEssen);
            this.Controls.Add(this.LblEintraege);
            this.Controls.Add(this.LblIndex);
            this.Controls.Add(this.LblAuswahl);
            this.Controls.Add(this.LblAnz);
            this.Controls.Add(this.BtnZufall);
            this.Name = "Form1";
            this.Text = "Form1";
            this.PnlErweitern.ResumeLayout(false);
            this.PnlErweitern.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BtnZufall;
        private System.Windows.Forms.Label LblAnz;
        private System.Windows.Forms.Label LblAuswahl;
        private System.Windows.Forms.Label LblIndex;
        private System.Windows.Forms.Label LblEintraege;
        private System.Windows.Forms.ListBox LstEssen;
        private System.Windows.Forms.Panel PnlErweitern;
        private System.Windows.Forms.Button BtnEinfuegen;
        private System.Windows.Forms.RadioButton RadBtnEnde;
        private System.Windows.Forms.RadioButton RadBtnAuswahl;
        private System.Windows.Forms.Label LblNeuerEintragPosition;
        private System.Windows.Forms.RadioButton RadBtnAnfang;
        private System.Windows.Forms.TextBox TxbNeuerEintrag;
        private System.Windows.Forms.Label LblEintrgName;
        private System.Windows.Forms.Label LblErweitern;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button BtnErsetzen;
        private System.Windows.Forms.TextBox TxbErsetzen;
        private System.Windows.Forms.Label LblMit;
        private System.Windows.Forms.Label LblAendern;
        private System.Windows.Forms.Button BtnLstLoeschen;
        private System.Windows.Forms.Button BntEintrLoeschen;
        private System.Windows.Forms.Label LblMehrfach;
        private System.Windows.Forms.CheckBox ChkMehrfach;
        private System.Windows.Forms.Label LblMehrfachAusgabe;
    }
}

