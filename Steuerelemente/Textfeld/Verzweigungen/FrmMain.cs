﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Verzweigungen
{
    public partial class FrmMain : Form
    {
        public FrmMain()
        {
            InitializeComponent();
        }

        private void BtnZwei_Click(object sender, EventArgs e)
        {
            if (NmbX.Value > 0)
            {
                LblAus.Text = "Die Angegebene Zahl ist größer als 0";
                NmbX.BackColor = Color.LightGreen;
            }
            else
            {
                LblAus.Text = "Die Angegebene Zahl ist kleiner oder gleich 0";
                NmbX.BackColor = Color.LightBlue;
            }
        }

        private void BtnEins_Click(object sender, EventArgs e)
        {
            if (NmbX.Value > 0)
            {
                LblAus.Text = "Die Angegebene Zahl ist größer als 0";
                NmbX.BackColor = Color.LightGreen;
            }
        }

        private void NmbX_ValueChanged(object sender, EventArgs e)
        {
            LblAus.Text = "...";
        }

        private void BtnTmp_Click(object sender, EventArgs e)
        {
            decimal x = NmbX.Value;
            LblAus.Text = x > 0 ? "x > 0" : x == 0 ? "x = 0" : "x < 0";
            NmbX.BackColor = x > 0 ? Color.LightGreen : x == 0 ? Color.LightBlue : Color.Red;
        }

        private void BtnUnd_Click(object sender, EventArgs e)
        {
            decimal x = NmbX.Value, y = NmbY.Value;
            LblAus.Text = (x > 0 && y > 0) ? "x und y sind größer als 0" : "x und y sind nicht größer als 0";
            NmbX.BackColor = (x > 0 && y > 0) ? Color.LightSkyBlue : Color.Red;
            NmbY.BackColor = NmbX.BackColor;
        }

        private void BtnOder_Click(object sender, EventArgs e)
        {
            decimal x = NmbX.Value, y = NmbY.Value;
            LblAus.Text = (x > 0 || y > 0) ? "x und/oder y sind größer als 0" : "x und y sind kleiner als 0";
            NmbX.BackColor = (x > 0 || y > 0) ? Color.LightSkyBlue : Color.Red;
            NmbY.BackColor = NmbX.BackColor;
        }

        private void BtnXor_Click(object sender, EventArgs e)
        {
            decimal x = NmbX.Value, y = NmbY.Value;
            LblAus.Text = (x > 0 ^ y > 0) ? "x oder y ist größer als 0" : "x und y sind größer oder kleiner als 0";
            NmbX.BackColor = (x > 0 ^ y > 0) ? Color.LightSkyBlue : Color.Red;
            NmbY.BackColor = NmbX.BackColor;
        }

        private void BtnSchachtel_Click(object sender, EventArgs e)
        {
            if (NmbX.Value > 0)
            {
                LblAus.Text = "Die Angegebene Zahl ist größer als 0";
                NmbX.BackColor = Color.LightGreen;
            }
            else if (NmbX.Value == 0) 
            {
                LblAus.Text = "Die Angegebene Zahl ist gleich 0";
                NmbX.BackColor = Color.LightBlue;
            }
            else
            {
                LblAus.Text = "Die Angegebene Zahl ist kleiner als 0";
                NmbX.BackColor = Color.Red;
            }

        }
    }
}
