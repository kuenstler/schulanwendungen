﻿namespace Verzweigungen
{
    partial class FrmMain
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblX = new System.Windows.Forms.Label();
            this.LblY = new System.Windows.Forms.Label();
            this.NmbX = new System.Windows.Forms.NumericUpDown();
            this.NmbY = new System.Windows.Forms.NumericUpDown();
            this.BtnEins = new System.Windows.Forms.Button();
            this.BtnZwei = new System.Windows.Forms.Button();
            this.BtnTmp = new System.Windows.Forms.Button();
            this.BtnUnd = new System.Windows.Forms.Button();
            this.BtnOder = new System.Windows.Forms.Button();
            this.LblAus = new System.Windows.Forms.Label();
            this.BtnXor = new System.Windows.Forms.Button();
            this.BtnSchachtel = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.NmbX)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.NmbY)).BeginInit();
            this.SuspendLayout();
            // 
            // LblX
            // 
            this.LblX.AutoSize = true;
            this.LblX.Location = new System.Drawing.Point(13, 13);
            this.LblX.Name = "LblX";
            this.LblX.Size = new System.Drawing.Size(15, 13);
            this.LblX.TabIndex = 0;
            this.LblX.Text = "x:";
            // 
            // LblY
            // 
            this.LblY.AutoSize = true;
            this.LblY.Location = new System.Drawing.Point(99, 15);
            this.LblY.Name = "LblY";
            this.LblY.Size = new System.Drawing.Size(15, 13);
            this.LblY.TabIndex = 1;
            this.LblY.Text = "y:";
            // 
            // NmbX
            // 
            this.NmbX.Location = new System.Drawing.Point(35, 13);
            this.NmbX.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NmbX.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.NmbX.Name = "NmbX";
            this.NmbX.Size = new System.Drawing.Size(58, 20);
            this.NmbX.TabIndex = 2;
            this.NmbX.ValueChanged += new System.EventHandler(this.NmbX_ValueChanged);
            // 
            // NmbY
            // 
            this.NmbY.Location = new System.Drawing.Point(121, 13);
            this.NmbY.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.NmbY.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            -2147483648});
            this.NmbY.Name = "NmbY";
            this.NmbY.Size = new System.Drawing.Size(55, 20);
            this.NmbY.TabIndex = 3;
            // 
            // BtnEins
            // 
            this.BtnEins.Location = new System.Drawing.Point(13, 45);
            this.BtnEins.Name = "BtnEins";
            this.BtnEins.Size = new System.Drawing.Size(75, 35);
            this.BtnEins.TabIndex = 4;
            this.BtnEins.Text = "einseitige\r\nAlternative";
            this.BtnEins.UseVisualStyleBackColor = true;
            this.BtnEins.Click += new System.EventHandler(this.BtnEins_Click);
            // 
            // BtnZwei
            // 
            this.BtnZwei.Location = new System.Drawing.Point(94, 45);
            this.BtnZwei.Name = "BtnZwei";
            this.BtnZwei.Size = new System.Drawing.Size(75, 35);
            this.BtnZwei.TabIndex = 5;
            this.BtnZwei.Text = "zweiseitige\r\nAlternative";
            this.BtnZwei.UseVisualStyleBackColor = true;
            this.BtnZwei.Click += new System.EventHandler(this.BtnZwei_Click);
            // 
            // BtnTmp
            // 
            this.BtnTmp.Location = new System.Drawing.Point(175, 45);
            this.BtnTmp.Name = "BtnTmp";
            this.BtnTmp.Size = new System.Drawing.Size(75, 35);
            this.BtnTmp.TabIndex = 6;
            this.BtnTmp.Text = "temporärer\r\nOperator";
            this.BtnTmp.UseVisualStyleBackColor = true;
            this.BtnTmp.Click += new System.EventHandler(this.BtnTmp_Click);
            // 
            // BtnUnd
            // 
            this.BtnUnd.Location = new System.Drawing.Point(13, 86);
            this.BtnUnd.Name = "BtnUnd";
            this.BtnUnd.Size = new System.Drawing.Size(75, 35);
            this.BtnUnd.TabIndex = 7;
            this.BtnUnd.Text = "Lokik: x\r\nund y";
            this.BtnUnd.UseVisualStyleBackColor = true;
            this.BtnUnd.Click += new System.EventHandler(this.BtnUnd_Click);
            // 
            // BtnOder
            // 
            this.BtnOder.Location = new System.Drawing.Point(94, 86);
            this.BtnOder.Name = "BtnOder";
            this.BtnOder.Size = new System.Drawing.Size(75, 35);
            this.BtnOder.TabIndex = 8;
            this.BtnOder.Text = "Logik: x\r\noder y";
            this.BtnOder.UseVisualStyleBackColor = true;
            this.BtnOder.Click += new System.EventHandler(this.BtnOder_Click);
            // 
            // LblAus
            // 
            this.LblAus.AutoSize = true;
            this.LblAus.Location = new System.Drawing.Point(12, 124);
            this.LblAus.Name = "LblAus";
            this.LblAus.Size = new System.Drawing.Size(49, 13);
            this.LblAus.TabIndex = 9;
            this.LblAus.Text = "Ausgabe";
            // 
            // BtnXor
            // 
            this.BtnXor.Location = new System.Drawing.Point(175, 86);
            this.BtnXor.Name = "BtnXor";
            this.BtnXor.Size = new System.Drawing.Size(75, 35);
            this.BtnXor.TabIndex = 10;
            this.BtnXor.Text = "Logik: x\r\nXOR y";
            this.BtnXor.UseVisualStyleBackColor = true;
            this.BtnXor.Click += new System.EventHandler(this.BtnXor_Click);
            // 
            // BtnSchachtel
            // 
            this.BtnSchachtel.Location = new System.Drawing.Point(256, 45);
            this.BtnSchachtel.Name = "BtnSchachtel";
            this.BtnSchachtel.Size = new System.Drawing.Size(86, 35);
            this.BtnSchachtel.TabIndex = 11;
            this.BtnSchachtel.Text = "geschachtelte\r\nAlternative\r\n";
            this.BtnSchachtel.UseVisualStyleBackColor = true;
            this.BtnSchachtel.Click += new System.EventHandler(this.BtnSchachtel_Click);
            // 
            // FrmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(509, 261);
            this.Controls.Add(this.BtnSchachtel);
            this.Controls.Add(this.BtnXor);
            this.Controls.Add(this.LblAus);
            this.Controls.Add(this.BtnOder);
            this.Controls.Add(this.BtnUnd);
            this.Controls.Add(this.BtnTmp);
            this.Controls.Add(this.BtnZwei);
            this.Controls.Add(this.BtnEins);
            this.Controls.Add(this.NmbY);
            this.Controls.Add(this.NmbX);
            this.Controls.Add(this.LblY);
            this.Controls.Add(this.LblX);
            this.Name = "FrmMain";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.NmbX)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.NmbY)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblX;
        private System.Windows.Forms.Label LblY;
        private System.Windows.Forms.NumericUpDown NmbX;
        private System.Windows.Forms.NumericUpDown NmbY;
        private System.Windows.Forms.Button BtnEins;
        private System.Windows.Forms.Button BtnZwei;
        private System.Windows.Forms.Button BtnTmp;
        private System.Windows.Forms.Button BtnUnd;
        private System.Windows.Forms.Button BtnOder;
        private System.Windows.Forms.Label LblAus;
        private System.Windows.Forms.Button BtnXor;
        private System.Windows.Forms.Button BtnSchachtel;

    }
}

