﻿namespace UebungS._113
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.LstL = new System.Windows.Forms.ListBox();
            this.LstR = new System.Windows.Forms.ListBox();
            this.BtnToR = new System.Windows.Forms.Button();
            this.BtnToL = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // LstL
            // 
            this.LstL.FormattingEnabled = true;
            this.LstL.Items.AddRange(new object[] {
            "Malta",
            "Zypern",
            "Slovenien",
            "Estland",
            "Rumänien"});
            this.LstL.Location = new System.Drawing.Point(13, 13);
            this.LstL.Name = "LstL";
            this.LstL.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.LstL.Size = new System.Drawing.Size(120, 95);
            this.LstL.TabIndex = 0;
            // 
            // LstR
            // 
            this.LstR.FormattingEnabled = true;
            this.LstR.Items.AddRange(new object[] {
            "Belgien",
            "Spanien",
            "Italien",
            "Portugal",
            "Dänemark"});
            this.LstR.Location = new System.Drawing.Point(221, 13);
            this.LstR.Name = "LstR";
            this.LstR.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
            this.LstR.Size = new System.Drawing.Size(120, 95);
            this.LstR.TabIndex = 1;
            // 
            // BtnToR
            // 
            this.BtnToR.Location = new System.Drawing.Point(140, 13);
            this.BtnToR.Name = "BtnToR";
            this.BtnToR.Size = new System.Drawing.Size(75, 23);
            this.BtnToR.TabIndex = 2;
            this.BtnToR.Text = ">>";
            this.BtnToR.UseVisualStyleBackColor = true;
            this.BtnToR.Click += new System.EventHandler(this.BtnToR_Click);
            // 
            // BtnToL
            // 
            this.BtnToL.Location = new System.Drawing.Point(140, 43);
            this.BtnToL.Name = "BtnToL";
            this.BtnToL.Size = new System.Drawing.Size(75, 23);
            this.BtnToL.TabIndex = 3;
            this.BtnToL.Text = "<<";
            this.BtnToL.UseVisualStyleBackColor = true;
            this.BtnToL.Click += new System.EventHandler(this.BtnToL_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(353, 117);
            this.Controls.Add(this.BtnToL);
            this.Controls.Add(this.BtnToR);
            this.Controls.Add(this.LstR);
            this.Controls.Add(this.LstL);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ListBox LstL;
        private System.Windows.Forms.ListBox LstR;
        private System.Windows.Forms.Button BtnToR;
        private System.Windows.Forms.Button BtnToL;
    }
}

