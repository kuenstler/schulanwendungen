﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UebungS._113
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnToR_Click(object sender, EventArgs e)
        {
            if (LstL.SelectedIndex == -1)
            {
                MessageBox.Show("Bitte etwas zum rüberschieben auswählen");
            }
            while(LstL.SelectedIndex != -1)
            {
                LstR.Items.Add(LstL.SelectedItem);
                LstL.Items.Remove(LstL.SelectedItem);
            }
        }

        private void BtnToL_Click(object sender, EventArgs e)
        {
            if (LstR.SelectedIndex == -1)
            {
                MessageBox.Show("Bitte etwas zum rüberschieben auswählen");
            }
            while (LstR.SelectedIndex != -1)
            {
                LstL.Items.Add(LstR.SelectedItem);
                LstR.Items.Remove(LstR.SelectedItem);
            }
        }
    }
}
