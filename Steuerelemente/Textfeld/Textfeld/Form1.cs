﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Textfeld
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void TxbBrt_TextChanged(object sender, EventArgs e)
        {
            double netto, brutto, mwst;
            string Tnetto, Tbrutto, Tmwst;
            Tbrutto = TxbBrt.Text;
            brutto = Convert.ToDouble(Tbrutto);
            netto = brutto / (Rad19.Checked ? 1.19 : (Rad7.Checked ? 1.07 : 1));
            mwst = brutto - netto;
            Tnetto = string.Format("{0:c}", netto);
            Tmwst = string.Format("{0:c}", mwst);
            if (Tnetto != TxbNet.Text){
                TxbNet.Text = Tnetto;
            }
            if (Tmwst != TxbMwst.Text){
                TxbMwst.Text = Tmwst;
            }
        }

        private void TxbNet_TextChanged(object sender, EventArgs e)
        {
            double netto, brutto, mwst;
            string Tnetto, Tbrutto, Tmwst;
            Tnetto = TxbNet.Text;
            netto = Convert.ToDouble(Tnetto);
            brutto = netto * 119 / 100;
            mwst = brutto - netto;
            Tmwst = string.Format("{0:c}", mwst);
            Tbrutto= string.Format("{0:c}", brutto);
            if (Tbrutto != TxbBrt.Text)
            {
                TxbBrt.Text = Tbrutto;
            }
            if (Tmwst != TxbMwst.Text)
            {
                TxbMwst.Text = Tmwst;
            }

        }

        private void TxbMwst_TextChanged(object sender, EventArgs e)
        {
            double netto, brutto, mwst;
            string Tnetto, Tbrutto, Tmwst;
            Tmwst = TxbMwst.Text;
            mwst = Convert.ToDouble(Tmwst);
            brutto = mwst * 1.19;
            netto = brutto - mwst;
            Tnetto = string.Format("{0:c}", netto);
            Tbrutto = string.Format("{0:c}", brutto);
            if (Tnetto != TxbNet.Text)
            {
                TxbNet.Text = Tnetto;
            }
            if (Tbrutto != TxbBrt.Text)
            {
                TxbBrt.Text = Tbrutto;
            }

        }

        private void Rad19_CheckedChanged(object sender, EventArgs e)
        {
            double netto, brutto, mwst;
            string Tnetto, Tbrutto, Tmwst;
            Tbrutto = TxbBrt.Text;
            brutto = Convert.ToDouble(Tbrutto);
            netto = brutto / (Rad19.Checked ? 1.19 : (Rad7.Checked ? 1.07 : 1));
            mwst = brutto - netto;
            Tnetto = string.Format("{0:c}", netto);
            Tmwst = string.Format("{0:c}", mwst);
            if (Tnetto != TxbNet.Text)
            {
                TxbNet.Text = Tnetto;
            }
            if (Tmwst != TxbMwst.Text)
            {
                TxbMwst.Text = Tmwst;
            }
        }

        private void Rad7_CheckedChanged(object sender, EventArgs e)
        {
            double netto, brutto, mwst;
            string Tnetto, Tbrutto, Tmwst;
            Tbrutto = TxbBrt.Text;
            brutto = Convert.ToDouble(Tbrutto);
            netto = brutto / (Rad19.Checked ? 1.19 : (Rad7.Checked ? 1.07 : 1));
            mwst = brutto - netto;
            Tnetto = string.Format("{0:c}", netto);
            Tmwst = string.Format("{0:c}", mwst);
            if (Tnetto != TxbNet.Text)
            {
                TxbNet.Text = Tnetto;
            }
            if (Tmwst != TxbMwst.Text)
            {
                TxbMwst.Text = Tmwst;
            }
        }

        private void Rad0_CheckedChanged(object sender, EventArgs e)
        {
            double netto, brutto, mwst;
            string Tnetto, Tbrutto, Tmwst;
            Tbrutto = TxbBrt.Text;
            brutto = Convert.ToDouble(Tbrutto);
            netto = brutto / (Rad19.Checked ? 1.19 : (Rad7.Checked ? 1.07 : 1));
            mwst = brutto - netto;
            Tnetto = string.Format("{0:c}", netto);
            Tmwst = string.Format("{0:c}", mwst);
            if (Tnetto != TxbNet.Text)
            {
                TxbNet.Text = Tnetto;
            }
            if (Tmwst != TxbMwst.Text)
            {
                TxbMwst.Text = Tmwst;
            }
        }


    }
}
