﻿namespace Textfeld
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxbBrt = new System.Windows.Forms.TextBox();
            this.TxbNet = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.TxbMwst = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.Rad0 = new System.Windows.Forms.RadioButton();
            this.Rad7 = new System.Windows.Forms.RadioButton();
            this.Rad19 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // TxbBrt
            // 
            this.TxbBrt.Location = new System.Drawing.Point(103, 12);
            this.TxbBrt.Name = "TxbBrt";
            this.TxbBrt.Size = new System.Drawing.Size(100, 20);
            this.TxbBrt.TabIndex = 0;
            this.TxbBrt.Text = "1";
            this.TxbBrt.TextChanged += new System.EventHandler(this.TxbBrt_TextChanged);
            // 
            // TxbNet
            // 
            this.TxbNet.Location = new System.Drawing.Point(103, 39);
            this.TxbNet.Name = "TxbNet";
            this.TxbNet.Size = new System.Drawing.Size(100, 20);
            this.TxbNet.TabIndex = 1;
            this.TxbNet.Text = "1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Brutto";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(33, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Netto";
            // 
            // TxbMwst
            // 
            this.TxbMwst.Location = new System.Drawing.Point(103, 65);
            this.TxbMwst.Name = "TxbMwst";
            this.TxbMwst.Size = new System.Drawing.Size(100, 20);
            this.TxbMwst.TabIndex = 4;
            this.TxbMwst.Text = "1";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(16, 65);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(32, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Mwst";
            // 
            // Rad0
            // 
            this.Rad0.AutoSize = true;
            this.Rad0.Location = new System.Drawing.Point(19, 91);
            this.Rad0.Name = "Rad0";
            this.Rad0.Size = new System.Drawing.Size(39, 17);
            this.Rad0.TabIndex = 6;
            this.Rad0.TabStop = true;
            this.Rad0.Text = "0%";
            this.Rad0.UseVisualStyleBackColor = true;
            this.Rad0.CheckedChanged += new System.EventHandler(this.Rad0_CheckedChanged);
            // 
            // Rad7
            // 
            this.Rad7.AutoSize = true;
            this.Rad7.Location = new System.Drawing.Point(19, 115);
            this.Rad7.Name = "Rad7";
            this.Rad7.Size = new System.Drawing.Size(39, 17);
            this.Rad7.TabIndex = 7;
            this.Rad7.TabStop = true;
            this.Rad7.Text = "7%";
            this.Rad7.UseVisualStyleBackColor = true;
            this.Rad7.CheckedChanged += new System.EventHandler(this.Rad7_CheckedChanged);
            // 
            // Rad19
            // 
            this.Rad19.AutoSize = true;
            this.Rad19.Checked = true;
            this.Rad19.Location = new System.Drawing.Point(19, 138);
            this.Rad19.Name = "Rad19";
            this.Rad19.Size = new System.Drawing.Size(45, 17);
            this.Rad19.TabIndex = 8;
            this.Rad19.TabStop = true;
            this.Rad19.Text = "19%";
            this.Rad19.UseVisualStyleBackColor = true;
            this.Rad19.CheckedChanged += new System.EventHandler(this.Rad19_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.Rad19);
            this.Controls.Add(this.Rad7);
            this.Controls.Add(this.Rad0);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.TxbMwst);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.TxbNet);
            this.Controls.Add(this.TxbBrt);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxbBrt;
        private System.Windows.Forms.TextBox TxbNet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox TxbMwst;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.RadioButton Rad0;
        private System.Windows.Forms.RadioButton Rad7;
        private System.Windows.Forms.RadioButton Rad19;
    }
}

