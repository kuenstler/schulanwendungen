﻿namespace Mehrfachauswahl
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.TxbIn = new System.Windows.Forms.TextBox();
            this.LblAus = new System.Windows.Forms.Label();
            this.BtnVer = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // TxbIn
            // 
            this.TxbIn.Location = new System.Drawing.Point(13, 13);
            this.TxbIn.Name = "TxbIn";
            this.TxbIn.Size = new System.Drawing.Size(100, 20);
            this.TxbIn.TabIndex = 0;
            this.TxbIn.TextChanged += new System.EventHandler(this.BtnVer_Click);
            // 
            // LblAus
            // 
            this.LblAus.AutoSize = true;
            this.LblAus.Location = new System.Drawing.Point(12, 65);
            this.LblAus.Name = "LblAus";
            this.LblAus.Size = new System.Drawing.Size(49, 13);
            this.LblAus.TabIndex = 1;
            this.LblAus.Text = "Ausgabe";
            // 
            // BtnVer
            // 
            this.BtnVer.Location = new System.Drawing.Point(12, 39);
            this.BtnVer.Name = "BtnVer";
            this.BtnVer.Size = new System.Drawing.Size(75, 23);
            this.BtnVer.TabIndex = 2;
            this.BtnVer.Text = "(Verarbeiten)";
            this.BtnVer.UseVisualStyleBackColor = true;
            this.BtnVer.Click += new System.EventHandler(this.BtnVer_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.BtnVer);
            this.Controls.Add(this.LblAus);
            this.Controls.Add(this.TxbIn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TxbIn;
        private System.Windows.Forms.Label LblAus;
        private System.Windows.Forms.Button BtnVer;
    }
}

