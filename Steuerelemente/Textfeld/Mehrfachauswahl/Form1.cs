﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mehrfachauswahl
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void BtnVer_Click(object sender, EventArgs e)
        {
            string ein = TxbIn.Text.ToLower(), ergebnis = "";
            switch (ein)
            {
                case "france":
                    ergebnis += "Frankreich";
                    break;
                case "bordeaux":
                    ergebnis += "Atlantik\n";
                    goto case "france";
                case "nizza":
                    ergebnis += "Cote d'Azur\n";
                    goto case "france";
                default:
                    ergebnis += "restliche Fälle";
                    break;
            }
            LblAus.Text = ergebnis;
        }

    }
}
